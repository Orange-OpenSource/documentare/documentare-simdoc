MVN=mvn ${MAVEN_CLI_OPTS} ${MAVEN_OPTS}
VERSION=`git describe`

# Used to build debian package
# here we consider that artifacts are already built (to speed up continuous integration)
# so we do nothing here
build:

# Build debian package
deb:
	git config --global --add safe.directory ${PWD} # to avoid git describe error since git checks dir ownership vs user id
	dch -v ${VERSION} "git update, version ${VERSION}"
	bash .dh_build.sh
	mv ../simdoc*.deb .

# Debian package install
install:
	rm -rf usr && mkdir -p usr/share/java/simdoc/lib
	cp simdoc/simdoc-server/target/simdoc-server-*-runner.jar usr/share/java/simdoc/simdoc-server.jar
	cp -rf simdoc/simdoc-server/target/lib/* usr/share/java/simdoc/lib/
	rm -rf etc && mkdir -p etc/systemd/system/ && cp debian/simdoc-server.service etc/systemd/system/

clean:
#	debclean


deploy-core-snapshots:
	(cd simdoc/core/java/ && ${MVN} clean deploy)

build-servers:
	(cd simdoc/simdoc-server/ && ${MVN} install)

build-apps:
	(cd simdoc/apps && ${MVN} install)

integration-test:
	(cd simdoc/apps/ && ./refIntegrationTest.sh)

tar_apps:
	rm -rf apps && mkdir -p apps
	cp simdoc/apps/prep-data/target/prep*.jar apps/prep-data.jar
	cp simdoc/apps/ncd/target/ncd*.jar apps/ncd.jar
	cp simdoc/apps/ncd-remote/target/ncd*.jar apps/ncd-remote.jar
	cp simdoc/apps/clustering-remote/target/clustering-remote*.jar apps/clustering-remote.jar
	cp simdoc/apps/prep-clustering/target/prep*.jar apps/prep-clustering.jar
	cp simdoc/apps/similarity-clustering/target/sim*.jar apps/similarity-clustering.jar
	cp simdoc/apps/graph/target/graph*.jar apps/graph.jar
	tar cvjf apps.tar.bz2 apps

tar_servers:
	mkdir tar_servers
	cp -r simdoc/simdoc-server/target/lib tar_servers && cp simdoc/simdoc-server/target/simdoc-server-*-runner.jar tar_servers/simdoc-server.jar
	tar cvjf tar_servers.tar.bz2 tar_servers
