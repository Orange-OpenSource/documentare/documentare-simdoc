import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import static org.assertj.core.api.BDDAssertions.then;

class GenerateCorpusTest {

  public static final int FILE_SIZE_MIN = 100;
  //public static final int FILE_SIZE_MAX = 100 * 1024;
  public static final int FILE_SIZE_MAX = 2000;
  public static final int FILE_SIZE_STEP = 50;

  public static final File CORPUS_DIRECTORY = new File("/tmp/documentare-random-corpus");
  private final String corpusDirectoryPath = CORPUS_DIRECTORY.getAbsolutePath();


  @Test
  void generate_prefix_with_ordered_letters() {
    // given
    PrefixGenerator prefixGenerator = new PrefixGenerator();

    // when
    ArrayList<String> prefixes = new ArrayList<>();
    for (int i = 0; i < PrefixGenerator.MAX_ITERATIONS; i++) {
      prefixes.add(prefixGenerator.get());
    }

    // then
    System.out.println(prefixes);
    then(prefixes.get(0)).isEqualTo("aa");
    then(prefixes.get(25)).isEqualTo("az");
    then(prefixes.get(26)).isEqualTo("ba");
    then(prefixes.get(675)).isEqualTo("zz");
  }

  @Disabled
  @Test
  void generate_random_files_corpus_with_variable_size() throws IOException {
    createCorpusDirectory();
    final PrefixGenerator prefixGenerator = new PrefixGenerator();
    int i = 0;
    for (int fileSize = FILE_SIZE_MIN; fileSize <= FILE_SIZE_MAX && i < PrefixGenerator.MAX_ITERATIONS; fileSize += FILE_SIZE_STEP, i++) {
      createRandomFile(fileSize, corpusDirectoryPath, prefixGenerator.get());
    }
  }

  private void createRandomFile(int fileSize, String corpusDirectoryPath, String prefix) throws IOException {
    byte[] bytes = new byte[fileSize];
    ThreadLocalRandom.current().nextBytes(bytes);

    File file = new File(corpusDirectoryPath + "/" + prefix + fileSize + ".txt");
    FileUtils.writeByteArrayToFile(file, bytes);
  }

  private void createCorpusDirectory() throws IOException {
    FileUtils.deleteQuietly(CORPUS_DIRECTORY);
    FileUtils.forceMkdir(CORPUS_DIRECTORY);
  }

  private static class PrefixGenerator {
    static final int MAX_ITERATIONS = 676;

    private int countColumn0 = 0;
    private int countColumn1 = 0;

    private char[] letters = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public String get() {
      int previousColumn0Index = (countColumn0 - 1) % 26;
      int column0Index = (countColumn0++) % 26;

      if (column0Index < previousColumn0Index) {
        countColumn1++;
      }

      if (countColumn1 > 25) {
        throw new IllegalStateException("Max number of prefixes reached");
      }

      char letterColumn0 = letters[column0Index];
      char letterColumn1 = letters[countColumn1];
      return String.format("%c%c", letterColumn1, letterColumn0);
    }
  }
}
