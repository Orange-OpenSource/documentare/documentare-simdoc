package com.orange.documentare.app.sa;
/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Joel Gardes & Christophe Maldivi
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.app.sa.cmdline.CommandLineException;
import com.orange.documentare.app.sa.cmdline.CommandLineOptions;
import com.orange.documentare.core.comp.bwt.Bwt;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;

import static org.apache.commons.io.FileUtils.write;

public class SuffixArrayApp {

  private static CommandLineOptions commandLineOptions;

  public static void main(String[] args) throws IOException, ParseException {
    try {
      commandLineOptions = new CommandLineOptions(args);
      doTheJob();
    } catch (CommandLineException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void doTheJob() throws IOException {
    FileUtils.deleteDirectory(commandLineOptions.output);
    commandLineOptions.output.mkdir();

    long total = 0L;
    for (File inputFile : listFilesWithOrder()) {
      byte[] inputBytes = FileUtils.readFileToByteArray(inputFile);
      final String fileName = inputFile.getName();
      File saOutputFile = new File(commandLineOptions.output.getAbsolutePath() + File.separator + fileName + ".java-sa");
      total = writeSuffixArrayIndices(inputBytes, fileName, saOutputFile, total);
    }
    long s = total / 1000000;
    long ms = total / 1000 - s * 1000;
    long us = total - ms * 1000 - s * 1000000;
    System.out.printf("javasufsort total took %d s %d ms %d us for directory %s%n", s, ms, us, commandLineOptions.input.getAbsolutePath());


    total = 0L;
    for (File inputFile : listFilesWithOrder()) {
      byte[] inputBytes = FileUtils.readFileToByteArray(inputFile);
      final String fileName = inputFile.getName();
      File saOutputFile = new File(commandLineOptions.output.getAbsolutePath() + File.separator + fileName + ".java-sa");
      total = writeSuffixArrayIndicesLibdivsufsortJNI(inputBytes, fileName, saOutputFile, total);
    }
    s = total / 1000000;
    ms = total / 1000 - s * 1000;
    us = total - ms * 1000 - s * 1000000;
    System.out.printf("java libdivsufsort JNI total took %d s %d ms %d us for directory %s%n", s, ms, us, commandLineOptions.input.getAbsolutePath());
  }

  private static File[] listFilesWithOrder() {
    File[] files = commandLineOptions.input.listFiles();
    Arrays.sort(files);
    return files;
  }

  private static long writeSuffixArrayIndices(byte[] inputBytes, String fileName, File saOutputFile, long total) throws IOException {
    Instant t0 = Instant.now();
    int[] indices = Bwt.computeSuffixArrayIndices(inputBytes);
    Instant t1 = Instant.now();
    long diffMicro = diffMicro(t0, t1);
    long s = diffMicro / 1000000;
    long ms = diffMicro / 1000 - s * 1000;
    long us = diffMicro - ms * 1000 - s * 1000000;
    System.out.printf("javasufsort took %d s %d ms %d us for file %s%n", s, ms, us, fileName);
    total = total + diffMicro;
    StringBuilder b = new StringBuilder();
    for (int val : indices) {
      if (val < indices.length / 2) {
        b.append(val);
        b.append(" ");
      }
    }
    b.append("\n");
    write(saOutputFile, b.toString(), "utf8");
    return total;
  }

  private static long writeSuffixArrayIndicesLibdivsufsortJNI(byte[] inputBytes, String fileName, File saOutputFile, long total) throws IOException {
    Instant t0 = Instant.now();
    Bwt.USE_LIBDIVSUFSORT_JNI = true;
    int[] indices = Bwt.computeSuffixArrayIndices(inputBytes);
    Instant t1 = Instant.now();
    long diffMicro = diffMicro(t0, t1);
    long s = diffMicro / 1000000;
    long ms = diffMicro / 1000 - s * 1000;
    long us = diffMicro - ms * 1000 - s * 1000000;
    System.out.printf("java libdivsufsort JNI took %d s %d ms %d us for file %s%n", s, ms, us, fileName);
    total = total + diffMicro;
    StringBuilder b = new StringBuilder();
    for (int val : indices) {
      if (val < indices.length / 2) {
        b.append(val);
        b.append(" ");
      }
    }
    b.append("\n");
    write(saOutputFile, b.toString(), "utf8");
    return total;
  }

  private static long diffMicro(Instant t1, Instant t2) {
    Instant diff = t2.minusSeconds(t1.getEpochSecond());
    diff = diff.minusNanos(t1.getNano());
    return diff.getEpochSecond() * 1000 * 1000 + diff.getNano() / 1000;
  }
}
