#!/bin/sh

ABS_IMG=`pwd`/$1
CLUSTERING_OPTS="$2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15 $16 $17 $18 $19 $20"

echo
echo Do images directory clustering with:
echo   - on images directory: $ABS_IMG
echo   - with clustering options: $CLUSTERING_OPTS
echo
echo ============================ GO! ===============================
echo

rm -f *.json *.dot *.pdf *.json.gz *.csv.gz *.txt
rm -rf safe-working-dir  source-symlinks  thumbnails

java -jar apps/prep-data.jar -d $ABS_IMG && \
java -jar apps/ncd.jar -j1 bytes-data.json && \
java -jar apps/prep-clustering.jar -json ncd_regular_files_model.json.gz -writeCSV && \
java -jar apps/similarity-clustering.jar -json prep_clustering_ready.json.gz $CLUSTERING_OPTS && \
java -jar apps/graph.jar -metadata metadata.json -json sc_graph_input.json.gz && \
twopi graph.dot -Goverlap=prism | gvmap -e | neato -Ecolor="#55555522" -n2 -Tpdf > graph.pdf
echo && \
echo && \
echo ============================ OK, DONE! =============================== && \
echo && \
echo && \
echo
