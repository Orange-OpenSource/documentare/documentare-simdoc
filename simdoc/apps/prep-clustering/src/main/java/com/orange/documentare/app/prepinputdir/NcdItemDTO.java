package com.orange.documentare.app.prepinputdir;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.comp.DistanceItem;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.TriangleVertices;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

@Getter
@NoArgsConstructor
public class NcdItemDTO implements ClusteringItem, DistanceItem {
  /**
   * file name with directory hierarchy below provided root directory
   */
  public String relativeFilename;

  public String humanReadableId;
  public NearestItem[] nearestItems;
  public TriangleVertices triangleVertices;
  public Integer duplicateOf;

  void updateHumanReadableId() {
    int filenameStart = relativeFilename.lastIndexOf("/");
    this.humanReadableId = relativeFilename.substring(filenameStart + 1);
    relativeFilename = null;
  }

  @Override
  public Optional<Integer> duplicateOf() {
    return Optional.ofNullable(duplicateOf);
  }

  /**
   * not used here
   */

  @Override
  public Integer getClusterId() {
    return null;
  }

  @Override
  public boolean triangleVerticesAvailable() {
    return false;
  }

  @Override
  public byte[] getBytes() {
    return null;
  }

  @Override
  public byte[] loadBytes() {
    return null;
  }
}
