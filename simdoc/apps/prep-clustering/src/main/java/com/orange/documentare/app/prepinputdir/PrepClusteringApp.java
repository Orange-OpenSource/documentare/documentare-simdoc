package com.orange.documentare.app.prepinputdir;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.app.prepinputdir.cmdline.CommandLineOptions;
import com.orange.documentare.core.comp.distance.DistancesArray;
import com.orange.documentare.core.comp.distance.DistancesArrayDTO;
import com.orange.documentare.core.comp.distance.matrix.DistancesMatrixCsvGzipWriter;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.comp.DistanceItem;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class PrepClusteringApp {

  private static final String MATRIX = "ncd_matrix.csv.gz";
  private static final String NEAREST = "ncd_nearests.csv.gz";
  private static final File CLUSTERING_EXPORT_FILE = new File("prep_clustering_ready.json.gz");

  public static void main(String[] args) {
    System.out.println("\n[PrepClustering - Start]");
    CommandLineOptions commandLineOptions;
    try {
      commandLineOptions = new CommandLineOptions(args);
    } catch (Exception e) {
      CommandLineOptions.showHelp();
      return;
    }
    try {
      doTheJob(commandLineOptions);
      System.out.println("\n[PrepClustering - Done]");
    } catch (Exception e) {
      log.error("[PrepClustering - ERROR]", e);
      CommandLineOptions.showHelp();
    }
  }

  private static void doTheJob(CommandLineOptions commandLineOptions) throws IOException {
    RegularFilesDistancesDTO regularFilesDistances = loadRegularFilesDistances(commandLineOptions.getInputJsonGz());
    boolean writeCSV = commandLineOptions.isWriteCSV();
    if (writeCSV) {
      writeCSVFiles(regularFilesDistances);
    }
    DistancesArrayDTO distancesArrayDTO = regularFilesDistances.distancesArray;
    if (distancesArrayDTO.getOnSameArray()) {
      writeJsonForClustering(regularFilesDistances, commandLineOptions.getKNearestNeighbours());
    }
  }

  private static void writeCSVFiles(RegularFilesDistancesDTO regularFilesDistances) throws IOException {
    DistanceItem[] items1 = regularFilesDistances.items1;
    DistanceItem[] items2 = regularFilesDistances.items2;
    items2 = items2 == null ? items1 : items2;
    DistancesArray distancesArray = regularFilesDistances.distancesArray.toBusinessObject();

    DistancesMatrixCsvGzipWriter csvWriter = new DistancesMatrixCsvGzipWriter(items1, items2, distancesArray);
    System.out.println("[Write matrix CSV]");
    csvWriter.writeTo(new File(MATRIX), false);
    System.out.println("[Write nearest CSV]");
    csvWriter.writeTo(new File(NEAREST), true);
  }

  private static void writeJsonForClustering(RegularFilesDistancesDTO regularFilesDistances, int kNearestNeighbours) throws IOException {
    System.out.println("[Export clustering model]");
    ClusteringModelDTO clusteringModel = new ClusteringModelDTO(regularFilesDistances, kNearestNeighbours);
    exportToJson(clusteringModel);
  }

  private static RegularFilesDistancesDTO loadRegularFilesDistances(File file) throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    RegularFilesDistancesDTO regularFilesDistances = (RegularFilesDistancesDTO)
      jsonGenericHandler.getObjectFromJsonGzipFile(RegularFilesDistancesDTO.class, file);
    regularFilesDistances.updateHumanReadableId();
    return regularFilesDistances;
  }

  private static void exportToJson(Object object) throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);
    jsonGenericHandler.writeObjectToJsonGzipFile(object, PrepClusteringApp.CLUSTERING_EXPORT_FILE);
  }
}
