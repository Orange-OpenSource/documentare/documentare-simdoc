package com.orange.documentare.app.prepinputdir;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.DistancesArray;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.TriangleVertices;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

public class ClusteringModelDTO {
  public NcdItemDTO[] items;
  public int kNearestNeighbours;

  ClusteringModelDTO(RegularFilesDistancesDTO regularFilesDistances, int kNearestNeighbours) {
    this.items = regularFilesDistances.items1;

    this.kNearestNeighbours =
      kNearestNeighbours <= 0 ? items.length : kNearestNeighbours;

    DistancesArray distancesArray = regularFilesDistances.distancesArray.toBusinessObject();
    setupItems(distancesArray);
  }

  private void setupItems(DistancesArray distancesArray) {
    List<NcdItemDTO> itemsList = Arrays.asList(items);
    for (int i = 0; i < items.length; i++) {
      NearestItem vertex2 = distancesArray.nearestItemOf(i);
      NearestItem vertex3 = distancesArray.nearestItemOfBut(vertex2.getIndex(), i);
      NearestItem[] nearestArray = distancesArray.nearestItemsFor(itemsList, i);

      items[i].triangleVertices = new TriangleVertices(nearestArray, vertex3, kNearestNeighbours);
      items[i].nearestItems = nearestArray;
    }
  }
}
