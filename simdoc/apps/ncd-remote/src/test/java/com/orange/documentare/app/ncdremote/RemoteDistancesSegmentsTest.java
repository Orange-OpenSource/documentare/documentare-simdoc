package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.documentare.app.ncdremote.MatrixDistancesSegments.MatrixDistancesSegment;
import com.orange.documentare.app.ncdremote.request.RequestBuilder;
import com.orange.documentare.app.ncdremote.request.Requests;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import feign.Response;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.fest.assertions.Assertions.assertThat;

public class RemoteDistancesSegmentsTest {

  private final TestAnimalsElements testAnimalsElements = new TestAnimalsElements();

  @Test
  public void remote_computation_on_same_elements() throws IOException {
    // Given
    ExportModelDTO referenceModel = readReferenceForSameArray();

    BytesData[] elements = testAnimalsElements.elements();
    MatrixDistancesSegments matrixDistancesSegments = new MatrixDistancesSegments(elements, elements);
    matrixDistancesSegments = matrixDistancesSegments.buildSegments();

    RemoteDistancesSegments remoteDistancesSegments =
      new RemoteDistancesSegments(new TestRequestBuilder("map_task_id_results.json"), matrixDistancesSegments);

    // When
    File rHosts = new File(getClass().getResource("/remoteHosts.json").getFile());
    List<MatrixDistancesSegment> segments = remoteDistancesSegments.compute(rHosts);

    // Then
    ExportModelDTO exportModel = new ExportModelDTO(elements, elements, segments);
    assertThat(exportModel).isEqualTo(referenceModel);
  }

  @Test
  public void remote_computation_on_distinct_elements_arrays() throws IOException {
    // Given
    ExportModelDTO referenceModel = readReferenceForDistinctArray();

    BytesData[] elements1 = testAnimalsElements.elements();
    BytesData[] elements2 = testAnimalsElements.elements();
    MatrixDistancesSegments matrixDistancesSegments = new MatrixDistancesSegments(elements1, elements2);
    matrixDistancesSegments = matrixDistancesSegments.buildSegments();

    RemoteDistancesSegments remoteDistancesSegments =
      new RemoteDistancesSegments(new TestRequestBuilder("map_task_id_results_distinct_arrays.json"), matrixDistancesSegments);

    // When
    File rHosts = new File(getClass().getResource("/remoteHosts.json").getFile());
    List<MatrixDistancesSegment> segments = remoteDistancesSegments.compute(rHosts);

    // Then
    segments.forEach(segment -> doAssertion(segment, referenceModel));

    ExportModelDTO exportModel = new ExportModelDTO(elements1, elements2, segments);
    assertThat(exportModel.distancesArray.getDistancesArray()).isEqualTo(referenceModel.distancesArray.getDistancesArray());
    assertThat(exportModel).isEqualTo(referenceModel);
  }

  private void doAssertion(MatrixDistancesSegment segment, ExportModelDTO model) {
    String id = new File(segment.element.filepaths.get(0)).getName();
    int refIndex = IntStream.range(0, model.items1.length)
      .filter(index -> model.items1[index].relativeFilename.equals(id))
      .findFirst()
      .getAsInt();
    assertThat(segment.distances).isEqualTo(model.distancesArray.getDistancesFor(refIndex));
  }

  private ExportModelDTO readReferenceForSameArray() throws IOException {
    return readReference("/animals-dna-same-array-ncd_regular_files_model.json.gz");
  }

  private ExportModelDTO readReferenceForDistinctArray() throws IOException {
    return readReference("/animals-dna-ncd_regular_files_model.json.gz");
  }

  private ExportModelDTO readReference(String resId) throws IOException {
    File file = new File(getClass().getResource(resId).getFile());
    return (ExportModelDTO) (new JsonGenericHandler()).getObjectFromJsonGzipFile(ExportModelDTO.class, file);
  }

  private class TestRequestBuilder implements RequestBuilder {

    private final List<String> ids;
    private final Map<String, DistancesRequestResult> map;

    TestRequestBuilder(String map_task_id_results) throws IOException {
      File res = new File(getClass().getResource("/" + map_task_id_results).getFile());
      map = (Map<String, DistancesRequestResult>) JsonGenericHandler.instance().getObjectFromJsonFileNewApi(JsonContainer.class, res);
      ids = new ArrayList<>(map.keySet());
    }

    @Override
    public Requests.Distance buildInitDistanceComputationRequest(ExecutorContext context) {
      String taskId = ids.iterator().next();
      ids.remove(taskId);
      return (seg) -> new RemoteTask(seg.element.ids.get(0));
    }

    @Override
    public Requests.DistanceResult buildGetDistanceComputationResultRequest(String remoteServiceUrl) {
      return (taskId) -> {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writer().writeValueAsString(map.get(taskId));
        return Response.builder()
          .body(body, Charset.defaultCharset())
          .status(200)
          .headers(new HashMap<>())
          .build();
      };
    }
  }
}
