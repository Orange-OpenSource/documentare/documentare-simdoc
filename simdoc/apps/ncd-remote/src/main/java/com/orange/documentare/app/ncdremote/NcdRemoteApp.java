package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.app.ncdremote.MatrixDistancesSegments.MatrixDistancesSegment;
import com.orange.documentare.app.ncdremote.cmdline.CommandLineOptions;
import com.orange.documentare.app.ncdremote.request.Requests;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.comp.distance.bytesdistances.PreppedBytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import lombok.AllArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class NcdRemoteApp {

  @AllArgsConstructor
  static class ResultToExportDTO {
    Object o;
    File file;
  }

  private static final File FILES_DISTANCES_EXPORT_FILE = new File("ncd_regular_files_model.json.gz");

  private static JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);

  public static void main(String[] args) {
    System.out.println("\n[NCD REMOTE - Start]");
    CommandLineOptions options;
    try {
      options = new CommandLineOptions(args);
    } catch (Exception e) {
      CommandLineOptions.showHelp(e);
      return;
    }
    try {
      doTheJob(options);
      System.out.println("[NCD REMOTE - Done]");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private static void doTheJob(CommandLineOptions o) throws IOException {
    System.out.println("\n[Kill all remote tasks]");
    KillAllTasks.serialKiller(o.getRemoteHosts());

    ResultToExportDTO resultToExport = doTheJobForRegularFiles(o.getD1(), o.getD2(), o.getJ1(), o.getJ2(), o.getRemoteHosts());
    System.out.println("\n[Export model]");
    // Optim: bytes allocated in do* functions are available for the garbage collector now!
    exportToJson(resultToExport);
  }

  private static ResultToExportDTO doTheJobForRegularFiles(File directory1, File directory2, File json1, File json2, File rHosts) throws IOException {
    BytesData[] bytesData1;
    BytesData[] bytesData2;
    if (json1 == null) {
      bytesData1 = BytesData.loadFromDirectory(directory1, BytesData.relativePathIdProvider(directory1));
      bytesData2 = directory1.equals(directory2) ?
        bytesData1 : BytesData.loadFromDirectory(directory2, BytesData.relativePathIdProvider(directory2));
    } else {
      bytesData1 = loadPreppedBytesData(json1);
      bytesData2 = json1.equals(json2) ? bytesData1 : loadPreppedBytesData(json2);
    }

    MatrixDistancesSegments matrixDistancesSegments = new MatrixDistancesSegments(bytesData1, bytesData2);
    matrixDistancesSegments = matrixDistancesSegments.buildSegments();

    RemoteDistancesSegments remoteDistancesSegments = new RemoteDistancesSegments(new Requests(), matrixDistancesSegments);
    List<MatrixDistancesSegment> segments = remoteDistancesSegments.compute(rHosts);

    ExportModelDTO exportModel = new ExportModelDTO(bytesData1, bytesData2, segments);
    return new ResultToExportDTO(exportModel, FILES_DISTANCES_EXPORT_FILE);
  }

  private static BytesData[] loadPreppedBytesData(File json) throws IOException {
    return ((PreppedBytesData) jsonGenericHandler.getObjectFromJsonFileNewApi(PreppedBytesData.class, json)).bytesData;
  }

  private static void exportToJson(ResultToExportDTO resultToExport) throws IOException {
    jsonGenericHandler.writeObjectToJsonGzipFile(resultToExport.o, resultToExport.file);
  }
}
