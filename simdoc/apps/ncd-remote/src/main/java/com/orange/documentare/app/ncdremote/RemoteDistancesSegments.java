package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.app.ncdremote.MatrixDistancesSegments.MatrixDistancesSegment;
import com.orange.documentare.app.ncdremote.request.RequestBuilder;
import com.orange.documentare.app.ncdremote.request.Requests;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import feign.FeignException;
import feign.Response;
import feign.jackson.JacksonDecoder;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

import static java.lang.System.currentTimeMillis;

@Slf4j
public class RemoteDistancesSegments implements RequestsProvider {
  private final Map<Integer, MatrixDistancesSegment> idMap = new HashMap<>();
  private final RequestBuilder requestBuilder;
  private final List<MatrixDistancesSegment> segmentsToCompute;
  private final ResponseCollector<MatrixDistancesSegment> responseCollector;

  public RemoteDistancesSegments(RequestBuilder requestBuilder, MatrixDistancesSegments matrixDistancesSegments) {
    this.requestBuilder = requestBuilder;
    this.segmentsToCompute = new ArrayList<>(matrixDistancesSegments.segments);
    responseCollector = new ResponseCollectorImpl(segmentsToCompute.size());
    IntStream.range(0, segmentsToCompute.size())
            .forEach(index -> idMap.put(index, segmentsToCompute.get(index)));
  }

  public List<MatrixDistancesSegment> compute(File rHosts) throws IOException {
    do {
      dispatchSegmentsToCompute(rHosts);
    } while (!segmentsToCompute.isEmpty());

    return responseCollector.responses();
  }

  private void dispatchSegmentsToCompute(File rHosts) throws IOException {
    RequestsProvider requestsProvider = this;
    LocalAvailableRemoteServices availableRemoteServices = new LocalAvailableRemoteServices(rHosts);
    RequestsExecutor requestsExecutor = new RequestsExecutor(requestsProvider, responseCollector, availableRemoteServices);
    requestsExecutor.exec();
  }

  @Override
  public Optional<RequestExecutor> getPendingRequestExecutor() {
    return Optional.of(context -> {
      Optional<MatrixDistancesSegment> segment = nextSegment();
      if (!segment.isPresent()) {
        return;
      }
      request(context, segment);
    });
  }

  private void request(ExecutorContext context, Optional<MatrixDistancesSegment> segment) {
    long t0 = currentTimeMillis();

    Requests.Distance distanceRequest = requestBuilder.buildInitDistanceComputationRequest(context);
    Try.of(() -> doRequest(context, segment, distanceRequest))
            .onSuccess((res) -> onSuccess(context, segment, t0, res))
            .onFailure(throwable -> handleError(context, segment, throwable));
  }

  private DistancesRequestResult doRequest(ExecutorContext context, Optional<MatrixDistancesSegment> segment, Requests.Distance distanceRequest) throws IOException, InterruptedException {
    log.info("[POST REQUEST] {}", context.remoteService.url);
    RemoteTask remoteTask = distanceRequest.distance(segment.get());
    if (remoteTask == null || remoteTask.id == null) {
      throw new IllegalStateException("invalid remote task ids: " + remoteTask);
    }
    DistancesRequestResult result = waitForResult(remoteTask.id, context.remoteService.url);

    return result;
  }

  private void onSuccess(ExecutorContext context, Optional<MatrixDistancesSegment> segment, long t0, DistancesRequestResult res) {
    context.responseCollector.add(segment.get().withDistances(res.distances));
    context.serviceStatusListener.serviceProvidedTaskResult(context.remoteService);
    long dt = (currentTimeMillis() - t0) / 1000;
    log.info(String.format("\uD83D\uDE01 [SUCCESS %d%s] from %s took %ds (%.2fs/elem)", progress(), '%', context.remoteService.url, dt, (float) dt / segment.get().elements.length));
  }

  private void handleError(ExecutorContext context, Optional<MatrixDistancesSegment> segment, Throwable e) {
    context.requestsProvider.failedToHandleRequest(getKeyByValue(idMap, segment.get()));
    if (e instanceof FeignException) {
      int status = ((FeignException) e).status();
      if (status == 503) {
        context.serviceStatusListener.serviceCanNotHandleMoreTasks(context.remoteService);
        log.info("\uD83D\uDE13  [INFO] Service {} can not handle more tasks", context.remoteService.url, status, e.getMessage());
      } else {
        log.error("\uD83D\uDE21  [ERROR] Request to {} failed with status {}: {}", context.remoteService.url, status, e.getMessage());
      }
    } else {
      log.error("\uD83D\uDE21  [ERROR] Request to {} failed: {}", context.remoteService.url, e.getMessage());
    }
  }

  private DistancesRequestResult waitForResult(String taskId, String remoteServiceUrl) throws IOException, InterruptedException {
    WaitingPeriod waitingPeriod = new WaitingPeriod();
    Requests.DistanceResult distanceResult = requestBuilder.buildGetDistanceComputationResultRequest(remoteServiceUrl);
    Response response;
    do {
      waitingPeriod.sleep();
      response = distanceResult.distanceResult(taskId);
    } while (response.status() == 204);

    if (response.status() == 200) {
      return (DistancesRequestResult) (new JacksonDecoder()).decode(response, DistancesRequestResult.class);
    }

    throw new IllegalStateException("Failed to compute distance, http response: " + response.toString());
  }

  private int progress() {
    return 100 - ((segmentsToCompute.size() * 100) / idMap.size());
  }

  private synchronized Optional<MatrixDistancesSegment> nextSegment() {
    if (segmentsToCompute.isEmpty()) {
      return Optional.empty();
    }

    MatrixDistancesSegment segment = segmentsToCompute.get(0);
    segmentsToCompute.remove(0);
    return Optional.of(segment);
  }

  @Override
  public synchronized boolean empty() {
    return segmentsToCompute.isEmpty();
  }

  @Override
  public synchronized void failedToHandleRequest(int requestId) {
    // re-add request in the pending list since it failed
    segmentsToCompute.add(idMap.get(requestId));
  }

  private int getKeyByValue(Map<Integer, MatrixDistancesSegment> map, MatrixDistancesSegment value) {
    return map.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue(), value))
            .map(Map.Entry::getKey)
            .findFirst()
            .get();
  }
}
