package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.orange.documentare.core.model.json.JsonGenericHandler;

public class LocalAvailableRemoteServices implements AvailableRemoteServices {

  private final List<RemoteService> availableServices;
  private static final JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);

  LocalAvailableRemoteServices(File rHosts) throws IOException {
    availableServices = Arrays.asList(loadRemoteServices(rHosts));
  }

  @Override
  public void update() {
    // FIXME, in next version:
    // - here we should clear the available services
    // - and update it through a request the service discovery server
  }

  @Override
  public List<RemoteService> services() {
    return List.copyOf(availableServices);
  }

  private static RemoteService[] loadRemoteServices(File rHosts) throws IOException {
    return (RemoteService[]) jsonGenericHandler.getObjectFromJsonFileNewApi(RemoteService[].class, rHosts);
  }

}
