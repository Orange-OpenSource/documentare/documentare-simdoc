package com.orange.documentare.app.simclustering.cmdline;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.EnrollParameters;
import com.orange.documentare.core.system.CommandLineException;
import org.apache.commons.cli.*;

import static com.orange.documentare.core.model.ref.clustering.ClusteringParameters.*;

public class CommandLineOptions {
  private static final String HELP = "h";
  private static final String DISTANCES_FILE = "json";
  private static final String ACUT = "acut";
  private static final String QCUT = "qcut";
  private static final String SCUT = "scut";
  private static final String SLOOP = "sloop";
  private static final String CCUT = "ccut";

  private static final String ENROLL = "enroll";
  private static final String ENROLL_ACUT = "eacut";
  private static final String ENROLL_QCUT = "eqcut";
  private static final String ENROLL_SCUT = "escut";
  private static final String ENROLL_NOSLOOP = "enosloop";
  private static final String ENROLL_CCUT = "eccut";

  private static final Options options = new Options();

  private final SimClusteringOptions.SimClusteringOptionsBuilder optionsBuilder = SimClusteringOptions.builder();

  public CommandLineOptions(String[] args) throws ParseException {
    init(args);
  }

  public SimClusteringOptions simClusteringOptions() {
    return optionsBuilder.build();
  }

  private void init(String[] args) throws ParseException {
    CommandLine commandLine = getCommandLineFromArgs(args);
    boolean helpRequested = commandLine.hasOption(HELP);
    if (helpRequested) {
      throw new CommandLineException("\nPrint this help message\n");
    } else {
      initOptions(commandLine);
    }
  }

  private void initOptions(CommandLine commandLine) {
    ClusteringParameters.ClusteringParametersBuilder builder = optionsBuilder.clusteringParametersBuilder;

    if (commandLine.hasOption(ACUT)) {
      builder.acut(Float.parseFloat(commandLine.getOptionValue(ACUT, String.valueOf(A_DEFAULT_SD_FACTOR))));
    }
    if (commandLine.hasOption(QCUT)) {
      builder.qcut(Float.parseFloat(commandLine.getOptionValue(QCUT, String.valueOf(Q_DEFAULT_SD_FACTOR))));
    }
    if (commandLine.hasOption(SCUT)) {
      builder.scut(Float.parseFloat(commandLine.getOptionValue(SCUT, String.valueOf(SCUT_DEFAULT_SD_FACTOR))));
    }
    if (commandLine.hasOption(SLOOP)) {
      builder.sloop();
    }
    if (commandLine.hasOption(CCUT)) {
      builder.ccut(Integer.parseInt(commandLine.getOptionValue(CCUT, String.valueOf(CCUT_DEFAULT_PERCENTILE))));
    }
    if (commandLine.hasOption(ENROLL)) {
      initEnrollOptions(commandLine, builder);
    }
    if (commandLine.hasOption(DISTANCES_FILE)) {
      optionsBuilder.regularFile(commandLine.getOptionValue(DISTANCES_FILE));
    }
  }

  private void initEnrollOptions(CommandLine commandLine, ClusteringParametersBuilder builder) {
    builder.enroll();
    EnrollParameters.EnrollParametersBuilder enrollBuilder = EnrollParameters.builder();

    if (commandLine.hasOption(ENROLL_ACUT)) {
      enrollBuilder.acut(Float.parseFloat(commandLine.getOptionValue(ACUT, String.valueOf(A_DEFAULT_SD_FACTOR))));
    }
    if (commandLine.hasOption(ENROLL_QCUT)) {
      enrollBuilder.qcut(Float.parseFloat(commandLine.getOptionValue(QCUT, String.valueOf(Q_DEFAULT_SD_FACTOR))));
    }
    if (commandLine.hasOption(ENROLL_SCUT)) {
      enrollBuilder.scut(Float.parseFloat(commandLine.getOptionValue(SCUT, String.valueOf(SCUT_DEFAULT_SD_FACTOR))));
    }
    if (commandLine.hasOption(ENROLL_NOSLOOP)) {
      enrollBuilder.disableSloop();
    }
    if (commandLine.hasOption(ENROLL_CCUT)) {
      enrollBuilder.ccut(Integer.parseInt(commandLine.getOptionValue(CCUT, String.valueOf(CCUT_DEFAULT_PERCENTILE))));
    }

    builder.enrollParameters(enrollBuilder.build());
  }

  private CommandLine getCommandLineFromArgs(String[] args) throws ParseException {
    Option help = new Option(HELP, "print this message");
    Option enroll = new Option(ENROLL, "enroll singletons");

    Option distanceJson = Option.builder(DISTANCES_FILE)
      .desc("distances file (.json.gz)")
      .hasArg()
      .build();

    Option sloop = new Option(SLOOP, "subgraph scalpel, adaptative mode");
    Option area = Option.builder(ACUT)
      .optionalArg(true)
      .hasArgs()
      .desc("graph area scissor, optional argument: standard deviation factor, default=" + A_DEFAULT_SD_FACTOR)
      .build();
    Option q = Option.builder(QCUT)
      .optionalArg(true)
      .hasArgs()
      .desc("graph equilaterality scissor, optional argument: standard deviation factor, default=" + Q_DEFAULT_SD_FACTOR)
      .build();
    Option sSd = Option.builder(SCUT)
      .optionalArg(true)
      .hasArgs()
      .desc("subgraph scalpel, optional argument: standard deviation factor, default=" + SCUT_DEFAULT_SD_FACTOR)
      .build();
    Option cTile = Option.builder(CCUT)
      .optionalArg(true)
      .hasArgs()
      .desc("cluster scalpel, optional argument: percentile threshold, default=" + CCUT_DEFAULT_PERCENTILE)
      .build();

    // Enroll singletons
    Option eNoSloop = new Option(ENROLL_NOSLOOP, "Enroll singletons: disable sloop");
    Option eArea = Option.builder(ENROLL_ACUT)
      .optionalArg(true)
      .hasArgs()
      .desc("Enroll singletons, graph area scissor, optional argument: standard deviation factor, default=" + A_DEFAULT_SD_FACTOR)
      .build();
    Option eQ = Option.builder(ENROLL_QCUT)
      .optionalArg(true)
      .hasArgs()
      .desc("Enroll singletons, graph equilaterality scissor, optional argument: standard deviation factor, default=" + Q_DEFAULT_SD_FACTOR)
      .build();
    Option eSSd = Option.builder(ENROLL_SCUT)
      .optionalArg(true)
      .hasArgs()
      .desc("Enroll singletons, subgraph scalpel, optional argument: standard deviation factor, default=" + SCUT_DEFAULT_SD_FACTOR)
      .build();
    Option eCTile = Option.builder(ENROLL_CCUT)
      .optionalArg(true)
      .hasArgs()
      .desc("Enroll singletons, cluster scalpel, optional argument: percentile threshold, default=" + CCUT_DEFAULT_PERCENTILE)
      .build();

    options.addOption(help);
    options.addOption(distanceJson);

    options.addOption(sloop);
    options.addOption(q);
    options.addOption(area);
    options.addOption(sSd);
    options.addOption(cTile);

    // Enroll singletons
    options.addOption(enroll);
    options.addOption(eNoSloop);
    options.addOption(eQ);
    options.addOption(eArea);
    options.addOption(eSSd);
    options.addOption(eCTile);

    CommandLineParser parser = new DefaultParser();
    return parser.parse(options, args);
  }

  public static void showHelp(Exception e) {
    System.out.println();
    System.out.println("[ERROR] " + e.getMessage());
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("SimClustering ", options);
  }
}
