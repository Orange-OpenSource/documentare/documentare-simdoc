package com.orange.documentare.app.simclustering;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.app.simclustering.cmdline.CommandLineOptions;
import com.orange.documentare.app.simclustering.cmdline.SimClusteringOptions;
import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class SimClusteringApp {
  private static final File GRAPH_OUTPUT = new File("sc_graph_input.json.gz");

  private static SimClusteringOptions options;

  public static void main(String[] args) {
    System.out.println("\n[SimClustering - Start]");
    try {
      options = (new CommandLineOptions(args)).simClusteringOptions();
    } catch (Exception e) {
      CommandLineOptions.showHelp(e);
      return;
    }
    try {
      doTheJob();
      System.out.println("\n[SimClustering - End]");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private static void doTheJob() throws IOException {
    System.out.println("Clustering parameters, " + options.clusteringParameters.toString());
    doTheJobForRegularFiles();
  }

  private static void doTheJobForRegularFiles() throws IOException {
    InputItem[] inputItems = getInputItemsFrom(options.regularFile);
    computeClustering(inputItems);
  }

  private static InputItem[] getInputItemsFrom(File jsonFile) throws IOException {
    JsonGenericHandler jsonHandler = new JsonGenericHandler(true);
    log.info("Start unzip...");
    ImportModel importModel = (ImportModel) jsonHandler.getObjectFromJsonGzipFile(ImportModel.class, jsonFile);
    log.info("...done");
    return importModel.getItems();
  }

  private static void computeClustering(ClusteringItem[] items) throws IOException {
    Clustering clustering = getClusteringGraphFor(items);
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);
    jsonGenericHandler.writeObjectToJsonGzipFile(ClusteringDTO.Companion.fromBusinessObject(clustering), GRAPH_OUTPUT);
  }

  private static Clustering getClusteringGraphFor(ClusteringItem[] items) {
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();
    return clusteringGraphBuilder.doClustering(items, options.clusteringParameters);
  }
}
