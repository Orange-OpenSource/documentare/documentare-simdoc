package com.orange.documentare.app.simclustering;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.TriangleVertices;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class InputItem implements ClusteringItem {
  private String humanReadableId;

  private NearestItem[] nearestItems;
  private TriangleVertices triangleVertices;

  private Integer clusterId;
  private Boolean clusterCenter;

  private byte[] bytes;

  private Integer duplicateOf;

  @Override
  public boolean triangleVerticesAvailable() {
    return triangleVertices != null;
  }

  @Override
  public Optional<Integer> duplicateOf() {
    return Optional.ofNullable(duplicateOf);
  }
}
