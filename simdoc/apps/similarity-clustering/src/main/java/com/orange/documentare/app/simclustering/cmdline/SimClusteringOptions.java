package com.orange.documentare.app.simclustering.cmdline;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.system.CommandLineException;

import java.io.File;

public class SimClusteringOptions {
  public final File regularFile;
  public final ClusteringParameters clusteringParameters;

  private SimClusteringOptions(File regularFile, ClusteringParameters clusteringParameters) {
    this.regularFile = regularFile;
    this.clusteringParameters = clusteringParameters;
  }

  public static SimClusteringOptionsBuilder builder() {
    return new SimClusteringOptionsBuilder();
  }

  public static class SimClusteringOptionsBuilder {
    public final ClusteringParameters.ClusteringParametersBuilder clusteringParametersBuilder = ClusteringParameters.builder();
    private File regularFile;

    private SimClusteringOptionsBuilder() {

    }

    public void regularFile(String optionValue) {
      regularFile = new File(optionValue);
    }

    public SimClusteringOptions build() {
      checkOptions();
      return new SimClusteringOptions(regularFile, clusteringParametersBuilder.build());
    }

    private void checkOptions() {
      if (regularFile == null || !regularFile.exists()) {
        throw new CommandLineException("Missing input file");
      }
    }
  }
}
