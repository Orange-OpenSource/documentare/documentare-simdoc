package com.orange.documentare.app.simclustering.cmdline;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.system.CommandLineException;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

import static com.orange.documentare.core.model.ref.clustering.ClusteringParameters.*;
import static org.fest.assertions.Assertions.assertThat;

public class CommandLineOptionsTest {

  @Test(expected = CommandLineException.class)
  public void missing_input_files_raised_exception() throws ParseException {
    // Given
    String[] args = {};
    // When
    (new CommandLineOptions(args)).simClusteringOptions();
    // Then
  }

  @Test
  public void enable_acut_qcut_scut_ccut_with_defaults() throws ParseException {
    // Given
    String[] args = {
      "-acut", "-qcut", "-scut", "-ccut", "-json", "."
    };

    // When
    ClusteringParameters options = (new CommandLineOptions(args)).simClusteringOptions().clusteringParameters;

    // Then
    assertThat(options.acut()).isTrue();
    assertThat(options.qcut()).isTrue();
    assertThat(options.scut()).isTrue();
    assertThat(options.ccut()).isTrue();
    assertThat(options.knn()).isFalse();

    assertThat(options.acutSdFactor).isEqualTo(A_DEFAULT_SD_FACTOR);
    assertThat(options.qcutSdFactor).isEqualTo(Q_DEFAULT_SD_FACTOR);
    assertThat(options.scutSdFactor).isEqualTo(SCUT_DEFAULT_SD_FACTOR);
    assertThat(options.ccutPercentile).isEqualTo(CCUT_DEFAULT_PERCENTILE);
  }

  @Test
  public void enable_acut_qcut_scut_ccut() throws ParseException {
    // Given
    String[] args = {
      "-acut", "0.1", "-qcut", "0.2", "-scut", "0.3", "-ccut", "23", "-json", "."
    };

    // When
    ClusteringParameters options = (new CommandLineOptions(args)).simClusteringOptions().clusteringParameters;

    // Then
    assertThat(options.acut()).isTrue();
    assertThat(options.qcut()).isTrue();
    assertThat(options.scut()).isTrue();
    assertThat(options.ccut()).isTrue();
    assertThat(options.knn()).isFalse();

    assertThat(options.acutSdFactor).isEqualTo(0.1f);
    assertThat(options.qcutSdFactor).isEqualTo(0.2f);
    assertThat(options.scutSdFactor).isEqualTo(0.3f);
    assertThat(options.ccutPercentile).isEqualTo(23);
  }
}