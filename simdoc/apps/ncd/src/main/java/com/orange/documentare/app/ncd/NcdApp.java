package com.orange.documentare.app.ncd;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;

import com.orange.documentare.app.ncd.cmdline.CommandLineOptions;
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;
import com.orange.documentare.core.comp.distance.Distance;
import com.orange.documentare.core.comp.distance.DistanceParameters;
import com.orange.documentare.core.comp.distance.DistancesArray;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.comp.distance.bytesdistances.PreppedBytesData;
import com.orange.documentare.core.comp.measure.ProgressListener;
import com.orange.documentare.core.comp.ncd.cache.NcdCache;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.postgresql.cache.PostgresCache;
import com.orange.documentare.core.system.measure.MemoryWatcher;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NcdApp {

  @AllArgsConstructor
  @NoArgsConstructor
  static class ResultToExportDTO {

    Object o;
    File file;
  }

  private static final File FILES_DISTANCES_EXPORT_FILE = new File("ncd_regular_files_model.json.gz");

  private static final ProgressListener progressListener =
    (step, progress) -> System.out.print("\r" + progress.displayString(step.toString()));


  private static final JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);


  public static void main(String[] args) {
    setupNcdCache();

    System.out.println("\n[NCD - Start]");
    CommandLineOptions options;
    try {
      options = new CommandLineOptions(args);
    }
    catch (Exception e) {
      CommandLineOptions.showHelp(e);
      return;
    }
    try {
      doTheJob(options);
      PostgresCache.flushQueueSync();

      FileUtils.writeStringToFile(new File("ncd-stats.txt"), NcdCache.INSTANCE.stats(), Charset.defaultCharset());
      System.out.println("[NCD - Done]");
      System.exit(0);
    }
    catch (Exception e) {
      log.error("[NCD - ERROR] " + e.getMessage(), e);
      System.exit(-1);
    }
  }

  private static void doTheJob(CommandLineOptions o) throws IOException {
    MemoryWatcher.watch();
    ResultToExportDTO resultToExport;
    resultToExport = doTheJobForRegularFiles(o.getD1(), o.getD2(), o.getJ1(), o.getJ2(), o.getDivsufsort());

    System.out.println("\n[Export model]");
    // Optim: bytes allocated in do* functions are available for the garbage collector now!
    exportToJson(resultToExport);

    MemoryWatcher.stopWatching();
  }

  private static ResultToExportDTO doTheJobForRegularFiles(File directory1, File directory2, File json1, File json2, Boolean divsufsort) throws IOException {
    BytesData[] bytesData1;
    BytesData[] bytesData2;
    if (json1 == null) {
      bytesData1 = BytesData.loadFromDirectory(directory1, BytesData.relativePathIdProvider(directory1));
      bytesData2 = directory1.equals(directory2) ?
        bytesData1 : BytesData.loadFromDirectory(directory2, BytesData.relativePathIdProvider(directory2));

    }
    else {
      // NB: withBytes is used since provided json may not contained bytes arrays, but only filepaths info
      bytesData1 = BytesData.withBytes(loadPreppedBytesData(json1));
      bytesData2 = json1.equals(json2) ? bytesData1 : BytesData.withBytes(loadPreppedBytesData(json2));
    }
    SuffixArrayAlgorithm suffixArrayAlgorithm = divsufsort ? SuffixArrayAlgorithm.LIBDIVSUFSORT : SuffixArrayAlgorithm.SAIS;
    DistanceParameters distanceParameters = new DistanceParameters(suffixArrayAlgorithm);
    Distance distance = new Distance(distanceParameters, progressListener);
    DistancesArray distancesArray = distance.computeDistancesBetweenCollections(bytesData1, bytesData2);

    ExportModelDTO exportModel = ExportModelDTO.with(bytesData1, bytesData2, distancesArray);
    return new ResultToExportDTO(exportModel, FILES_DISTANCES_EXPORT_FILE);
  }

  private static BytesData[] loadPreppedBytesData(File json) throws IOException {
    return ((PreppedBytesData) jsonGenericHandler.getObjectFromJsonFileNewApi(PreppedBytesData.class, json)).bytesData;
  }

  private static void exportToJson(ResultToExportDTO resultToExport) throws IOException {
    jsonGenericHandler.writeObjectToJsonGzipFile(resultToExport.o, resultToExport.file);
  }

  private static void setupNcdCache() {
    boolean disabled = PostgresCache.disableIfPostgresNotRunning(PostgresCache.DEFAULT_HOSTNAME, PostgresCache.DEFAULT_PORT);
    PostgresCache.disableOperationalTests();
    if (!disabled) {
      NcdCache.INSTANCE.initCachePersistence();
      waitForPostgresCacheToBeReady();
    }
  }

  private static void waitForPostgresCacheToBeReady() {
    while (!PostgresCache.isReady()) {
      try {
        Thread.sleep(100);
      }
      catch (InterruptedException e) {
        log.error("[NCD - ERROR] " + e.getMessage(), e);
        System.exit(-1);
      }
    }
  }
}
