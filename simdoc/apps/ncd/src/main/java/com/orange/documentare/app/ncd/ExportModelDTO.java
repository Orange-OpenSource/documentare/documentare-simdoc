package com.orange.documentare.app.ncd;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.DistancesArray;
import com.orange.documentare.core.comp.distance.DistancesArrayDTO;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;

import java.util.Arrays;

public class ExportModelDTO {

  public static class Item {
    public String relativeFilename;
  }

  public Item[] items1;
  public Item[] items2;
  public DistancesArrayDTO distancesArray;

  public static ExportModelDTO with(BytesData[] bytesData1, BytesData[] bytesData2, DistancesArray distancesArray) {
    ExportModelDTO dto = new ExportModelDTO();
    dto.items1 = buildItems(bytesData1);
    dto.items2 = bytesData1 == bytesData2 ? null : buildItems(bytesData2);
    dto.distancesArray = DistancesArrayDTO.Companion.fromBusinessObject(distancesArray);
    return dto;
  }

  private static Item[] buildItems(BytesData[] bytesDataArray) {
    return Arrays.stream(bytesDataArray)
      .map(bytesData -> bytesData.ids.get(0))
      .map(relativeFilename -> {
        Item item = new Item();
        item.relativeFilename = relativeFilename;
        return item;
      })
      .toArray(Item[]::new);
  }
}
