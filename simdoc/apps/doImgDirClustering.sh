#!/bin/sh

ABS_IMG=`pwd`/$1
CLUSTERING_OPTS="$2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15 $16 $17 $18 $19 $20"

echo
echo Do images directory clustering with:
echo   - on images directory: $ABS_IMG
echo   - with clustering options: $CLUSTERING_OPTS
echo
echo ============================ GO! ===============================
echo

(cd prep-data && ./go.sh -d $ABS_IMG) && \
(cd ncd && ./go.sh -j1 ../prep-data/bytes-data.json) && \
(cd prep-clustering && ./go.sh -json ../ncd/ncd_regular_files_model.json.gz -writeCSV) && \
(cd similarity-clustering/ && ./go.sh -json ../prep-clustering/prep_clustering_ready.json.gz $CLUSTERING_OPTS) && \
(cd graph && ./go.sh -metadata ../prep-data/metadata.json -json ../similarity-clustering/sc_graph_input.json.gz && ./show.sh) && \
echo && \
echo && \
echo ============================ OK, DONE! =============================== && \
echo && \
echo && \
echo
