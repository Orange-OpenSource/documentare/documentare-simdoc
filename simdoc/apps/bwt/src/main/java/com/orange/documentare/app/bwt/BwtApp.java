package com.orange.documentare.app.bwt;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.app.bwt.cmdline.CommandLineException;
import com.orange.documentare.app.bwt.cmdline.CommandLineOptions;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import com.orange.documentare.core.comp.bwt.Bwt;

import java.io.File;
import java.io.IOException;
import java.time.Instant;

import static org.apache.commons.io.FileUtils.write;

public class BwtApp {

  private static CommandLineOptions commandLineOptions;

  public static void main(String[] args) throws IOException, ParseException {
    try {
      commandLineOptions = new CommandLineOptions(args);
      doTheJob();
    } catch (CommandLineException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void doTheJob() throws IOException {
    byte[] inputBytes = FileUtils.readFileToByteArray(commandLineOptions.input);

    writeSuffixArrayIndices(inputBytes, commandLineOptions.input.getAbsolutePath());
  }

  private static void writeSuffixArrayIndices(byte[] inputBytes, String filePath) throws IOException {
    Instant t0 = Instant.now();
    int[] indices = Bwt.computeSuffixArrayIndices(inputBytes);
    Instant t1 = Instant.now();
    long diffMicro = diffMicro(t0, t1);
    long s = diffMicro/1000000;
    long ms = diffMicro/1000 - s*1000;
    long us = diffMicro - ms*1000 -s*1000000;
    System.out.printf("javasufsort took %d s %d ms %d us for file %s%n", s, ms, us, filePath);
    StringBuilder b = new StringBuilder();
    for (int val : indices) {
      if (val < indices.length/2) {
        b.append(val);
        b.append(" ");
      }
    }
    b.append("\n");
    write(new File(commandLineOptions.output.getAbsolutePath() + ".sa"), b.toString(), "utf8");
  }

  private static long diffMicro(Instant t1, Instant t2) {
    Instant diff = t2.minusSeconds(t1.getEpochSecond());
    diff = diff.minusNanos(t1.getNano());
    return diff.getEpochSecond() * 1000 * 1000 + diff.getNano() / 1000;
  }
}
