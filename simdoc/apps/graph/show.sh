#!/bin/sh

twopi graph.dot -Goverlap=prism | gvmap -e | neato -Ecolor="#55555522" -n2 -Tpdf > graph.pdf
twopi graph-internal.dot -Goverlap=prism | gvmap -e | neato -Ecolor="#55555522" -n2 -Tpdf > graph-internal.pdf
