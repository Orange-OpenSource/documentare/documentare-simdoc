/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.app.graph.importexport.internalgraph;

import org.apache.commons.text.StringEscapeUtils;

import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import com.orange.documentare.core.system.inputfilesconverter.FilesMap;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class InternalLabelProvider {

  private final FilesMap map;

  public String getName(GraphItem graphItem) {
    int index = Integer.parseInt(graphItem.vertexName);
    String simpleFilename = map.simpleFilenameAt(index);
    return StringEscapeUtils.escapeHtml4(simpleFilename);
  }
}
