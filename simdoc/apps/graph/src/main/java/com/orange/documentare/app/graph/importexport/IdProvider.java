package com.orange.documentare.app.graph.importexport;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.jgrapht.dotexport.DOT;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;


public class IdProvider {

  public static String getName(ClusteringElement element) {
    return String.format("c%d_%s", element.clusterId, DOT.getWithoutAnyExtension(element.id));
  }
}
