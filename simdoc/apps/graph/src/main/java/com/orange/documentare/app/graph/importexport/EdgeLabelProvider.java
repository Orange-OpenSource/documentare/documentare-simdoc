package com.orange.documentare.app.graph.importexport;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import static org.jgrapht.nio.DefaultAttribute.createAttribute;

import java.util.HashMap;
import java.util.Map;

import org.jgrapht.nio.Attribute;

import com.orange.documentare.core.comp.clustering.graph.jgrapht.JGraphEdge;
import com.orange.documentare.core.comp.distance.Distance;

public class EdgeLabelProvider {

  public static String getName(JGraphEdge edge) {
    return String.valueOf(edge.getEdge().length * 1000 / Distance.DISTANCE_INT_CONV_FACTOR);
  }

  public static Map<String, Attribute> getAttributes(JGraphEdge jGraphEdge) {
    Map<String, Attribute> map = new HashMap<>();
    map.put("label", createAttribute(getName(jGraphEdge)));
    return map;
  }
}
