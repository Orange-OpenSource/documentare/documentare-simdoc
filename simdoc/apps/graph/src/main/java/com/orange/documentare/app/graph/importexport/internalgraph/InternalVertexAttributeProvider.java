package com.orange.documentare.app.graph.importexport.internalgraph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import static org.jgrapht.nio.DefaultAttribute.createAttribute;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.jgrapht.nio.Attribute;

import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import com.orange.documentare.core.model.ref.clustering.graph.SubGraph;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class InternalVertexAttributeProvider {

  private static final String ALIEN_COLOR = "red";
  private static final String CENTER_COLOR = "green";
  private static final String ENROLLED_COLOR = "blue";
  private static final String DUPLICATE_COLOR = "yellow";
  private static final String REGULAR_COLOR = "black";
  private static final String FILLED_STYLE = "filled";

  private final Optional<File> imageDirectory;
  private final ClusteringGraph clusteringGraph;
  private final List<Integer> duplicatesIndices;

  public Map<String, Attribute> getComponentAttributes(GraphItem graphItem) {
    Map<String, Attribute> attrs = new HashMap<>();
    addColorAttr(attrs, graphItem);
    imageDirectory.ifPresent(f -> addImageAttr(attrs, graphItem));
    return attrs;
  }

  private void addImageAttr(Map<String, Attribute> attrs, GraphItem graphItem) {
    String vertexImageFileName = String.format("%s/%s.png", imageDirectory.get().getAbsolutePath(), graphItem.vertexName);
    attrs.put("image", createAttribute(vertexImageFileName));
  }

  private void addColorAttr(Map<String, Attribute> attrs, GraphItem graphItem) {
    String color = REGULAR_COLOR;
    String style = "";

    if (isGraphItemAloneInSubGraph(clusteringGraph, graphItem)) {
      if (graphItem.triangleSingleton) {
        color = ALIEN_COLOR;
        style = FILLED_STYLE;
      }
      else if (graphItem.clusterCenter) {
        color = CENTER_COLOR;
        style = FILLED_STYLE;
      }
      else if (graphItem.enrolled()) {
        color = ENROLLED_COLOR;
        style = FILLED_STYLE;
      }
    }
    else {
      if (graphItem.clusterCenter) {
        color = CENTER_COLOR;
        style = FILLED_STYLE;
      }
      else if (graphItem.enrolled()) {
        color = ENROLLED_COLOR;
        style = FILLED_STYLE;
      }
      else if (duplicatesIndices.contains(graphItem.vertex1Index)) {
        color = DUPLICATE_COLOR;
        style = FILLED_STYLE;
      }
    }

    attrs.put("color", createAttribute(color));
    if (!style.isEmpty()) {
      attrs.put("style", createAttribute(style));
    }
  }

  private boolean isGraphItemAloneInSubGraph(ClusteringGraph clusteringGraph, GraphItem graphItem) {
    SubGraph subGraph = clusteringGraph.getSubGraphs().get(graphItem.subgraphId);
    return subGraph.itemIndices.size() == 1;
  }
}
