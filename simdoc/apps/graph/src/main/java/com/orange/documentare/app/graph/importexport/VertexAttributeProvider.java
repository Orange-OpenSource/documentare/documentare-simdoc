package com.orange.documentare.app.graph.importexport;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import static org.jgrapht.nio.DefaultAttribute.createAttribute;

import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import lombok.RequiredArgsConstructor;

import org.jgrapht.nio.Attribute;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
public class VertexAttributeProvider {
  private static final String CENTER_COLOR = "green";
  private static final String ENROLLED_COLOR = "blue";
  private static final String REGULAR_COLOR = "black";
  private static final String DUPLICATE_COLOR = "yellow";
  private static final String FILLED_STYLE = "filled";

  private final Optional<File> imageDirectory;
  private final Clustering clustering;
  private final List<Integer> duplicatesIndices;


  public Map<String, Attribute> getComponentAttributes(ClusteringElement element) {
    Map<String, Attribute> attrs = new HashMap<>();
    addColorAttr(attrs, element);
    imageDirectory.ifPresent(f -> addImageAttr(attrs, element));
    return attrs;
  }

  private void addImageAttr(Map<String, Attribute> attrs, ClusteringElement element) {
    String vertexImageFileName = String.format("%s/%s.png", imageDirectory.get().getAbsolutePath(), element.id);
    attrs.put("image", createAttribute(vertexImageFileName));
  }

  private void addColorAttr(Map<String, Attribute> attrs, ClusteringElement element) {
    String color = REGULAR_COLOR;
    String style = "";

    if (isGraphItemAloneInSubGraph(element)) {
      if (element.clusterCenter()) {
        color = CENTER_COLOR;
        style = FILLED_STYLE;
      } else if (element.enrolled()) {
        color = ENROLLED_COLOR;
        style = FILLED_STYLE;
      }
    } else {
      if (element.clusterCenter()) {
        color = CENTER_COLOR;
        style = FILLED_STYLE;
      } else if (element.enrolled()) {
        color = ENROLLED_COLOR;
        style = FILLED_STYLE;
      } else if (duplicatesIndices.contains(clustering.elements.indexOf(element))) {
        color = DUPLICATE_COLOR;
        style = FILLED_STYLE;
      }
    }

    attrs.put("color", createAttribute(color));
    if (!style.isEmpty()) {
      attrs.put("style", createAttribute(style));
    }
  }

  private boolean isGraphItemAloneInSubGraph(ClusteringElement element) {
    int elementIndex = clustering.elements.indexOf(element);
    return clustering.subgraphs.stream()
      .filter(s -> s.elementsIndices.contains(elementIndex))
      .anyMatch(s -> s.elementsIndices.size() == 1);
  }
}
