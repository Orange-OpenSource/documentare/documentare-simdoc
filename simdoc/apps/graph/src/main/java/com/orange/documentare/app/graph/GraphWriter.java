package com.orange.documentare.app.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jgrapht.graph.AbstractBaseGraph;
import org.jgrapht.nio.dot.DOTExporter;

import com.orange.documentare.app.graph.cmdline.CommandLineOptions;
import com.orange.documentare.app.graph.importexport.EdgeLabelProvider;
import com.orange.documentare.app.graph.importexport.IdProvider;
import com.orange.documentare.app.graph.importexport.VertexAttributeProvider;
import com.orange.documentare.app.graph.importexport.internalgraph.InternalIdProvider;
import com.orange.documentare.app.graph.importexport.internalgraph.InternalVertexAttributeProvider;
import com.orange.documentare.core.comp.clustering.graph.jgrapht.ExternalGraphBuilder;
import com.orange.documentare.core.comp.clustering.graph.jgrapht.InternalJGraphTBuilder;
import com.orange.documentare.core.comp.clustering.graph.jgrapht.JGraphEdge;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO;
import com.orange.documentare.core.system.inputfilesconverter.Metadata;

public class GraphWriter {

  private static final String DOT_INTERNAL_OUTPUT = "graph-internal.dot";
  private static final String DOT_OUTPUT = "graph.dot";
  private static CommandLineOptions options;

  public static void main(String[] args) {
    System.out.println("\n[Graph - Start]");
    try {
      options = new CommandLineOptions(args);
    }
    catch (Exception e) {
      CommandLineOptions.showHelp();
      return;
    }
    try {
      doTheJob(options);
      System.out.println("\n[Graph - Done]");
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private static void doTheJob(CommandLineOptions options) throws IOException {
    Optional<File> imageDirectory = buildThumbnails();
    Clustering clustering = getClusteringFrom(options.getGraphJsonFile());

    AbstractBaseGraph<GraphItem, JGraphEdge> internalGraph = getJGraphTInternalGraph(clustering.graph);
    exportInternalGraph(internalGraph, DOT_INTERNAL_OUTPUT, imageDirectory, clustering);

    // make sure we do not rely on internal graph
    clustering = clustering.dropGraph();
    AbstractBaseGraph<ClusteringElement, JGraphEdge> graph = getJGraphTGraph(clustering);
    exportGraph(graph, DOT_OUTPUT, imageDirectory, clustering);
  }

  private static Optional<File> buildThumbnails() throws IOException {
    return options.getImageDirectory().isPresent() ?
      Optional.of(options.getImageDirectory().get()) :
      options.getMetadata().isPresent() ? doBuildThumbnails(options.getMetadata().get()) : Optional.empty();
  }

  private static Clustering getClusteringFrom(File inputJsonFile) throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    return ((ClusteringDTO) jsonGenericHandler.getObjectFromJsonGzipFile(ClusteringDTO.class, inputJsonFile))
      .toBusinessObject();
  }

  private static AbstractBaseGraph<GraphItem, JGraphEdge> getJGraphTInternalGraph(ClusteringGraph clusteringGraph) {
    InternalJGraphTBuilder jGraphTBuilder = new InternalJGraphTBuilder();
    return jGraphTBuilder.getJGraphTFrom(clusteringGraph);
  }

  private static void exportInternalGraph(AbstractBaseGraph<GraphItem, JGraphEdge> graph, String fileName, Optional<File> imageDirectory, Clustering clustering)
    throws IOException
  {
    InternalVertexAttributeProvider internalVertexAttributeProvider = new InternalVertexAttributeProvider(imageDirectory, clustering.graph, duplicatesIndices(clustering.elements));

    DOTExporter<GraphItem, JGraphEdge> exporter = new DOTExporter<>();
    exporter.setVertexIdProvider(InternalIdProvider::getName);
    exporter.setVertexAttributeProvider(internalVertexAttributeProvider::getComponentAttributes);
    exporter.setEdgeAttributeProvider(EdgeLabelProvider::getAttributes);

    FileWriter writer = new FileWriter(fileName);
    exporter.exportGraph(graph, writer);
  }

  private static AbstractBaseGraph<ClusteringElement, JGraphEdge> getJGraphTGraph(Clustering clustering) {
    ExternalGraphBuilder jGraphTBuilder = new ExternalGraphBuilder();
    return jGraphTBuilder.getJGraphTFrom(clustering);
  }

  private static void exportGraph(AbstractBaseGraph<ClusteringElement, JGraphEdge> graph, String fileName, Optional<File> imageDirectory, Clustering clustering)
    throws IOException
  {
    VertexAttributeProvider vertexAttributeProvider = new VertexAttributeProvider(imageDirectory, clustering, duplicatesIndices(clustering.elements));

    DOTExporter<ClusteringElement, JGraphEdge> exporter = new DOTExporter<>();
    exporter.setVertexIdProvider(IdProvider::getName);
    exporter.setVertexAttributeProvider(vertexAttributeProvider::getComponentAttributes);
    exporter.setEdgeAttributeProvider(EdgeLabelProvider::getAttributes);

    FileWriter writer = new FileWriter(fileName);
    exporter.exportGraph(graph, writer);
  }

  private static List<Integer> duplicatesIndices(List<ClusteringElement> elements) {
    return elements.stream()
      .filter(el -> el.duplicateOf().isPresent())
      .map(el -> el.duplicateOf)
      .collect(Collectors.toList());
  }

//  private static ComponentNameProvider internalLabelProvider() throws IOException {
//    if (options.getMetadata().isPresent()) {
//      JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
//      FilesMap filesMap = ((Metadata) jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata.class, options.getMetadata().get())).filesMap;
//      return new InternalLabelProvider(filesMap);
//    } else {
//      return null;
//    }
//  }
//
//  private static ComponentNameProvider labelProvider() throws IOException {
//    if (options.getMetadata().isPresent()) {
//      JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
//      FilesMap filesMap = ((Metadata) jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata.class, options.getMetadata().get())).filesMap;
//      return new LabelProvider(filesMap);
//    } else {
//      return null;
//    }
//  }

  private static Optional<File> doBuildThumbnails(File metadataFile) throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    Metadata metadata = (Metadata) jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata.class, metadataFile);
    ThumbnailsBuilder thumbnailsBuilder = new ThumbnailsBuilder(metadata, options.getThumbnailsSourceDirectory(), metadata.inputDirectoryPath);
    return thumbnailsBuilder.build();
  }
}
