#!/bin/sh

echo
echo "Test regular mode"
echo
./refIntegrationTest/doImgClusteringOn.sh refIntegrationTest/anatomie_crop.png && \
gunzip similarity-clustering/sc_graph_input.json.gz && \
jq -S "." similarity-clustering/sc_graph_input.json > similarity-clustering/sc_graph_input.json_sorted && \
diff similarity-clustering/sc_graph_input.json_sorted refIntegrationTest/sc_graph_input.json && \
echo && \
echo "Integration test OK" && \
exit 0

echo "[FAILED] Integration test" && exit 1
