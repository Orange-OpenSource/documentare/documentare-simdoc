#!/bin/sh

SERVER=localhost
PWD=`pwd`

curl \
-H "accept: application/json;charset=utf-8" \
-H "Content-Type: application/json;charset=utf-8" \
-d "{ \"element\": [ { \"ids\": [ \"d1\" ], \"filepaths\": [ \"$PWD/images/d1.png\" ] } ], \"elements\": [ { \"ids\": [ \"d2\" ], \"filepaths\": [ \"$PWD/images/d2.png\" ] }, { \"ids\": [ \"s1\" ], \"filepaths\": [ \"$PWD/images/s1.png\" ] } ], \"rawConverter\": true}" \
$SERVER:2407/distances
