#!/bin/sh

SERVER=localhost
PWD=`pwd`
rm -rf images-clustering
mkdir images-clustering

curl \
-H "accept: application/json;charset=utf-8" \
-H "Content-Type: application/json;charset=utf-8" \
-d "{ \"inputDirectory\": \"$PWD/images\", \"outputDirectory\": \"$PWD/images-clustering\" }" \
$SERVER:2407/clustering
