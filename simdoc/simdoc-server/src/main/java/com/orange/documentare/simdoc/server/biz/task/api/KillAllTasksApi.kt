/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.task.api

import com.orange.documentare.core.computationserver.biz.task.TaskController
import org.slf4j.LoggerFactory
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.ok

@Path("/kill-all-tasks")
open class KillAllTasksApi {

  private val log = LoggerFactory.getLogger(KillAllTasksApi::class.java)

  lateinit var taskController: TaskController

  @POST
  open fun killAll(): Response {
    log.info("kill all tasks")
    taskController.killAll()
    return ok().build()
  }
}
