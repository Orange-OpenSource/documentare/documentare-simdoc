package com.orange.documentare.simdoc.server.biz

import com.orange.documentare.core.comp.ncd.cache.NcdCache
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController
import com.orange.documentare.core.computationserver.biz.distances.DistancesController
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.TaskController
import com.orange.documentare.simdoc.server.biz.clustering.api.ClusteringApi
import com.orange.documentare.simdoc.server.biz.distances.api.DistancesComputationApi
import com.orange.documentare.simdoc.server.biz.task.InfraTasks
import com.orange.documentare.simdoc.server.biz.task.api.KillAllTasksApi
import com.orange.documentare.simdoc.server.biz.task.api.TaskApi
import com.orange.documentare.simdoc.server.biz.task.api.TasksAvailableResultsApi
import io.quarkus.runtime.StartupEvent
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.event.Observes
import javax.inject.Inject

class InfraDependencyInjection {

  @ConfigProperty(name = "distances.prepdir")
  lateinit var distancePrepDir: String

  @ConfigProperty(name = "rawimage.cachedir")
  lateinit var rawImageCacheDir: String

  @Inject
  lateinit var sharedDirectory: SharedDirectory

  @Inject
  lateinit var clusteringApi: ClusteringApi

  @Inject
  lateinit var distancesComputationApi: DistancesComputationApi

  @Inject
  lateinit var taskApi: TaskApi

  @Inject
  lateinit var tasksAvailableResultsApi: TasksAvailableResultsApi

  @Inject
  lateinit var killAllTasksApi: KillAllTasksApi

  @Inject
  lateinit var infraTasks: InfraTasks

  fun doDependencyInjection(@Observes event: StartupEvent) {
    NcdCache.initCachePersistence()

    val tasks = infraTasks.tasks
    val appPrepData = AppPrepData(distancePrepDir, rawImageCacheDir, sharedDirectory)
    val clusteringController = ClusteringController(appPrepData, sharedDirectory, tasks)
    val distancesController = DistancesController(tasks, appPrepData)
    val taskController = TaskController(tasks)

    clusteringApi.clusteringController = clusteringController
    distancesComputationApi.distancesController = distancesController
    taskApi.taskController = taskController
    tasksAvailableResultsApi.taskController = taskController
    killAllTasksApi.taskController = taskController
  }
}
