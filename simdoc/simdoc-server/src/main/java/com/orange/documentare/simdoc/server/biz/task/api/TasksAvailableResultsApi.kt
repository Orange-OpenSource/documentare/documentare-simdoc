/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.task.api

import com.orange.documentare.core.computationserver.biz.task.TaskController
import org.slf4j.LoggerFactory
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/tasks")
open class TasksAvailableResultsApi {

  private val log = LoggerFactory.getLogger(TasksAvailableResultsApi::class.java)

  lateinit var taskController: TaskController

  @GET
  @Path("/availableResultsTasksId")
  @Produces(MediaType.APPLICATION_JSON)
  open fun retrieveAvailableResultsTasksId(): Response {
    log.debug("retrieve available results tasks id")

    val availableResultsTasksId = taskController.retrieveAvailableResultsTasksId()

    return Response.ok()
      .entity(availableResultsTasksId)
      .build()
  }
}
