package com.orange.documentare.simdoc.server.biz.task

/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.computationserver.biz.task.Tasks
import javax.enterprise.context.ApplicationScoped

// @ApplicationScoped: Useful to be able to share it with integration tests
@ApplicationScoped
open class InfraTasks {
  val tasks = Tasks()
}
