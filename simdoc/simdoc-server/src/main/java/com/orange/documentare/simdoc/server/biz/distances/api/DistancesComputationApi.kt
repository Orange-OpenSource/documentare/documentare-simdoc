/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.distances.api

import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.distances.DistancesController
import com.orange.documentare.core.computationserver.biz.distances.DistancesController.DistanceRequestState.*
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Timed
import org.slf4j.LoggerFactory
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/distances")
open class DistancesComputationApi {

  private val log = LoggerFactory.getLogger(DistancesComputationApi::class.java)

  lateinit var distancesController: DistancesController

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Timed(name = "distancesTimer", description = "A measure of how long it takes to enqueue a distances computation request.", unit = MetricUnits.MILLISECONDS)
  open fun computeDistances(distanceRequest: DistancesRequest?, @QueryParam("callbackUrl") callbackUrl: String? = null): Response {

    if (distanceRequest == null) {
      log.error("[DISTANCES] bad request: empty body")
      return Response.status(400).build()
    }

    val status = distancesController.launchDistancesComputation(distanceRequest)
    return when (status.state) {
      BAD_REQUEST -> Response.status(400).entity(status.error).build()
      CAN_NOT_ACCEPT_A_NEW_TASK -> Response.status(503).entity(status.error).build()
      ERROR -> Response.status(500).entity(status.error).build()
      TASK_LAUNCHED -> Response.ok().entity(RemoteTaskDTO(status.taskId ?: "")).build()
    }
  }
}
