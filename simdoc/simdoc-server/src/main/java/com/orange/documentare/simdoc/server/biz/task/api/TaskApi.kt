/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.task.api

import com.orange.documentare.core.computationserver.biz.task.TaskController
import com.orange.documentare.core.computationserver.biz.task.TaskController.TaskState.*
import org.slf4j.LoggerFactory
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/task")
open class TaskApi {

  private val log = LoggerFactory.getLogger(TaskApi::class.java)

  lateinit var taskController: TaskController

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  open fun retrieveTaskResult(@PathParam("id") taskId: String): Response {
    log.debug("retrieve task '$taskId'")

    val (taskState, result) = taskController.retrieveTaskResult(taskId)

    return when (taskState) {
      NOT_FOUND -> Response.status(404)
      NOT_FINISHED -> Response.noContent()
      ERROR -> {
        log.info("retrieve DONE WITH ERROR task '$taskId'")
        Response.status(207).entity(result)
      }
      DONE -> {
        log.debug("retrieve DONE task '$taskId'")
        Response.ok().entity(result!!)
      }
    }.build()
  }
}
