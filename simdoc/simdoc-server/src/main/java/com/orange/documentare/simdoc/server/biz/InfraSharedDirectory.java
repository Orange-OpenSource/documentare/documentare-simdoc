package com.orange.documentare.simdoc.server.biz;

import java.io.File;
import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orange.documentare.core.computationserver.biz.SharedDirectory;

import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class InfraSharedDirectory implements SharedDirectory {

  private Logger log = LoggerFactory.getLogger(InfraSharedDirectory.class);

  @ConfigProperty(name = "shared.directory.available")
  boolean sharedDirectoryAvailable;

  @ConfigProperty(name = "shared.directory.root.path")
  String sharedDirectoryRootPath;

  void initSharedDirectory(@Observes StartupEvent event) throws IOException {
    if (sharedDirectoryAvailable) {
      File directory = new File(sharedDirectoryRootPath);
      boolean directoryExists = directory.exists() && directory.isDirectory();
      sharedDirectoryRootPath += "/";
      if (!directoryExists) {
        throw new IOException("Shared directory is not accessible: '" + sharedDirectoryRootPath + "'");
      }
    }
    log.info("Shared directory, available = {}, path = {}", sharedDirectoryAvailable, sharedDirectoryRootPath);
  }

  @Override
  public boolean sharedDirectoryAvailable() {
    return sharedDirectoryAvailable;
  }

  @Nullable
  @Override
  public String sharedDirectoryRootPath() {
    return sharedDirectoryRootPath;
  }
}

