/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.clustering.api

import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController.ClusteringRequestState.*
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Timed
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.status

@Path("/clustering")
open class ClusteringApi {

  private val log: Logger = LoggerFactory.getLogger(ClusteringApi::class.java)

  lateinit var clusteringController: ClusteringController

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Timed(name = "clusteringTimer", description = "A measure of how long it takes to enqueue a clustering request.", unit = MetricUnits.MILLISECONDS)
  open fun computeClustering(clusteringRequest: ClusteringRequest?, @QueryParam("callbackUrl") callbackUrl: String?): Response {

    if (clusteringRequest == null) {
      log.error("[CLUSTERING] bad request: empty body")
      return status(400).build()
    }

    val requestStatus = clusteringController.launchClustering(clusteringRequest)
    return when (requestStatus.state) {
      BAD_REQUEST -> status(400).entity(requestStatus.error).build()
      CAN_NOT_ACCEPT_A_NEW_TASK -> status(503).entity(requestStatus.error).build()
      ERROR -> status(500).entity(requestStatus.error).build()
      TASK_LAUNCHED -> Response.ok().entity(RemoteTaskDTO(requestStatus.taskId ?: "")).build()
    }
  }
}
