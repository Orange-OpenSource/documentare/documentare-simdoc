/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.stats.api

import com.orange.documentare.core.computationserver.biz.stats.CachesStats
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.ok

@Path("/stats")
class StatsApi {

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  fun stats(): Response = ok().entity(CachesStats.get()).build()
}
