/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.task

import com.orange.documentare.core.computationserver.biz.task.AvailableResultsTasksIdDTO
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
class TasksAvailableResultsIntTest {

  @Inject
  lateinit var tasks: InfraTasks

  private fun tasks() = tasks.tasks

  @BeforeEach
  @AfterEach
  fun `clean up tasks`() {
    tasks().killAll()
  }

  @Test
  fun `returns 200, and finished tasks id, 'ok' and 'not ok'`() {
    // given
    // not finished task
    tasks().newTask()
    val okId = tasks().newTask()
    val notOkId = tasks().newTask()
    tasks().addResult(okId, "ok")
    tasks().addErrorResult(notOkId, "not ok")

    // when
    val response = `when`()
      .get("/tasks/availableResultsTasksId")
      .then()
      .statusCode(200)
      .extract()
      .body()

    // then
    val availableResultsTasksIdDTO = response.`as`(AvailableResultsTasksIdDTO::class.java)
    then(availableResultsTasksIdDTO.availableResultsTasksId).containsExactlyInAnyOrder(okId, notOkId)
  }

  private fun `when`() = RestAssured.given()
    .`when`()
    .contentType(ContentType.JSON)
    .accept(ContentType.JSON)
}
