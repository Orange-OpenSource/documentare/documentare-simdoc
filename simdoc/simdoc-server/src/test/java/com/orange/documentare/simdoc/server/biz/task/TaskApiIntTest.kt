/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.task

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.biz.task.TaskLaunchAwait
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import javax.inject.Inject

@QuarkusTest
class TaskApiIntTest {

  @Inject
  lateinit var tasks: InfraTasks

  private fun tasks() = tasks.tasks

  @BeforeEach
  @AfterEach
  fun `clean up tasks`() {
    tasks().killAll()
  }

  class FakeResult {
    lateinit var res: String
  }

  @Test
  fun `returns 200 when task finished successfully`() {
    // given
    val taskId = tasks().newTask()
    tasks().addResult(taskId, """{ "res": "a result" }""")

    // when
    val response = `when`()
      .get("/task/$taskId")
      .then()
      .statusCode(200)
      .extract()
      .body()

    then(response.`as`(FakeResult::class.java).res).isEqualTo("a result")
  }

  @Test
  fun `returns 204 when task is not finished yet`() {
    // given
    val taskId = tasks().newTask()

    // when
    `when`()
      .get("/task/$taskId")
      .then()
      .statusCode(204)
  }

  @Test
  fun `returns 404 NOT_FOUND for an unknown task id`() {
    `when`()
      .get("/task/1234")
      .then()
      .statusCode(404)
  }

  @Test
  fun `returns 207 Multistatus when clustering task is in error`() {
    // given
    val taskId = tasks().newTask()
    tasks().addErrorResult(taskId, ClusteringDTO.fromBusinessObject(Clustering.error(ClusteringParameters.builder().build(), "an error occurred")))

    // when
    val response = `when`()
      .get("/task/$taskId")
      .then()
      .statusCode(207)
      .extract()
      .body()

    // then
    val clustering = response.`as`(ClusteringDTO::class.java).toBusinessObject()
    assertThat(clustering.failed()).isTrue()
    assertThat(clustering.error).isEqualTo("an error occurred")
  }

  @Test
  fun `returns 207 Multistatus when distance computation task is in error`() {
    // given
    val taskId = tasks().newTask()
    tasks().addErrorResult(taskId, DistancesRequestResultDTO.error("an error occurred"))

    // when
    val response = `when`()
      .get("/task/$taskId")
      .then()
      .statusCode(207)
      .extract()
      .body()

    // then
    val distancesRequestResult = response.`as`(DistancesRequestResultDTO::class.java)
    assertThat(distancesRequestResult.error).isTrue()
    assertThat(distancesRequestResult.errorMessage).isEqualTo("an error occurred")
  }

  @Test
  fun `returns 503 Not available if we can not accept new task`() {
    // given
    val file = File(TaskApiIntTest::class.java.getResource("/test-dir/f2").file)
    val bytesData = BytesData(listOf("f2"), listOf(file.absolutePath))
    val element = arrayOf(bytesData)
    val req: DistancesRequest = DistancesRequest.builder()
      .element(element)
      .compareTo(element)
      .build()

    val runnableObject = object : TaskLaunchAwait {
      override fun waitTaskInBackground(simdocTaskId: String) {
      }
    }
    repeat(tasks().maxTasks) {
      val taskId = tasks().newTask()
      tasks().run({ Thread.sleep(Long.MAX_VALUE) }, taskId, runnableObject)
    }

    // when
    `when`()
      .body(req)
      .post("/distances")
      .then()
      .statusCode(503)
  }

  private fun `when`() = given()
    .`when`()
    .contentType(ContentType.JSON)
    .accept(ContentType.JSON)

  @Test
  fun `kill all tasks`() {
    given()
      .`when`()
      .post("/kill-all-tasks")
      .then()
      .statusCode(200)
  }
}
