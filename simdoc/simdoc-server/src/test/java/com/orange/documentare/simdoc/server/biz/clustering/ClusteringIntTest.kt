/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.clustering

import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.simdoc.server.biz.task.InfraTasks
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.util.*
import javax.inject.Inject


@QuarkusTest
class ClusteringIntTest {

  @Inject
  lateinit var tasks: InfraTasks

  lateinit var req: ClusteringRequest

  @BeforeEach
  fun setup(@TempDir outputDir: File) {
    req = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDir.absolutePath)
      .build()
  }

  @Test
  fun `returns 400 BAD Request when request is invalid, input directory empty`() {
    // given
    val invalidRequest: ClusteringRequest = ClusteringRequest.builder().build()

    // when
    val response = `when`()
      .body(invalidRequest)
      .post("/clustering")
      .then()
      .statusCode(400)
      .extract().body()

    // then
    then(response.asString()).isEqualTo("inputDirectory and bytesData are missing")
  }

  @Test
  fun `returns 400 BAD Request when request is invalid, input directory not reachable`(@TempDir outputDir: File) {
    // given
    val invalidRequest: ClusteringRequest = ClusteringRequest.builder()
      .inputDirectory("/r/t/y/u/u")
      .outputDirectory(outputDir.absolutePath)
      .build()

    // when
    val response = `when`()
      .body(invalidRequest)
      .post("/clustering")
      .then()
      .statusCode(400)
      .extract().body()

    // then
    then(response.asString()).isEqualTo("inputDirectory can not be reached: /r/t/y/u/u")
  }


  @Test
  fun `returns 200 OK with a task Id when clustering server is available`() {
    // when
    val response = `when`()
      .body(req)
      .post("/clustering")
      .then()
      .statusCode(200)
      .extract().body()

    // then
    val remoteTask = response.`as`(RemoteTaskDTO::class.java)
    then(remoteTask.id).isNotBlank

    val uuid = UUID.fromString(remoteTask.id)
    then(uuid.version()).isEqualTo(4)

    do {
      Thread.sleep(10)
    } while (!tasks.tasks.isDone(remoteTask.id))

  }

  private fun `when`() = given()
    .`when`()
    .contentType(ContentType.JSON)
    .accept(ContentType.JSON)

  companion object {
    private fun inputDirectory(): String {
      return File(ClusteringIntTest::class.java.getResource("/test-dir").file).absolutePath
    }
  }
}
