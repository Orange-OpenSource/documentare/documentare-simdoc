/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.stats

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

@QuarkusTest
class StatsIntTest {

  @Test
  fun `returns 200 when task finished successfully`() {
    val response = given()
      .`when`()
      .accept(ContentType.TEXT)
      .get("/stats")
      .then()
      .statusCode(200)
      .extract()
      .body()

    // then
    val stats = response.asString()
    then(stats).contains("Raw cache hits and misses")
    then(stats).contains("ncd computation duration statsDurationMillis")
  }
}