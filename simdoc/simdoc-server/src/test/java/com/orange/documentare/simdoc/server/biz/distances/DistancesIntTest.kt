/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.simdoc.server.biz.distances

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*

@QuarkusTest
class DistancesIntTest {

  private val element = arrayOf(load("f2"))
  private val elements = loadAnimals()

  private val req: DistancesRequest = DistancesRequest.builder()
    .element(element)
    .compareTo(elements)
    .build()

  @Test
  fun `returns 400 BAD Request when request is invalid`() {
    `when`()
      .post("/distances")
      .then()
      .statusCode(400)
  }

  @Test
  fun `returns 200 OK with a task Id when clustering server is available`() {
    // when
    val response = `when`()
      .body(req)
      .post("/distances")
      .then()
      .statusCode(200)
      .extract().body()

    // then
    val remoteTask = response.`as`(RemoteTaskDTO::class.java)
    assertThat(remoteTask.id).isNotBlank()

    val uuid = UUID.fromString(remoteTask.id)
    assertThat(uuid.version()).isEqualTo(4)
  }

  private fun `when`() = given()
    .`when`()
    .contentType(ContentType.JSON)
    .accept(ContentType.JSON)

  companion object {
    private val FILES = arrayOf("f3", "f1", "f4", "f2")

    private fun load(id: String): BytesData {
      val path = "/test-dir/"
      val file = File(DistancesIntTest::class.java.getResource(path + id).file)
      return BytesData(listOf(id), listOf(file.absolutePath))
    }

    private fun loadAnimals() = FILES.map { this.load(it) }.toTypedArray()
  }
}
