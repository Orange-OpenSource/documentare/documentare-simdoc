package com.orange.documentare.core.comp.clustering.compute

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringItem
import com.orange.documentare.core.model.ref.comp.DistanceItem
import com.orange.documentare.core.model.ref.comp.NearestItem
import com.orange.documentare.core.model.ref.comp.TriangleVertices
import java.util.*

internal class SimClusteringItem(private val humanReadableId: String) : ClusteringItem, DistanceItem {
  override fun getBytes() = loadBytes()

  private lateinit var nearestItems: Array<NearestItem>
  private lateinit var triangleVertices: TriangleVertices
  private var clusterId: Int? = null
  private var duplicateOf: Int? = null

  override fun getClusterId(): Int? = clusterId
  override fun getNearestItems(): Array<NearestItem> = nearestItems
  override fun getTriangleVertices(): TriangleVertices = triangleVertices
  override fun getHumanReadableId() = humanReadableId
  override fun triangleVerticesAvailable() = true
  override fun duplicateOf() = Optional.ofNullable(this.duplicateOf)

  fun setNearestItems(nearestItems: Array<NearestItem>) {
    this.nearestItems = nearestItems
  }

  fun setTriangleVertices(triangleVertices: TriangleVertices) {
    this.triangleVertices = triangleVertices
  }

  /**
   * Not used
   */
  override fun loadBytes() = null
}
