package com.orange.documentare.core.comp.clustering.graph.check;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.Cluster;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.Edge;
import com.orange.documentare.core.model.ref.clustering.Subgraph;
import com.orange.documentare.core.model.ref.clustering.graph.*;
import lombok.RequiredArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class CheckGraph {
  private final Clustering clustering;
  private final ClusteringGraph clusteringGraph;

  public void check() {
    itemsFoundOnce();
    subgraphsAndClustersCoherency();
  }

  private void itemsFoundOnce() {
    for (GraphItem graphItem : clusteringGraph.getItems()) {
      int itemIndex = graphItem.vertex1Index;
      itemFoundOnce(itemIndex);

      boolean sameCount = clusteringGraph.getItems().size() == clustering.elements.size();
      boolean sameId = clustering.elements.get(itemIndex).id == graphItem.vertexName;
      boolean sameClusterId = clustering.elements.get(itemIndex).clusterId == graphItem.clusterId;
      boolean sameClusterCenter = clustering.elements.get(itemIndex).clusterCenter() == graphItem.clusterCenter;
      boolean sameEnrolled = clustering.elements.get(itemIndex).enrolled() == graphItem.enrolled();

      if (!sameCount || !sameId || !sameClusterId || !sameClusterCenter || !sameEnrolled) {
        throwNewException(String.format("Cluster and Graph Items (%d) are not coherent", itemIndex));
      }
    }
  }

  private void itemFoundOnce(int itemIndex) {
    itemFoundOnceInSubgraphs(itemIndex);
    itemFoundOnceInClusters(itemIndex);
  }

  private void itemFoundOnceInSubgraphs(int itemIndex) {
    int count = 0;
    for (SubGraph subGraph : clusteringGraph.getSubGraphs().values()) {
      if (subGraph.itemIndices.contains(itemIndex)) {
        count++;
      }
    }
    if (count != 1) {
      throwNewException(String.format("Item %d found %d times in Graph subgraphs", itemIndex, count));
    }

    count = 0;
    for (Subgraph subgraph : clustering.subgraphs) {
      if (subgraph.elementsIndices.contains(itemIndex)) {
        count++;
      }
    }
    if (count != 1) {
      throwNewException(String.format("Item %d found %d times in Clustering subgraphs", itemIndex, count));
    }
  }

  private void itemFoundOnceInClusters(int itemIndex) {
    int count = 0;
    for (GraphCluster cluster : clusteringGraph.getClusters().values()) {
      if (cluster.itemIndices.contains(itemIndex)) {
        count++;
      }
    }
    if (count != 1) {
      throwNewException(String.format("Item %d found %d times in Graph clusters", itemIndex, count));
    }

    count = 0;
    for (Cluster cluster : clustering.clusters) {
      if (cluster.elementsIndices.contains(itemIndex)) {
        count++;
      }
    }
    if (count != 1) {
      throwNewException(String.format("Item %d found %d times in Clustering clusters", itemIndex, count));
    }
  }

  private void subgraphsAndClustersCoherency() {
    Collection<SubGraph> subgraphs = clusteringGraph.getSubGraphs().values();
    Collection<GraphCluster> clusters = clusteringGraph.getClusters().values();
    checkSize(subgraphs, clusters);
    checkClustersInSubGraphs(subgraphs);
    checkEdgesInSubgraphs(subgraphs);
    checkClustersSubGraph(clusters);
    checkItemsInSubGraphAndCluster();
  }

  private void checkSize(Collection<SubGraph> subgraphs, Collection<GraphCluster> clusters) {
    if (subgraphs.size() > clusters.size()) {
      throwNewException(String.format("Less clusters (%d) than subgraphs (%d)", clusters.size(), subgraphs.size()));
    }

    boolean sameSubgraphsCount = clustering.subgraphs.size() == subgraphs.size();
    boolean sameClustersCount = clustering.clusters.size() == clusters.size();
    if (!sameSubgraphsCount || !sameClustersCount) {
      throwNewException("graph and clustering data structures do not have same subgraphs/clusters count");
    }
  }

  private void checkClustersInSubGraphs(Collection<SubGraph> subgraphs) {
    for (SubGraph subGraph : subgraphs) {
      checkClustersInSubGraph(subGraph);
    }
  }

  private void checkClustersInSubGraph(SubGraph subGraph) {
    Map<Integer, GraphCluster> clusters = clusteringGraph.getClusters();
    for (int clusterIndex : subGraph.clusterIndices) {
      if (clusters.get(clusterIndex).subgraphId != subGraph.getGroupId()) {
        throwNewException(String.format("cluster (index = %d) and subgraph (ids = %s) are not coherent", clusterIndex, subGraph.getGroupId()));
      }
    }
  }

  private void checkEdgesInSubgraphs(Collection<SubGraph> subgraphs) {
    subgraphs.stream().forEach(subgraph -> checkEdgesInSubgraph(subgraph));
    clustering.subgraphs.stream().forEach(subgraph -> checkEdgesInSubgraph(subgraph));
  }

  private void checkEdgesInSubgraph(SubGraph subGraph) {
    List<GraphEdge> edges = subGraph.edges;
    List<Integer> indices = subGraph.itemIndices;
    edges.stream().forEach(edge -> {
      if (!indices.contains(edge.vertex1Index) || !indices.contains(edge.vertex2Index)) {
        throwNewException(String.format("subGraph (index = %d) contains an alien index: %s", subGraph.getGroupId(), edges));
      }
    });
  }

  private void checkEdgesInSubgraph(Subgraph subgraph) {
    List<Edge> edges = subgraph.edges;
    List<Integer> indices = subgraph.elementsIndices;
    edges.stream().forEach(edge -> {
      if (!indices.contains(edge.v1Index) || !indices.contains(edge.v2Index)) {
        throwNewException("a subgraph contains an alien index");
      }
    });
  }

  private void checkClustersSubGraph(Collection<GraphCluster> clusters) {
    for (GraphCluster graphCluster : clusters) {
      checkClustersSubGraph(graphCluster);
    }
  }

  private void checkClustersSubGraph(GraphCluster graphCluster) {
    SubGraph subgraph = clusteringGraph.getSubGraphs().get(graphCluster.subgraphId);
    if (!subgraph.clusterIndices.contains(graphCluster.getGroupId())) {
      throwNewException(String.format("cluster (ids = %d) and subgraph (ids = %s) are not coherent", graphCluster.getGroupId(), subgraph.getGroupId()));
    }
  }

  private void checkItemsInSubGraphAndCluster() {
    for (GraphItem graphItem : clusteringGraph.getItems()) {
      checkItemInSubGraphAndCluster(graphItem);
    }
  }

  private void checkItemInSubGraphAndCluster(GraphItem graphItem) {
    int itemIndex = graphItem.vertex1Index;
    if (!clusteringGraph.getClusters().get(graphItem.clusterId).itemIndices.contains(itemIndex)) {
      throwNewException(String.format("item (index = %d) not found in cluster (ids = %d)", itemIndex, graphItem.clusterId));
    }
    if (!clusteringGraph.getSubGraphs().get(graphItem.subgraphId).itemIndices.contains(itemIndex)) {
      throwNewException(String.format("item (index = %d) not found in subgraph (ids = %d)", itemIndex, graphItem.subgraphId));
    }
  }

  private void throwNewException(String message) {
    throw new CheckGraphException(message);
  }
}
