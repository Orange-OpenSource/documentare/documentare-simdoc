/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.stats

import com.orange.documentare.core.model.ref.comp.DoubleStatistics
import com.orange.documentare.core.model.ref.comp.IntStatistics
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

data class DescriptiveStats(private val percentile: Double = 50.0) {

  class PercentileOutOfRangeException(msg: String) : RuntimeException(msg)

  init {
    if (percentile <= 0 || percentile > 100) {
      throw PercentileOutOfRangeException("provided percentile ($percentile) must be in range ]0..100]")
    }
  }

  fun statistics(list: List<Int>): IntStatistics {
    val doubleStats = statistics(list.map { it.toDouble() })
    doubleStats.apply {
      return IntStatistics(min.toInt(), max.toInt(), mean, standardDeviation, percentile)
    }
  }

  fun statistics(list: List<Double>): DoubleStatistics {
    val s = list.stream().collect(
      { DoubleSummaryStatistics() },
      DoubleSummaryStatistics::accept,
      { obj: DoubleSummaryStatistics, other: DoubleSummaryStatistics? -> obj.combine(other) }
    )

    return DoubleStatistics(s.min, s.max, s.average, standardDeviation(list, s.average), percentile(list, percentile))
  }

  private fun standardDeviation(list: List<Double>, average: Double): Double {
    if (list.isEmpty()) {
      return Double.NaN
    }
    if (list.size == 1) {
      return 0.0
    }

    return sqrt(list.map { (it - average).pow(2.0) }.sum() / (list.size - 1))
  }

  private fun percentile(listNotSorted: List<Double>, percentile: Double): Double {

    val list = listNotSorted.sorted()

    if (list.isEmpty()) {
      return Double.NaN
    }
    if (list.size == 1) {
      return list.first()
    }
    if (percentile == 100.0) {
      return list.last()
    }

    val ratio = percentile / 100.0
    val index = (list.size + 1).toDouble() * ratio
    val floor = index.toInt()
    val diff = index - floor

    if (floor < 1) {
      return list.first()
    }
    if (floor >= list.size) {
      return list.last()
    }

    val lowValue = list[floor - 1]
    val highValue = list[floor]

    return lowValue + (highValue - lowValue) * diff
  }

}