package com.orange.documentare.core.comp.clustering.graph.subgraphs;
/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.scissors.GraphScissor;
import com.orange.documentare.core.comp.clustering.graph.scissors.ScissorTrigger;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphGroup;

import java.util.Collection;

public class InvalidEdgeDistanceScissor extends GraphScissor {

  private final ScissorTrigger trigger;

  public InvalidEdgeDistanceScissor(ClusteringGraph clusteringGraph, Collection<? extends GraphGroup> groups) {
    super(clusteringGraph, groups);
    trigger = new InvalidEdgeDistanceTrigger();
  }

  @Override
  protected ScissorTrigger getScissorTrigger() {
    return trigger;
  }
}
