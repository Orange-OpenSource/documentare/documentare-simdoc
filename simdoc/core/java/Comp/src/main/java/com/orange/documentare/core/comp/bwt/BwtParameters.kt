package com.orange.documentare.core.comp.bwt

data class BwtParameters(val algorithm: SuffixArrayAlgorithm)