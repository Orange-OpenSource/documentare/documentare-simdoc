/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.graph.voronoi

import com.orange.documentare.core.comp.clustering.graph.GraphvizPath
import com.orange.documentare.core.comp.clustering.graph.jgrapht.JGraphEdge
import com.orange.documentare.core.comp.clustering.graph.jgrapht.dotexport.DOT
import com.orange.documentare.core.comp.clustering.graph.jgrapht.dotexport.ExportVertexNameProvider
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem
import com.orange.documentare.core.system.nativeinterface.NativeInterface
import org.apache.commons.io.FileUtils
import org.jgrapht.Graph
import org.jgrapht.nio.dot.DOTExporter
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.*

/**
 * Voronoi maps.
 * NB: concurrent access are handled with UUID.
 */
data class Voronoi(private val clusteringGraph: ClusteringGraph, private val graph: Graph<GraphItem, JGraphEdge>) {

  companion object {
    const val TMP_DIR_PATH = "/tmp"
    const val TMP_FILE_PREFIX = "tmp_graph_"
    private const val TMP_FILE_ROOT_PATH = "$TMP_DIR_PATH/$TMP_FILE_PREFIX"
  }

  private val graphDotIn: String
  private val graphDotOut: String
  private val cmdFilterLog: String

  private val log = LoggerFactory.getLogger(Voronoi::class.java)

  init {
    val sessionId = UUID.randomUUID().toString()
    graphDotIn = "${TMP_FILE_ROOT_PATH}in_$sessionId.dot"
    graphDotOut = "${TMP_FILE_ROOT_PATH}out_$sessionId.dot"
    cmdFilterLog = "${TMP_FILE_ROOT_PATH}$sessionId.log"
  }

  fun mapClusterId() {
    exportGraph()
    NativeInterface.launch(graphVizCmd(), null, cmdFilterLog)
    importGraph()
    cleanup()
  }

  private fun cleanup() {
    listOf(graphDotIn, graphDotOut, cmdFilterLog).forEach {
      File(it).delete()
    }
  }

  private fun exportGraph() {
    val dotExporter = DOTExporter<GraphItem, JGraphEdge>()
    dotExporter.setVertexIdProvider(ExportVertexNameProvider::getName)
    try {
      dotExporter.exportGraph(graph, FileWriter(graphDotIn))
    } catch (e: IOException) {
      log.error("Voronoi treatment failed", e)
      throw VoronoiTreatmentException(e.message)
    }
  }

  private fun graphVizCmd() =
    GraphvizPath.PATH + "sfdp $graphDotIn | " +
      GraphvizPath.PATH + "gvmap -e -o $graphDotOut && grep -E 'g_.*\\[|cluster=' $graphDotOut | grep -v '\\-\\-' | grep -v 'cluster=-1' | sed \"s/\\[//\" | sed \"s/cluster=//\" | sed \"s/,//\""

  private fun importGraph() {
    try {
      val importString = importString()
      updateItemsClusterId(importString)
    } catch (e: IOException) {
      throwVoronoiException(e)
    }
  }

  private fun importString() = FileUtils.readFileToString(File(cmdFilterLog), "utf-8")

  private fun updateItemsClusterId(importStr: String) {
    val lines = importStr.lines().map { it.trim() }
    val vertexNameToClusterIdMap = mutableMapOf<String, Int>()
    lines.forEach {
      val strings = it.split("\\s+".toRegex())
      if (strings.size == 2) {
        vertexNameToClusterIdMap[strings[0]] = parseClusterId(strings[1])
      }
    }

    vertexNameToClusterIdMap.map { setClusterIdWith(it.key, it.value) }
  }

  private fun setClusterIdWith(vertexDotName: String, clusterId: Int) {
    val graphItem = findGraphItemMatching(vertexDotName)
    graphItem!!.clusterId = clusterId
  }

  private fun parseClusterId(clusterIdString: String): Int {
    return try {
      clusterIdString.toInt()
    } catch (e: NumberFormatException) {
      throwVoronoiException(e)
      -1
    }
  }

  private fun findGraphItemMatching(vertexDotName: String): GraphItem? {
    for (graphItem in clusteringGraph.getItems()) {
      if (vertexDotName == DOT.getDOTVertexName(graphItem.vertexName)) {
        return graphItem
      }
    }
    throwVoronoiException(IllegalStateException("Failed to find mathing item in graphviz output"))
    return null
  }

  private fun throwVoronoiException(e: Exception) {
    log.error("Voronoi treatment failed", e)
    throw VoronoiTreatmentException(e.message)
  }
}
