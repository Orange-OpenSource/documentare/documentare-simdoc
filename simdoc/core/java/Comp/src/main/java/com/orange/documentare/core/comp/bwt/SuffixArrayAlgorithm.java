package com.orange.documentare.core.comp.bwt;

public enum SuffixArrayAlgorithm {
  SAIS, LIBDIVSUFSORT
}
