package com.orange.documentare.core.comp.ncd.cache

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import org.apache.commons.codec.digest.DigestUtils
import java.util.*

object BytesUniqueHashKey {
  fun buildUniqueHashKey(bytes: ByteArray): String = DigestUtils.md5Hex(bytes).uppercase(Locale.getDefault())

  fun arraysAreEqual(key1: String, key2: String): Boolean = key1 == key2

  fun buildUniqueHashKey(key1: String, key2: String): String {
    return if (key1 > key2) {
      key1 + key2
    } else {
      key2 + key1
    }
  }
}
