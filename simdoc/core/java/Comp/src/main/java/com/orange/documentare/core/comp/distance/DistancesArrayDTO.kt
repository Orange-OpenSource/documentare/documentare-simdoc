/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.distance

class DistancesArrayDTO {
  var distancesArray: Array<IntArray>? = null
  var onSameArray: Boolean = false
  var nbColumns: Int = 0

  fun getDistancesFor(itemIndex: Int): IntArray {
    return DistancesArray.getDistancesFor(distancesArray, itemIndex, onSameArray, nbColumns)
  }

  fun toBusinessObject() = DistancesArray(distancesArray, onSameArray, nbColumns)

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is DistancesArrayDTO) return false

    if (distancesArray != null) {
      if (other.distancesArray == null) return false
      if (!distancesArray!!.contentDeepEquals(other.distancesArray!!)) return false
    } else if (other.distancesArray != null) return false
    if (onSameArray != other.onSameArray) return false
    if (nbColumns != other.nbColumns) return false

    return true
  }

  override fun hashCode(): Int {
    var result = distancesArray?.contentDeepHashCode() ?: 0
    result = 31 * result + onSameArray.hashCode()
    result = 31 * result + nbColumns
    return result
  }


  companion object {
    fun fromBusinessObject(bo: DistancesArray) = DistancesArrayDTO().also { dto ->
      dto.distancesArray = bo.distancesArray
      dto.onSameArray = bo.onSameArray
      dto.nbColumns = bo.nbColumns
    }
  }
}
