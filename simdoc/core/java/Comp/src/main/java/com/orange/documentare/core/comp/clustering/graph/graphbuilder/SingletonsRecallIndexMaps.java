package com.orange.documentare.core.comp.clustering.graph.graphbuilder;

import com.orange.documentare.core.model.ref.clustering.ClusteringItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class SingletonsRecallIndexMaps {
  final Map<Integer, Integer> oldToNew;
  final Map<Integer, Integer> newToOld;

  public SingletonsRecallIndexMaps(List<ClusteringItem> clusteringItems, List<ClusteringItem> singletons) {
    oldToNew = singletonsOldToNewIndexMap(clusteringItems, singletons);
    newToOld = singletonsNewToOldIndexMap(clusteringItems, singletons);
  }

  Map<Integer, Integer> singletonsOldToNewIndexMap(List<ClusteringItem> clusteringItems, List<ClusteringItem> singletons) {
    Map<Integer, Integer> singletonsOldToNewIndexMap = new HashMap<>();
    IntStream.range(0, singletons.size()).forEach(i ->
      singletonsOldToNewIndexMap.put(clusteringItems.indexOf(singletons.get(i)), i)
    );
    return singletonsOldToNewIndexMap;
  }

  private Map<Integer, Integer> singletonsNewToOldIndexMap(List<ClusteringItem> clusteringItems, List<ClusteringItem> singletons) {
    Map<Integer, Integer> singletonsNewToOldIndexMap = new HashMap<>();
    IntStream.range(0, singletons.size()).forEach(i ->
      singletonsNewToOldIndexMap.put(i, clusteringItems.indexOf(singletons.get(i)))
    );
    return singletonsNewToOldIndexMap;
  }
}
