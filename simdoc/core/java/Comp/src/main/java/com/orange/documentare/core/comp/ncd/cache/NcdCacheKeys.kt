package com.orange.documentare.core.comp.ncd.cache

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

class NcdCacheKeys(bytes1: ByteArray, bytes2: ByteArray) {
  val bytes1Key = BytesUniqueHashKey.buildUniqueHashKey(bytes1)
  val bytes2Key = BytesUniqueHashKey.buildUniqueHashKey(bytes2)
  val byteArraysKey = BytesUniqueHashKey.buildUniqueHashKey(bytes1Key, bytes2Key)
  val equalArrays = BytesUniqueHashKey.arraysAreEqual(bytes1Key, bytes2Key)

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is NcdCacheKeys) return false

    if (bytes1Key != other.bytes1Key) return false
    if (bytes2Key != other.bytes2Key) return false
    if (byteArraysKey != other.byteArraysKey) return false
    if (equalArrays != other.equalArrays) return false

    return true
  }

  override fun hashCode(): Int {
    var result = bytes1Key.hashCode()
    result = 31 * result + bytes2Key.hashCode()
    result = 31 * result + byteArraysKey.hashCode()
    result = 31 * result + equalArrays.hashCode()
    return result
  }
}