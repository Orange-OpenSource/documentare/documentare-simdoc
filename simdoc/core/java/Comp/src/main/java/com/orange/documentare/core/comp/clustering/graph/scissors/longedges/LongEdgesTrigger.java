package com.orange.documentare.core.comp.clustering.graph.scissors.longedges;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.scissors.ScissorTrigger;
import com.orange.documentare.core.model.ref.comp.IntStatistics;
import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphGroup;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Collectors;

@RequiredArgsConstructor
class LongEdgesTrigger implements ScissorTrigger {

  private final float standardDeviationDistanceFactor;

  @Getter(AccessLevel.PACKAGE)
  private int edgeLengthThreshold;

  @Override
  public void initForGroup(GraphGroup group) {
    edgeLengthThreshold = getEdgeLengthCutThreshold(group);
  }

  @Override
  public boolean shouldRemove(GraphEdge edge) {
    return edge.length > edgeLengthThreshold;
  }

  private int getEdgeLengthCutThreshold(GraphGroup group) {
    edgeLengthThreshold = Integer.MAX_VALUE;
    edgeLengthThreshold = getEdgeLengthThreshold(getStatisticsFor(group));
    edgeLengthThreshold = getEdgeLengthThreshold(getStatisticsFor(group));
    return edgeLengthThreshold;
  }

  private IntStatistics getStatisticsFor(GraphGroup group) {
    return (new DescriptiveStats()).statistics(group.edges.stream()
      .filter(edge -> edge.length <= edgeLengthThreshold)
      .map(edge -> edge.length)
      .collect(Collectors.toList())
    );
  }

  private int getEdgeLengthThreshold(IntStatistics stats) {
    return (int) (stats.getMean() + standardDeviationDistanceFactor * stats.getStandardDeviation());
  }
}
