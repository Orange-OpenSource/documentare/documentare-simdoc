package com.orange.documentare.core.comp.clustering.graph.subgraphs;
/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.scissors.ScissorTrigger;
import com.orange.documentare.core.comp.distance.Distance;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphGroup;

public class InvalidEdgeDistanceTrigger implements ScissorTrigger {

  @Override
  public boolean shouldRemove(GraphEdge edge) {
    return edge.length >= Distance.DISTANCE_INT_CONV_FACTOR;
  }

  @Override
  public void initForGroup(GraphGroup group) {
    // not used
  }
}
