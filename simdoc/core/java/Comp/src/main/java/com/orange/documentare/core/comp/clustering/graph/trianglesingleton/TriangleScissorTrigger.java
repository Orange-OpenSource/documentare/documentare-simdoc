package com.orange.documentare.core.comp.clustering.graph.trianglesingleton;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.comp.DoubleStatistics;
import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats;
import com.orange.documentare.core.comp.clustering.stats.TrianglesStats;
import com.orange.documentare.core.model.ref.clustering.ClusteringCoreParameters;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Getter(AccessLevel.PACKAGE)
class TriangleScissorTrigger {
  private final boolean acut;
  private final boolean qcut;
  private final float qThreshold;
  private final float areaThreshold;

  TriangleScissorTrigger(List<GraphItem> items, ClusteringCoreParameters clusteringParameters) {
    acut = clusteringParameters.acut();
    qcut = clusteringParameters.qcut();
    TrianglesStats trianglesStats = getTriangleStats(items);
    qThreshold = getQCutThreshold(trianglesStats.getTrianglesEquilaterality(), clusteringParameters.qcutSdFactor);
    areaThreshold = getAreaThreshold(trianglesStats.getTrianglesArea(), clusteringParameters.acutSdFactor);
  }

  boolean shouldCut(GraphItem graphItem) {
    boolean shouldCut =
      (qcut && graphItem.q < qThreshold) || (acut && graphItem.area > areaThreshold);

    if (shouldCut && log.isDebugEnabled()) {
      log.debug(String.format("Cut item %s (q %f - a %f - triangulation qThresh %f aThresh %f)", graphItem.vertexName, graphItem.q, graphItem.area, qThreshold, areaThreshold));
    }
    return shouldCut;
  }

  private float getQCutThreshold(DoubleStatistics trianglesEquilaterality, float qcutSdFactor) {
    return (float) (trianglesEquilaterality.getMean() - qcutSdFactor * trianglesEquilaterality.getStandardDeviation());
  }

  private float getAreaThreshold(DoubleStatistics trianglesArea, float acutSdFactor) {
    return (float) (trianglesArea.getMean() + acutSdFactor * trianglesArea.getStandardDeviation());
  }

  private TrianglesStats getTriangleStats(List<GraphItem> items) {
    DoubleStatistics qStats = new DescriptiveStats()
      .statistics(items.stream().filter(item -> !item.triangleSingleton).map(item -> (double) item.q).collect(Collectors.toList()));
    DoubleStatistics aStats = new DescriptiveStats()
      .statistics(items.stream().filter(item -> !item.triangleSingleton).map(item -> (double) item.area).collect(Collectors.toList()));

    return new TrianglesStats(qStats, aStats);
  }
}
