package com.orange.documentare.core.comp.clustering.graph.jgrapht;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.graph.AbstractBaseGraph;
import org.jgrapht.graph.WeightedPseudograph;

import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import com.orange.documentare.core.model.ref.clustering.Edge;

import lombok.AccessLevel;
import lombok.Getter;

@Getter(AccessLevel.PROTECTED)
public class ExternalGraphBuilder {

  private Graph<ClusteringElement, JGraphEdge> graph;
  private Clustering clustering;

  public AbstractBaseGraph<ClusteringElement, JGraphEdge> getJGraphTFrom(Clustering clustering) {
    this.clustering = clustering;
    graph = new WeightedPseudograph<>(JGraphEdge.class);
    clustering.elements.forEach(element -> graph.addVertex(element));
    addEdges(clustering);
    return (AbstractBaseGraph<ClusteringElement, JGraphEdge>) graph;
  }

  protected void addEdges(Clustering clustering) {
    clustering.subgraphs.forEach(subgraph -> subgraph.edges.forEach(this::addEdge));
  }

  private void addEdge(Edge edge) {
    List<ClusteringElement> elements = clustering.elements;
    JGraphEdge e = graph.addEdge(elements.get(edge.v1Index), elements.get(edge.v2Index));
    e.init(edge.v1Index, edge.v2Index, edge.length);
    getGraph().setEdgeWeight(e, edge.length);
  }
}
