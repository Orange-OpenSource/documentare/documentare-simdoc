package com.orange.documentare.core.comp.distance

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm

data class DistanceParameters(val suffixArrayAlgorithm: SuffixArrayAlgorithm) {
  companion object {
    fun defaults(): DistanceParameters = DistanceParameters(SuffixArrayAlgorithm.LIBDIVSUFSORT)
  }
}