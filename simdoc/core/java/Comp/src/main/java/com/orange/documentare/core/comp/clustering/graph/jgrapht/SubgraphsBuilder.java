package com.orange.documentare.core.comp.clustering.graph.jgrapht;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.subgraphs.SameSubgraph;
import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import com.orange.documentare.core.model.ref.clustering.graph.SubGraph;
import com.orange.documentare.core.model.ref.comp.DoubleStatistics;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class SubgraphsBuilder {
  private final ClusteringGraph clusteringGraph;

  public Graph computeSubGraphs(InternalGraphBuilder internalGraphBuilder) {
    Graph graph = detectSubGraphs(internalGraphBuilder);
    setSubgraphsEdges(graph);
    return graph;
  }

  private Graph detectSubGraphs(InternalGraphBuilder internalGraphBuilder) {
    Graph graph = internalGraphBuilder.getJGraphTFrom(clusteringGraph);
    List<Collection<Integer>> jSubGraphs = internalGraphBuilder.getAndUpdateSubGraphs(graph);
    setSubGraphsFrom(jSubGraphs);
    return graph;
  }

  private void setSubGraphsFrom(List<Collection<Integer>> jSubGraphs) {
    Map<Integer, SubGraph> subGraphs = clusteringGraph.getSubGraphs();
    subGraphs.clear();
    for (int i = 0; i < jSubGraphs.size(); i++) {
      subGraphs.put(i, getSubGraphFrom(jSubGraphs.get(i), i));
    }
  }

  private SubGraph getSubGraphFrom(Collection<Integer> jSubGraph, int subGraphId) {
    List<GraphItem> graphItems = clusteringGraph.getItems();
    jSubGraph.stream().map(graphItems::get).forEach(graphItem -> graphItem.subgraphId = subGraphId);

    DoubleStatistics qStats = (new DescriptiveStats())
      .statistics(jSubGraph.stream().map(graphItems::get).map(item -> (double) item.q).collect(Collectors.toList()));
    DoubleStatistics aStats = (new DescriptiveStats())
      .statistics(jSubGraph.stream().map(graphItems::get).map(item -> (double) item.area).collect(Collectors.toList()));

    return new SubGraph(subGraphId, jSubGraph, qStats, aStats);
  }

  private void setSubgraphsEdges(Graph<GraphItem, JGraphEdge> graph) {
    SameGroup sameGroup = new SameSubgraph();
    GroupEdges groupEdges = new GroupEdges(clusteringGraph.getItems(), clusteringGraph.getSubGraphs(), sameGroup);
    groupEdges.updateGroupsEdges(graph.edgeSet().stream().map(JGraphEdge::getEdge).collect(Collectors.toList()));
  }
}
