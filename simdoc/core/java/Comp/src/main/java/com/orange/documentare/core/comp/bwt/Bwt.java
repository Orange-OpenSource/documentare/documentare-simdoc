package com.orange.documentare.core.comp.bwt;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import com.orange.documentare.core.nativelib.LibDivsufsortNative;

public class Bwt {

  interface SuffixArrayAlgo {
    void run(byte[] inputArray, int[] outputArray, int arraySize);
  }

  private final LibDivsufsortNative libDivsufsortNative = new LibDivsufsortNative();

  public final BwtParameters bwtParameters;
  final SuffixArrayAlgo saAlgo;
  final SuffixArrayAlgo libDivSufSort = libDivsufsortNative::divsufsort;
  final SuffixArrayAlgo sais = VanillaSais::suffixsort;

  public Bwt(BwtParameters bwtParameters) {
    this.bwtParameters = bwtParameters;
    saAlgo = bwtParameters.getAlgorithm() == SuffixArrayAlgorithm.LIBDIVSUFSORT ? libDivSufSort : sais;
  }

  public byte[] getBwt(byte[] bytes) {
    int[] suffixArrayIndices = computeSuffixArrayIndices(bytes);
    int length = suffixArrayIndices.length;

    byte[] outputBytes = new byte[length / 2];
    int outputIndex = 0;
    for (int saIndex : suffixArrayIndices) {
      if (saIndex < length / 2) {
        int origIndex = saIndex - 1;
        origIndex = origIndex < 0 ? length / 2 - 1 : origIndex;
        outputBytes[outputIndex++] = bytes[origIndex];
      }
    }
    return outputBytes;
  }

  public int[] computeSuffixArrayIndices(byte[] bytes) {
    byte[] saisInput = getDoubleBytesCopy(bytes);
    int length = saisInput.length;
    int[] suffixArrayIndices = new int[length];
    saAlgo.run(saisInput, suffixArrayIndices, length);
    return suffixArrayIndices;
  }

  private byte[] getDoubleBytesCopy(byte[] bytes) {
    int length = bytes.length;
    byte[] doubleBytes = new byte[length * 2];
    System.arraycopy(bytes, 0, doubleBytes, 0, length);
    System.arraycopy(bytes, 0, doubleBytes, length, length);
    return doubleBytes;
  }
}
