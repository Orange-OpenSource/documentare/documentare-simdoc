package com.orange.documentare.core.comp.distance.bytesdistances;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PreppedBytesData {
  public final BytesData[] bytesData;

  public PreppedBytesData() {
    bytesData = null;
  }
}
