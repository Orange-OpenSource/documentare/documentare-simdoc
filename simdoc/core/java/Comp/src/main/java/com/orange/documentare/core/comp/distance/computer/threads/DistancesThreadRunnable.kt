package com.orange.documentare.core.comp.distance.computer.threads

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.computer.DistancesComputer
import com.orange.documentare.core.comp.measure.ProgressListener
import com.orange.documentare.core.comp.measure.TreatmentStep
import com.orange.documentare.core.model.ref.comp.DistanceItem
import com.orange.documentare.core.system.measure.Progress
import org.slf4j.LoggerFactory

class DistancesThreadRunnable(
  private val itemIndices: IntArray,
  private val items: Array<DistanceItem>,
  private val computer: DistancesComputer,
  private val listener: ProgressListener?) : Runnable {

  override fun run() {
    log.debug("[Distance thread] start")
    for (i in itemIndices.indices) {
      computeDistancesFor(i)
      showProgress()
    }
    log.debug("[Distance thread] done")
  }

  private fun computeDistancesFor(index: Int) {
    val startIndex = if (computer.isWorkingOnSameArray) itemIndices[index] + 1 else 0
    val distancesToItem = computer.distance.compute(items[index], computer.itemsToCompareTo, startIndex)
    computer.putResult(itemIndices[index], distancesToItem)
    computer.incTasksFinished()
  }

  private fun showProgress() {
    listener?.onProgressUpdate(TreatmentStep.NCD, Progress(computeProgressPercent(), computer.computeTimeSinceStartInSec()))
  }

  private fun computeProgressPercent(): Int {
    val initialSize = computer.itemsNumber
    return 100 - (initialSize - computer.getTasksFinishedCount()) * 100 / initialSize
  }

  companion object {
    private val log = LoggerFactory.getLogger(DistancesThreadRunnable::class.java)
  }
}
