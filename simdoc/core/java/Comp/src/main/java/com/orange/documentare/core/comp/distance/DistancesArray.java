package com.orange.documentare.core.comp.distance;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.comp.DistanceItem;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.NearestItemsProvider;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@EqualsAndHashCode
public final class DistancesArray implements NearestItemsProvider {
  /**
   * we expose these data in order to be able to serialize it in NCD
   */
  final int[][] distancesArray;
  final boolean onSameArray;
  final int nbColumns;

  public DistancesArray(int nbRows, int nbColumns, boolean onSameArray) {
    this(new int[nbRows][nbColumns], onSameArray, nbColumns);
  }

  DistancesArray(int[][] distancesArray, boolean onSameArray, int nbColumns) {
    this.nbColumns = nbColumns;
    this.onSameArray = onSameArray;
    this.distancesArray = distancesArray;
  }

  public synchronized void setDistancesForItem(int itemIndex, int[] distancesForItem) {
    distancesArray[itemIndex] = distancesForItem;
  }

  public int[] getDistancesFor(int itemIndex) {
    return getDistancesFor(distancesArray, itemIndex, onSameArray, nbColumns);
  }

  static int[] getDistancesFor(int[][] distancesMatrix, int itemIndex, boolean onSameArray, int nbColumns) {
    return onSameArray ? getDistancesOnSameArrayFor(distancesMatrix, itemIndex, nbColumns) : distancesMatrix[itemIndex];
  }

  public int get(int rowIndex, int columnIndex) {
    return onSameArray ? getOnSameArray(distancesArray, rowIndex, columnIndex) : distancesArray[rowIndex][columnIndex];
  }

  public DistancesArray removeDuplicates() {
    List<Integer> duplicateIndices = IntStream.range(0, distancesArray.length)
      .filter(index -> findDuplicateForIndex(index).isPresent())
      .boxed()
      .collect(Collectors.toList());

    // rebuild a non optimized matrix to remove duplicates
    int rows = distancesArray.length;
    int[][] array = new int[rows][rows];
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array[0].length; j++) {
        array[i][j] = get(i, j);
      }
    }

    int matrixRows = rows - duplicateIndices.size();

    int[][] noDuplicatesMatrix = new int[matrixRows][matrixRows];

    int skipRowCount = 0;
    for (int i = 0; i < rows; i++) {
      if (duplicateIndices.contains(i)) {
        skipRowCount++;
        continue;
      }
      int skipColCount = 0;
      for (int j = 0; j < rows; j++) {
        if (duplicateIndices.contains(j)) {
          skipColCount++;
          continue;
        }
        noDuplicatesMatrix[i - skipRowCount][j - skipColCount] = array[i][j];
      }
    }

    DistancesArray distancesArray = new DistancesArray(matrixRows, matrixRows, true);
    int[][] halfMatrix = distancesArray.distancesArray;
    for (int i = 0; i < noDuplicatesMatrix.length; i++) {
      halfMatrix[i] = new int[noDuplicatesMatrix.length - i - 1];
      System.arraycopy(noDuplicatesMatrix[i], i + 1, halfMatrix[i], 0, noDuplicatesMatrix.length - i - 1);
    }

    return distancesArray;
  }

  public NearestItem nearestItemOf(int itemIndex) {
    return nearestItemOfBut(itemIndex, -1);
  }

  public NearestItem nearestItemOfBut(int itemIndex, int excludeIndex) {
    int minDistance = Integer.MAX_VALUE;
    int nearestIndex = -1;
    for (int i = 0; i < nbColumns; i++) {
      int distance = get(itemIndex, i);
      if (distance < minDistance && i != itemIndex && i != excludeIndex) {
        minDistance = distance;
        nearestIndex = i;
      }
    }
    return new NearestItem(nearestIndex, minDistance);
  }

  @Override
  public NearestItem[] nearestItemsFor(List<? extends DistanceItem> items, int itemIndex) {
    List<Integer> nearestIndices = getSortedByDistanceIndicesFor(items, itemIndex);
    NearestItem[] nearestItems = new NearestItem[nearestIndices.size()];
    fillNearestItems(itemIndex, nearestItems, nearestIndices);
    return nearestItems;
  }

  private void fillNearestItems(int itemIndex, NearestItem[] nearestItems, List<Integer> nearestIndices) {
    for (int i = 0; i < nearestItems.length; i++) {
      int nearestIndex = nearestIndices.get(i);
      nearestItems[i] = new NearestItem(nearestIndex, get(itemIndex, nearestIndex));
    }
  }

  private Optional<Integer> findDuplicateForIndex(int index) {
    NearestItem nearestItem = nearestItemOf(index);
    boolean duplicate = nearestItem.getDistance() == 0 && nearestItem.getIndex() < index;
    return duplicate ? Optional.of(nearestItem.getIndex()) : Optional.empty();
  }

  private List<Integer> getSortedByDistanceIndicesFor(List<? extends DistanceItem> items, int itemIndex) {
    List<Integer> indices = getIndices();
    indices.sort(new DistanceIndexComparator(items, getDistancesFor(itemIndex), itemIndex));
    return indices;
  }

  private List<Integer> getIndices() {
    Integer[] indices = new Integer[nbColumns];
    for (int i = 0; i < indices.length; i++) {
      indices[i] = i;
    }
    return Arrays.asList(indices);
  }

  /**
   * The distance array is a square, and rows&columns elements are the same.
   * Here we implement the "half-matrix" optimization: d[i][j] = d[j][i]
   */
  private static int getOnSameArray(int[][] distancesMatrix, int rowIndex, int columnIndex) {
    if (rowIndex == columnIndex) {
      return 0;
    } else if (rowIndex < columnIndex) {
      // -1 : the '0' in the median is not in the array
      return distancesMatrix[rowIndex][columnIndex - rowIndex - 1];
    } else {
      return distancesMatrix[columnIndex][rowIndex - columnIndex - 1];
    }
  }

  /**
   * the "half-matrix" optimization let's us compute half of the matrix, but
   * here we need to generate the row as a whole.
   */
  private static int[] getDistancesOnSameArrayFor(int[][] distancesMatrix, int itemIndex, int nbColumns) {
    int[] row = new int[nbColumns];
    for (int i = 0; i < nbColumns; i++) {
      row[i] = getOnSameArray(distancesMatrix, itemIndex, i);
    }
    return row;
  }

  public Duplicates duplicates() {
    Duplicates duplicates = new Duplicates();

    IntStream.range(0, distancesArray.length)
      .forEach(index -> {
        Optional<Integer> duplicateIndex = findDuplicateForIndex(index);
        duplicateIndex.ifPresent(integer -> duplicates.add(index, integer));
      });

    return duplicates;
  }
}
