package com.orange.documentare.core.comp.clustering.graph.clusters;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.jgrapht.GroupEdges;
import com.orange.documentare.core.comp.clustering.graph.jgrapht.SameGroup;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphCluster;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Slf4j
public class BindClusterToSubgraph {

  public static void updateClusterEdges(ClusteringGraph clusteringGraph, Collection<GraphEdge> edges) {
    SameGroup sameGroup = new SameCluster();
    GroupEdges groupEdges = new GroupEdges(clusteringGraph.getItems(), clusteringGraph.getClusters(), sameGroup);
    groupEdges.updateGroupsEdges(edges);
  }

  public static void bindClusterToSubgraph(ClusteringGraph clusteringGraph) {
    clusteringGraph.getItems().forEach(item -> {
      GraphCluster graphCluster = getGraphCluster(item, clusteringGraph);
      checkCluster(graphCluster, item.subgraphId, item.clusterId);
      addItemIndexToCluster(graphCluster, item.vertex1Index);
    });
  }

  private static void checkCluster(GraphCluster graphCluster, int subgraphId, int clusterId) {
    if (graphCluster.subgraphId != subgraphId) {
      throwBindException(new IllegalStateException("items in same cluster but with different subgraph ids..."));
    }
    if (graphCluster.getGroupId() != clusterId) {
      throwBindException(new IllegalStateException("items in same cluster but with different cluster ids..."));
    }
  }

  private static void addItemIndexToCluster(GraphCluster graphCluster, int itemIndex) {
    List<Integer> itemIndices = graphCluster.itemIndices;
    if (itemIndices.contains(itemIndex)) {
      throwBindException(new IllegalStateException("Cluster already contains item"));
    } else {
      itemIndices.add(itemIndex);
    }
  }

  private static GraphCluster getGraphCluster(GraphItem graphItem, ClusteringGraph clusteringGraph) {
    Map<Integer, GraphCluster> clusters = clusteringGraph.getClusters();
    GraphCluster graphCluster = clusters.get(graphItem.clusterId);
    if (graphCluster == null) {
      return newGraphCLuster(graphItem, clusteringGraph);
    }
    return graphCluster;
  }

  private static GraphCluster newGraphCLuster(GraphItem graphItem, ClusteringGraph clusteringGraph) {
    GraphCluster graphCluster = new GraphCluster(0, null);
    int clusterId = graphItem.clusterId;
    int subgraphId = graphItem.subgraphId;
    graphCluster.setGroupId(clusterId);
    graphCluster.subgraphId = subgraphId;
    addNewCluster(graphCluster, clusterId, subgraphId, clusteringGraph);
    return graphCluster;
  }

  private static void addNewCluster(GraphCluster graphCluster, int clusterId, int subgraphId, ClusteringGraph clusteringGraph) {
    clusteringGraph.getSubGraphs().get(subgraphId).clusterIndices.add(clusterId);
    if (clusteringGraph.getClusters().containsKey(clusterId)) {
      throwBindException(new IllegalStateException("A cluster with same ids already exists"));
    } else {
      clusteringGraph.getClusters().put(clusterId, graphCluster);
    }
  }

  private static void throwBindException(IllegalStateException e) {
    log.error("Voronoi treatment failed", e);
    throw e;
  }
}
