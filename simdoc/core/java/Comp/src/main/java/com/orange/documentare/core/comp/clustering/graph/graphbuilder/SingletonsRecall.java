package com.orange.documentare.core.comp.clustering.graph.graphbuilder;

import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.clustering.EnrollParameters;
import com.orange.documentare.core.model.ref.clustering.graph.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class SingletonsRecall {

  static class GraphInfo {
    final int clusterIdMax;
    final int subgraphIdMax;
    final Map<Integer, SubGraph> subGraphs;
    final Map<Integer, GraphCluster> clusters;
    final SingletonsRecallIndexMaps indexMaps;

    public GraphInfo(ClusteringGraph clusteringGraph, SingletonsRecallIndexMaps indexMaps) {
      clusterIdMax = clusteringGraph.clusterIdMax() + 1;
      subgraphIdMax = clusteringGraph.subgraphIdMax() + 1;
      subGraphs = clusteringGraph.getSubGraphs();
      clusters = clusteringGraph.getClusters();
      this.indexMaps = indexMaps;
    }
  }

  private final ClusteringGraphBuilder clusteringGraphBuilder;
  private final List<ClusteringItem> clusteringItems;
  private final EnrollParameters enrollParameters;

  public void doRecallAndUpdateGraph(ClusteringGraph clusteringGraph) {
    ClusteringItem[] initialGraphSingletons = retrieveSingletonsItemsFrom(clusteringItems, clusteringGraph);

    if (initialGraphSingletons.length > 3) {
      doJob(clusteringGraph, initialGraphSingletons);
    }
  }

  private void doJob(ClusteringGraph initialGraph, ClusteringItem[] initialGraphSingletons) {
    SingletonsRecallIndexMaps indexMaps = new SingletonsRecallIndexMaps(clusteringItems, Arrays.asList(initialGraphSingletons));
    GraphInfo initialGraphInfo = new GraphInfo(initialGraph, indexMaps);

    ClusteringGraph singletonsGraph = buildSingletonsGraph(indexMaps, initialGraphSingletons);

    removeInitialGraphSingletonsFromSubgraphsAndClusters(initialGraph, initialGraphInfo);
    updateInitialGraphSingletons(initialGraph, singletonsGraph, initialGraphInfo);
    updateInitialGraphSubgraphAndClusters(initialGraph, singletonsGraph, initialGraphInfo);
  }

  private ClusteringGraph buildSingletonsGraph(SingletonsRecallIndexMaps indexMaps, ClusteringItem[] initialGraphSingletons) {
    SingletonForReGraph[] singletonsCopy = buildSingletonsForRegraph(indexMaps.oldToNew, clusteringItems, initialGraphSingletons);
    ClusteringGraph singletonsClusteringGraph = clusteringGraphBuilder.doBuild(singletonsCopy, enrollParameters);
    return singletonsClusteringGraph;
  }

  void removeInitialGraphSingletonsFromSubgraphsAndClusters(ClusteringGraph initialGraph, GraphInfo initialGraphInfo) {
    List<GraphItem> initialGraphSingletons = retrieveSingletonsFrom(initialGraph.getItems(), initialGraphInfo.indexMaps);

    initialGraphSingletons.forEach(initialGraphSingleton -> {
      initialGraph.getSubGraphs().remove(initialGraphSingleton.subgraphId);
      initialGraph.getClusters().remove(initialGraphSingleton.clusterId);
    });
  }

  void updateInitialGraphSingletons(ClusteringGraph initialGraph, ClusteringGraph singletonsGraph, GraphInfo initialGraphInfo) {
    List<GraphItem> initialGraphSingletons = retrieveSingletonsFrom(initialGraph.getItems(), initialGraphInfo.indexMaps);

    initialGraphSingletons.forEach(initialSingleton -> {
      GraphItem itemInSingletonsGraph =
        singletonsGraph.getItems().get(initialGraphInfo.indexMaps.oldToNew.get(initialSingleton.vertex1Index));

      initialSingleton.clusterId = itemInSingletonsGraph.clusterId + initialGraphInfo.clusterIdMax;
      initialSingleton.subgraphId = itemInSingletonsGraph.subgraphId + initialGraphInfo.subgraphIdMax;

      Integer vertex2Index = initialGraphInfo.indexMaps.newToOld.get(itemInSingletonsGraph.vertex2Index);
      initialSingleton.vertex2Index = vertex2Index == null ? -1 : vertex2Index;

      Integer vertex3Index = initialGraphInfo.indexMaps.newToOld.get(itemInSingletonsGraph.vertex3Index);
      initialSingleton.vertex3Index = vertex3Index == null ? -1 : vertex3Index;

      initialSingleton.clusterCenter = itemInSingletonsGraph.clusterCenter;
      initialSingleton.enrolled = true;
    });
  }

  void updateInitialGraphSubgraphAndClusters(ClusteringGraph initialGraph, ClusteringGraph singletonsGraph, GraphInfo initialGraphInfo) {
    singletonsGraph.getSubGraphs().forEach((subgraphId, subgraph) -> {
      initialGraph.getSubGraphs().put(subgraphId + initialGraphInfo.subgraphIdMax, subgraph);

      subgraph.setGroupId(subgraph.getGroupId() + initialGraphInfo.subgraphIdMax);

      List<Integer> clusterIndices = subgraph.clusterIndices;
      for (int i = 0; i < clusterIndices.size(); i++) {
        clusterIndices.set(i, clusterIndices.get(i) + initialGraphInfo.clusterIdMax);
      }

      updateItemIndices(initialGraphInfo.indexMaps.newToOld, subgraph);
      updateEdges(subgraph.itemIndices, subgraph, initialGraphInfo.indexMaps.newToOld);
    });

    singletonsGraph.getClusters().forEach((clusterId, cluster) -> {
      initialGraph.getClusters().put(clusterId + initialGraphInfo.clusterIdMax, cluster);

      cluster.subgraphId = cluster.subgraphId + initialGraphInfo.subgraphIdMax;
      cluster.setGroupId(cluster.getGroupId() + initialGraphInfo.clusterIdMax);

      updateItemIndices(initialGraphInfo.indexMaps.newToOld, cluster);
      updateEdges(initialGraphInfo.subGraphs.get(cluster.subgraphId).itemIndices, cluster, initialGraphInfo.indexMaps.newToOld);
    });
  }

  private List<GraphItem> retrieveSingletonsFrom(List<GraphItem> items, SingletonsRecallIndexMaps indexMaps) {
    List<GraphItem> singletons = new ArrayList<>();
    indexMaps.oldToNew.keySet().forEach(
      singletonIndex -> singletons.add(items.get(singletonIndex))
    );
    return singletons;
  }

  /**
   * NB: item indices were updated just before...
   */
  void updateEdges(List<Integer> subgraphItemIndices, GraphGroup graphGroup, Map<Integer, Integer> newToOld) {
    List<GraphEdge> edgesToRemove = new ArrayList<>();
    graphGroup.edges.forEach(edge -> {
        int vertex1NewIndex = edge.vertex1Index;
        int vertex2NewIndex = edge.vertex2Index;
        int oldVertex1Index = newToOld.getOrDefault(vertex1NewIndex, -1);
        int oldVertex2Index = newToOld.getOrDefault(vertex2NewIndex, -1);
        if (!subgraphItemIndices.contains(oldVertex1Index) || !subgraphItemIndices.contains(oldVertex2Index)) {
          edgesToRemove.add(edge);
        } else {
          edge.vertex1Index = oldVertex1Index;
          edge.vertex2Index = oldVertex2Index;
        }
      }
    );
    graphGroup.edges.removeAll(edgesToRemove);
  }

  private void updateItemIndices(Map<Integer, Integer> singletonsNewToOldIndexMap, GraphGroup graphGroup) {
    List<Integer> itemIndices = graphGroup.itemIndices;
    for (int i = 0; i < itemIndices.size(); i++) {
      itemIndices.set(i, singletonsNewToOldIndexMap.get(itemIndices.get(i)));
    }
  }

  SingletonForReGraph[] buildSingletonsForRegraph(Map<Integer, Integer> singletonsOldToNewIndexMap, List<ClusteringItem> clusteringItems, ClusteringItem[] singletons) {
    List<SingletonForReGraph> singletonsCopy = Arrays.stream(singletons)
      .map(singleton -> new SingletonForReGraph(singleton, clusteringItems.indexOf(singleton), singletonsOldToNewIndexMap))
      .collect(Collectors.toList());

    return singletonsCopy.toArray(new SingletonForReGraph[singletonsCopy.size()]);
  }

  ClusteringItem[] retrieveSingletonsItemsFrom(List<ClusteringItem> clusteringItems, ClusteringGraph clusteringGraph) {
    List<ClusteringItem> singletonList = clusteringGraph.getSubGraphs().values().stream()
      .filter(subGraph -> subGraph.itemIndices.size() == 1)
      .map(subGraph -> subGraph.itemIndices.get(0))
      .map(singletonIndex -> clusteringItems.get(singletonIndex))
      .collect(Collectors.toList());

    log.info("[Enroll] found {} singletons", singletonList.size());

    return singletonList.toArray(new ClusteringItem[singletonList.size()]);
  }
}
