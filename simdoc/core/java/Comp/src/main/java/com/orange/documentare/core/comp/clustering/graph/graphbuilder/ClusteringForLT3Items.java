package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import com.orange.documentare.core.model.ref.clustering.*;

import java.util.ArrayList;
import java.util.List;

class ClusteringForLT3Items {

  public static Clustering build(ClusteringItem[] clusteringItems, ClusteringParameters parameters) {
    switch (clusteringItems.length) {
      case 0:
        return forNotItems(parameters);
      case 1:
        return forOneItem(clusteringItems[0], parameters);
      case 2:
        return forTwoItems(clusteringItems[0], clusteringItems[1], parameters);
    }
    throw new IllegalStateException("More than 2 items!");
  }

  private static Clustering forNotItems(ClusteringParameters parameters) {
    return new Clustering(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), parameters, null, null);
  }

  private static Clustering forOneItem(ClusteringItem clusteringItem, ClusteringParameters parameters) {
    List<ClusteringElement> elements = new ArrayList<>();
    elements.add(new ClusteringElement(clusteringItem.getHumanReadableId(), 0, true, false, null, null));

    List<Subgraph> subgraphs = new ArrayList<>();
    List<Integer> itemsIndices = new ArrayList<>();
    itemsIndices.add(0);
    subgraphs.add(new Subgraph(itemsIndices, new ArrayList<>(), 0));

    List<Cluster> clusters = new ArrayList<>();
    clusters.add(new Cluster(itemsIndices));

    return new Clustering(elements, subgraphs, clusters, parameters, null, null);
  }

  private static Clustering forTwoItems(ClusteringItem clusteringItem0, ClusteringItem clusteringItem1, ClusteringParameters parameters) {
    List<ClusteringElement> elements = new ArrayList<>();
    elements.add(new ClusteringElement(
      clusteringItem0.getHumanReadableId(), 0, true, null, null, null)
    );
    elements.add(new ClusteringElement(
      clusteringItem1.getHumanReadableId(), 0, null, null, null, null)
    );

    List<Subgraph> subgraphs = new ArrayList<>();
    List<Integer> itemsIndices = new ArrayList<>();
    itemsIndices.add(0);
    itemsIndices.add(1);
    subgraphs.add(new Subgraph(itemsIndices, new ArrayList<>(), 0));

    List<Cluster> clusters = new ArrayList<>();
    clusters.add(new Cluster(itemsIndices));

    return new Clustering(elements, subgraphs, clusters, parameters, null, null);
  }
}
