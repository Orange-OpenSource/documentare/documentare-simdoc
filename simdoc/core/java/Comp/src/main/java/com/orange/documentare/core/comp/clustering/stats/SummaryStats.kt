package com.orange.documentare.core.comp.clustering.stats

import kotlin.math.max
import kotlin.math.min

class SummaryStats {
  private var min = Double.POSITIVE_INFINITY
  private var max = Double.NEGATIVE_INFINITY
  private var mean = Double.NaN
  private var sum = 0.0
  private var count = 0L

  @Synchronized
  fun addValue(value: Double) {
    count++
    sum += value
    mean = sum / count
    min = if (min.isNaN()) {
      value
    } else {
      min(min, value)
    }
    max = if (max.isNaN()) {
      value
    } else {
      max(max, value)
    }
  }

  @Synchronized
  fun getMin() = min

  @Synchronized
  fun getMax() = max

  @Synchronized
  fun getMean() = mean

  @Synchronized
  fun getCount() = count

  @Synchronized
  override fun toString() = "count=$count / min=$min / max=$max / mean=$mean"

  @Synchronized
  fun clear() {
    min = Double.POSITIVE_INFINITY
    max = Double.NEGATIVE_INFINITY
    mean = Double.NaN
    sum = 0.0
    count = 0L
  }
}