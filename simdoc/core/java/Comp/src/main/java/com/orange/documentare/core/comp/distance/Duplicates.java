package com.orange.documentare.core.comp.distance;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.orange.documentare.core.model.ref.clustering.Cluster;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;

public class Duplicates {

  private final Map<Integer, List<Integer>> mapIndexToKeepToItsListOfDuplicates = new HashMap<>();
  private final Map<Integer, Integer> mapDuplicateIndexToIndexToKeep = new HashMap<>();

  public int count() {
    return mapIndexToKeepToItsListOfDuplicates.values().stream().mapToInt(List::size).sum();
  }

  public Optional<List<Integer>> forIndex(int index) {
    return Optional.ofNullable(mapIndexToKeepToItsListOfDuplicates.get(index));
  }

  public void add(int duplicateIndex, Integer indexToKeep) {
    List<Integer> list = mapIndexToKeepToItsListOfDuplicates.getOrDefault(indexToKeep, new ArrayList<>());
    list.add(duplicateIndex);
    mapIndexToKeepToItsListOfDuplicates.put(indexToKeep, list);
    mapDuplicateIndexToIndexToKeep.put(duplicateIndex, indexToKeep);
  }

  public List<Integer> indicesWithoutDuplicates(int elementsCount) {
    List<Integer> list = new ArrayList<>(IntStream.range(0, elementsCount).boxed().toList());
    mapIndexToKeepToItsListOfDuplicates.values().forEach(list::removeAll);
    return list;
  }

  public Clustering addDuplicatesToClustering(Clustering clustering, String[] idsWithDuplicates) {
    List<ClusteringElement> elements = addDuplicatedElements(clustering, idsWithDuplicates);
    elements = updateClusterIdOfDuplicates(elements, elements);

    List<Cluster> clustersWithRemappedMultisetIndices = remapClustersMultisetIndices(clustering, idsWithDuplicates.length);

    return new Clustering(
      elements, clustering.subgraphs, clustersWithRemappedMultisetIndices,
      clustering.parameters, clustering.graph, clustering.error
    );
  }

  private List<ClusteringElement> updateClusterIdOfDuplicates(List<ClusteringElement> elements, List<ClusteringElement> finalElements) {
    return elements.stream()
      .map(el -> el.duplicateOf().isPresent() ?
        new ClusteringElement(el.id, finalElements.get(el.duplicateOf).clusterId, null, null, el.duplicateOf, el.distanceToCenter) :
        el
      )
      .collect(Collectors.toList());
  }

  private List<ClusteringElement> addDuplicatedElements(Clustering cl, String[] idsWithDuplicates) {
    List<ClusteringElement> elements = new ArrayList<>();
    int noDuplicateElementsIndex = 0;
    for (int i = 0; i < idsWithDuplicates.length; i++) {
      if (mapDuplicateIndexToIndexToKeep.containsKey(i)) {
        ClusteringElement duplicate = ClusteringElement.duplicate(idsWithDuplicates[i], mapDuplicateIndexToIndexToKeep.get(i));
        elements.add(duplicate);
      }
      else {
        elements.add(cl.elements.get(noDuplicateElementsIndex));
        noDuplicateElementsIndex++;
      }
    }
    return elements;
  }

  private List<Cluster> remapClustersMultisetIndices(Clustering clustering, int elementsCount) {
    if (clustering.clusters == null) {
      return null;
    }
    Map<Integer, Integer> mapNoDuplicatesToWithDuplicates =
      mapNoDuplicatesToWithDuplicates(mapDuplicateIndexToIndexToKeep.keySet(), elementsCount);
    return clustering.clusters.stream()
      .map(cl -> remapClusterMultisetIndices(cl, mapNoDuplicatesToWithDuplicates))
      .collect(Collectors.toList());
  }

  private Cluster remapClusterMultisetIndices(Cluster cluster, Map<Integer, Integer> mapNoDuplicatesToWithDuplicates) {
    if (cluster.multisetElementsIndices == null) {
      return new Cluster(cluster.elementsIndices);
    }
    List<Integer> multisetElementsIndices = new ArrayList<>();
    cluster.multisetElementsIndices.forEach(index -> multisetElementsIndices.add(
      mapNoDuplicatesToWithDuplicates.get(index)
    ));
    return new Cluster(cluster.elementsIndices, multisetElementsIndices);
  }

  static Map<Integer, Integer> mapNoDuplicatesToWithDuplicates(
    Collection<Integer> duplicatesIndices, int elementsCount)
  {
    Map<Integer, Integer> map = new HashMap<>();

    int duplicatesCount = 0;
    for (int i = 0; i < elementsCount; i++) {
      if (duplicatesIndices.contains(i)) {
        duplicatesCount++;
      }
      else {
        map.put(i - duplicatesCount, i);
      }
    }
    return map;
  }
}
