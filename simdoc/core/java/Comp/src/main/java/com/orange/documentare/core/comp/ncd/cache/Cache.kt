package com.orange.documentare.core.comp.ncd.cache

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap
import java.util.concurrent.ConcurrentMap


data class Cache<T, R>(val maxSize: Long) {

  private val  cache: ConcurrentMap<T, R> = ConcurrentLinkedHashMap.Builder<T, R>()
    .maximumWeightedCapacity(maxSize)
    .build()

  fun getIfPresent(key: T) = cache[key]
  fun put(key: T, entry: R) = cache.put(key, entry)
  fun invalidateAll() {
    cache.clear()
  }
  fun size() = cache.size
}