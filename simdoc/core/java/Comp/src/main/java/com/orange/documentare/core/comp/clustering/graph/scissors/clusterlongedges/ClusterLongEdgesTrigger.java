package com.orange.documentare.core.comp.clustering.graph.scissors.clusterlongedges;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.scissors.ScissorTrigger;
import com.orange.documentare.core.model.ref.comp.IntStatistics;
import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphGroup;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
class ClusterLongEdgesTrigger implements ScissorTrigger {
  private final List<GraphItem> graphItems;
  private final float thresholdPercentile;

  @Getter(AccessLevel.PACKAGE)
  private int edgeLengthThreshold;

  @Override
  public void initForGroup(GraphGroup group) {
    initCutThreshold(group);
  }

  @Override
  public boolean shouldRemove(GraphEdge edge) {
    return edge.length > edgeLengthThreshold;
  }

  private void initCutThreshold(GraphGroup group) {
    DescriptiveStats stats = new DescriptiveStats(thresholdPercentile);
    List<Integer> edgesLength = group.edges.stream().map(edge -> edge.length).collect(Collectors.toList());
    IntStatistics statistics = stats.statistics(edgesLength);
    edgeLengthThreshold = (int) statistics.getPercentile();
  }
}
