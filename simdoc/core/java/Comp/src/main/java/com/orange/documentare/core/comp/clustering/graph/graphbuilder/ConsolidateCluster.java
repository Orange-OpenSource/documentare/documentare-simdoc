package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the
 */

import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphCluster;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.SubGraph;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

class ConsolidateCluster {

  static void mergeMultiClusters(ClusteringGraph clusteringGraph) {
    Collection<GraphCluster> clusters = clusteringGraph.getClusters().values();
    GraphCluster biggerCluster = biggerCluster(clusters);
    int biggerClusterId = biggerCluster.getGroupId();

    updateItemsWith1ClusterCenter1Subgraph1Cluster(clusteringGraph, biggerClusterId);
    SubGraph subgraph = mergeSubgraphs(clusteringGraph);
    GraphCluster cluster = rebuildSingleCluster(clusteringGraph, subgraph.edges);

    clusteringGraph.getSubGraphs().clear();
    clusteringGraph.getSubGraphs().put(0, subgraph);
    clusteringGraph.getClusters().clear();
    clusteringGraph.getClusters().put(0, cluster);
  }

  private static GraphCluster rebuildSingleCluster(ClusteringGraph clusteringGraph, List<GraphEdge> allEdges) {
    GraphCluster cluster = new GraphCluster(0, null);
    cluster.setGroupId(0);
    cluster.edges.addAll(allEdges);
    clusteringGraph.getClusters().values().forEach(cl -> {
      cluster.itemIndices.addAll(cl.itemIndices);
    });
    return cluster;
  }

  private static SubGraph mergeSubgraphs(ClusteringGraph clusteringGraph) {
    SubGraph subgraph = new SubGraph();
    subgraph.setGroupId(0);
    subgraph.clusterIndices.add(0);
    clusteringGraph.getSubGraphs().values().forEach(s -> {
      subgraph.itemIndices.addAll(s.itemIndices);
      subgraph.edges.addAll(s.edges);
    });
    return subgraph;
  }

  private static void updateItemsWith1ClusterCenter1Subgraph1Cluster(ClusteringGraph clusteringGraph, int biggerClusterId) {
    clusteringGraph.getItems().forEach(item -> {
      if (item.clusterCenter && item.clusterId != biggerClusterId) {
        item.clusterCenter = false;
      }
      item.subgraphId = 0;
      item.clusterId = 0;
    });
  }

  private static GraphCluster biggerCluster(Collection<GraphCluster> clusters) {
    return clusters.stream()
      .max(Comparator.comparingInt(cluster -> cluster.itemIndices.size())).get();
  }
}
