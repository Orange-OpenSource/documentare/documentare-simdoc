/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.compute

import com.orange.documentare.core.comp.distance.Distance
import com.orange.documentare.core.comp.distance.DistancesArray
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.model.ref.clustering.Cluster
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringElement


internal class DistancesToCentroid(private val bytesDataWithDuplicates: Array<BytesData>, val distance: Distance) {

  fun addOnClusterElements(distances: DistancesComputationResult, cl: Clustering): Clustering {
    if (cl.clusters.isEmpty()) {
      return cl
    }

    val mapElementsWithDistancesToCentroid = cl.clusters
      .map { cluster -> addDistancesToCentroidForCluster(cluster, distances, cl) }
      .reduce { map1, map2 -> map1.plus(map2) }

    val elementsWithDistancesToCentroid = cl.elements
      .map { element -> element.id }
      .map { id -> mapElementsWithDistancesToCentroid.getValue(id) }

    return Clustering(elementsWithDistancesToCentroid, cl.subgraphs, cl.clusters, cl.parameters, cl.graph, cl.error)
  }

  private fun addDistancesToCentroidForCluster(cluster: Cluster, distances: DistancesComputationResult, clustering: Clustering): Map<String, ClusteringElement> {
    return if (cluster.multisetElementsIndices == null || cluster.multisetElementsIndices.size < 2) {
      addDistancesToClusterCenter(cluster, distances, clustering.elements)
    } else {
      addDistancesToCentroid(cluster, clustering.elements)
    }
  }

  private fun addDistancesToClusterCenter(cluster: Cluster, distances: DistancesComputationResult, elements: List<ClusteringElement>): Map<String, ClusteringElement> {
    val map = HashMap<String, ClusteringElement>()
    cluster.elementsIndices.map { index -> elements[index] }.forEach { element ->
      if (element.clusterCenter()) {
        map[element.id] = element
      } else {
        val distanceToCenter = distanceToClusterCenter(cluster, elements, distances, element.id)
        map[element.id] = ClusteringElement(element.id, element.clusterId, element.clusterCenter, element.enrolled, element.duplicateOf, distanceToCenter)
      }
    }
    return map
  }

  private fun distanceToClusterCenter(cluster: Cluster, elements: List<ClusteringElement>, distances: DistancesComputationResult, elementId: String): Int? {
    val center = cluster.elementsIndices
      .filter { index -> elements[index].clusterCenter() }
      .map { index -> elements[index] }
      .first()
    return distances.of(elementId, center.id)
  }

  private fun addDistancesToCentroid(cluster: Cluster, elements: List<ClusteringElement>): Map<String, ClusteringElement> {
    val bytesDataToComputeDistancesToCentroid = retrieveCentroidBytesData(cluster, elements)
    val centroid = BytesData(cluster.multisetElementsIndices.map { elements[it].id }, mergeCentroidBytes(cluster, elements))

    val distancesArray = distance.computeDistancesBetweenCollections(bytesDataToComputeDistancesToCentroid, arrayOf(centroid))

    var i = 0
    val fromClusterIndexToNotInCentroidIndex = HashMap<Int, Int>()
    cluster.elementsIndices.forEach {
      if (!cluster.multisetElementsIndices.contains(it)) {
        fromClusterIndexToNotInCentroidIndex[it] = i++
      }
    }
    val map = HashMap<String, ClusteringElement>()
    addDistancesForElementsOutsideCentroid(cluster, elements, distancesArray, fromClusterIndexToNotInCentroidIndex, map)
    addDistancesForElementsInCentroid(cluster, elements, map)

    return map
  }

  private fun mergeCentroidBytes(cluster: Cluster, elements: List<ClusteringElement>): ByteArray? {
    return cluster.multisetElementsIndices
      .map { index -> lookUpBytesDataWithoutDuplicates(elements[index].id).loadBytes() }
      .reduce { arr1, arr2 -> arr1 + arr2 }
  }

  private fun addDistancesForElementsInCentroid(cluster: Cluster, elements: List<ClusteringElement>, map: HashMap<String, ClusteringElement>) {
    cluster.elementsIndices
      .filter { index -> cluster.multisetElementsIndices.contains(index) }
      .forEach { index ->
        val element = elements[index]
        map[element.id] = element
      }
  }

  private fun addDistancesForElementsOutsideCentroid(cluster: Cluster, elements: List<ClusteringElement>, distancesArray: DistancesArray, remapIndices: HashMap<Int, Int>, map: HashMap<String, ClusteringElement>) {
    cluster.elementsIndices
      .filterNot { index -> cluster.multisetElementsIndices.contains(index) }
      .forEach { index ->
        val element = elements[index]
        val distanceToCentroid = distancesArray.get(remapIndices[index]!!, 0)
        map[element.id] = ClusteringElement(element.id, element.clusterId, element.clusterCenter, element.enrolled, element.duplicateOf, distanceToCentroid)
      }
  }

  private fun retrieveCentroidBytesData(cluster: Cluster, elements: List<ClusteringElement>): Array<BytesData> {
    return cluster.elementsIndices
      .filterNot { index -> cluster.multisetElementsIndices.contains(index) }
      .map { index -> lookUpBytesDataWithoutDuplicates(elements[index].id) }
      .toTypedArray()
  }

  private fun lookUpBytesDataWithoutDuplicates(id: String) =
    bytesDataWithDuplicates.first { bytesData -> bytesData.ids[0] == id }
}