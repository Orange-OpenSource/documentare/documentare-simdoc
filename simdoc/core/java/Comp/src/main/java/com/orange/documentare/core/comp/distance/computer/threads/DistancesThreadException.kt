/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.distance.computer.threads

data class DistancesThreadException(
  val exceptionName: String,
  val exceptionMessage: String?,
  val causeName: String?,
  val causeMessage: String?,
  val msg: String
) : RuntimeException(msg) {

  companion object {

    fun build(rootException: Throwable) =
      DistancesThreadException(
        rootException.javaClass.simpleName,
        rootException.message,
        rootException.cause?.javaClass?.simpleName,
        rootException.cause?.message,
        toString(rootException)
      )

    fun toString(rootException: Throwable) =
      "[DistancesThreadException] ${rootException.javaClass.simpleName} - " +
        "${rootException.message} - " +
        "${rootException.cause?.javaClass?.simpleName} - " +
        "${rootException.cause?.message}"
  }
}
