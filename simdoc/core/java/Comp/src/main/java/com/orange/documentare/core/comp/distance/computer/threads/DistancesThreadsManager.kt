package com.orange.documentare.core.comp.distance.computer.threads

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.computer.DistancesComputer
import com.orange.documentare.core.comp.measure.ProgressListener
import com.orange.documentare.core.comp.measure.TreatmentStep
import com.orange.documentare.core.model.ref.comp.DistanceItem
import com.orange.documentare.core.system.measure.Progress
import org.slf4j.LoggerFactory
import java.lang.Runtime.getRuntime
import java.util.*
import java.util.concurrent.Executors.newFixedThreadPool
import java.util.concurrent.Future

class DistancesThreadsManager(private val computer: DistancesComputer) {
  private val futures = Collections.synchronizedList(ArrayList<Future<*>>())

  private var listener: ProgressListener? = null

  fun run() {
    log.info("START clustering with '${availableProcessors()}' cores")
    var itemIndicesToProcess: IntArray? = computer.itemsIndicesToProcess
    while (itemIndicesToProcess != null) {
      startForNextBasketElement(itemIndicesToProcess)
      itemIndicesToProcess = computer.itemsIndicesToProcess
    }
    waitTheEnd()
    listener?.onProgressUpdate(TreatmentStep.NCD, Progress(100, computer.computeTimeSinceStartInSec()))
    log.info("Clustering DONE")
  }

  fun setListener(listener: ProgressListener?) {
    this.listener = listener
  }

  private fun startForNextBasketElement(itemIndicesToProcess: IntArray) {
    val items = computer.getItemsToProcess(itemIndicesToProcess)
    startTask(itemIndicesToProcess, items)
  }

  private fun startTask(itemIndicesToProcess: IntArray, items: Array<DistanceItem>) {
    val future = executor.submit(DistancesThreadRunnable(itemIndicesToProcess, items, computer, listener)) as Future<*>
    futures.add(future)
  }

  private fun waitTheEnd() {
    while (futures.isNotEmpty()) {
      val future = futures[0]
      waitForFuture(future)
      futures.remove(future)
    }
  }

  private fun waitForFuture(future: Future<*>) {
    try {
      future.get()
    } catch (throwable: Throwable) {
      showFutureErrorAndRethrow(throwable)
    }
  }

  private fun showFutureErrorAndRethrow(throwable: Throwable) {
    log.error("An error occurred in a thread which was computing distances for the clustering; we stop clustering at once and raise the exception to our parent.\n\"Time traveling is just too dangerous. Better that I devote myself to study the other great mystery of the universe: women!\"", throwable)
    throw DistancesThreadException.build(throwable)
  }

  companion object {
    /* static since we want to reuse this thread pool until the end of the process */
    private val executor = newFixedThreadPool(availableProcessors())

    private val log = LoggerFactory.getLogger(DistancesThreadsManager::class.java)

    fun availableProcessors(): Int {
      return getRuntime().availableProcessors()
    }
  }
}
