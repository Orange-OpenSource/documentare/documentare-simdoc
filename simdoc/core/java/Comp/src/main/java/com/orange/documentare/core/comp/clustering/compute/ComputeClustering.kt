package com.orange.documentare.core.comp.clustering.compute

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder
import com.orange.documentare.core.comp.distance.Distance
import com.orange.documentare.core.comp.distance.DistanceParameters
import com.orange.documentare.core.comp.distance.DistancesArray
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.comp.TriangleVertices

class ComputeClustering(distanceParameters: DistanceParameters) {

  val distance = Distance(distanceParameters)

  fun compute(bytesData: Array<BytesData>, parameters: ClusteringParameters): Clustering {
    val distances = computeDistances(bytesData)

    var cl = computeGraphClustering(distances.withoutDuplicates, parameters)
    cl = DistancesToCentroid(bytesData, distance).addOnClusterElements(distances.withoutDuplicates, cl)

    return if (distances.hasDuplication) distances.addDuplications(cl) else cl
  }

  private fun computeGraphClustering(distancesWithoutDuplicates: DistancesComputationResult, parameters: ClusteringParameters): Clustering {
    val simClusteringItems = initClusteringItems(distancesWithoutDuplicates, parameters)
    return ClusteringGraphBuilder().doClustering(simClusteringItems, parameters)
  }

  private fun computeDistances(bytesDataArray: Array<BytesData>): DistancesComputationResult {
    val distanceArray = distance.computeDistancesInCollection(bytesDataArray)
    val ids = bytesDataArray.map { bytesData -> bytesData.ids[0] }
    return DistancesComputationResult(ids, distanceArray)
  }

  private fun initClusteringItems(distances: DistancesComputationResult, parameters: ClusteringParameters): Array<SimClusteringItem> {
    val simClusteringItems = distances.ids.map { SimClusteringItem(it) }.toTypedArray()

    val k = if (parameters.knn()) parameters.knnThreshold else distances.ids.size
    buildTriangulationVertices(simClusteringItems, distances.distancesArray, k)

    return simClusteringItems
  }

  /** Memory in place creation, it is optimal since we do not allocate nearest arrays  */
  private fun buildTriangulationVertices(simClusteringItems: Array<SimClusteringItem>, distancesArray: DistancesArray, k: Int) {
    if (simClusteringItems.size < 3) {
      return
    }

    val itemsList = simClusteringItems.asList()
    for (i in simClusteringItems.indices) {
      val vertex2 = distancesArray.nearestItemOf(i)
      val vertex3 = distancesArray.nearestItemOfBut(vertex2.index, i)
      val vertex1Nearest = distancesArray.nearestItemsFor(itemsList, i)

      val item = simClusteringItems[i]

      // FIXME we keep nearest items for singletons experimentation on Jo's side
      item.nearestItems = vertex1Nearest
      item.triangleVertices = TriangleVertices(vertex1Nearest, vertex3, k)
    }
  }
}
