package com.orange.documentare.core.comp.ncd

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.bwt.Bwt
import com.orange.documentare.core.comp.ncd.cache.NcdCache
import com.orange.documentare.core.comp.ncd.cache.NcdCacheKeys
import com.orange.documentare.core.comp.rle.RunLength
import org.slf4j.LoggerFactory
import kotlin.math.max
import kotlin.math.min

open class Ncd(val ncdParameters: NcdParameters) {

  private val compressedLengthMethod = RunLength()

  val bwt = Bwt(ncdParameters.bwtParameters)

  fun computeNcd(vanillaX: ByteArray, vanillaY: ByteArray): Float {
    val keys = NcdCacheKeys(vanillaX, vanillaY)
    var ncd: Float? = NcdCache.getNcd(keys)
    if (ncd == null) {
      val t0 = System.currentTimeMillis()
      ncd = doComputeNcd(vanillaX, vanillaY, keys)
      val computationDuration = System.currentTimeMillis() - t0
      NcdCache.addNcd(keys, ncd, computationDuration.toInt(), ncdParameters)
    }
    if (keys.equalArrays && ncd != 0f) {
      log.warn("arrays md5 keys are equal (${keys.bytes1Key}), but ncd is $ncd")
    }
    return ncd
  }

  open fun doComputeNcd(vanillaX: ByteArray, vanillaY: ByteArray, keys: NcdCacheKeys): Float {
    val taggedX = buildTaggedArray(vanillaX)
    val taggedY = buildTaggedArray(vanillaY)

    val xCompressedLength = computeCompressedLengthOf(taggedX, keys.bytes1Key)
    val yCompressedLength = computeCompressedLengthOf(taggedY, keys.bytes2Key)

    // 'x == y' optimization is done outside when we do not want to check the compression method symmetry
    val xy = mergeXY(taggedX, taggedY)
    // no cache optimization for xy since outside optimization should avoid computing both ncd(x, y) and ncd(y, x)
    val xyCompressedLength = doComputeCompressedLengthOf(xy)

    return computeNcd(xCompressedLength, yCompressedLength, xyCompressedLength)
  }

  private fun buildTaggedArray(vanilla: ByteArray): ByteArray {
    val taggedVanilla = ByteArray(vanilla.size + SIMDOC_MAGIC_NUMBER.size)
    System.arraycopy(SIMDOC_MAGIC_NUMBER, 0, taggedVanilla, 0, SIMDOC_MAGIC_NUMBER.size)
    System.arraycopy(vanilla, 0, taggedVanilla, SIMDOC_MAGIC_NUMBER.size, vanilla.size)
    return taggedVanilla
  }

  private fun mergeXY(x: ByteArray, y: ByteArray): ByteArray {
    val xLen = x.size
    val yLen = y.size
    val xy = ByteArray(xLen + yLen)
    System.arraycopy(x, 0, xy, 0, xLen)
    try {
      System.arraycopy(y, 0, xy, xLen, yLen)
    } catch (e: ArrayIndexOutOfBoundsException) {
      log.error(String.format("Ncd merge xy, arrays length error: %d %d", xLen, yLen))
    }

    return xy
  }

  private fun computeCompressedLengthOf(tagged: ByteArray, cacheKey: String): Int {
    var compressedLength: Int? = NcdCache.getCompression(cacheKey)?.toInt()
    if (compressedLength == null) {
      val t0 = System.currentTimeMillis()
      compressedLength = doComputeCompressedLengthOf(tagged)
      val computationDuration = System.currentTimeMillis() - t0
      NcdCache.addCompression(cacheKey, compressedLength.toFloat(), computationDuration.toInt())
    }
    return compressedLength
  }

  private fun doComputeCompressedLengthOf(bytes: ByteArray): Int {
    val bwtResult = bwt.getBwt(bytes)
    return compressedLengthMethod.computeCompressedLengthOf(bwtResult)
  }

  private fun computeNcd(xLen: Int, yLen: Int, xyLen: Int): Float {
    return (xyLen - min(xLen, yLen)).toFloat() / max(xLen, yLen)
  }

  companion object {
    private val SIMDOC_MAGIC_NUMBER = "JoTOphe".toByteArray()

    private val log = LoggerFactory.getLogger(Ncd::class.java)
  }
}
