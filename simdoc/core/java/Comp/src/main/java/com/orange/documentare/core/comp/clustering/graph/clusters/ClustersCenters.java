package com.orange.documentare.core.comp.clustering.graph.clusters;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.graph.GraphCluster;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import lombok.RequiredArgsConstructor;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ClustersCenters {

  public static List<Integer> findCenters(List<GraphItem> items, Collection<GraphCluster> clusters) {
    return clusters.stream()
      .map(cluster -> findCenter(items, cluster))
      .collect(Collectors.toList());
  }

  private static int findCenter(List<GraphItem> items, GraphCluster cluster) {
    return cluster.itemIndices.size() == 1 ?
      cluster.itemIndices.get(0) :
      updateCenterNonSingleton(items, cluster);
  }

  private static int updateCenterNonSingleton(List<GraphItem> items, GraphCluster cluster) {
    int maxEdgesCount = -1;
    int centerIndex = 0;
    Map<Integer, Integer> edgesCount = getEdgesCountFor(cluster);
    for (int index : cluster.itemIndices) {
      GraphItem item = items.get(index);
      item.clusterCenter = false;
      int count = edgesCount.get(item.vertex1Index);
      if (count > maxEdgesCount) {
        maxEdgesCount = count;
        centerIndex = index;
      }
    }
    return centerIndex;
  }

  private static Map<Integer, Integer> getEdgesCountFor(GraphCluster cluster) {
    Map<Integer, Integer> edgesCount = new HashMap<>();
    for (GraphEdge edge : cluster.edges) {
      updateEdgesCountWith(edgesCount, edge);
    }
    return edgesCount;
  }

  private static void updateEdgesCountWith(Map<Integer, Integer> edgesCount, GraphEdge edge) {
    int index1 = edge.vertex1Index;
    int index2 = edge.vertex2Index;
    Integer count1 = edgesCount.get(index1);
    Integer count2 = edgesCount.get(index2);
    count1 = count1 == null ? 1 : count1 + 1;
    count2 = count2 == null ? 1 : count2 + 1;
    edgesCount.put(index1, count1);
    edgesCount.put(index2, count2);
  }
}
