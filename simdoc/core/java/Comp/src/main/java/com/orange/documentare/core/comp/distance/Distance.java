package com.orange.documentare.core.comp.distance;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.bwt.BwtParameters;
import com.orange.documentare.core.comp.distance.computer.DistancesComputer;
import com.orange.documentare.core.comp.measure.ProgressListener;
import com.orange.documentare.core.comp.ncd.Ncd;
import com.orange.documentare.core.comp.ncd.NcdParameters;
import com.orange.documentare.core.model.ref.comp.DistanceItem;

public class Distance {

  public static final int DISTANCE_INT_CONV_FACTOR = 1000000;

  public final DistanceParameters distanceParameters;

  final Ncd ncd;

  private final ProgressListener progressListener;

  public Distance(DistanceParameters distanceParameters) {
    this(distanceParameters, null);
  }

  public Distance(DistanceParameters distanceParameters, ProgressListener progressListener) {
    this.distanceParameters = distanceParameters;
    this.progressListener = progressListener;
    this.ncd = new Ncd(new NcdParameters(new BwtParameters(distanceParameters.getSuffixArrayAlgorithm())));
  }

  public DistancesArray computeDistancesBetweenCollections(DistanceItem[] items1, DistanceItem[] items2) {
    DistancesComputer computer = new DistancesComputer(items1, items2, this);
    computer.setProgressListener(progressListener);
    computer.compute();
    return computer.distancesArray;
  }

  public DistancesArray computeDistancesInCollection(DistanceItem[] items) {
    return computeDistancesBetweenCollections(items, items);
  }

  public int[] compute(DistanceItem item, DistanceItem[] items) {
    return compute(item, items, 0);
  }

  public int[] compute(DistanceItem item, DistanceItem[] items, int startIndex) {
    int[] array = new int[items.length - startIndex];
    for (int i = startIndex; i < items.length; i++) {
      DistanceItem arrayItem = items[i];
      array[i - startIndex] = item.equals(arrayItem) ? 0 : compute(item, arrayItem);
    }
    return array;
  }

  public int compute(DistanceItem item1, DistanceItem item2) {
    float distance = ncd.computeNcd(item1.loadBytes(), item2.loadBytes());
    return (int) (distance * DISTANCE_INT_CONV_FACTOR);
  }
}
