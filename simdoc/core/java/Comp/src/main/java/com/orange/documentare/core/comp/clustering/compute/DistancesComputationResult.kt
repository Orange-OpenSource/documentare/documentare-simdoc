package com.orange.documentare.core.comp.clustering.compute

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.DistancesArray
import com.orange.documentare.core.comp.distance.Duplicates
import com.orange.documentare.core.model.ref.clustering.Clustering

internal class DistancesComputationResult(val ids: List<String>, val distancesArray: DistancesArray) {
  val duplicates: Duplicates = distancesArray.duplicates()
  val hasDuplication: Boolean = duplicates.count() != 0
  val withoutDuplicates: DistancesComputationResult = withoutDuplicates()

  fun addDuplications(cl: Clustering): Clustering {
    return duplicates.addDuplicatesToClustering(cl, ids.toTypedArray())
  }

  fun of(fileId: String, centerId: String): Int? {
    val fileIndice = indexOf(ids, fileId)
    val centerIndice = indexOf(ids, centerId)
    val distance = distancesArray.get(fileIndice, centerIndice)
    return if (distance == 0) null else distance
  }

  private fun withoutDuplicates(): DistancesComputationResult {
    if (duplicates.count() == 0) {
      return this
    }
    val distancesArrayWithoutDuplicates = distancesArray.removeDuplicates()
    val ids = this.ids
    val idsWithoutDuplicates = duplicates.indicesWithoutDuplicates(ids.size).map { ids[it] }
    return DistancesComputationResult(idsWithoutDuplicates, distancesArrayWithoutDuplicates)
  }

  private fun indexOf(ids: List<String>, fileId: String): Int {
    for (i in ids.indices) {
      if (ids[i] == fileId) {
        return i
      }
    }
    throw IllegalStateException("file id not found: $fileId")
  }
}
