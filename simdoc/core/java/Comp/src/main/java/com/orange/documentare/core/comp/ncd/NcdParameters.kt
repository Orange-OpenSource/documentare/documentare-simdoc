package com.orange.documentare.core.comp.ncd

import com.orange.documentare.core.comp.bwt.BwtParameters

data class NcdParameters(val bwtParameters: BwtParameters) {
}