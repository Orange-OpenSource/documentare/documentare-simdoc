/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.distance.bytesdistances;


import com.orange.documentare.core.model.ref.comp.DistanceItem;
import com.orange.documentare.core.system.Bytes;
import lombok.EqualsAndHashCode;
import org.apache.commons.io.FileUtils;

import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbTransient;
import java.io.File;
import java.io.IOException;
import java.util.*;

@EqualsAndHashCode
public final class BytesData implements DistanceItem {

  public interface FileIdProvider {
    String idFor(File file);
  }

  public final List<String> ids;
  @JsonbProperty(nillable = true)
  public final List<String> filepaths;
  @JsonbProperty(nillable = true)
  public final byte[] bytes;

  @JsonbTransient
  @Override
  public String getHumanReadableId() {
    return ids.toString();
  }

  @Override
  // in file mode, bytes should always be null and we should rely on the file cache to retrieve dat
  // => indeed, we rely on the cache to free file data under memory pressure
  public byte[] loadBytes() {
    return bytes != null ? bytes : loadBytesFromFile(filepaths);
  }

  public BytesData(List<String> ids, List<String> filepaths) {
    this(ids, filepaths, null);
    for (String filepath : filepaths) {
      if (!(new File(filepath)).isFile()) {
        throw new IllegalStateException("File is not a file: " + filepath);
      }
    }
  }

  public BytesData(List<String> ids, byte[] bytes) {
    this(ids, null, bytes);
  }

  public BytesData() {
    this(null, null, null);
  }

  public static BytesData[] withBytes(BytesData[] bytesData) {
    return Arrays.stream(bytesData)
      .map(BytesData::withBytes)
      .toArray(BytesData[]::new);
  }

  public static BytesData withBytes(BytesData b) {
    return new BytesData(b.ids, b.filepaths, loadBytesFromFile(b.filepaths));
  }

  public static BytesData[] loadFromDirectory(File directory) {
    return loadFromDirectory(directory, null);
  }

  public static BytesData[] loadFromDirectory(File directory, FileIdProvider fileIdProvider) {
    if (!directory.isDirectory()) {
      throw new IllegalStateException(String.format("Failed to load data from invalid directory '%s': not a directory", directory.getAbsolutePath()));
    }

    FileIdProvider idProvider = fileIdProvider == null ? File::getAbsolutePath : fileIdProvider;
    Collection<File> files = FileUtils.listFiles(directory, null, true);
    return files.stream()
      .filter(file -> !file.isHidden())
      .sorted() // For the sake of tests: it is mandatory to keep same order across different test platform...
      .map(file -> new BytesData(new ArrayList<>(Collections.singletonList(idProvider.idFor(file))), new ArrayList<String>(Collections.singletonList(file.getAbsolutePath())), null))
      .toArray(BytesData[]::new);
  }

  public static FileIdProvider relativePathIdProvider(File rootDirectory) {
    return file -> {
      String filepath = file.getAbsolutePath();
      String relativeFileName = filepath.replace(rootDirectory.getAbsolutePath(), "");
      if (relativeFileName.startsWith(File.separator)) {
        relativeFileName = relativeFileName.substring(1);
      }
      return relativeFileName;
    };
  }

  @JsonbCreator
  public BytesData(
    @JsonbProperty("ids") List<String> ids,
    @JsonbProperty("filepaths") List<String> filepaths,
    @JsonbProperty("bytes") byte[] bytes) {
    this.ids = ids;
    this.filepaths = filepaths;
    this.bytes = bytes;
  }

  private static byte[] loadBytesFromFile(List<String> filepaths) {
    byte[] bytes = new byte[]{};
    for (String filepath : filepaths) {
      File file = new File(filepath);
      if (!file.isFile()) {
        throw new IllegalStateException("Not a file: " + file.getAbsolutePath());
      }
      try {
        bytes = Bytes.concat(bytes, FileUtils.readFileToByteArray(file));
      } catch (IOException e) {
        throw new IllegalStateException("Not a file: " + file.getAbsolutePath());
      }
    }
    return bytes;
  }
}
