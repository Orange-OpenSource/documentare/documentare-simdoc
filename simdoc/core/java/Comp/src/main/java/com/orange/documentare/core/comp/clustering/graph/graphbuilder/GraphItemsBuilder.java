package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.geometry.Equilaterality;
import com.orange.documentare.core.comp.clustering.geometry.Heron;
import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.TriangleVertices;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
class GraphItemsBuilder {
  private final ClusteringItem[] items;
  private final int knnThreshold;

  List<GraphItem> initGraphItems() {
    List<GraphItem> graphItems = new ArrayList<>();
    for (int i = 0; i < items.length; i++) {
      GraphItem graphItem = buildGraphItemFor(i);
      graphItems.add(graphItem);
    }
    updateVerticesIndex(graphItems);
    return graphItems;
  }

  private GraphItem buildGraphItemFor(int itemIndex) {
    ClusteringItem item = items[itemIndex];
    TriangleVertices triangleVertices = item.triangleVerticesAvailable() ?
      item.getTriangleVertices() :
      new TriangleVertices(item, items, knnThreshold);

    boolean knnSingleton = triangleVertices.getKnnSingleton();
    GraphItem graphItem = buildGraphItem(itemIndex, knnSingleton);
    if (!knnSingleton) {
      addTriangleInfo(graphItem, triangleVertices);
    }
    return graphItem;
  }

  private GraphItem buildGraphItem(int itemIndex, boolean knnSingleton) {
    GraphItem graphItem = new GraphItem();
    ClusteringItem clusteringItem = items[itemIndex];
    if (knnSingleton) {
      graphItem.knnSingleton = true;
    }
    graphItem.vertexName = clusteringItem.getHumanReadableId();
    graphItem.vertex1 = clusteringItem;
    graphItem.vertex1Index = itemIndex;
    return graphItem;
  }

  private void addTriangleInfo(GraphItem graphItem, TriangleVertices triangleVertices) {
    if (items.length == 1) {
      return;
    }
    graphItem.vertex2 = getClusteringItemFrom(triangleVertices.getVertex2());

    if (items.length == 2) {
      int edge12 = triangleVertices.getEdge12() + 1;
      graphItem.edgesLength = new int[]{edge12};
      return;
    }

    graphItem.vertex3 = getClusteringItemFrom(triangleVertices.getVertex3());

    // +1 to avoid null distance, which would disturb area and Q computation
    int edge12 = triangleVertices.getEdge12() + 1;
    int edge23 = triangleVertices.getEdge23() + 1;
    int edge31 = triangleVertices.getEdge13() + 1;
    graphItem.edgesLength = new int[]{edge12, edge23, edge31};

    float area = Heron.get(edge12, edge23, edge31);
    graphItem.area = area;
    graphItem.q = Equilaterality.get(area, edge12, edge23, edge31);
  }

  private ClusteringItem getClusteringItemFrom(NearestItem nearestItem) {
    for (int i = 0; i < items.length; i++) {
      if (i == nearestItem.getIndex()) {
        return items[i];
      }
    }
    throw new IllegalStateException("Failed to find clustering item");
  }

  private void updateVerticesIndex(List<GraphItem> graphItems) {
    List<ClusteringItem> itemsList = Arrays.asList(items);
    for (GraphItem graphItem : graphItems) {
      graphItem.vertex2Index = itemsList.indexOf(graphItem.vertex2);
      graphItem.vertex3Index = itemsList.indexOf(graphItem.vertex3);
    }
  }
}
