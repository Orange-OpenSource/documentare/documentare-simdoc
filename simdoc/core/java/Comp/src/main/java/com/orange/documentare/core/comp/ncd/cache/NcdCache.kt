package com.orange.documentare.core.comp.ncd.cache

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.clustering.stats.SummaryStats
import com.orange.documentare.core.comp.ncd.NcdParameters
import com.orange.documentare.core.postgresql.cache.PostgresCache
import com.orange.documentare.core.postgresql.cache.PostgresCacheStats

object NcdCache {
  private const val CACHE_MAX_SIZE: Long = 2000000

  private val cache = Cache<String, CacheEntry>(CACHE_MAX_SIZE)

  private val ncdStats: SummaryStats = SummaryStats()
  private val compressionStats: SummaryStats = SummaryStats()

  private var ncdHit = 0
  private var ncdMiss = 0
  private var compHit = 0
  private var compMiss = 0

  private var saAlgoSAIS = 0
  private var saAlgoLIBDIVSUFSORT = 0

  fun initCachePersistence() {
    initCachePersistence("localhost", PostgresCache.DEFAULT_PORT, "postgres", "sandbox")
  }

  fun initCachePersistence(dbHost: String = PostgresCache.DEFAULT_HOSTNAME, dbPort: Int = PostgresCache.DEFAULT_PORT, username: String = "postgres", password: String = "sandbox") {
    PostgresCache.setup(dbHost, dbPort, username, password, {})
  }

  fun getNcd(ncdCacheKeys: NcdCacheKeys): Float? {
    var ncd = cache.getIfPresent(ncdCacheKeys.byteArraysKey)?.value
    if (ncd == null) {
      ncdMiss++
      ncd = PostgresCache.get(ncdCacheKeys.byteArraysKey)
      ncd?.let { cache.put(ncdCacheKeys.byteArraysKey, CacheEntry(it, 0)) }
    } else {
      ncdHit++
    }
    return ncd
  }

  fun addNcd(ncdCacheKeys: NcdCacheKeys, result: Float, computationDuration: Int, ncdParameters: NcdParameters) {
    updateNcdParametersStats(ncdParameters)
    ncdStats.addValue(computationDuration.toDouble())
    addToCache(ncdCacheKeys.byteArraysKey, result, computationDuration)
  }

  private fun updateNcdParametersStats(ncdParameters: NcdParameters) {
    when (ncdParameters.bwtParameters.algorithm) {
      SuffixArrayAlgorithm.SAIS -> saAlgoSAIS++
      SuffixArrayAlgorithm.LIBDIVSUFSORT -> saAlgoLIBDIVSUFSORT++
    }
  }

  fun getCompression(bytesKey: String): Float? {
    var comp = cache.getIfPresent(bytesKey)?.value
    if (comp == null) {
      compMiss++
      comp = PostgresCache.get(bytesKey)
      comp?.let { cache.put(bytesKey, CacheEntry(comp, 0)) }
    } else {
      compHit++
    }
    return comp
  }

  fun addCompression(bytesKey: String, result: Float, computationDuration: Int) {
    compressionStats.addValue(computationDuration.toDouble())
    addToCache(bytesKey, result, computationDuration)
  }

  fun ncdHit(): Int = ncdHit
  fun ncdMiss(): Int = ncdMiss
  fun compressionHit(): Int = compHit
  fun compressionMiss(): Int = compMiss

  fun getComputationDuration(key: String): Int? {
    return cache.getIfPresent(key)?.computationDuration
  }

  fun stats(): String =
    "" +
      "ncdHit ($ncdHit) / ncdMiss ($ncdMiss)\n" +
      "compHit($compHit) / compMiss($compMiss)\n\n" +
      "ncd computation duration statsDurationMillis:\n $ncdStats\n\n" +
      "comp computation duration statsDurationMillis:\n $compressionStats\n\n" +
      "SA algo, sais=$saAlgoSAIS / libdivsufsort=$saAlgoLIBDIVSUFSORT\n\n" +
      PostgresCacheStats.stats()

  private fun clearStats() {
    PostgresCacheStats.clearStats()
    ncdStats.clear()
    compressionStats.clear()
    ncdHit = 0
    ncdMiss = 0
    compHit = 0
    compMiss = 0
    saAlgoSAIS = 0
    saAlgoLIBDIVSUFSORT = 0
  }

  fun clear() {
    cache.invalidateAll()
    clearStats()
  }

  private fun addToCache(key: String, result: Float, computationDuration: Int) {
    cache.put(key, CacheEntry(result, computationDuration))
    PostgresCache.add(key, result)
  }
}

