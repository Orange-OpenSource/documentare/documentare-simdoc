package com.orange.documentare.core.comp.distance;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.comp.DistanceItem;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class DistanceArrayNearestItemOfTest {


  @Test
  public void find_the_nearest_item_of_one_item() {
    // given
    DistancesArray distancesArray = new DistancesArray(1, 5, false);
    distancesArray.setDistancesForItem(0, new int[]{0, 40, 60, 20, 90});
    // when
    NearestItem nearestItem = distancesArray.nearestItemOf(0);

    // then
    assertThat(nearestItem.getIndex()).isEqualTo(3);
    assertThat(nearestItem.getDistance()).isEqualTo(20);
  }

  @Test
  public void find_the_nearest_item_of_one_item_with_exclusion() {
    // given
    DistancesArray distancesArray = new DistancesArray(1, 5, false);
    distancesArray.setDistancesForItem(0, new int[]{0, 40, 60, 20, 90});
    // when
    NearestItem nearestItem = distancesArray.nearestItemOfBut(0, 3);

    // then
    assertThat(nearestItem.getIndex()).isEqualTo(1);
    assertThat(nearestItem.getDistance()).isEqualTo(40);
  }

  @Test
  public void find_the_nearest_items() {
    // given
    List<DistanceItem> distanceItems = Arrays.asList(new Item[]{new Item("0"), new Item("1"), new Item("2"), new Item("3"), new Item("4")});
    DistancesArray distancesArray = new DistancesArray(1, 5, false);
    distancesArray.setDistancesForItem(0, new int[]{0, 0, 90, 20, 10});

    // when
    NearestItem[] nearests = distancesArray.nearestItemsFor(distanceItems, 0);

    // then
    assertThat(nearests).hasSize(5);
    assertThat(nearests[0].getIndex()).isEqualTo(0);
    assertThat(nearests[1].getIndex()).isEqualTo(1);
    assertThat(nearests[2].getIndex()).isEqualTo(4);
    assertThat(nearests[3].getIndex()).isEqualTo(3);
    assertThat(nearests[4].getIndex()).isEqualTo(2);
  }

  @Getter
  @RequiredArgsConstructor
  class Item implements DistanceItem {
    private final String humanReadableId;
    private final byte[] bytes = null;

    @Override
    public byte[] loadBytes() {
      return bytes;
    }
  }
}
