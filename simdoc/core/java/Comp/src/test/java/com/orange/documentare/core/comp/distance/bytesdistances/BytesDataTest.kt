/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.distance.bytesdistances

import com.orange.documentare.core.model.json.JsonGenericHandler
import org.apache.commons.io.FileUtils
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*

class BytesDataTest {

  private val jsonGenericHandler = JsonGenericHandler(true)

  @AfterEach
  fun cleanUp() {
    FileUtils.deleteQuietly(File(FILE_PATH))
    FileUtils.deleteQuietly(File(JSON_FILE_PATH))
    FileUtils.deleteQuietly(File(BYTES_DATA_JSON))
  }

  @Test
  fun write_file_data_as_json_and_reload_it_from_json() {
    // given
    FileUtils.writeStringToFile(File(FILE_PATH), FILE_CONTENT, "utf-8")
    val id1 = ArrayList(listOf("id1"))
    val filePath = ArrayList(listOf(FILE_PATH))
    var bytesDataFile = BytesData(id1, filePath)

    // when
    jsonGenericHandler.writeObjectToJsonFileNewApi(bytesDataFile, File(JSON_FILE_PATH))
    bytesDataFile = jsonGenericHandler.getObjectFromJsonFileNewApi(BytesData::class.java, File(JSON_FILE_PATH)) as BytesData

    // then
    assertThat(bytesDataFile.ids).isEqualTo(id1)
    assertThat(bytesDataFile.filepaths).isEqualTo(filePath)
    assertThat(bytesDataFile.bytes).isNull()
  }

  @Test
  fun write_bytes_data_as_json_and_reload_it_from_json() {
    // given
    val bytes = ByteArray(1)
    bytes[0] = 12
    val id2 = ArrayList(listOf("id2"))
    val bytesDataByte = BytesData(id2, bytes)

    // when
    jsonGenericHandler.writeObjectToJsonFileNewApi(bytesDataByte, File(BYTES_DATA_JSON))
    val actualBytesDataByte = jsonGenericHandler.getObjectFromJsonFileNewApi(BytesData::class.java, File(BYTES_DATA_JSON)) as BytesData

    // then
    assertThat(bytesDataByte.ids).isEqualTo(id2)
    assertThat(actualBytesDataByte.bytes).isEqualTo(bytesDataByte.bytes)
  }

  @Test
  fun write_file_data_as_json_and_reload_it_from_json_with_new_api() {
    // given
    FileUtils.writeStringToFile(File(FILE_PATH), FILE_CONTENT, "utf-8")
    val id1 = ArrayList(listOf("id1"))
    val filePath = ArrayList(listOf(FILE_PATH))
    var bytesDataFile = BytesData(id1, filePath)

    // when
    jsonGenericHandler.writeObjectToJsonFileNewApi(bytesDataFile, File(JSON_FILE_PATH))
    bytesDataFile = jsonGenericHandler.getObjectFromJsonFileNewApi(BytesData::class.java, File(JSON_FILE_PATH)) as BytesData

    // then
    assertThat(bytesDataFile.ids).isEqualTo(id1)
    assertThat(bytesDataFile.filepaths).isEqualTo(filePath)
    assertThat(bytesDataFile.bytes).isNull()
  }

  @Test
  fun write_bytes_data_as_json_and_reload_it_from_json_with_new_api() {
    // given
    val bytes = ByteArray(1)
    bytes[0] = 12
    val id2 = ArrayList(listOf("id2"))
    val bytesDataByte = BytesData(id2, bytes)

    // when
    jsonGenericHandler.writeObjectToJsonFileNewApi(bytesDataByte, File(BYTES_DATA_JSON))
    val actualBytesDataByte = jsonGenericHandler.getObjectFromJsonFileNewApi(BytesData::class.java, File(BYTES_DATA_JSON)) as BytesData

    // then
    assertThat(bytesDataByte.ids).isEqualTo(id2)
    assertThat(actualBytesDataByte.bytes).isEqualTo(bytesDataByte.bytes)
  }


  @Test
  fun exception_is_raised_when_file_can_not_be_read() {
    // given
    val id1 = ArrayList(listOf("id3"))
    // when
    val tryRes = runCatching { BytesData(id1, ArrayList(listOf("/pouet-pouet"))) }

    // then
    assertThat(tryRes.isFailure).isTrue()
  }

  @Test
  fun build_data_array_from_directory_content() {
    // Given
    val directory = File(javaClass.getResource(INPUT_DIR).file)

    // When
    val bytesDataArray = BytesData.loadFromDirectory(directory)

    // Then
    assertThat(bytesDataArray).hasSize(2)
    // check bytes are null, since we are doing lazy loading through a cache
    assertThat(bytesDataArray[0].bytes).isNull()
    assertThat(bytesDataArray[1].bytes).isNull()
  }

  @Test
  fun build_data_array_from_directory_content_with_id_provider() {
    // Given
    val directory = File(javaClass.getResource(INPUT_DIR).file)
    val fileIdProvider = { file: File -> file.name + "@" }

    // When
    val bytesDataArray = BytesData.loadFromDirectory(directory, fileIdProvider)

    // Then
    assertThat(bytesDataArray).hasSize(2)
    assertThat(bytesDataArray).contains(BytesData(ArrayList(listOf("human@")), ArrayList(listOf(File(javaClass.getResource("$INPUT_DIR/human").file).absolutePath))))
  }

  @Test
  fun build_data_array_from_directory_content_with_file_id_provider() {
    // Given
    val directory = File(javaClass.getResource(INPUT_DIR).file)
    val fileIdProvider = BytesData.relativePathIdProvider(directory)

    // When
    val bytesDataArray = BytesData.loadFromDirectory(directory, fileIdProvider)

    // Then
    assertThat(bytesDataArray).hasSize(2)
    assertThat(bytesDataArray).contains(BytesData(ArrayList(listOf("human")), ArrayList(listOf(File(javaClass.getResource("$INPUT_DIR/human").file).absolutePath))))
    assertThat(bytesDataArray).contains(BytesData(ArrayList(listOf("subdir/opossum")), ArrayList(listOf(File(javaClass.getResource("$INPUT_DIR/subdir/opossum").file).absolutePath))))
  }

  @Test
  fun build_data_array_from_directory_content_without_hidden_file() {
    // Given
    val directory = File(javaClass.getResource(INPUT_DIR).file)
    val fileIdProvider = { file: File -> file.name }

    // When
    val bytesDataArray = BytesData.loadFromDirectory(directory, fileIdProvider)

    // Then
    assertThat(listOf(*bytesDataArray).contains(BytesData(ArrayList(listOf(".hidden-file")), ArrayList(listOf(File(javaClass.getResource("$INPUT_DIR/.hidden-file").file).absolutePath))))).isFalse()
  }

  @Test
  fun build_data_array_from_directory_content_without_bytes_then_add_bytes_in_new_array() {
    // Given
    val directory = File(javaClass.getResource(INPUT_DIR).file)
    val bytesDataArray = BytesData.loadFromDirectory(directory)

    // When
    val bytesDataArrayWithBytes = BytesData.withBytes(bytesDataArray)

    // Then
    assertThat(bytesDataArrayWithBytes).hasSize(2)
    assertThat(bytesDataArrayWithBytes[0].bytes).isNotNull
    assertThat(bytesDataArrayWithBytes[1].bytes).isNotNull
  }

  companion object {

    private const val FILE_PATH = "titi"
    private const val FILE_CONTENT = "tata"
    private const val JSON_FILE_PATH = "fileBytesData.json"
    private const val BYTES_DATA_JSON = "byteData.json"

    private const val INPUT_DIR = "/bytes-data-input-dir"
  }
}
