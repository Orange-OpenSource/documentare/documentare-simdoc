package com.orange.documentare.core.comp.distance;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.comp.DistanceItem;
import lombok.Getter;

@Getter
class TestItem implements DistanceItem {
  private final byte[] bytes;
  private final int id;
  private final String humanReadableId = "";

  TestItem(byte[] bytes, int id) {
    this.bytes = bytes;
    this.id = id;
  }

  @Override
  public byte[] loadBytes() {
    return bytes;
  }
}
