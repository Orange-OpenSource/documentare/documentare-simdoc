package com.orange.documentare.core.comp.clustering.graph.graphbuilder;

import com.orange.documentare.core.comp.clustering.graph.Item;
import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class SingletonsRecallIndexMapsTest {

  @Test
  public void build_new_singletons_objects_to_do_second_graph_build_on_singletons_only() {
    // given
    List<ClusteringItem> clusteringItems = buildItems();
    List<ClusteringItem> singletons = new ArrayList<>();
    singletons.add(clusteringItems.get(2));
    singletons.add(clusteringItems.get(3));

    // do
    SingletonsRecallIndexMaps indexMaps = new SingletonsRecallIndexMaps(clusteringItems, singletons);

    // then
    Assertions.assertThat(indexMaps.oldToNew.containsKey(2)).isTrue();
    Assertions.assertThat(indexMaps.oldToNew.containsKey(3)).isTrue();
    Assertions.assertThat(indexMaps.oldToNew.size()).isEqualTo(2);
    Assertions.assertThat(indexMaps.oldToNew.get(2)).isEqualTo(0);
    Assertions.assertThat(indexMaps.oldToNew.get(3)).isEqualTo(1);
    Assertions.assertThat(indexMaps.oldToNew.containsKey(2)).isTrue();
    Assertions.assertThat(indexMaps.newToOld.containsKey(0)).isTrue();
    Assertions.assertThat(indexMaps.newToOld.containsKey(1)).isTrue();
    Assertions.assertThat(indexMaps.newToOld.size()).isEqualTo(2);
    Assertions.assertThat(indexMaps.newToOld.get(0)).isEqualTo(2);
    Assertions.assertThat(indexMaps.newToOld.get(1)).isEqualTo(3);
  }

  private List<ClusteringItem> buildItems() {
    List<ClusteringItem> items = new ArrayList<>();
    IntStream.range(0, 4).forEach(i -> items.add(Item.withHumanReadableId("" + i)));
    return items;
  }
}