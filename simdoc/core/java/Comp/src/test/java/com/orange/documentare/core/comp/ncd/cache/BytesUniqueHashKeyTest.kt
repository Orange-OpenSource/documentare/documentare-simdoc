package com.orange.documentare.core.comp.ncd.cache

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import com.orange.documentare.core.comp.ncd.cache.BytesUniqueHashKey.buildUniqueHashKey
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.nio.charset.Charset


class BytesUniqueHashKeyTest {

  @Test
  fun `compute unique hash key of a byte array`() {
    // given
    val bytes: ByteArray = "12345".toByteArray(Charset.forName("utf8"))

    // when
    val key = buildUniqueHashKey(bytes)

    // then
    assertThat(key).isEqualTo("827CCB0EEA8A706C4C34A16891F84E7B")
  }

  @Test
  fun `compute unique hash key for two byte arrays, independent of arrays order`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))
    val key1 = buildUniqueHashKey(bytes1)
    val key2 = buildUniqueHashKey(bytes2)

    // when
    val key = buildUniqueHashKey(key1, key2)
    val switchedOrderKey = buildUniqueHashKey(key2, key1)

    // then
    assertThat(key).isEqualTo("AB56B4D92B40713ACC5AF89985D4B786827CCB0EEA8A706C4C34A16891F84E7B")
    assertThat(key).isEqualTo(switchedOrderKey)
  }

  @Test
  fun `bytes arrays are equal`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val key1 = buildUniqueHashKey(bytes1)
    val key2 = buildUniqueHashKey(bytes2)

    // when
    val equal = BytesUniqueHashKey.arraysAreEqual(key1, key2)

    // then
    assertThat(equal).isTrue()
  }

  @Test
  fun `bytes arrays are NOT equal`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcd".toByteArray(Charset.forName("utf8"))
    val key1 = buildUniqueHashKey(bytes1)
    val key2 = buildUniqueHashKey(bytes2)

    // when
    val equal = BytesUniqueHashKey.arraysAreEqual(key1, key2)

    // then
    assertThat(equal).isFalse()
  }

}