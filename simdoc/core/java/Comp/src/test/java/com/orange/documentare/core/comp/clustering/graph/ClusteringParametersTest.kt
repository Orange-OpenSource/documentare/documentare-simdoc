package com.orange.documentare.core.comp.clustering.graph

/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.EnrollParameters
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.Test

class ClusteringParametersTest {

  @Test
  fun `check generated equals works`() {
    // given
    val acutSd = 1.123f
    // when
    val parameters = ClusteringParameters.builder()
      .acut(acutSd)
      .build()

    // then
    assertThat(parameters).isEqualTo(ClusteringParameters.builder()
      .acut(acutSd)
      .build()
    )
    assertThat(parameters).isNotEqualTo(ClusteringParameters.builder().build())
  }

  @Test
  fun build_parameters_with_default_values() {
    // When
    val parameters = ClusteringParameters.builder().build()

    // Then
    assertThat(parameters.qcut()).isFalse()
    assertThat(parameters.acut()).isFalse()
    assertThat(parameters.scut()).isFalse()
    assertThat(parameters.ccut()).isFalse()
    assertThat(parameters.knn()).isFalse()
    assertThat(parameters.sloop).isFalse()
    assertThat(parameters.consolidateCluster).isFalse()
    assertThat(parameters.enroll).isFalse()
    assertThat(parameters.enrollParameters.sloop).isTrue()
    assertThat(parameters.enrollParameters.scutSdFactor).isEqualTo(4f)
  }

  @Test
  fun build_parameters() {
    // When
    val parameters = ClusteringParameters.builder()
      .acut()
      .qcut()
      .scut()
      .ccut()
      .sloop()
      .consolidateCluster()
      .enroll()
      .enrollParameters(
        EnrollParameters.builder()
          .disableSloop()
          .acut()
          .qcut()
          .build())
      .build()

    // Then
    assertThat(parameters.qcut()).isTrue()
    assertThat(parameters.acut()).isTrue()
    assertThat(parameters.scut()).isTrue()
    assertThat(parameters.scutSdFactor).isEqualTo(ClusteringParameters.SCUT_DEFAULT_SD_FACTOR)
    assertThat(parameters.ccut()).isTrue()
    assertThat(parameters.knn()).isFalse()
    assertThat(parameters.sloop).isTrue()
    assertThat(parameters.consolidateCluster).isTrue()
    assertThat(parameters.enroll).isTrue()
    assertThat(parameters.enrollParameters.sloop).isFalse()
    assertThat(parameters.enrollParameters.acutSdFactor).isEqualTo(2f)
    assertThat(parameters.enrollParameters.qcutSdFactor).isEqualTo(2f)
  }


  @Test
  fun build_parameters_with_non_defaults() {
    // When
    val parameters = ClusteringParameters.builder()
      .acut(0.1f)
      .qcut(0.2f)
      .scut(0.3f)
      .ccut(23)
      .knn(12)
      .build()

    // Then
    assertThat(parameters.acutSdFactor).isEqualTo(0.1f)
    assertThat(parameters.qcutSdFactor).isEqualTo(0.2f)
    assertThat(parameters.scutSdFactor).isEqualTo(0.3f)
    assertThat(parameters.ccutPercentile).isEqualTo(23)
    assertThat(parameters.knnThreshold).isEqualTo(12)
  }

  @Test
  fun build_parameters_with_sloop() {
    // When
    val parameters = ClusteringParameters.builder()
      .sloop()
      .build()

    // Then
    assertThat(parameters.scutSdFactor).isEqualTo(3f)
    assertThat(parameters.sloop).isTrue()
  }

  @Test
  fun build_parameters_with_sloop_and_scut_std_factor_value() {
    // Given

    // When
    val parameters = ClusteringParameters.builder()
      .sloop()
      .scut(1.1f)
      .build()

    // Then
    assertThat(parameters.scutSdFactor).isEqualTo(1.1f)
    assertThat(parameters.sloop).isTrue()
  }
}
