/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.graph

import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder
import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph.ClusteringGraphDTO
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FileUtils.readFileToString
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import java.io.File

class ClusteringGraphBuilderScalpelTest {

  @AfterEach
  fun cleanup() {
    FileUtils.deleteQuietly(File(GRAPH_OUTPUT))
  }

  @Test
  fun shouldBuildGraphFromClusteringItemsInput() {
    // given
    val jsonGenericHandler = JsonGenericHandler(true)
    val importModel = jsonGenericHandler.getObjectFromJsonGzipFile(ImportModel::class.java, File(javaClass.getResource(CLUSTERING_INPUT).file)) as ImportModel
    importModel.loadItemsBytes()
    val parameters = ClusteringParameters.builder()
      .acut()
      .qcut()
      .scut(1f)
      .build()
    val clusteringGraphBuilder = ClusteringGraphBuilder()

    // when
    val clustering = clusteringGraphBuilder.doClustering(importModel.items, parameters)
    val output = File(GRAPH_OUTPUT)
    jsonGenericHandler.writeObjectToJsonFileNewApi(ClusteringGraphDTO.fromBusinessObject(clustering.graph), output)

    // then
    val expected = File(javaClass.getResource(GRAPH_OUTPUT_REF).file)
    val expectedJsonString = readFileToString(expected, "utf-8")
    val outputJsonString = readFileToString(output, "utf-8")
    JSONAssert.assertEquals(expectedJsonString, outputJsonString, true)
  }

  companion object {
    private const val CLUSTERING_INPUT = "/bestioles_nearests_for_clustering.json.gz"
    private const val GRAPH_OUTPUT_REF = "/bestioles_graph_scalpel_ref.json"
    private const val GRAPH_OUTPUT = "bestioles_graph_scalpel.json"
  }
}
