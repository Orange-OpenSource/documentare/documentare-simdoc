package com.orange.documentare.core.comp.clustering.graph;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder;
import com.orange.documentare.core.model.ref.clustering.*;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import org.junit.runner.RunWith;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(ZohhakRunner.class)
public class ClusteringGraphBuilderSyntheticScalpelTest implements Item.ItemInit {

  @TestWith({"false, 3, 1, 2", "false, 1.96, 3, 3", "true, 2.2, 3, 3"})
  public void compute_graph_with_scut(boolean sloop, float scut, int subGraphsNb, int clustersNb) {
    // given
    ClusteringItem[] clusteringItems = Item.buildClusteringItems(this, 6);
    ClusteringParameters parameters = sloop ?
      ClusteringParameters.builder().scut(scut).sloop().build() :
      ClusteringParameters.builder().scut(scut).build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

    // do
    Clustering clustering =
      clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    List<ClusteringElement> elements = clustering.elements;
    List<Subgraph> subGraphs = clustering.subgraphs;
    List<Cluster> clusters = clustering.clusters;
    assertThat(elements).hasSize(clusteringItems.length);
    assertThat(subGraphs).hasSize(subGraphsNb);
    assertThat(clusters).hasSize(clustersNb);
  }

  @TestWith({"false, 1.96", "true, 2.2"})
  public void check_items_are_in_correct_cluster(boolean sloop, float scut) {
    // given
    ClusteringItem[] clusteringItems = Item.buildClusteringItems(this, 6);
    ClusteringParameters parameters = sloop ?
      ClusteringParameters.builder().scut(scut).sloop().build() :
      ClusteringParameters.builder().scut(scut).build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

    // do
    Clustering clustering =
      clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    List<ClusteringElement> elements = clustering.elements;
    List<Subgraph> subGraphs = clustering.subgraphs;
    List<Cluster> clusters = clustering.clusters;
    assertThat(elements.get(0).clusterId).isEqualTo(1);
    assertThat(elements.get(1).clusterId).isEqualTo(1);
    assertThat(elements.get(2).clusterId).isEqualTo(1);
    assertThat(subGraphs.get(0).elementsIndices).containsExactly(0, 1, 2);
    assertThat(clusters.get(0).elementsIndices).containsExactly(0, 1, 2);
    assertThat(subGraphs.get(0).edges).hasSize(10);

    assertThat(elements.get(3).clusterId).isEqualTo(2);
    assertThat(subGraphs.get(1).elementsIndices).containsExactly(3);
    assertThat(clusters.get(1).elementsIndices).containsExactly(3);
    assertThat(subGraphs.get(1).edges).hasSize(0);

    assertThat(elements.get(4).clusterId).isEqualTo(3);
    assertThat(elements.get(5).clusterId).isEqualTo(3);
    assertThat(subGraphs.get(2).elementsIndices).containsExactly(4, 5);
    assertThat(clusters.get(2).elementsIndices).containsExactly(4, 5);
    assertThat(subGraphs.get(2).edges).hasSize(2);
    assertThat(subGraphs.get(2).edges.get(0).v1Index).isEqualTo(4);
    assertThat(subGraphs.get(2).edges.get(0).v2Index).isEqualTo(5);
    assertThat(subGraphs.get(2).edges.get(0).length).isEqualTo(6);
  }

  @Override
  public void init(Item[] items) {
    items[0].setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 5), new NearestItem(2, 5), new NearestItem(3, 10), new NearestItem(4, 40), new NearestItem(5, 45)});
    items[1].setNearestItems(new NearestItem[]{new NearestItem(1, 0), new NearestItem(0, 5), new NearestItem(2, 5), new NearestItem(3, 10), new NearestItem(4, 45), new NearestItem(5, 47)});
    items[2].setNearestItems(new NearestItem[]{new NearestItem(2, 0), new NearestItem(0, 5), new NearestItem(1, 5), new NearestItem(3, 10), new NearestItem(4, 45), new NearestItem(5, 47)});
    items[3].setNearestItems(new NearestItem[]{new NearestItem(3, 0), new NearestItem(0, 10), new NearestItem(1, 10), new NearestItem(2, 10), new NearestItem(4, 45), new NearestItem(5, 47)});

    items[4].setNearestItems(new NearestItem[]{new NearestItem(4, 0), new NearestItem(5, 5), new NearestItem(0, 40), new NearestItem(1, 47), new NearestItem(2, 47), new NearestItem(3, 47)});
    items[5].setNearestItems(new NearestItem[]{new NearestItem(5, 0), new NearestItem(4, 5), new NearestItem(0, 45), new NearestItem(1, 47), new NearestItem(2, 47), new NearestItem(3, 47)});
  }
}
