/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.distance

import com.orange.documentare.core.model.json.JsonGenericHandler
import org.apache.commons.io.FileUtils
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

class DistancesArrayDTOTest {

  val jsonGenericHandler: JsonGenericHandler = JsonGenericHandler.instance()
  val file = File("test.json")

  @BeforeEach
  @AfterEach
  fun cleanup() {
    FileUtils.deleteQuietly(file)
  }

  @Test
  fun `can convert to DTO object`() {
    // given
    val distancesArray = DistancesArray(2, 2, true)
    distancesArray.setDistancesForItem(0, intArrayOf(10))
    distancesArray.setDistancesForItem(1, intArrayOf(10))

    // when
    val dto = DistancesArrayDTO.fromBusinessObject(distancesArray)
    jsonGenericHandler.writeObjectToJsonFileNewApi(dto, file)
    val readDTO = jsonGenericHandler.getObjectFromJsonFileNewApi(DistancesArrayDTO::class.java, file) as DistancesArrayDTO
    val readBO = readDTO.toBusinessObject()

    // then
    assertThat(readBO).isEqualTo(distancesArray)
    assertThat(readDTO.distancesArray).isEqualTo(distancesArray.distancesArray)
    assertThat(readDTO.getDistancesFor(0)).isEqualTo(intArrayOf(0, 10))
  }
}