/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.graph.graphbuilder

import com.orange.documentare.core.comp.clustering.graph.ImportModel
import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.EnrollParameters
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph.ClusteringGraphDTO
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FileUtils.readFileToString
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import java.io.File

class SingletonsRecallIntegrationTest {

  @AfterEach
  fun cleanup() {
    FileUtils.deleteQuietly(File(GRAPH_OUTPUT))
  }

  @Test
  fun graph_is_built_with_singleton_recall() {
    // given
    val jsonGenericHandler = JsonGenericHandler(true)
    val importModel = jsonGenericHandler.getObjectFromJsonGzipFile(ImportModel::class.java, File(javaClass.getResource(CLUSTERING_INPUT).file)) as ImportModel
    importModel.loadItemsBytes()
    val parameters = ClusteringParameters.builder().acut().qcut()
      .enroll()
      .enrollParameters(EnrollParameters.builder().acut().qcut().disableSloop().build())
      .build()
    val clusteringGraphBuilder = ClusteringGraphBuilder()
    // do
    val clustering = clusteringGraphBuilder.doClustering(importModel.items, parameters)
    val output = File(GRAPH_OUTPUT)
    jsonGenericHandler.writeObjectToJsonFileNewApi(ClusteringGraphDTO.fromBusinessObject(clustering.graph), output)
    // then
    val expected = File(javaClass.getResource(GRAPH_OUTPUT_REF).file)
    val expectedJsonString = readFileToString(expected, "utf-8")
    val outputJsonString = readFileToString(output, "utf-8")
    JSONAssert.assertEquals(expectedJsonString, outputJsonString, true)
  }

  companion object {

    private const val CLUSTERING_INPUT = "/bestioles_nearests_for_clustering.json.gz"
    private const val GRAPH_OUTPUT_REF = "/bestioles_graph_singleton_recall_ref.json"
    private const val GRAPH_OUTPUT = "bestioles_graph_singleton_recall.json"
  }
}
