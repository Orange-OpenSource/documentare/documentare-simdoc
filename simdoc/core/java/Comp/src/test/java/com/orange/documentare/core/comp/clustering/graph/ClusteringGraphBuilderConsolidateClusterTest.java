package com.orange.documentare.core.comp.clustering.graph;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder;
import com.orange.documentare.core.comp.distance.Distance;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.TriangleVertices;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.junit.Test;

import java.util.Optional;
import java.util.stream.IntStream;

import static org.fest.assertions.Assertions.assertThat;

public class ClusteringGraphBuilderConsolidateClusterTest {
  private static final int D = Distance.DISTANCE_INT_CONV_FACTOR;

  ClusteringParameters parameters = ClusteringParameters.builder()
    .consolidateCluster()
    .build();

  ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

  @Test
  public void no_items_should_not_crash() {
    // given
    Item[] clusteringItems = new Item[0];

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(0);
  }

  @Test
  public void one_item_should_produce_a_cluster_with_id_0() {
    // given
    Item[] clusteringItems = new Item[]{new Item(String.valueOf(0))};

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(1);
    assertThat(clustering.elements.get(0).clusterId).isEqualTo(0);
  }

  @Test
  public void two_items_should_produce_a_cluster_with_id_0() {
    // given
    Item[] clusteringItems = fillWith2Items();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(1);
    assertThat(clustering.elements.get(0).clusterId).isEqualTo(0);
  }

  @Test
  public void three_close_items_are_in_same_cluster_with_id_0_with_a_cluster_center() {
    // given
    Item[] clusteringItems = fillWith3CloseItems();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(1);
    assertThat(clustering.elements.get(0).clusterCenter()).isTrue();
    assertThat(clustering.elements.get(0).clusterId).isEqualTo(0);
  }

  @Test
  public void four_items_with_one_distance_GT1_are_STILL_in_same_cluster() {
    // given
    ClusteringItem[] clusteringItems = fillWith4ItemsWith1atDistanceGT1();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(1);
    assertThat(clustering.elements.get(0).clusterCenter()).isTrue();
    assertThat(clustering.elements.get(0).clusterId).isEqualTo(0);
  }

  @Test
  public void five_items_with_two_far_from_others_are_STILL_in_same_cluster() {
    // given
    ClusteringItem[] clusteringItems = fillWith5ItemsWith2FarAway();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(1);
    assertThat(clustering.elements.get(0).clusterCenter()).isTrue();
    assertThat(clustering.elements.get(0).clusterId).isEqualTo(0);
  }

  @Test
  public void seven_items_as_two_disconnected_triangles_are_STILL_in_same_cluster() {
    // given
    ClusteringItem[] clusteringItems = fillWith7ItemsAs2DisconnectedTriangles();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(1);
    IntStream.range(0, 7)
      .forEach(i -> assertThat(clustering.elements.get(i).clusterCenter()).isEqualTo(i == 3));
  }

  @Test
  public void twelve_items_as_two_disconnected_triangles_and_3_clusters_are_STILL_in_same_cluster() {
    // given
    ClusteringItem[] clusteringItems = fillWith12ItemsFor2SubgraphsAnd3Clusters();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters).hasSize(1);
    assertThat(clustering.elements.stream()
      .filter(el -> el.clusterCenter())
      .count()).isEqualTo(1);
  }

  @Test
  public void twelve_items_as_two_disconnected_triangles_and_3_clusters_generate_a_multiset_with_three_elements_and_a_cluster_with_id_0() {
    // given
    ClusteringItem[] clusteringItems = fillWith12ItemsFor2SubgraphsAnd3Clusters();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    assertThat(clustering.clusters.get(0).multisetElementsIndices).containsExactly(0, 5, 8);
    assertThat(clustering.elements.get(0).clusterId).isEqualTo(0);
  }

  private Item[] fillWith2Items() {
    Item[] items = initItems(2);
    items[0].setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 10)});
    items[1].setNearestItems(new NearestItem[]{new NearestItem(1, 0), new NearestItem(0, 10)});
    return items;
  }

  private Item[] fillWith3CloseItems() {
    Item[] items = initItems(3);
    items[0].setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 10), new NearestItem(2, 10)});
    items[1].setNearestItems(new NearestItem[]{new NearestItem(1, 0), new NearestItem(0, 10), new NearestItem(2, 10)});
    items[2].setNearestItems(new NearestItem[]{new NearestItem(2, 0), new NearestItem(0, 10), new NearestItem(1, 10)});
    return items;
  }

  private Item[] fillWith4ItemsWith1atDistanceGT1() {
    Item[] items = initItems(4);
    items[0].setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 10), new NearestItem(2, 10), new NearestItem(3, D)});
    items[1].setNearestItems(new NearestItem[]{new NearestItem(1, 0), new NearestItem(0, 10), new NearestItem(2, 10), new NearestItem(3, D)});
    items[2].setNearestItems(new NearestItem[]{new NearestItem(2, 0), new NearestItem(0, 10), new NearestItem(1, 10), new NearestItem(3, D)});
    items[3].setNearestItems(new NearestItem[]{new NearestItem(3, 0), new NearestItem(0, D), new NearestItem(1, D), new NearestItem(2, D)});
    return items;
  }

  private Item[] fillWith5ItemsWith2FarAway() {
    int farAway = 100000;
    Item[] items = initItems(5);
    items[0].setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 10), new NearestItem(2, 10), new NearestItem(3, farAway), new NearestItem(4, farAway)});
    items[1].setNearestItems(new NearestItem[]{new NearestItem(1, 0), new NearestItem(0, 10), new NearestItem(2, 10), new NearestItem(3, farAway), new NearestItem(4, farAway)});
    items[2].setNearestItems(new NearestItem[]{new NearestItem(2, 0), new NearestItem(0, 10), new NearestItem(1, 10), new NearestItem(3, farAway), new NearestItem(4, farAway)});

    items[3].setNearestItems(new NearestItem[]{new NearestItem(3, 0), new NearestItem(4, 10), new NearestItem(0, farAway), new NearestItem(1, farAway), new NearestItem(2, farAway)});
    items[4].setNearestItems(new NearestItem[]{new NearestItem(4, 0), new NearestItem(3, 10), new NearestItem(0, farAway), new NearestItem(1, farAway), new NearestItem(2, farAway)});
    return items;
  }

  private Item[] fillWith7ItemsAs2DisconnectedTriangles() {
    Item[] items = initItems(7);
    items[0].setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 10), new NearestItem(2, 10), new NearestItem(3, 100), new NearestItem(4, 100), new NearestItem(5, 100)});
    items[1].setNearestItems(new NearestItem[]{new NearestItem(1, 0), new NearestItem(0, 10), new NearestItem(2, 10), new NearestItem(3, 100), new NearestItem(4, 100), new NearestItem(5, 100)});
    items[2].setNearestItems(new NearestItem[]{new NearestItem(2, 0), new NearestItem(0, 10), new NearestItem(1, 10), new NearestItem(3, 100), new NearestItem(4, 100), new NearestItem(5, 100)});

    items[3].setNearestItems(new NearestItem[]{new NearestItem(3, 0), new NearestItem(4, 10), new NearestItem(5, 10), new NearestItem(0, 100), new NearestItem(1, 100), new NearestItem(2, 100)});
    items[4].setNearestItems(new NearestItem[]{new NearestItem(4, 0), new NearestItem(3, 10), new NearestItem(5, 10), new NearestItem(0, 100), new NearestItem(1, 100), new NearestItem(2, 100)});
    items[5].setNearestItems(new NearestItem[]{new NearestItem(5, 0), new NearestItem(3, 10), new NearestItem(4, 10), new NearestItem(0, 100), new NearestItem(1, 100), new NearestItem(2, 100)});
    items[6].setNearestItems(new NearestItem[]{new NearestItem(6, 0), new NearestItem(3, 10), new NearestItem(4, 10), new NearestItem(0, 100), new NearestItem(1, 100), new NearestItem(2, 100)});
    return items;
  }

  private Item[] fillWith12ItemsFor2SubgraphsAnd3Clusters() {
    int farAway = 100000;
    Item[] items = initItems(12);
    items[0].setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 10), new NearestItem(2, 10), new NearestItem(3, farAway), new NearestItem(4, farAway)});
    items[1].setNearestItems(new NearestItem[]{new NearestItem(1, 0), new NearestItem(0, 10), new NearestItem(2, 10), new NearestItem(3, farAway), new NearestItem(4, farAway)});
    items[2].setNearestItems(new NearestItem[]{new NearestItem(2, 0), new NearestItem(0, 10), new NearestItem(1, 10), new NearestItem(3, farAway), new NearestItem(4, farAway)});

    items[3].setNearestItems(new NearestItem[]{new NearestItem(3, 0), new NearestItem(4, 10), new NearestItem(0, farAway), new NearestItem(1, farAway), new NearestItem(2, farAway)});
    items[4].setNearestItems(new NearestItem[]{new NearestItem(4, 0), new NearestItem(3, 10), new NearestItem(0, farAway), new NearestItem(1, farAway), new NearestItem(2, farAway)});


    items[5].setNearestItems(new NearestItem[]{new NearestItem(5, 0), new NearestItem(6, 10), new NearestItem(7, 10), new NearestItem(8, 100), new NearestItem(9, 100), new NearestItem(10, 100)});
    items[6].setNearestItems(new NearestItem[]{new NearestItem(6, 0), new NearestItem(5, 10), new NearestItem(7, 10), new NearestItem(8, 100), new NearestItem(9, 100), new NearestItem(10, 100)});
    items[7].setNearestItems(new NearestItem[]{new NearestItem(7, 0), new NearestItem(5, 10), new NearestItem(6, 10), new NearestItem(8, 100), new NearestItem(9, 100), new NearestItem(10, 100)});

    items[8].setNearestItems(new NearestItem[]{new NearestItem(8, 0), new NearestItem(9, 10), new NearestItem(7, 10), new NearestItem(5, 100), new NearestItem(6, 100), new NearestItem(7, 100)});
    items[9].setNearestItems(new NearestItem[]{new NearestItem(9, 0), new NearestItem(8, 10), new NearestItem(7, 10), new NearestItem(5, 100), new NearestItem(6, 100), new NearestItem(7, 100)});
    items[10].setNearestItems(new NearestItem[]{new NearestItem(10, 0), new NearestItem(8, 10), new NearestItem(9, 10), new NearestItem(5, 100), new NearestItem(6, 100), new NearestItem(7, 100)});
    items[11].setNearestItems(new NearestItem[]{new NearestItem(11, 0), new NearestItem(8, 10), new NearestItem(9, 10), new NearestItem(5, 100), new NearestItem(6, 100), new NearestItem(7, 100)});
    return items;
  }

  private Item[] initItems(int number) {
    Item[] items = new Item[number];
    for (int i = 0; i < items.length; i++) {
      items[i] = new Item(String.valueOf(i));
    }
    return items;
  }

  @Getter
  @Setter
  @RequiredArgsConstructor
  class Item implements ClusteringItem {
    private final String humanReadableId;
    private Float nearestTriangleArea;
    private Integer clusterId;
    private Boolean clusterCenter;
    private NearestItem[] nearestItems;
    private byte[] bytes;

    private TriangleVertices triangleVertices;
    private Integer duplicateOf;

    @Override
    public Optional<Integer> duplicateOf() {
      return Optional.ofNullable(duplicateOf);
    }

    @Override
    public boolean triangleVerticesAvailable() {
      return triangleVertices != null;
    }
  }
}
