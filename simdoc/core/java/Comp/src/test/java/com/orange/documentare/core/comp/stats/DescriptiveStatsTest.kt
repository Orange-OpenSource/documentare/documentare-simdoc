package com.orange.documentare.core.comp.stats

import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats
import com.orange.documentare.core.model.ref.comp.DoubleStatistics
import com.orange.documentare.core.model.ref.comp.IntStatistics
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DescriptiveStatsTest {

  @Test
  fun `compute Int(s) min max mean std and percentile`() {
    // given
    val list = listOf(-10, -1, 51, 25)

    // when
    val statistics = DescriptiveStats().statistics(list)

    then(statistics).isEqualTo(IntStatistics(-10, 51, 16.25, 27.5121185419565, 12.0))
  }

  @Test
  fun `compute Double(s) min max mean std and percentile`() {
    // given
    val list = listOf(-10.1, -1.2, 51.3, 25.6)

    // when
    val statistics = DescriptiveStats().statistics(list)

    then(statistics).isEqualTo(DoubleStatistics(-10.1, 51.3, 16.4, 27.776848873357345, 12.200000000000001))
  }
}