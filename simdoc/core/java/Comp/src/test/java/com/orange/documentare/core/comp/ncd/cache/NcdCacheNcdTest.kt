package com.orange.documentare.core.comp.ncd.cache

import com.orange.documentare.core.comp.bwt.BwtParameters
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.ncd.NcdParameters
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.charset.Charset

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

class NcdCacheNcdTest {

  @BeforeEach
  internal fun setup() {
    NcdCache.clear()
  }

  @Test
  fun `we can know that a ncd result is not in the cache for two byte arrays`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))

    // when
    val ncdCacheKeys = NcdCacheKeys(bytes1, bytes2)
    val ncdResult: Float? = NcdCache.getNcd(ncdCacheKeys)

    // then
    assertThat(ncdResult).isNull()
  }

  @Test
  fun `we can add a ncd result in the cache for two byte arrays, then retrieve it in any order`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))
    val result = 0.3f
    val computationDuration = 123

    // when
    val ncdCacheKeys = NcdCacheKeys(bytes1, bytes2)
    NcdCache.addNcd(ncdCacheKeys, result, computationDuration, NcdParameters(BwtParameters(SuffixArrayAlgorithm.SAIS)))
    val retrievedResult = NcdCache.getNcd(ncdCacheKeys)
    val retrievedResultWithSwitchedArrays = NcdCache.getNcd(ncdCacheKeys)
    val retrievedComputationDuration = NcdCache.getComputationDuration(ncdCacheKeys.byteArraysKey)

    // then
    assertThat(retrievedResult).isEqualTo(result)
    assertThat(retrievedResultWithSwitchedArrays).isEqualTo(result)
    assertThat(retrievedComputationDuration).isEqualTo(123)
  }

  @Test
  fun `cache hits and misses are computed`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))
    val result = 0.3f
    val computationDuration = 123

    // when
    val ncdCacheKeys = NcdCacheKeys(bytes1, bytes2)
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)

    NcdCache.addNcd(ncdCacheKeys, result, computationDuration, NcdParameters(BwtParameters(SuffixArrayAlgorithm.SAIS)))
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)

    // then
    assertThat(NcdCache.ncdHit()).isEqualTo(2)
    assertThat(NcdCache.ncdMiss()).isEqualTo(4)
  }
}
