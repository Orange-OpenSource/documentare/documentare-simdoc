/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.graph.graphbuilder

import com.orange.documentare.core.comp.clustering.graph.Item
import com.orange.documentare.core.comp.clustering.graph.Item.ItemInit
import com.orange.documentare.core.model.ref.clustering.ClusteringItem
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.EnrollParameters
import com.orange.documentare.core.model.ref.comp.NearestItem
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class SingletonsRecallTest : ItemInit {

  @Test
  fun compute_graph_with_singleton_recall() {
    // given
    val clusteringItems = Item.buildClusteringItems(this, 7)
    val parameters = ClusteringParameters.builder().acut(0.1f).qcut()
      .enroll().enrollParameters(EnrollParameters.builder().acut(0.1f).qcut().disableSloop().build())
      .build()
    val clusteringGraphBuilder = ClusteringGraphBuilder()

    // when
    val clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters)
    val clusteringGraph = clustering.graph

    // then
    clusteringGraph.apply {
      assertThat(getSubGraphs()).hasSize(3)
      assertThat(getClusters()).hasSize(3)
      assertThat(getSubGraphs()[0]!!.itemIndices).containsExactly(0, 1, 2)
      assertThat(getSubGraphs()[5]!!.itemIndices).containsExactly(3, 4, 5)
      assertThat(getSubGraphs()[6]!!.itemIndices).containsExactly(6)
      assertThat(getClusters()[1]!!.itemIndices).containsExactly(0, 1, 2)
      assertThat(getClusters()[7]!!.itemIndices).containsExactly(3, 4, 5)
      assertThat(getClusters()[8]!!.itemIndices).containsExactly(6)
    }
  }

  @Test
  fun compute_graph_and_retrieve_singletons() {
    // given
    val clusteringItems = Item.buildClusteringItems(this, 7)
    val parameters = EnrollParameters.builder().acut(0.1f).qcut().build()
    val clusteringGraphBuilder = ClusteringGraphBuilder()
    val singletonsRecall = SingletonsRecall(clusteringGraphBuilder, listOf(*clusteringItems), parameters)
    val clusteringGraph = clusteringGraphBuilder.doBuild(clusteringItems, parameters)

    // when
    val singletons = singletonsRecall.retrieveSingletonsItemsFrom(listOf(*clusteringItems), clusteringGraph)

    // then
    assertThat(singletons).hasSize(4)
    assertThat(singletons).contains(clusteringItems[3], clusteringItems[4], clusteringItems[5], clusteringItems[6])
  }

  @Test
  fun build_new_singletons_objects_to_do_second_graph_build_on_singletons_only() {
    // given
    val clusteringItems = Item.buildClusteringItems(this, 7)
    val parameters = ClusteringParameters.builder().acut(0.1f).qcut().build()
    val clusteringGraphBuilder = ClusteringGraphBuilder()
    val singletonsRecall = SingletonsRecall(clusteringGraphBuilder, listOf(*clusteringItems), parameters.enrollParameters)
    val clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters)
    val singletons = singletonsRecall.retrieveSingletonsItemsFrom(listOf(*clusteringItems), clustering.graph)
    // To make sure we can rely on singleton index in our assertions
    Arrays.sort(singletons, Comparator.comparing { obj: ClusteringItem -> obj.humanReadableId })
    val clusteringItemsList = listOf(*clusteringItems)
    val indexMaps = SingletonsRecallIndexMaps(clusteringItemsList, listOf(*singletons))

    // when
    val singletonsForRegraph = singletonsRecall.buildSingletonsForRegraph(indexMaps.oldToNew, clusteringItemsList, singletons)

    // then
    assertThat(singletons).hasSize(4)
    val singleton0 = singletonsForRegraph[0]
    assertThat(singleton0.nearestItems[0].index).isEqualTo(0)
    assertThat(singleton0.nearestItems[1].index).isEqualTo(1)
    assertThat(singleton0.nearestItems[2].index).isEqualTo(2)
    assertThat(singleton0.nearestItems[3].index).isEqualTo(3)
    val singleton2 = singletonsForRegraph[2]
    assertThat(singleton2.nearestItems[0].index).isEqualTo(2)
    assertThat(singleton2.nearestItems[1].index).isEqualTo(1)
    assertThat(singleton2.nearestItems[2].index).isEqualTo(0)
    assertThat(singleton2.nearestItems[3].index).isEqualTo(3)
  }

  override fun init(items: Array<Item>) {
    items.let {
      it[0].nearestItems = arrayOf(NearestItem(0, 0), NearestItem(1, 10), NearestItem(2, 10), NearestItem(3, 200), NearestItem(4, 200), NearestItem(5, 200), NearestItem(6, 1000))
      it[1].nearestItems = arrayOf(NearestItem(1, 0), NearestItem(0, 10), NearestItem(2, 10), NearestItem(3, 200), NearestItem(4, 200), NearestItem(5, 200), NearestItem(6, 1000))
      it[2].nearestItems = arrayOf(NearestItem(2, 0), NearestItem(0, 10), NearestItem(1, 10), NearestItem(3, 200), NearestItem(4, 200), NearestItem(5, 200), NearestItem(6, 1000))
      it[3].nearestItems = arrayOf(NearestItem(3, 0), NearestItem(4, 100), NearestItem(5, 102), NearestItem(0, 200), NearestItem(1, 200), NearestItem(2, 200), NearestItem(6, 1000))
      it[4].nearestItems = arrayOf(NearestItem(4, 0), NearestItem(3, 100), NearestItem(5, 101), NearestItem(0, 200), NearestItem(1, 200), NearestItem(2, 200), NearestItem(6, 1000))
      it[5].nearestItems = arrayOf(NearestItem(5, 0), NearestItem(4, 101), NearestItem(3, 102), NearestItem(0, 200), NearestItem(1, 200), NearestItem(2, 200), NearestItem(6, 1000))
      it[6].nearestItems = arrayOf(NearestItem(6, 0), NearestItem(0, 1000), NearestItem(1, 1000), NearestItem(2, 1000), NearestItem(3, 1000), NearestItem(4, 1000), NearestItem(5, 1000))
    }
  }
}