package com.orange.documentare.core.comp.ncd.cache

import com.orange.documentare.core.comp.bwt.BwtParameters
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.ncd.NcdParameters
import com.orange.documentare.core.postgresql.cache.PostgresCache
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.charset.Charset

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can persistencetribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

class NcdCachePersistenceTest {

  @BeforeEach
  fun setup() {
    NcdCache.clear()
    PostgresCache.testOnlySetup()
  }

  @Test
  fun `update persistence cache for ncd result`() {
    if (PostgresCache.isDisabled()) {
      println("⚠️ POSTGRES CACHE is disabled, test is skipped")
      return
    }

    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))
    val result = 0.3f
    val computationDuration = 123

    // when
    val ncdCacheKeys = NcdCacheKeys(bytes1, bytes2)
    NcdCache.addNcd(ncdCacheKeys, result, computationDuration, NcdParameters(BwtParameters(SuffixArrayAlgorithm.SAIS)))

    // then
    PostgresCache.flushQueueSync()
    val retrievedResultInPersistence = PostgresCache.get(ncdCacheKeys.byteArraysKey)
    assertThat(retrievedResultInPersistence).isEqualTo(result)
  }

  @Test
  fun `update persistence cache for comp result`() {
    if (PostgresCache.isDisabled()) {
      println("⚠️ POSTGRES CACHE is disabled, test is skipped")
      return
    }

    //given
    val key = "1234"
    val result = 0.3f
    val computationDuration = 123

    // when
    NcdCache.addCompression(key, result, computationDuration)

    // then
    PostgresCache.flushQueueSync()
    val retrievedResultInPersistence = PostgresCache.get(key)
    assertThat(retrievedResultInPersistence).isEqualTo(result)
  }

  @Test
  fun `in memory cache should be updated with persistence cache on get for ncd`() {
    if (PostgresCache.isDisabled()) {
      println("⚠️ POSTGRES CACHE is disabled, test is skipped")
      return
    }

    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))
    val result = 0.3f

    val ncdCacheKeys = NcdCacheKeys(bytes1, bytes2)
    PostgresCache.add(ncdCacheKeys.byteArraysKey, result)
    PostgresCache.flushQueueSync()

    // when
    NcdCache.getNcd(ncdCacheKeys)

    // then
    assertThat(NcdCache.ncdHit()).isEqualTo(0)
    assertThat(PostgresCache.cacheHit()).isEqualTo(1)

    // when
    NcdCache.getNcd(ncdCacheKeys)

    // then
    assertThat(NcdCache.ncdHit()).isEqualTo(1)
    assertThat(PostgresCache.cacheHit()).isEqualTo(1)
  }

  @Test
  fun `in memory cache should be updated with persistence cache on get for comp`() {
    if (PostgresCache.isDisabled()) {
      println("⚠️ POSTGRES CACHE is disabled, test is skipped")
      return
    }

    // given
    val key = "1234"
    val result = 0.3f

    PostgresCache.add(key, result)
    PostgresCache.flushQueueSync()

    // when
    NcdCache.getCompression(key)

    // then
    assertThat(NcdCache.compressionHit()).isEqualTo(0)
    assertThat(PostgresCache.cacheHit()).isEqualTo(1)

    // when
    NcdCache.getCompression(key)

    // then
    assertThat(NcdCache.compressionHit()).isEqualTo(1)
    assertThat(PostgresCache.cacheHit()).isEqualTo(1)
  }
}
