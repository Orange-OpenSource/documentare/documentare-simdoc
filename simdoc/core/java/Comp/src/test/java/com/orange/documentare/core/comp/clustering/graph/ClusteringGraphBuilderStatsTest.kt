/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.graph

import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder
import com.orange.documentare.core.model.ref.clustering.ClusteringItem
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import org.fest.assertions.Assertions.assertThat
import org.junit.Test

class ClusteringGraphBuilderStatsTest {

  @Test
  fun compute_graph_for_no_item() {
    // given
    val clusteringItems: Array<ClusteringItem?> = emptyArray()
    val parameters = ClusteringParameters.builder().build()
    val clusteringGraphBuilder = ClusteringGraphBuilder()
    ClusteringGraphBuilder.clearStats()

    // when
    clusteringGraphBuilder.doClustering(clusteringItems, parameters)
    val stats = ClusteringGraphBuilder.statsDurationMillis()

    // then
    assertThat(stats).isEqualTo("clustering duration millis:\n" +
      "count=1 / min=0.0 / max=0.0 / mean=0.0")
  }
}