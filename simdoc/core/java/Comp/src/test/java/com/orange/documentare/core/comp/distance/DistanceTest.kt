/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.distance

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.comp.distance.computer.threads.DistancesThreadException
import com.orange.documentare.core.comp.measure.TreatmentStep
import com.orange.documentare.core.model.ref.comp.DistanceItem
import com.orange.documentare.core.system.measure.Progress
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import java.io.File
import java.util.*

class DistanceTest {
  private val distance = Distance(DistanceParameters.defaults())

  private val bytes1 = byteArrayOf(1, 2)
  private val bytes2 = byteArrayOf(3, 4)
  private val bytesData1 = BytesData(ArrayList(listOf("1")), bytes1)
  private val bytesData2 = BytesData(ArrayList(listOf("2")), bytes2)

  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `ncd parameters are built with distance parameters`(suffixArrayAlgorithm: SuffixArrayAlgorithm) {
    // given
    val distanceParameters = DistanceParameters(suffixArrayAlgorithm)
    val distanceWithProvidedSAAlgo = Distance(distanceParameters)
    // then
    then(distanceWithProvidedSAAlgo.distanceParameters).isEqualTo(distanceParameters)
    then(distanceWithProvidedSAAlgo.ncd.ncdParameters.bwtParameters.algorithm).isEqualTo(suffixArrayAlgorithm)
  }

  @Test
  fun compute_distance_of_two_items() {
    // given
    val bytes1 = byteArrayOf(1, 2, 3)
    val bytes2 = byteArrayOf(1, 2, 3, 4, 5, 6)
    val item1 = TestItem(bytes1, 1)
    val item2 = TestItem(bytes2, 2)
    // do
    val ncdResult = distance.compute(item1, item2)
    // then
    val expected = 307692
    then(ncdResult).isEqualTo(expected)
  }

  @Test
  fun distance_for_same_data_should_be_null() {
    // given
    val bytes1 = byteArrayOf(1, 2, 3)
    val bytes2 = byteArrayOf(1, 2, 3)
    val item1 = TestItem(bytes1, 1)
    val item2 = TestItem(bytes2, 2)
    // do
    val ncdResult = distance.compute(item1, item2)
    // then
    val expected = 0
    then(expected).isEqualTo(ncdResult)
  }

  @Test
  fun compute_distance_to_all_items() {
    // given
    val bytes1 = byteArrayOf(1, 2, 3)
    val bytes2 = byteArrayOf(1, 2, 3, 4, 5, 6)
    val bytes3 = byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8)
    val item1 = TestItem(bytes1, 1)
    val item2 = TestItem(bytes2, 2)
    val item3 = TestItem(bytes3, 3)
    val items = arrayOf<DistanceItem>(item2, item3)
    // do
    val distances = distance.compute(item1, items)
    // then
    val expected1 = 307692
    val expected2 = 400000
    then(distances[0]).isEqualTo(expected1)
    then(distances[1]).isEqualTo(expected2)
  }

  @Test
  fun compute_distances_between_two_arrays() {
    // Given
    val bytesDataArray1 = arrayOf(bytesData1)
    val bytesDataArray2 = arrayOf(bytesData2)
    val expectedDistancesArray = DistancesArray(1, 1, false)
    expectedDistancesArray.setDistancesForItem(0, intArrayOf(333333))

    // When
    val distancesArray = distance.computeDistancesBetweenCollections(bytesDataArray1, bytesDataArray2)

    // Then
    then(distancesArray).isEqualTo(expectedDistancesArray)
  }

  @Test
  fun compute_distances_between_array_elements() {
    // Given
    val bytesDataArray = arrayOf(bytesData1, bytesData2)

    // NB: due to optimization on same array, matrix is represented as a triangle internally...
    val expectedDistancesArray = DistancesArray(2, 2, true)
    expectedDistancesArray.setDistancesForItem(0, intArrayOf(333333))
    expectedDistancesArray.setDistancesForItem(1, intArrayOf())

    // When
    val distancesArray = distance.computeDistancesInCollection(bytesDataArray)

    // Then
    then(distancesArray).isEqualTo(expectedDistancesArray)
  }

  @org.junit.jupiter.api.Test
  fun `a biz exception is raised in clustering if a thread crash occurs during clustering`() {
    // given
    val dir1 = "/bestioles_comp_dir"

    val progressListener = { _: TreatmentStep, _: Progress -> throw OutOfMemoryError("crash!") }
    val directory1 = File(javaClass.getResource(dir1).file)
    val fileIdProvider = { file: File -> file.name }
    val bytesDatas = BytesData.loadFromDirectory(directory1, fileIdProvider)

    // do
    runCatching { Distance(DistanceParameters.defaults(), progressListener).computeDistancesInCollection(bytesDatas) }
      // then
      .onSuccess { fail("should throw") }
      .onFailure {
        then(it).isInstanceOf(DistancesThreadException::class.java)
        val ex = it as DistancesThreadException
        then(ex.exceptionName).isEqualTo("ExecutionException")
        then(ex.exceptionMessage).isEqualTo("java.lang.OutOfMemoryError: crash!")
        then(ex.causeName).isEqualTo("OutOfMemoryError")
        then(ex.causeMessage).isEqualTo("crash!")
      }
  }
}