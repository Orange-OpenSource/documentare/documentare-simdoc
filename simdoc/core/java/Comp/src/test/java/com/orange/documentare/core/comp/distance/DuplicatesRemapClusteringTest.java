package com.orange.documentare.core.comp.distance;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.Cluster;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.fest.assertions.Assertions.assertThat;

public class DuplicatesRemapClusteringTest {

  @Test
  public void add_duplicates_to_clustering_and_check_elements_are_remapped_correctly() {
    // given
    String[] idsWithDuplicates = {"0", "1", "2", "3", "4"};
    String[] idsNoDuplicate = {"0", "1", "3"};
    Duplicates duplicates = new Duplicates();
    duplicates.add(2, 1);
    duplicates.add(4, 1);

    List<ClusteringElement> noDuplicatesElements = buildNoDuplicatesElements(idsNoDuplicate);
    Clustering cl = new Clustering(noDuplicatesElements, null, null, null, null, null);

    // when
    cl = duplicates.addDuplicatesToClustering(cl, idsWithDuplicates);

    // then
    assertThat(cl.elements).hasSize(5);

    assertThat(cl.elements.get(0).clusterId).isEqualTo(0);
    assertThat(cl.elements.get(1).clusterId).isEqualTo(1);
    assertThat(cl.elements.get(2).clusterId).isEqualTo(1);
    assertThat(cl.elements.get(3).clusterId).isEqualTo(3);
    assertThat(cl.elements.get(4).clusterId).isEqualTo(1);

    assertThat(cl.elements.get(0).duplicateOf).isNull();
    assertThat(cl.elements.get(1).duplicateOf).isNull();
    assertThat(cl.elements.get(2).duplicateOf).isEqualTo(1);
    assertThat(cl.elements.get(3).duplicateOf).isNull();
    assertThat(cl.elements.get(4).duplicateOf).isEqualTo(1);
  }

  @Test
  public void map_no_duplicates_indices_to_indices_with_duplicates() {
    // given
    Collection<Integer> duplicatesIndices = Arrays.asList(0, 3, 6, 9);
    int elementsCount = 10;

    // when
    Map<Integer, Integer> map =
      Duplicates.mapNoDuplicatesToWithDuplicates(duplicatesIndices, elementsCount);

    // then
    assertThat(map.get(0)).isEqualTo(1);
    assertThat(map.get(1)).isEqualTo(2);
    assertThat(map.get(2)).isEqualTo(4);
    assertThat(map.get(3)).isEqualTo(5);
    assertThat(map.get(4)).isEqualTo(7);
    assertThat(map.get(5)).isEqualTo(8);
  }

  @Test
  public void add_duplicates_to_clustering_and_check_multiset_elements_are_remapped_correctly() {
    // given
    String[] idsWithDuplicates = {"0", "1", "2", "3", "4", "5", "6"};
    String[] idsNoDuplicate = {"1", "4", "5"};
    Duplicates duplicates = new Duplicates();
    duplicates.add(0, 1);
    duplicates.add(2, 1);
    duplicates.add(3, 1);
    duplicates.add(6, 1);

    List<ClusteringElement> noDuplicatesElements = buildNoDuplicatesElements(idsNoDuplicate);
    List<Cluster> clusters = clusterListWithOneClusterWithMultisetElementsIndices(0, 1, 2);
    Clustering cl = new Clustering(noDuplicatesElements, null, clusters, null, null, null);

    // when
    cl = duplicates.addDuplicatesToClustering(cl, idsWithDuplicates);

    // then
    assertThat(cl.clusters.get(0).multisetElementsIndices).containsExactly(1, 4, 5);
  }

  @Test
  public void add_duplicates_to_clustering_and_check_multiset_elements_are_remapped_correctly_for_all_clusters() {
    // given
    String[] idsWithDuplicates = {"0", "1", "2", "3", "4"};
    String[] idsNoDuplicate = {"0", "1", "3"};
    Duplicates duplicates = new Duplicates();
    duplicates.add(2, 1);
    duplicates.add(4, 1);

    List<ClusteringElement> noDuplicatesElements = buildNoDuplicatesElements(idsNoDuplicate);
    List<Cluster> clusters = clusterListWithTwoClustersWithMultisetElementsIndices(1, 2);
    Clustering cl = new Clustering(noDuplicatesElements, null, clusters, null, null, null);

    // when
    cl = duplicates.addDuplicatesToClustering(cl, idsWithDuplicates);

    // then
    assertThat(cl.clusters.get(0).multisetElementsIndices).containsExactly(1);
    assertThat(cl.clusters.get(1).multisetElementsIndices).containsExactly(3);
  }


  private List<Cluster> clusterListWithOneClusterWithMultisetElementsIndices(Integer... multisetElementsIndices) {
    List<Integer> indices = Arrays.asList(multisetElementsIndices);
    Cluster cluster = new Cluster(null, indices);
    return Arrays.asList(cluster);
  }

  private List<Cluster> clusterListWithTwoClustersWithMultisetElementsIndices(int index1, int index2) {
    List<Integer> indices = Arrays.asList(index1);
    Cluster cluster1 = new Cluster(null, indices);
    indices = Arrays.asList(index2);
    Cluster cluster2 = new Cluster(null, indices);
    return Arrays.asList(cluster1, cluster2);
  }

  private List<ClusteringElement> buildNoDuplicatesElements(String[] idsNoDuplicate) {
    return Arrays.stream(idsNoDuplicate)
      .map(id -> new ClusteringElement(id, Integer.parseInt(id), null, null, null, null))
      .collect(Collectors.toList());
  }
}
