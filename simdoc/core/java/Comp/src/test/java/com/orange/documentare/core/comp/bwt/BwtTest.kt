package com.orange.documentare.core.comp.bwt

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class BwtTest {
    @Test
    fun sa_algo_is_chosen_thanks_to_bwt_parameters() {
      var bwt = Bwt(BwtParameters(SuffixArrayAlgorithm.LIBDIVSUFSORT))
      then(bwt.saAlgo).isEqualTo(bwt.libDivSufSort)

      bwt = Bwt(BwtParameters(SuffixArrayAlgorithm.SAIS))
      then(bwt.saAlgo).isEqualTo(bwt.sais)
    }
}