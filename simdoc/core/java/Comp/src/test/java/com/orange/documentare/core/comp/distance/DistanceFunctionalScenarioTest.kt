/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.distance

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData.FileIdProvider
import com.orange.documentare.core.comp.distance.matrix.DistancesMatrixCsvGzipWriter
import com.orange.documentare.core.model.io.Gzip.getStringFromGzipFile
import org.apache.commons.io.FileUtils
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.io.File
import java.io.IOException

class DistanceFunctionalScenarioTest {

  @AfterEach
  fun cleanup() {
    FileUtils.deleteQuietly(TEST_MATRIX_OUTPUT)
    FileUtils.deleteQuietly(TEST_NEARESTS_OUTPUT)
    FileUtils.deleteQuietly(TEST_MATRIX_OUTPUT_SAME_ARRAY)
    FileUtils.deleteQuietly(TEST_NEARESTS_OUTPUT_SAME_ARRAY)
  }

  @Test
  @Throws(IOException::class)
  fun compute_distances_matrix_between_elements_of_a_directory() {
    shouldCompute(TEST_DIRECTORY_A, TEST_DIRECTORY_A, TEST_REFERENCE_AA, false)
  }

  @Test
  @Throws(IOException::class)
  fun compute_distances_matrix_between_two_directories() {
    shouldCompute(TEST_DIRECTORY_A, TEST_DIRECTORY_B, TEST_REFERENCE_AB, false)
  }

  @Test
  @Throws(IOException::class)
  fun compute_distances_nearest_array_between_two_directories() {
    shouldCompute(TEST_DIRECTORY_A, TEST_DIRECTORY_B, TEST_REFERENCE_AB_NEARESTS, true)
  }

  @Throws(IOException::class)
  fun shouldCompute(dir1: String, dir2: String, ref: String, nearestsMode: Boolean) {
    // given
    val directory1 = File(javaClass.getResource(dir1).file)
    val directory2 = File(javaClass.getResource(dir2).file)
    val fileIdProvider = FileIdProvider { obj: File -> obj.name }
    val bytesDatas1 = BytesData.loadFromDirectory(directory1, fileIdProvider)
    val bytesDatas2 = BytesData.loadFromDirectory(directory2, fileIdProvider)
    val refFileResource = getStringFromGzipFile(File(javaClass.getResource(ref).file))

    // do
    val distancesArray = Distance(DistanceParameters.defaults()).computeDistancesBetweenCollections(bytesDatas1, bytesDatas2)
    val writer = DistancesMatrixCsvGzipWriter(bytesDatas1, if (directory1 == directory2) bytesDatas1 else bytesDatas2, distancesArray)
    writer.writeTo(if (nearestsMode) TEST_NEARESTS_OUTPUT else TEST_MATRIX_OUTPUT, nearestsMode)
    val out = getStringFromGzipFile(if (nearestsMode) TEST_NEARESTS_OUTPUT else TEST_MATRIX_OUTPUT)

    // then
    then(refFileResource).isEqualTo(out)
  }

  companion object {
    private const val TEST_DIRECTORY_A = "/bestioles_comp_dir"
    private const val TEST_DIRECTORY_B = "/comp_dir"
    private const val TEST_REFERENCE_AA = "/bestioles.reference.gz"
    private const val TEST_REFERENCE_AB = "/a2b_matrix.reference.gz"
    private const val TEST_REFERENCE_AB_NEARESTS = "/a2b_nearests.reference.gz"
    private val TEST_MATRIX_OUTPUT = File("matrix.csv.gz")
    private val TEST_NEARESTS_OUTPUT = File("nearests.csv.gz")
    private val TEST_MATRIX_OUTPUT_SAME_ARRAY = File("matrix_same_array.csv.gz")
    private val TEST_NEARESTS_OUTPUT_SAME_ARRAY = File("nearests_same_array.csv.gz")
  }
}