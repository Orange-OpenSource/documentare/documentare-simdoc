package com.orange.documentare.core.comp.distance

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class DistanceParametersTest {

  @Test
  internal fun `default parameters uses libDivSufSort suffix array algorithm`() {
    then(DistanceParameters.defaults().suffixArrayAlgorithm).isEqualTo(SuffixArrayAlgorithm.LIBDIVSUFSORT)
  }
}