package com.orange.documentare.core.comp.stats

import com.orange.documentare.core.comp.clustering.stats.SummaryStats
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class SummaryStatsTest {

  @Test
  fun `init values`() {
    // when
    val summaryStats = SummaryStats()

    then(summaryStats.getCount()).isEqualTo(0)
    then(summaryStats.getMin()).isEqualTo(Double.POSITIVE_INFINITY)
    then(summaryStats.getMax()).isEqualTo(Double.NEGATIVE_INFINITY)
    then(summaryStats.getMean()).isNaN
  }

  @Test
  fun `clear values`() {
    // given
    val list = listOf(-10.1, -1.2, 51.3, 25.6)
    val summaryStats = SummaryStats()

    // when
    list.forEach { summaryStats.addValue(it) }
    summaryStats.clear()

    then(summaryStats.getCount()).isEqualTo(0)
    then(summaryStats.getMin()).isEqualTo(Double.POSITIVE_INFINITY)
    then(summaryStats.getMax()).isEqualTo(Double.NEGATIVE_INFINITY)
    then(summaryStats.getMean()).isNaN
  }

  @Test
  fun `compute Double(s) min max mean`() {
    // given
    val list = listOf(-10.1, -1.2, 51.3, 25.6)
    val summaryStats = SummaryStats()

    // when
    list.forEach { summaryStats.addValue(it) }

    then(summaryStats.getCount()).isEqualTo(4)
    then(summaryStats.getMin()).isEqualTo(-10.1)
    then(summaryStats.getMax()).isEqualTo(51.3)
    then(summaryStats.getMean()).isEqualTo(16.4)
  }
}
