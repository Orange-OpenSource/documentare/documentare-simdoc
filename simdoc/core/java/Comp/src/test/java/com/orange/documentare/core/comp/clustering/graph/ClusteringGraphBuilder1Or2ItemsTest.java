package com.orange.documentare.core.comp.clustering.graph;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder;
import com.orange.documentare.core.model.ref.clustering.*;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class ClusteringGraphBuilder1Or2ItemsTest {

  @Test
  public void compute_graph_for_no_item() {
    // given
    ClusteringItem[] clusteringItems = new Item[0];
    ClusteringParameters parameters = ClusteringParameters.builder().build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

    // do
    Clustering clustering =
      clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    List<ClusteringElement> elements = clustering.elements;
    List<Subgraph> subGraphs = clustering.subgraphs;
    List<Cluster> clusters = clustering.clusters;
    assertThat(elements).hasSize(0);
    assertThat(subGraphs).hasSize(0);
    assertThat(clusters).hasSize(0);
  }

  @Test
  public void compute_graph_for_1_item() {
    // given
    Item item = Item.withHumanReadableId("0");
    item.setNearestItems(new NearestItem[]{new NearestItem(0, 0)});
    ClusteringItem[] clusteringItems = new Item[]{item};
    ClusteringParameters parameters = ClusteringParameters.builder().build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

    // do
    Clustering clustering =
      clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    List<ClusteringElement> elements = clustering.elements;
    List<Subgraph> subGraphs = clustering.subgraphs;
    List<Cluster> clusters = clustering.clusters;
    assertThat(elements).hasSize(1);
    assertThat(elements.get(0).clusterId).isEqualTo(0);
    assertThat(elements.get(0).clusterCenter()).isTrue();
    assertThat(elements.get(0).enrolled).isFalse();
    assertThat(subGraphs).hasSize(1);
    assertThat(clusters).hasSize(1);
  }

  @Test
  public void compute_graph_for_2_item2() {
    // given
    Item item0 = Item.withHumanReadableId("0");
    Item item1 = Item.withHumanReadableId("1");
    item0.setNearestItems(new NearestItem[]{new NearestItem(0, 0), new NearestItem(1, 10)});
    ClusteringItem[] clusteringItems = new Item[]{item0, item1};
    ClusteringParameters parameters = ClusteringParameters.builder().build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

    // do
    Clustering clustering =
      clusteringGraphBuilder.doClustering(clusteringItems, parameters);

    // then
    List<ClusteringElement> elements = clustering.elements;
    List<Subgraph> subGraphs = clustering.subgraphs;
    List<Cluster> clusters = clustering.clusters;
    assertThat(elements).hasSize(2);
    assertThat(elements.get(0).clusterId).isEqualTo(0);
    assertThat(elements.get(0).clusterCenter()).isTrue();
    assertThat(elements.get(1).clusterId).isEqualTo(0);
    assertThat(elements.get(1).clusterCenter).isNull();

    assertThat(subGraphs).hasSize(1);
    assertThat(clusters).hasSize(1);
  }
}
