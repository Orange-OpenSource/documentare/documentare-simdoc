package com.orange.documentare.core.comp.ncd.cache

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class CacheTest {

  private val maxSize: Long = 10
  private val cache = Cache<String, Int>(maxSize)

  @Test
  fun `returns null optional if element is not in cache`() {
    // when
    val valFromCache = cache.getIfPresent("1")

    then(valFromCache).isNull()
  }

  @Test
  fun `adding a value in cache increases its size`() {
    // given
    val initialSize = cache.size()

    // when
    cache.put("1", 2)
    val actualSize = cache.size()

    then(actualSize).isEqualTo(initialSize + 1)
  }

  @Test
  fun `can retrieve a value present in cache`() {
    // given
    cache.put("1", 10)

    // when
    val valFromCache = cache.getIfPresent("1")

    then(valFromCache).isEqualTo(10)
  }

  @Test
  fun `cache size is limited to its max size`() {
    // when
    (0..2 * maxSize).forEach { cache.put("$it", it.toInt()) }

    then(cache.size()).isEqualTo(maxSize)
  }
}