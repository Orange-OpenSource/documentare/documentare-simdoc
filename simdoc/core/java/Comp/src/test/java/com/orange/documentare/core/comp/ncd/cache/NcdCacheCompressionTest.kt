package com.orange.documentare.core.comp.ncd.cache

import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


class NcdCacheCompressionTest {

  @BeforeEach
  internal fun setup() {
    NcdCache.clear()
  }


  @Test
  fun `we can know that a compression result is not in the cache`() {
    // given
    val key = "1234"

    // when
    val compressionResult: Float? = NcdCache.getCompression(key)

    // then
    assertThat(compressionResult).isNull()
  }

  @Test
  fun `we can add a compression result in the cache, then retrieve it`() {
    // given
    val key = "1234"
    val result = 0.3f
    val computationDuration = 123

    // when
    NcdCache.addCompression(key, result, computationDuration)
    val retrievedResult = NcdCache.getCompression(key)
    val retrievedComputationDuration = NcdCache.getComputationDuration(key)

    // then
    assertThat(retrievedResult).isEqualTo(0.3f)
    assertThat(retrievedComputationDuration).isEqualTo(123)
  }

  @Test
  fun `cache hits and misses are computed`() {
    // given
    val key = "1234"
    val result = 0.3f
    val computationDuration = 123

    // when
    NcdCache.getCompression(key)
    NcdCache.getCompression(key)
    NcdCache.getCompression(key)
    NcdCache.getCompression(key)

    NcdCache.addCompression(key, result, computationDuration)
    NcdCache.getCompression(key)
    NcdCache.getCompression(key)

    // then
    assertThat(NcdCache.compressionHit()).isEqualTo(2)
    assertThat(NcdCache.compressionMiss()).isEqualTo(4)
  }
}
