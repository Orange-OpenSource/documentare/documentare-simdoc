/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.graph.graphbuilder

import com.orange.documentare.core.comp.clustering.graph.Item
import com.orange.documentare.core.comp.clustering.graph.Item.ItemInit
import com.orange.documentare.core.comp.clustering.graph.voronoi.Voronoi
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.comp.NearestItem
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.io.File

class GraphvizTmpFilesAreCleanedTest : ItemInit {

  @Test
  fun `voronoi graphviz tmp files are deleted after treatment`() {
    // given
    val clusteringItems = Item.buildClusteringItems(this, 7)
    val parameters = ClusteringParameters.builder().build()
    val clusteringGraphBuilder = ClusteringGraphBuilder()

    val beforeTestTmpFilesCount = voronoiTmpFiles()

    // when
    clusteringGraphBuilder.doClustering(clusteringItems, parameters)

    // then
    val afterTestTmpFilesCount = voronoiTmpFiles()
    then(beforeTestTmpFilesCount.size - afterTestTmpFilesCount.size).isEqualTo(0)
  }

  private fun voronoiTmpFiles() =
    File(Voronoi.TMP_DIR_PATH).list()?.filter { it.contains(Voronoi.TMP_FILE_PREFIX) } ?: emptyList()

  override fun init(items: Array<Item>) {
    items.let {
      it[0].nearestItems = arrayOf(NearestItem(0, 0), NearestItem(1, 10), NearestItem(2, 10), NearestItem(3, 200), NearestItem(4, 200), NearestItem(5, 200), NearestItem(6, 1000))
      it[1].nearestItems = arrayOf(NearestItem(1, 0), NearestItem(0, 10), NearestItem(2, 10), NearestItem(3, 200), NearestItem(4, 200), NearestItem(5, 200), NearestItem(6, 1000))
      it[2].nearestItems = arrayOf(NearestItem(2, 0), NearestItem(0, 10), NearestItem(1, 10), NearestItem(3, 200), NearestItem(4, 200), NearestItem(5, 200), NearestItem(6, 1000))
      it[3].nearestItems = arrayOf(NearestItem(3, 0), NearestItem(4, 100), NearestItem(5, 102), NearestItem(0, 200), NearestItem(1, 200), NearestItem(2, 200), NearestItem(6, 1000))
      it[4].nearestItems = arrayOf(NearestItem(4, 0), NearestItem(3, 100), NearestItem(5, 101), NearestItem(0, 200), NearestItem(1, 200), NearestItem(2, 200), NearestItem(6, 1000))
      it[5].nearestItems = arrayOf(NearestItem(5, 0), NearestItem(4, 101), NearestItem(3, 102), NearestItem(0, 200), NearestItem(1, 200), NearestItem(2, 200), NearestItem(6, 1000))
      it[6].nearestItems = arrayOf(NearestItem(6, 0), NearestItem(0, 1000), NearestItem(1, 1000), NearestItem(2, 1000), NearestItem(3, 1000), NearestItem(4, 1000), NearestItem(5, 1000))
    }
  }
}