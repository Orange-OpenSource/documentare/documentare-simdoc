package com.orange.documentare.core.comp.clustering.graph.scissors.clusterlongedges;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.GraphItems;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphGroup;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import lombok.AccessLevel;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

public class ClusterLongEdgesTriggerTest {
  private static final float PERCENTILE = 75;
  private final List<GraphItem> graphItems = getGraphItems();

  @Getter(AccessLevel.PACKAGE)
  private ClusterLongEdgesTrigger clusterLongEdgesTrigger;

  @BeforeEach
  public void setup() {
    clusterLongEdgesTrigger = new ClusterLongEdgesTrigger(graphItems, PERCENTILE);
  }

  @Test
  public void shouldComputeThreshold() {
    // given
    GraphGroup graphGroup = getGraphGroup();
    // do
    clusterLongEdgesTrigger.initForGroup(graphGroup);

    then(250).isEqualTo(clusterLongEdgesTrigger.getEdgeLengthThreshold());
  }

  @Test
  public void shouldCutOrNot() {
    // given
    GraphGroup graphGroup = getGraphGroup();
    clusterLongEdgesTrigger.initForGroup(graphGroup);
    // do
    // then
    for (int i = 0; i < 3; i++) {
      then(clusterLongEdgesTrigger.shouldRemove(graphGroup.edges.get(i))).isFalse();
    }
    then(clusterLongEdgesTrigger.shouldRemove(graphGroup.edges.get(3))).isTrue();
  }

  private List<GraphItem> getGraphItems() {
    return new GraphItems();
  }

  private GraphGroup getGraphGroup() {
    GraphGroup graphGroup = new GraphGroup() {
    };

    List<Integer> itemIndices = new ArrayList<>();
    for (int i = 0; i < graphItems.size(); i++) {
      itemIndices.add(i);
    }
    graphGroup.itemIndices.addAll(itemIndices);

    graphGroup.edges.add(new GraphEdge(0, 1, 50));
    graphGroup.edges.add(new GraphEdge(1, 0, 50));
    graphGroup.edges.add(new GraphEdge(0, 2, 100));
    graphGroup.edges.add(new GraphEdge(1, 2, 300));
    return graphGroup;
  }
}
