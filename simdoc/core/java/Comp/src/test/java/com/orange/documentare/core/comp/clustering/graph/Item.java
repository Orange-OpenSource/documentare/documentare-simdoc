package com.orange.documentare.core.comp.clustering.graph;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.TriangleVertices;

import java.util.Optional;

public class Item implements ClusteringItem {

  public static Item withHumanReadableId(String humanId) {
    Item item = new Item();
    item.humanReadableId = humanId;
    return item;
  }

  public interface ItemInit {
    void init(Item[] items);
  }

  public String humanReadableId;
  public int itemId;
  public Float nearestTriangleArea;
  public Integer clusterId;
  public Boolean clusterCenter;
  public NearestItem[] nearestItems;
  public byte[] bytes;

  public TriangleVertices triangleVertices;

  public Integer duplicateOf;

  // FIXME: still used in real?
  /**
   * reversed fileName to speed up string comparison during Ncd. Useful here to create a "visible" output debugEnabled directory
   */
  public String fileNameReversed;

  @Override
  public Optional<Integer> duplicateOf() {
    return Optional.ofNullable(duplicateOf);
  }

  @Override
  public Integer getClusterId() {
    return clusterId;
  }

  @Override
  public NearestItem[] getNearestItems() {
    return nearestItems;
  }

  public void setNearestItems(NearestItem[] nearestItems) {
    this.nearestItems = nearestItems;
  }

  @Override
  public boolean triangleVerticesAvailable() {
    return triangleVertices != null;
  }

  @Override
  public TriangleVertices getTriangleVertices() {
    return triangleVertices;
  }

  @Override
  public String getHumanReadableId() {
    return humanReadableId;
  }

  @Override
  public byte[] getBytes() {
    return bytes;
  }

  public static ClusteringItem[] buildClusteringItems(ItemInit itemInit, int nbItems) {
    Item[] items = buildItems(nbItems);
    itemInit.init(items);

    int knn = items.length;
    for (Item item : items) {
      item.triangleVertices = new TriangleVertices(item, items, knn);
    }
    return items;
  }

  private static Item[] buildItems(int nbItems) {
    Item[] items = new Item[nbItems];
    for (int i = 0; i < items.length; i++) {
      items[i] = new Item();
      items[i].humanReadableId = String.valueOf(i);
    }
    return items;
  }
}
