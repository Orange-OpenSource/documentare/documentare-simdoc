package com.orange.documentare.core.comp.ncd

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.bwt.BwtParameters
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.ncd.cache.BytesUniqueHashKey
import com.orange.documentare.core.comp.ncd.cache.NcdCache
import com.orange.documentare.core.comp.ncd.cache.NcdCacheKeys
import org.assertj.core.api.BDDAssertions.then
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.mockito.Mockito.*

class NcdTest {
  private val ncd = Ncd(NcdParameters(BwtParameters(SuffixArrayAlgorithm.LIBDIVSUFSORT)))

  @BeforeEach
  internal fun setup() {
    NcdCache.clear()
  }

  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `bwt parameters are built with ncd parameters`(suffixArrayAlgorithm: SuffixArrayAlgorithm) {
    // given
    val ncdWithProvidedSAAlgo = Ncd(NcdParameters(BwtParameters(suffixArrayAlgorithm)))
    // then
    then(ncdWithProvidedSAAlgo.bwt.bwtParameters.algorithm).isEqualTo(suffixArrayAlgorithm)
  }

  @Test
  fun `a log appears if arrays are equal from the bytes md5sum but ncd is not 0`() {
    // given
    val mockedNcd = spy(ncd)
    val bytes = stringAlphabet.toByteArray()
    `when`(mockedNcd.doComputeNcd(bytes, bytes, NcdCacheKeys(bytes, bytes))).thenReturn(0.001f)
    // when
    mockedNcd.computeNcd(bytes, bytes)
  }

  @Test
  fun `can retrieve ncd from cache`() {
    // given
    val mockedNcd = mock(Ncd::class.java)
    val bytes1 = smallString1.toByteArray()
    val bytes2 = smallString2.toByteArray()
    NcdCache.addNcd(NcdCacheKeys(bytes1, bytes2), 0.12345f, 100, NcdParameters(BwtParameters(SuffixArrayAlgorithm.SAIS)))

    // when
    val ncdResult = mockedNcd.computeNcd(bytes1, bytes2)

    // then
    assertThat(ncdResult).isEqualTo(0.12345f)
    verify(mockedNcd, never()).doComputeNcd(bytes1, bytes2, NcdCacheKeys(bytes1, bytes2))
  }

  @Test
  fun `can NOT retrieve ncd from cache`() {
    // given
    val mockedNcd = spy(ncd)
    val bytes1 = smallString1.toByteArray()
    val bytes2 = smallString2.toByteArray()

    // when
    mockedNcd.computeNcd(bytes1, bytes2)

    // then
    verify(mockedNcd).doComputeNcd(bytes1, bytes2, NcdCacheKeys(bytes1, bytes2))
  }

  @Test
  fun compression_length_available_after_computation() {
    // given
    val bytes = stringAlphabet.toByteArray()
    // do
    ncd.computeNcd(bytes, bytes)
    // then
    assertThat(NcdCache.getCompression(BytesUniqueHashKey.buildUniqueHashKey(bytes))).isEqualTo(210f)
  }

  @Test
  fun distance_on_same_ncd_input() {
    // given
    val bytes = stringAlphabet.toByteArray()
    // do
    val ncdResult = ncd.computeNcd(bytes, bytes)
    // then
    assertThat(ncdResult).isEqualTo(0f)
  }


  @Test
  fun distance_on_distinct_inputs_but_same_data_is_null() {
    // given
    val bytes1 = stringAlphabet.toByteArray()
    val bytes2 = stringAlphabet.toByteArray().copyOf(stringAlphabet.length)
    // do
    val ncdResult = ncd.computeNcd(bytes1, bytes2)
    // then
    assertThat(ncdResult).isEqualTo(0f)
  }

  @Test
  fun compute_distance_on_strings() {
    // given
    val inputs = buildInputs()

    // do
    val ncdResult01 = ncd.computeNcd(inputs[0], inputs[1])
    val ncdResult23 = ncd.computeNcd(inputs[2], inputs[3])
    val ncdResult45 = ncd.computeNcd(inputs[4], inputs[5])

    // then
    assertThat(ncdResult01).isEqualTo(0f)
    assertThat(ncdResult23).isEqualTo(0.5714286f)
    assertThat(ncdResult45).isEqualTo(0.45454547f)
    assertThat(NcdCache.getCompression(BytesUniqueHashKey.buildUniqueHashKey(inputs[0]))).isEqualTo(210f)
    assertThat(NcdCache.getCompression(BytesUniqueHashKey.buildUniqueHashKey(inputs[1]))).isEqualTo(210f)
    assertThat(NcdCache.getCompression(BytesUniqueHashKey.buildUniqueHashKey(inputs[2]))).isEqualTo(210f)
    assertThat(NcdCache.getCompression(BytesUniqueHashKey.buildUniqueHashKey(inputs[3]))).isEqualTo(100f)
  }

  private fun buildInputs(): Array<ByteArray> {
    return arrayOf(stringAlphabet.toByteArray(), stringAlphabet.toByteArray(), stringAlphabet.toByteArray(), stringAlphabet2.toByteArray(), smallString1.toByteArray(), smallString2.toByteArray())
  }

  companion object {
    private const val smallString1 = "papa"
    private const val smallString2 = "toto"
    private const val stringAlphabet = "aaaabbbbccccddddeeeeffffgggghhhhiiiijjjjkkkkllllmmmmonnnpoooppppqqqqrrrrssssttttuuuuvvvvwwwwxxxxyyyyzzzz"
    private const val stringAlphabet2 = "aaaarrrrssssttttuuuuvvvvwwwwxxxxyyyyzzzz"
  }
}
