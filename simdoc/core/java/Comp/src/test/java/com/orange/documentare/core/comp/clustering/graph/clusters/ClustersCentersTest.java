package com.orange.documentare.core.comp.clustering.graph.clusters;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.graph.GraphCluster;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import lombok.RequiredArgsConstructor;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.fest.assertions.Assertions.assertThat;

public class ClustersCentersTest {
  private final List<GraphItem> items = new ArrayList<>();
  private final Collection<GraphCluster> clusters = new ArrayList<>();

  @Test
  public void a_singleton_is_a_cluster_center() {
    // given
    GraphItem singleton = new GraphItem();
    items.add(singleton);
    GraphCluster cluster = new GraphCluster();
    cluster.itemIndices.add(0);
    clusters.add(cluster);

    // when
    List<Integer> centers = ClustersCenters.findCenters(items, clusters);

    // then
    assertThat(centers).containsExactly(0);
  }

  @Test
  public void centers_are_found_for_two_distinct_clusters() {
    // given
    TestItemsAndClusters testItemsAndClusters = eightItemsTwoClustersWithCentersAtIndex3And6();
    items.addAll(testItemsAndClusters.graphItems);
    clusters.addAll(testItemsAndClusters.clusters);

    // when
    List<Integer> centers = ClustersCenters.findCenters(items, clusters);

    // then
    assertThat(centers).containsExactly(2, 6);
  }

  @RequiredArgsConstructor
  class TestItemsAndClusters {
    final List<GraphItem> graphItems;
    final List<GraphCluster> clusters;
  }

  private TestItemsAndClusters eightItemsTwoClustersWithCentersAtIndex3And6() {
    List<GraphItem> items = new ArrayList<>();
    IntStream.range(0, 8).forEach(index -> {
      GraphItem item = new GraphItem();
      item.vertex1Index = index;
      items.add(item);
    });

    GraphCluster cluster1 = new GraphCluster();
    Collections.addAll(cluster1.itemIndices, 0, 1, 2, 3);
    GraphCluster cluster2 = new GraphCluster();
    Collections.addAll(cluster2.itemIndices, 4, 5, 6, 7);

    Collections.addAll(cluster1.edges,
      new GraphEdge(0, 2, 10),
      new GraphEdge(1, 2, 10),
      new GraphEdge(2, 0, 10),
      new GraphEdge(3, 2, 10)
    );
    Collections.addAll(cluster2.edges,
      new GraphEdge(4, 6, 10),
      new GraphEdge(5, 6, 10),
      new GraphEdge(6, 4, 10),
      new GraphEdge(7, 6, 10)
    );
    List<GraphCluster> clusters = new ArrayList<>();
    Collections.addAll(clusters, cluster1, cluster2);
    return new TestItemsAndClusters(items, clusters);
  }
}