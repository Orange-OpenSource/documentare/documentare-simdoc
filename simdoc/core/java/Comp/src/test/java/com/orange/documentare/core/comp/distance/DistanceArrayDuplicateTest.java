package com.orange.documentare.core.comp.distance;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import org.junit.Test;

import java.util.stream.IntStream;

import static org.fest.assertions.Assertions.assertThat;

public class DistanceArrayDuplicateTest {

  @Test
  public void find_no_duplicate() {
    // given
    DistancesArray distancesArray = new DistancesArray(2, 2, false);
    distancesArray.setDistancesForItem(0, new int[]{0, 100});
    distancesArray.setDistancesForItem(1, new int[]{100, 0});

    // when
    Duplicates duplicates = distancesArray.duplicates();

    // then
    assertThat(duplicates.count()).isEqualTo(0);
    assertThat(duplicates.indicesWithoutDuplicates(2)).containsExactly(0, 1);
  }

  @Test
  public void elem0_is_not_a_duplicate_but_elem1_is_a_duplicate_of_elem_0() {
    // given
    DistancesArray distancesArray = new DistancesArray(2, 2, false);
    distancesArray.setDistancesForItem(0, new int[]{0, 0});
    distancesArray.setDistancesForItem(1, new int[]{0, 0});

    // when
    Duplicates duplicates = distancesArray.duplicates();

    // then
    assertThat(duplicates.count()).isEqualTo(1);
    assertThat(duplicates.forIndex(0).isPresent()).isTrue();
    assertThat(duplicates.forIndex(0).get()).contains(1);
    assertThat(duplicates.indicesWithoutDuplicates(2)).containsExactly(0);
  }

  @Test
  public void elem0_has_two_duplicates() {
    // given
    DistancesArray distancesArray = new DistancesArray(3, 3, false);
    distancesArray.setDistancesForItem(0, new int[]{0, 0, 0});
    distancesArray.setDistancesForItem(1, new int[]{0, 0, 0});
    distancesArray.setDistancesForItem(2, new int[]{0, 0, 0});

    // when
    Duplicates duplicates = distancesArray.duplicates();

    // then
    assertThat(duplicates.count()).isEqualTo(2);
    assertThat(duplicates.forIndex(0).isPresent()).isTrue();
    assertThat(duplicates.forIndex(0).get()).containsExactly(1, 2);
    assertThat(duplicates.indicesWithoutDuplicates(2)).containsExactly(0);
  }

  @Test
  public void elem0_is_not_a_duplicate_but_elem3_is_a_duplicate_of_elem_0() {
    // given
    DistancesArray distancesArray = new DistancesArray(4, 4, false);
    distancesArray.setDistancesForItem(0, new int[]{0, 10, 10, 0});
    distancesArray.setDistancesForItem(1, new int[]{10, 0, 10, 10});
    distancesArray.setDistancesForItem(2, new int[]{10, 10, 0, 10});
    distancesArray.setDistancesForItem(3, new int[]{0, 10, 10, 0});

    // when
    Duplicates duplicates = distancesArray.duplicates();

    // then
    assertThat(duplicates.count()).isEqualTo(1);
    assertThat(duplicates.forIndex(0).isPresent()).isTrue();
    assertThat(duplicates.forIndex(0).get()).contains(3);
    assertThat(duplicates.indicesWithoutDuplicates(4)).containsExactly(0, 1, 2);
  }

  @Test
  public void find_duplicate_on_non_optimized_matrix() {
    // given
    DistancesArray distancesArray = non_optimized_matrix();

    // when
    Duplicates duplicates = distancesArray.duplicates();

    // then
    assertThat(duplicates.count()).isEqualTo(1);
    assertThat(duplicates.forIndex(0).isPresent()).isTrue();
    assertThat(duplicates.forIndex(0).get()).contains(3);
    assertThat(duplicates.indicesWithoutDuplicates(9)).containsExactly(0, 1, 2, 4, 5, 6, 7, 8);
  }

  @Test
  public void find_duplicate_on_optimized_matrix() {
    // given
    DistancesArray distancesArray = optimized_matrix();

    // when
    Duplicates duplicates = distancesArray.duplicates();

    // then
    assertThat(duplicates.count()).isEqualTo(1);
    assertThat(duplicates.forIndex(0).isPresent()).isTrue();
    assertThat(duplicates.forIndex(0).get()).contains(3);
    assertThat(duplicates.indicesWithoutDuplicates(9)).containsExactly(0, 1, 2, 4, 5, 6, 7, 8);
  }

  @Test
  public void build_matrix_without_duplicate_on_non_optimized_matrix() {
    // given
    DistancesArray distancesArray = non_optimized_matrix();

    // when
    DistancesArray noDuplicatesArray = distancesArray.removeDuplicates();

    // then
    assertThat(noDuplicatesArray.distancesArray).hasSize(8);
    assertThat(noDuplicatesArray.distancesArray[0]).isEqualTo(new int[]{123809, 412556, 885714, 880952, 857142, 847619, 838095});
    assertThat(noDuplicatesArray.distancesArray[2]).isEqualTo(new int[]{856502, 865470, 905829, 878923, 860986});
    assertThat(noDuplicatesArray.distancesArray[3]).isEqualTo(new int[]{125714, 819148, 806122, 837320});
  }

  @Test
  public void build_matrix_without_duplicate_on_optimized_matrix() {
    // given
    DistancesArray distancesArray = optimized_matrix();

    // when
    DistancesArray noDuplicatesArray = distancesArray.removeDuplicates();

    // then
    assertThat(noDuplicatesArray.distancesArray).hasSize(8);
    assertThat(noDuplicatesArray.distancesArray[0]).isEqualTo(new int[]{123809, 412556, 885714, 880952, 857142, 847619, 838095});
    assertThat(noDuplicatesArray.distancesArray[2]).isEqualTo(new int[]{856502, 865470, 905829, 878923, 860986});
    assertThat(noDuplicatesArray.distancesArray[3]).isEqualTo(new int[]{125714, 819148, 806122, 837320});
  }

  private DistancesArray non_optimized_matrix() {
    int[][] matrix = new int[][]{
      new int[]{0, 123809, 412556, 0, 885714, 880952, 857142, 847619, 838095},
      new int[]{123809, 0, 372197, 880382, 894736, 899521, 866028, 856459, 851674},
      new int[]{412556, 372197, 0, 860986, 856502, 865470, 905829, 878923, 860986},
      new int[]{0, 880382, 860986, 0, 93023, 177142, 819148, 806122, 842105},
      new int[]{885714, 894736, 856502, 93023, 0, 125714, 819148, 806122, 837320},
      new int[]{880952, 899521, 865470, 177142, 125714, 0, 819148, 821428, 842105},
      new int[]{857142, 866028, 905829, 819148, 819148, 819148, 0, 158163, 794258},
      new int[]{847619, 856459, 878923, 806122, 806122, 821428, 158163, 0, 789473},
      new int[]{838095, 851674, 860986, 842105, 837320, 842105, 794258, 789473, 0}
    };

    DistancesArray distancesArray = new DistancesArray(matrix.length, matrix[0].length, false);
    int[][] array = distancesArray.distancesArray;
    IntStream.range(0, array.length)
      .forEach(index -> array[index] = matrix[index]);
    return distancesArray;
  }

  private DistancesArray optimized_matrix() {
    int[][] matrix = new int[][]{
      new int[]{123809, 412556, 0, 885714, 880952, 857142, 847619, 838095},
      new int[]{372197, 880382, 894736, 899521, 866028, 856459, 851674},
      new int[]{860986, 856502, 865470, 905829, 878923, 860986},
      new int[]{93023, 177142, 819148, 806122, 842105},
      new int[]{125714, 819148, 806122, 837320},
      new int[]{819148, 821428, 842105},
      new int[]{158163, 794258},
      new int[]{789473},
      new int[]{}
    };

    DistancesArray distancesArray = new DistancesArray(matrix.length, matrix[0].length, true);
    int[][] array = distancesArray.distancesArray;
    IntStream.range(0, array.length)
      .forEach(index -> array[index] = matrix[index]);
    return distancesArray;
  }
}
