package com.orange.documentare.core.comp.clustering.graph.graphbuilder

/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.Item
import com.orange.documentare.core.model.ref.clustering.ClusteringItem
import com.orange.documentare.core.model.ref.clustering.EnrollParameters
import com.orange.documentare.core.model.ref.clustering.graph.*
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*
import java.util.stream.IntStream

internal class SingletonsRecallUpdateTest {

  private val parameters = EnrollParameters.builder().build()
  private val clusteringGraphBuilder = ClusteringGraphBuilder()
  private val clusteringItems = buildClusteringItems()
  private val singletonsRecall = SingletonsRecall(clusteringGraphBuilder, clusteringItems, parameters)

  @Test
  fun initial_graph_singletons_items_are_updated_thanks_to_singletons_graph() {
    // given
    val initialGraph = buildInitialGraph()
    val singletonsGraph = buildSingletonsGraph()
    val indexMaps = SingletonsRecallIndexMaps(clusteringItems, clusteringItems)
    val initialGraphInfo = SingletonsRecall.GraphInfo(initialGraph, indexMaps)

    // when
    singletonsRecall.updateInitialGraphSingletons(initialGraph, singletonsGraph, initialGraphInfo)

    // then
    val graphItem0 = initialGraph.getItems()[0]
    val graphItem1 = initialGraph.getItems()[1]
    val graphItem2 = initialGraph.getItems()[2]
    assertThat(graphItem0.subgraphId).isEqualTo(33)
    assertThat(graphItem0.clusterId).isEqualTo(321)
    assertThat(graphItem0.clusterCenter).isTrue()
    assertThat(graphItem0.enrolled()).isTrue()
    assertThat(graphItem1.subgraphId).isEqualTo(34)
    assertThat(graphItem1.clusterId).isEqualTo(331)
    assertThat(graphItem1.clusterCenter).isFalse()
    assertThat(graphItem1.enrolled()).isTrue()
    assertThat(graphItem2.subgraphId).isEqualTo(34)
    assertThat(graphItem2.clusterId).isEqualTo(331)
    assertThat(graphItem1.clusterCenter).isFalse()
    assertThat(graphItem2.enrolled()).isTrue()
  }

  @Test
  fun initial_graph_subgraphs_and_clusters_are_updated_thanks_to_singletons_graph() {
    // given
    val initialGraph = buildInitialGraph()
    val singletonsGraph = buildSingletonsGraph()
    val indexMaps = SingletonsRecallIndexMaps(clusteringItems, clusteringItems)
    val initialGraphInfo = SingletonsRecall.GraphInfo(initialGraph, indexMaps)

    // when
    singletonsRecall.removeInitialGraphSingletonsFromSubgraphsAndClusters(initialGraph, initialGraphInfo)
    singletonsRecall.updateInitialGraphSubgraphAndClusters(initialGraph, singletonsGraph, initialGraphInfo)

    // then
    val subgraphs = initialGraph.getSubGraphs()
    val clusters = initialGraph.getClusters()
    assertThat(subgraphs).hasSize(2)
    assertThat(subgraphs.containsKey(33)).isTrue()
    assertThat(subgraphs.containsKey(34)).isTrue()
    assertThat(subgraphs[33]?.groupId).isEqualTo(33)
    assertThat(subgraphs[34]?.groupId).isEqualTo(34)
    assertThat(subgraphs[33]?.clusterIndices).containsExactly(321)
    assertThat(subgraphs[34]?.clusterIndices).containsExactly(331)
    assertThat(subgraphs[33]?.itemIndices).containsExactly(0)
    assertThat(subgraphs[34]?.itemIndices).containsExactly(1, 2)
    assertThat(clusters).hasSize(2)
    assertThat(clusters.containsKey(321)).isTrue()
    assertThat(clusters.containsKey(331)).isTrue()
    assertThat(clusters[321]?.groupId).isEqualTo(321)
    assertThat(clusters[331]?.groupId).isEqualTo(331)
    assertThat(clusters[321]?.itemIndices).containsExactly(0)
    assertThat(clusters[331]?.itemIndices).containsExactly(1, 2)
  }

  @Test
  fun update_edges_should_update_and_reject_edges_not_belonging_to_subgraph() {
    // Given
    val singletonsRecall = SingletonsRecall(null, null, null)
    val subgraphItemIndices = listOf(10, 20)
    val graphGroup = GraphCluster()

    val edges = graphGroup.edges
    edges.add(GraphEdge(1, 2, 1000))
    edges.add(GraphEdge(1, 5, 1000))
    edges.add(GraphEdge(5, 1, 1000))
    edges.add(GraphEdge(5, 5, 1000))

    val newToOld = HashMap<Int, Int>()
    newToOld[1] = 10
    newToOld[2] = 20

    // When
    singletonsRecall.updateEdges(subgraphItemIndices, graphGroup, newToOld)

    // Then
    assertThat(edges).containsExactly(GraphEdge(10, 20, 1000))
  }

  private fun buildClusteringItems(): List<ClusteringItem> {
    val items = ArrayList<ClusteringItem>()
    IntStream.range(0, 3).forEach { i -> items.add(Item.withHumanReadableId(i.toString())) }
    return items
  }

  private fun buildInitialGraph(): ClusteringGraph {
    val items = ArrayList<GraphItem>()
    val clusteringGraph = ClusteringGraph(items)

    items.add(buildSingletonGraphItem(0, 100, 10))
    items.add(buildSingletonGraphItem(1, 110, 11))
    items.add(buildSingletonGraphItem(2, 120, 12))

    clusteringGraph.getSubGraphs()[10] = buildSubGraph(10, 100, intArrayOf(0))
    clusteringGraph.getSubGraphs()[11] = buildSubGraph(11, 110, intArrayOf(1))
    clusteringGraph.getSubGraphs()[12] = buildSubGraph(12, 120, intArrayOf(2))
    clusteringGraph.getClusters()[100] = buildCluster(10, 100, intArrayOf(0))
    clusteringGraph.getClusters()[110] = buildCluster(11, 110, intArrayOf(1))
    clusteringGraph.getClusters()[120] = buildCluster(12, 120, intArrayOf(2))

    return clusteringGraph
  }

  private fun buildSingletonsGraph(): ClusteringGraph {
    val items = ArrayList<GraphItem>()
    val singletonsGraph = ClusteringGraph(items)

    items.add(buildSingletonGraphItem(0, 200, 20))
    items.add(buildSingletonGraphItem(1, 210, 21))
    items.add(buildSingletonGraphItem(2, 210, 21))

    items[0].clusterCenter = true

    singletonsGraph.getSubGraphs()[20] = buildSubGraph(20, 200, intArrayOf(0))
    singletonsGraph.getSubGraphs()[21] = buildSubGraph(21, 210, intArrayOf(1, 2))
    singletonsGraph.getClusters()[200] = buildCluster(20, 200, intArrayOf(0))
    singletonsGraph.getClusters()[210] = buildCluster(21, 210, intArrayOf(1, 2))

    return singletonsGraph
  }

  private fun buildSingletonGraphItem(index: Int, clusterId: Int, subgraphId: Int): GraphItem {
    val graphItem = GraphItem()
    graphItem.vertex1Index = index
    graphItem.clusterId = clusterId
    graphItem.subgraphId = subgraphId
    graphItem.vertex2Index = -1
    graphItem.vertex3Index = -1
    return graphItem
  }

  private fun buildSubGraph(subgraphId: Int, clusterId: Int, itemIndices: IntArray): SubGraph {
    val subGraph = SubGraph()
    subGraph.groupId = subgraphId
    subGraph.clusterIndices.add(clusterId)
    Arrays.stream(itemIndices).forEach { subGraph.itemIndices.add(it) }
    return subGraph
  }

  private fun buildCluster(subgraphId: Int, clusterId: Int, itemIndices: IntArray): GraphCluster {
    val cluster = GraphCluster()
    cluster.subgraphId = subgraphId
    cluster.groupId = clusterId
    Arrays.stream(itemIndices).forEach { cluster.itemIndices.add(it) }
    return cluster
  }
}
