package com.orange.documentare.core.comp.ncd.cache

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.bwt.BwtParameters
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.ncd.NcdParameters
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.charset.Charset


class NcdCacheStatsTest {

  @BeforeEach
  internal fun setup() {
    NcdCache.clear()
  }

  @Test
  fun `cache hits and misses are computed`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))

    // when
    val ncdCacheKeys = NcdCacheKeys(bytes1, bytes2)
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)

    NcdCache.addNcd(ncdCacheKeys, 0.1f, 100, NcdParameters(BwtParameters(SuffixArrayAlgorithm.LIBDIVSUFSORT)))
    NcdCache.addNcd(ncdCacheKeys, 0.2f, 200, NcdParameters(BwtParameters(SuffixArrayAlgorithm.SAIS)))
    NcdCache.addNcd(ncdCacheKeys, 0.3f, 300, NcdParameters(BwtParameters(SuffixArrayAlgorithm.SAIS)))
    NcdCache.getNcd(ncdCacheKeys)
    NcdCache.getNcd(ncdCacheKeys)

    val bytesKey = "123"
    NcdCache.getCompression(bytesKey)
    NcdCache.getCompression(bytesKey)
    NcdCache.addCompression(bytesKey, 1000F, 34)
    NcdCache.getCompression(bytesKey)

    // then
    then(NcdCache.stats()).contains(
      """ncdHit (2) / ncdMiss (4)
compHit(1) / compMiss(2)

ncd computation duration statsDurationMillis:
 count=3 / min=100.0 / max=300.0 / mean=200.0

comp computation duration statsDurationMillis:
 count=1 / min=34.0 / max=34.0 / mean=34.0

SA algo, sais=2 / libdivsufsort=1

PostgresCacheStats {
	cacheHits"""
    )
  }
}
