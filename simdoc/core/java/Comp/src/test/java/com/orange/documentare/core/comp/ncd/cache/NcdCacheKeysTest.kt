package com.orange.documentare.core.comp.ncd.cache

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.nio.charset.Charset

class NcdCacheKeysTest {

  @Test
  fun `arrays are not equal based on keys`() {
    // given
    val bytes1: ByteArray = "12345".toByteArray(Charset.forName("utf8"))
    val bytes2: ByteArray = "abcde".toByteArray(Charset.forName("utf8"))

    // when
    val ncdCacheKeys = NcdCacheKeys(bytes1, bytes2)

    // then
    assertThat(ncdCacheKeys.equalArrays).isFalse()
  }

  @Test
  fun `arrays are based on keys`() {
    // given
    val bytes: ByteArray = "12345".toByteArray(Charset.forName("utf8"))

    // when
    val ncdCacheKeys = NcdCacheKeys(bytes, bytes)

    // then
    assertThat(ncdCacheKeys.equalArrays).isTrue()
  }
}
