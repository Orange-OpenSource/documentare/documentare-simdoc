/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.graph

import org.apache.commons.io.FileUtils

import java.io.File
import java.io.IOException

class ImportModel {
  lateinit var items: Array<Item>

  @Throws(IOException::class)
  fun loadItemsBytes() {
    for (inputItem in items) {
      loadItemBytes(inputItem)
    }
  }

  @Throws(IOException::class)
  private fun loadItemBytes(item: Item) {
    val stringBuilder = StringBuilder(item.fileNameReversed)
    val filename = stringBuilder.reverse().toString()
    val bytes = FileUtils.readFileToByteArray(File(javaClass.getResource(filename).file))
    item.bytes = bytes
  }
}
