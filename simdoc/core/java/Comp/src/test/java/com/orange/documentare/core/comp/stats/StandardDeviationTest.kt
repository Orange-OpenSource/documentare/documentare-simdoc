package com.orange.documentare.core.comp.stats

import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class StandardDeviationTest {

  @Test
  fun `return Double NaN if list is empty`() {
    then(DescriptiveStats().statistics(emptyList<Int>()).standardDeviation).isNaN
  }

  @Test
  fun `return 0,0 for a list containing only one element`() {
    then(DescriptiveStats().statistics(listOf(10)).standardDeviation).isEqualTo(0.0)
  }

  @Test
  fun `return correct value for a list of several elements`() {
    // given
    val list = IntRange(-5, 9).toList()

    // when
    val statistics = DescriptiveStats().statistics(list)

    then(statistics.standardDeviation).isEqualTo(4.47213595499958)
  }
}
