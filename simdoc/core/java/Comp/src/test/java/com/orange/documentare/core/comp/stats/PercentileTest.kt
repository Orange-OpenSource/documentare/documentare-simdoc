package com.orange.documentare.core.comp.stats

import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats
import org.assertj.core.api.BDDAssertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class PercentileTest {

  @Test
  fun `raise exception if percentile is 0`() {
    // given
    val list = IntRange(0, 99).toList()

    val throwable = catchThrowable { DescriptiveStats(0.0).statistics(list) }
    then(throwable).isInstanceOf(DescriptiveStats.PercentileOutOfRangeException::class.java)
  }

  @Test
  fun `raise exception if percentile is greater than 100`() {
    // given
    val list = IntRange(0, 99).toList()

    val throwable = catchThrowable { DescriptiveStats(100.1).statistics(list) }
    then(throwable).isInstanceOf(DescriptiveStats.PercentileOutOfRangeException::class.java)
  }


  @Test
  fun `return last element if percentile is 100`() {
    // given
    val list = IntRange(0, 999999).toList()

    // when
    val statistics = DescriptiveStats(100.0).statistics(list)

    then(statistics.percentile).isEqualTo(999999.0)
  }


  @Test
  fun `return first element if percentile is too small`() {
    // given
    val list = IntRange(10, 15).toList()

    // when
    val statistics = DescriptiveStats(0.00000001).statistics(list)

    then(statistics.percentile).isEqualTo(10.0)
  }


  @Test
  fun `return Double NaN if list is empty`() {
    then(DescriptiveStats().statistics(emptyList<Int>()).percentile).isNaN
  }

  @Test
  fun `return single list element for any percentile`() {
    // given
    val list = listOf(10)

    then(DescriptiveStats(0.000000001).statistics(list).percentile).isEqualTo(10.0)
    then(DescriptiveStats(50.0).statistics(list).percentile).isEqualTo(10.0)
    then(DescriptiveStats(99.99999999).statistics(list).percentile).isEqualTo(10.0)
  }


  @Test
  fun `return last element if percentile is really close to 100`() {
    // given
    val list = IntRange(10, 15).toList()

    // when
    val statistics = DescriptiveStats(99.99999999).statistics(list)

    then(statistics.percentile).isEqualTo(15.0)
  }

  @Test
  fun `return correct element if percentile is 67,89`() {
    // given
    val list = IntRange(0, 9).toList()

    // when
    val statistics = DescriptiveStats(67.89).statistics(list)

    then(statistics.percentile).isEqualTo(6.4679)
  }

  @Test
  fun `sort list before doing percentile computation`() {
    // given
    val list = listOf(10, -1, -6, 39, 12)

    // when
    val percentile = DescriptiveStats(60.79).statistics(list).percentile

    then(percentile).isEqualTo(11.2948)
  }
}
