/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.comp.clustering.compute

import com.orange.documentare.core.comp.bwt.BwtParameters
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.distance.Distance
import com.orange.documentare.core.comp.distance.DistanceParameters
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.comp.ncd.Ncd
import com.orange.documentare.core.comp.ncd.NcdParameters
import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import org.assertj.core.api.BDDAssertions.then
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import java.io.File

internal class ComputeClusteringTest {

  private val dat0InCentroid = bytesData("1", "aaa")
  private val dat1 = bytesData("2", "aaa")
  private val dat2 = bytesData("3", "aab")
  private val dat3 = bytesData("4", "aac")
  private val dat4InCentroid = bytesData("5", "add")
  private val dat5 = bytesData("6", "add")
  private val dat6 = bytesData("7", "ddd")

  private val bytesData: Array<BytesData> = arrayOf(dat0InCentroid, dat1, dat2, dat3, dat4InCentroid, dat5, dat6)
  private val centroidDat = bytesData("centroid", "aaa" + "add")

  private val ncdDat2ToCentroid = computeDistance(dat2)
  private val ncdDat3ToCentroid = computeDistance(dat3)
  private val ncdDat6ToCentroid = computeDistance(dat6)

  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `distance instance is built with distance parameters`(suffixArrayAlgorithm: SuffixArrayAlgorithm) {
    // given
    val distanceParameters = DistanceParameters(suffixArrayAlgorithm)
    val computeClustering = ComputeClustering(distanceParameters)
    // then
    then(computeClustering.distance.distanceParameters).isEqualTo(distanceParameters)
  }

  @Test
  fun `do not crash if no data are present`() {
    // when
    ComputeClustering(DistanceParameters.defaults()).compute(emptyArray(), ClusteringParameters.builder().build())
  }

  @Test
  fun `detect duplicates and compute distance to center without multiset`() {
    // when
    var cl = ComputeClustering(DistanceParameters.defaults()).compute(bytesData, ClusteringParameters.builder().build())
    cl = cl.dropGraph()

    // then
    val expected = loadClustering("/compute-clustering/detect_duplicates_and_compute_distance_to_center_without_multiset.json")
    assertThat(cl).isEqualTo(expected)
  }

  @Test
  internal fun `all centroid elements should have a null distance to center`() {
    // when
    var cl = ComputeClustering(DistanceParameters.defaults()).compute(bytesData, ClusteringParameters.builder().consolidateCluster().build())
    cl = cl.dropGraph()

    // then
    assertThat(cl.clusters).hasSize(1)
    assertThat(cl.clusters[0].multisetElementsIndices).containsExactly(0, 4)
    assertThat(cl.elements[0].distanceToCenter == null).isTrue()
    assertThat(cl.elements[4].distanceToCenter == null).isTrue()
  }

  @Test
  internal fun `duplicates have a null distance to centroid`() {
    // when
    var cl = ComputeClustering(DistanceParameters.defaults()).compute(bytesData, ClusteringParameters.builder().consolidateCluster().build())
    cl = cl.dropGraph()

    // then
    assertThat(cl.elements[1].distanceToCenter == null).isTrue()
    assertThat(cl.elements[5].distanceToCenter == null).isTrue()
  }

  @Test
  internal fun `non centroid elements and non duplicates should have a distance to the centroid center`() {
    // when
    var cl = ComputeClustering(DistanceParameters.defaults()).compute(bytesData, ClusteringParameters.builder().consolidateCluster().build())
    cl = cl.dropGraph()

    // then
    assertThat(cl.elements[2].distanceToCenter).isEqualTo(ncdDat2ToCentroid)
    assertThat(cl.elements[3].distanceToCenter).isEqualTo(ncdDat3ToCentroid)
    assertThat(cl.elements[6].distanceToCenter).isEqualTo(ncdDat6ToCentroid)
  }

  @Test
  fun `compute distance to center WITH multiset`() {
    // when
    var cl = ComputeClustering(DistanceParameters.defaults()).compute(bytesData, ClusteringParameters.builder().consolidateCluster().build())
    cl = cl.dropGraph()

    // then
    val expected = loadClustering("/compute-clustering/compute_distance_to_center_WITH_multiset.json")
    assertThat(cl).isEqualTo(expected)
  }

  private fun loadClustering(resourceFilename: String) = (JsonGenericHandler.instance()
    .getObjectFromJsonFileNewApi(ClusteringDTO::class.java, File(javaClass.getResource(resourceFilename).file)) as ClusteringDTO)
    .toBusinessObject()

  private fun bytesData(id: String, string: String): BytesData {
    return BytesData(listOf(id), string.toByteArray())
  }

  private fun computeDistance(bytesData: BytesData): Int {
    val ncd = Ncd(NcdParameters(BwtParameters(SuffixArrayAlgorithm.LIBDIVSUFSORT)))
    return (ncd.computeNcd(centroidDat.bytes, bytesData.bytes) * Distance.DISTANCE_INT_CONV_FACTOR).toInt()
  }
}