package com.orange.documentare.core.comp.clustering.graph.scissors.longedges;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.GraphItems;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.GraphGroup;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

public class LongEdgesTriggerTest {
  private static final float STD_FACTOR = 2;
  private final List<GraphItem> graphItems = getGraphItems();

  private LongEdgesTrigger longEdgesTrigger;

  @BeforeEach
  public void setup() {
    longEdgesTrigger = new LongEdgesTrigger(STD_FACTOR);
  }

  @Test
  public void shouldComputeThreshold() {
    // given
    GraphGroup graphGroup = getGraphGroup();

    // when
    longEdgesTrigger.initForGroup(graphGroup);

    then(53).isEqualTo(longEdgesTrigger.getEdgeLengthThreshold());
  }

  @Test
  public void shouldCutOrNot() {
    // given
    GraphGroup graphGroup = getGraphGroup();
    longEdgesTrigger.initForGroup(graphGroup);
    // when
    for (int i = 0; i < 5; i++) {
      then(longEdgesTrigger.shouldRemove(graphGroup.edges.get(i))).isFalse();
    }
    then(longEdgesTrigger.shouldRemove(graphGroup.edges.get(5))).isTrue();
  }

  private List<GraphItem> getGraphItems() {
    return new GraphItems();
  }

  private GraphGroup getGraphGroup() {
    GraphGroup graphGroup = new GraphGroup() {
    };

    List<Integer> itemIndices = new ArrayList<>();
    for (int i = 0; i < graphItems.size(); i++) {
      itemIndices.add(i);
    }
    graphGroup.itemIndices.addAll(itemIndices);

    for (int i = 0; i < 4; i++) {
      graphGroup.edges.add(new GraphEdge(0, 1, 10));
    }
    graphGroup.edges.add(new GraphEdge(1, 2, 50));
    graphGroup.edges.add(new GraphEdge(2, 1, 300));
    return graphGroup;
  }
}
