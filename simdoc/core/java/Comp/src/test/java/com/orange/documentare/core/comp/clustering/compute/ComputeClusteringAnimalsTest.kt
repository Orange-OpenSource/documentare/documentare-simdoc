package com.orange.documentare.core.comp.clustering.compute

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.Distance
import com.orange.documentare.core.comp.distance.DistanceParameters
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.comp.DistanceItem
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File
import java.io.IOException

internal class ComputeClusteringAnimalsTest {

  @Test
  @Throws(IOException::class)
  fun `build one cluster of animals, with multiset and proper distances to centroid`() {
    // given
    val bytesDataArray = loadBytesData()
    val expectedMultisetElementsIndices = listOf(17, 3, 5, 8, 22, 23)

    val bytesDataCentroid = bytesDataCentroid(bytesDataArray, expectedMultisetElementsIndices)
    val elementsIndicesNotInCentroid = bytesDataArray.indices.filterNot { index -> expectedMultisetElementsIndices.contains(index) }
    val expectedDistancesToCentroid = distancesToCentroid(bytesDataCentroid, elementsIndicesNotInCentroid, bytesDataArray)

    // when
    val cl = ComputeClustering(DistanceParameters.defaults()).compute(bytesDataArray, ClusteringParameters.builder().consolidateCluster().build())

    // then
    assertThat(cl.clusters[0].multisetElementsIndices).isEqualTo(expectedMultisetElementsIndices)
    val actualDistancesToCentroid = elementsIndicesNotInCentroid.map { index -> cl.elements[index].distanceToCenter }
    assertThat(actualDistancesToCentroid).isEqualTo(expectedDistancesToCentroid)
  }

  private fun bytesDataCentroid(bytesDataArray: Array<BytesData>, expectedMultisetElementsIndices: List<Int>): ByteArray {
    val bytesList = expectedMultisetElementsIndices
      .map { index -> bytesDataArray[index].loadBytes() }
    var centroidBytes = ByteArray(0)
    bytesList.forEach { bytes -> centroidBytes += bytes }
    return centroidBytes
  }

  private fun distancesToCentroid(bytesDataCentroid: ByteArray, elementsIndices: List<Int>, bytesDataArray: Array<BytesData>): List<Int> {
    return elementsIndices.map { index ->
      val item1 = MyDistanceItem(bytesDataCentroid)
      val item2 = MyDistanceItem(bytesDataArray[index].loadBytes())
      Distance(DistanceParameters.defaults()).compute(item1, item2)
    }
  }

  class MyDistanceItem(private val bytesArray: ByteArray) : DistanceItem {
    override fun loadBytes() = bytesArray
    override fun getHumanReadableId() = ""
  }

  private fun loadBytesData(): Array<BytesData> {
    return BytesData.loadFromDirectory(File(javaClass.getResource("/bestioles").file), File::getName)
  }
}