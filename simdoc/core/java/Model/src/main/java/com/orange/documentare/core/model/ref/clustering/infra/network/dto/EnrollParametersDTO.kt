/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.ref.clustering.EnrollParameters

class EnrollParametersDTO {
  var qcutSdFactor: Float = 0.toFloat()
  var acutSdFactor: Float = 0.toFloat()
  var scutSdFactor: Float = 0.toFloat()
  var ccutPercentile: Int = 0
  var knnThreshold: Int = 0
  var sloop: Boolean = false
  var consolidateCluster: Boolean = false

  fun toBusinessObject() = EnrollParameters(qcutSdFactor, acutSdFactor, scutSdFactor, ccutPercentile, knnThreshold, sloop)

  companion object {
    fun fromBusinessObject(bo: EnrollParameters) = EnrollParametersDTO().also { dto ->
      dto.qcutSdFactor = bo.qcutSdFactor
      dto.acutSdFactor = bo.acutSdFactor
      dto.scutSdFactor = bo.scutSdFactor
      dto.ccutPercentile = bo.ccutPercentile
      dto.knnThreshold = bo.knnThreshold
      dto.sloop = bo.sloop
      dto.consolidateCluster = bo.consolidateCluster
    }
  }
}

