package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.ref.clustering.Cluster

class ClusterDTO {
  var elementsIndices: List<Int> = emptyList()
  var multisetElementsIndices: List<Int> = emptyList()

  fun toBusinessObject() = Cluster(elementsIndices, multisetElementsIndices)

  companion object {
    fun fromBusinessObject(bo: Cluster) = ClusterDTO().also { dto ->
      dto.elementsIndices = bo.elementsIndices
      dto.multisetElementsIndices = bo.multisetElementsIndices
    }
  }
}
