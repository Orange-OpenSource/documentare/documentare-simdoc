package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@ToString
@EqualsAndHashCode
public class Clustering {
  public final List<ClusteringElement> elements;
  public final List<Subgraph> subgraphs;
  public final List<Cluster> clusters;
  public final ClusteringParameters parameters;
  public final ClusteringGraph graph;
  public final String error;
  public final Boolean serviceUnavailable;

  public Clustering(List<ClusteringElement> elements, List<Subgraph> subgraphs, List<Cluster> clusters, ClusteringParameters parameters, ClusteringGraph graph, String error) {
    this.elements = elements;
    this.subgraphs = subgraphs;
    this.clusters = clusters;
    this.parameters = parameters;
    this.graph = graph;
    this.error = error;
    serviceUnavailable = null;
  }

  public Clustering(List<ClusteringElement> elements, List<Subgraph> subgraphs, List<Cluster> clusters, ClusteringParameters parameters, ClusteringGraph graph, String error, Boolean serviceUnavailable) {
    this.elements = elements;
    this.subgraphs = subgraphs;
    this.clusters = clusters;
    this.parameters = parameters;
    this.graph = graph;
    this.error = error;
    this.serviceUnavailable = serviceUnavailable;
  }

  public Clustering() {
    elements = null;
    subgraphs = null;
    clusters = null;
    parameters = null;
    graph = null;
    error = null;
    serviceUnavailable = null;
  }

  public static Clustering error(ClusteringParameters parameters, String message) {
    return new Clustering(null, null, null, parameters, null, message);
  }

  public static Clustering serviceUnavailable() {
    return new Clustering(null, null, null, null, null, "Clustering service unavailable", true);
  }

  public boolean failed() {
    return error != null;
  }

  public boolean serviceWasUnavailable() {
    return serviceUnavailable == true;
  }

  public Clustering dropGraph() {
    return new Clustering(elements, subgraphs, clusters, parameters, null, error);
  }

  public Clustering updateElementsId(List<ClusteringElement> updatedElements) {
    return new Clustering(updatedElements, subgraphs, clusters, parameters, graph, error);
  }

  public Clustering updateElementsIndice(List<ClusteringElement> updatedElements, List<Cluster> updatedClusters) {
    return new Clustering(updatedElements, subgraphs, updatedClusters, parameters, graph, error);
  }
}
