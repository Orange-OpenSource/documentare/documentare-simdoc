package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class GraphItem {
  /**
   * triangle first vertex
   */
  public String vertexName;

  /**
   * triangle first vertex
   */
  public ClusteringItem vertex1;

  /**
   * triangle second vertex
   */
  public ClusteringItem vertex2;
  /**
   * triangle third vertex
   */
  public ClusteringItem vertex3;
  /**
   * triangle first vertex
   */
  public int vertex1Index;

  /**
   * triangle second vertex
   */
  public int vertex2Index;
  /**
   * triangle third vertex
   */
  public int vertex3Index;
  /**
   * triangle area
   */
  public float area;

  /**
   * triangle equilaterality factor
   */
  public float q;

  /**
   * triangle edges length
   */
  public int[] edgesLength;

  /**
   * no triangle due to K Nearest Neighbours criteria
   */
  public Boolean knnSingleton;

  /**
   * given triangle equilaterality & area, indicates if we exclude this item as a singleton
   */
  public boolean triangleSingleton;

  public int subgraphId;
  public int clusterId;
  public boolean clusterCenter;

  /**
   * this item was a singleton in initial graph, enroll feature was activated, we keep this info here
   */
  public Boolean enrolled;

  public boolean isKnnSingleton() {
    return knnSingleton != null && knnSingleton;
  }

  public boolean enrolled() {
    return enrolled != null && enrolled;
  }
}
