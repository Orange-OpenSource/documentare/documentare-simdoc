package com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph

import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge

class GraphEdgeDTO {
  var vertex1Index: Int = -1
  var vertex2Index: Int = -1
  var length: Int = -1

  fun toBusinessObject() = GraphEdge(vertex1Index, vertex2Index, length)

  companion object {
    fun fromBusinessObject(bo: GraphEdge) = GraphEdgeDTO().also { dto ->
      dto.vertex1Index = bo.vertex1Index
      dto.vertex2Index = bo.vertex2Index
      dto.length = bo.length
    }
  }
}
