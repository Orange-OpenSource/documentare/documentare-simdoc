package com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph

import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph
import java.util.*

class ClusteringGraphDTO {
  var items: List<GraphItemDTO> = emptyList()
  var subGraphs = HashMap<Int, SubGraphDTO>()
  var clusters = HashMap<Int, GraphClusterDTO>()

  fun toBusinessObject() = ClusteringGraph(items.map { it.toBusinessObject() }).also { bo ->
    subGraphs.map { bo.subGraphs.put(unwrapKeyToInt(it.key), it.value.toBusinessObject()) }
    clusters.map { bo.clusters.put(unwrapKeyToInt(it.key), it.value.toBusinessObject()) }
  }

  private fun unwrapKeyToInt(key: Any) = when (key) {
    is Int -> key
    is String -> Integer.valueOf(key)
    else -> throw IllegalStateException("Failed to unwrap '$key' to Int")
  }

  companion object {
    fun fromBusinessObject(bo: ClusteringGraph) = ClusteringGraphDTO().also { dto ->
      dto.items = bo.items.map { GraphItemDTO.fromBusinessObject(it) }
      bo.subGraphs.map { dto.subGraphs.put(it.key, SubGraphDTO.fromBusinessObject(it.value)) }
      bo.clusters.map { dto.clusters.put(it.key, GraphClusterDTO.fromBusinessObject(it.value)) }
    }
  }
}
