/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.model.io

import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset
import java.util.zip.GZIPInputStream

object Gzip {
  @JvmStatic
  fun getStringFromGzipFile(file: File): String {
    val inputStream = GZIPInputStream(FileInputStream(file))
    val string = inputStream.readBytes().toString(Charset.forName("utf-8"))
    inputStream.close()
    return string
  }
}