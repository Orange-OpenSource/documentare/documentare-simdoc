package com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph

import com.orange.documentare.core.model.ref.clustering.graph.GraphItem

class GraphItemDTO {
  lateinit var vertexName: String
  var vertex1Index: Int = -1
  var vertex2Index: Int = -1
  var vertex3Index: Int = -1
  var area: Float = (-1).toFloat()
  var q: Float = (-1).toFloat()
  var edgesLength: IntArray? = null
  var knnSingleton: Boolean? = null
  var triangleSingleton: Boolean = false
  var subgraphId: Int = -1
  var clusterId: Int = -1
  var clusterCenter: Boolean = false
  var enrolled: Boolean? = null

  fun toBusinessObject() = GraphItem().also {
    it.vertexName = vertexName
    it.vertex1Index = vertex1Index
    it.vertex2Index = vertex2Index
    it.vertex3Index = vertex3Index
    it.area = area
    it.q = q
    it.edgesLength = edgesLength
    it.knnSingleton = knnSingleton
    it.triangleSingleton = triangleSingleton
    it.subgraphId = subgraphId
    it.clusterId = clusterId
    it.clusterCenter = clusterCenter
    it.enrolled = enrolled
  }

  companion object {
    fun fromBusinessObject(bo: GraphItem) = GraphItemDTO().also { dto ->
      dto.vertexName = bo.vertexName
      dto.vertex1Index = bo.vertex1Index
      dto.vertex2Index = bo.vertex2Index
      dto.vertex3Index = bo.vertex3Index
      dto.area = bo.area
      dto.q = bo.q
      dto.edgesLength = bo.edgesLength
      dto.knnSingleton = bo.knnSingleton
      dto.triangleSingleton = bo.triangleSingleton
      dto.subgraphId = bo.subgraphId
      dto.clusterId = bo.clusterId
      dto.clusterCenter = bo.clusterCenter
      dto.enrolled = bo.enrolled
    }
  }
}
