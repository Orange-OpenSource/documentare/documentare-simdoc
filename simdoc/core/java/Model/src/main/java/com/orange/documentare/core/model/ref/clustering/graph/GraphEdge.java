package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class GraphEdge {
  public int vertex1Index;
  public int vertex2Index;
  public final int length;

  public GraphEdge(int vertex1Index, int vertex2Index, int length) {
    this.vertex1Index = vertex1Index;
    this.vertex2Index = vertex2Index;
    this.length = length;
  }
}
