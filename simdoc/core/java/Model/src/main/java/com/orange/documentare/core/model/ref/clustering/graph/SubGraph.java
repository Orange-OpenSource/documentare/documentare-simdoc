package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.comp.DoubleStatistics;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Slf4j
@ToString
@EqualsAndHashCode(callSuper = true)
public class SubGraph extends GraphGroup {

  /**
   * indices of contained clusters
   */
  public final List<Integer> clusterIndices = new ArrayList<>();

  /**
   * mean equilaterality for all contained items
   */
  public final float meanQ;

  /**
   * equilaterality's standard deviation for all contained items
   */
  public final float standardDeviationQ;

  /**
   * mean area for all contained items
   */
  public final float meanArea;

  /**
   * area's standard deviation for all contained items
   */
  public final float standardDeviationArea;

  public SubGraph(List<Integer> clusterIndices, float meanQ, float standardDeviationQ, float meanArea, float standardDeviationArea) {
    this.clusterIndices.addAll(clusterIndices);
    this.meanQ = meanQ;
    this.standardDeviationQ = standardDeviationQ;
    this.meanArea = meanArea;
    this.standardDeviationArea = standardDeviationArea;
  }

  public SubGraph(int subGraphId, Collection<Integer> itemIndices, DoubleStatistics qStats, DoubleStatistics areaStats) {
    this(Collections.emptyList(), (float) qStats.getMean(), (float) qStats.getStandardDeviation(), (float) areaStats.getMean(), (float) areaStats.getStandardDeviation());
    initGraphGroup(subGraphId, itemIndices);
  }

  public SubGraph(int subGraphId, Collection<Integer> itemIndices, float meanQ) {
    this(Collections.emptyList(), meanQ, 0, 0, 0);
    initGraphGroup(subGraphId, itemIndices);
  }

  public SubGraph() {
    this(Collections.emptyList(), 0, 0, 0, 0);
  }

  private void initGraphGroup(int subGraphId, Collection<Integer> itemIndices) {
    setGroupId(subGraphId);
    this.itemIndices.addAll(itemIndices);
    debug();
  }

  private void debug() {
    if (log.isDebugEnabled()) {
      StringBuilder stringBuilderIndices = new StringBuilder();
      for (Integer index : itemIndices) {
        stringBuilderIndices.append(String.format("%d ", index));
      }
      log.debug("Subgraph {} [{}] [{} {}] [{} {}]", getGroupId(), stringBuilderIndices.toString(), meanQ, standardDeviationQ, meanArea, standardDeviationArea);
    }
  }
}
