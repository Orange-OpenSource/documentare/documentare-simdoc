package com.orange.documentare.core.model.ref.comp

data class DoubleStatistics(val min: Double, val max: Double, val mean: Double, val standardDeviation: Double, val percentile: Double)