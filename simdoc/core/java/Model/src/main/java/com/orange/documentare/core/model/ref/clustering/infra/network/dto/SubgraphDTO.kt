package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.ref.clustering.Subgraph

class SubgraphDTO {
  var elementsIndices: List<Int> = emptyList()
  var edges: List<EdgeDTO> = emptyList()
  var distanceThreshold: Int? = null

  fun toBusinessObject() = Subgraph(elementsIndices, edges.map { it.toBusinessObject() }, distanceThreshold)

  companion object {
    fun fromBusinessObject(bo: Subgraph) = SubgraphDTO().also { dto ->
      dto.elementsIndices = bo.elementsIndices
      dto.edges = bo.edges.map { EdgeDTO.fromBusinessObject(it) }
      dto.distanceThreshold = bo.distanceThreshold
    }
  }
}
