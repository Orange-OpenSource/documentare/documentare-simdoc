package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@ToString
@EqualsAndHashCode
public class ClusteringGraph {
  public final List<GraphItem> items;
  public final Map<Integer, SubGraph> subGraphs = new HashMap<>();
  public final Map<Integer, GraphCluster> clusters = new HashMap<>();

  public ClusteringGraph(List<GraphItem> items) {
    this.items = items;
  }

  public int clusterIdMax() {
    return items.stream()
      .mapToInt(graphItem -> graphItem.clusterId)
      .max().getAsInt();
  }

  public int subgraphIdMax() {
    return items.stream()
      .mapToInt(graphItem -> graphItem.subgraphId)
      .max().getAsInt();
  }

  public Clustering toCleanClusteringOutput(ClusteringParameters parameters) {
    List<ClusteringElement> clusteringElements = Collections.unmodifiableList(
      items.stream()
        .map(it -> new ClusteringElement(it.vertexName, it.clusterId, it.clusterCenter ? true : null, it.enrolled, null, null))
        .collect(Collectors.toList())
    );

    List<Subgraph> subgraphs = Collections.unmodifiableList(
      subGraphs.values().stream()
        .map(it -> new Subgraph(it.itemIndices,
          it.edges.stream()
            .map(g -> new Edge(g.vertex1Index, g.vertex2Index, g.length))
            .collect(Collectors.toList()), null)
        )
        .collect(Collectors.toList())
    );

    List<Cluster> clusters = Collections.unmodifiableList(
      this.clusters.values().stream()
        .map(it -> new Cluster(it.itemIndices, it.multisetElementsIndices))
        .collect(Collectors.toList())
    );

    return new Clustering(clusteringElements, subgraphs, clusters, parameters, this, null);
  }
}
