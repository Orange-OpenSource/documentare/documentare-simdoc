package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Cluster built by running Voronoi's algo on a subgraph
 */
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class GraphCluster extends GraphGroup {
  public int subgraphId;
  public List<Integer> multisetElementsIndices;

  public GraphCluster() {
    this(0, null);
  }

  public GraphCluster(int subgraphId, List<Integer> multisetElementsIndices) {
    this.subgraphId = subgraphId;
    this.multisetElementsIndices = multisetElementsIndices == null ? new ArrayList<>() : multisetElementsIndices;
  }

  public void initMultisetWith(List<Integer> centers) {
    multisetElementsIndices = centers;
  }
}
