/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.model.json;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class JsonGenericHandler {
  private final Jsonb jsonb;

  public static JsonGenericHandler instance() {
    return new JsonGenericHandler(true);
  }

  public JsonGenericHandler() {
    this(false);
  }

  public JsonGenericHandler(boolean prettyPrint) {
    JsonbConfig jsonbConfig = new JsonbConfig().withFormatting(prettyPrint);
    jsonb = JsonbBuilder.create(jsonbConfig);
  }

  public void writeObjectToJsonGzipFile(Object object, File file) throws IOException {
    GZIPOutputStream outputStream = new GZIPOutputStream(new FileOutputStream(file));
    jsonb.toJson(object, outputStream);
    outputStream.close();
  }

  public Object getObjectFromJsonGzipFile(Class<?> clazz, File file) throws IOException {
    GZIPInputStream gzipInputStream = new GZIPInputStream(new FileInputStream(file));
    Object object = jsonb.fromJson(gzipInputStream, clazz);
    gzipInputStream.close();
    return object;
  }

  public void writeObjectToJsonFileNewApi(Object object, File file) throws IOException {
    String json = jsonb.toJson(object);
    OutputStream outputStream = new FileOutputStream(file);
    outputStream.write(json.getBytes());
    outputStream.close();
  }

  public Object getObjectFromJsonFileNewApi(Class<?> clazz, File file) throws IOException {
    InputStream is = new FileInputStream(file);
    Object object = jsonb.fromJson(is, clazz);
    is.close();
    return object;
  }
}
