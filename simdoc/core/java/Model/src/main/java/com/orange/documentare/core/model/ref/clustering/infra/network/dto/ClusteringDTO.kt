package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph.ClusteringGraphDTO

class ClusteringDTO {
  var elements: List<ClusteringElementDTO> = emptyList()
  var subgraphs: List<SubgraphDTO> = emptyList()
  var clusters: List<ClusterDTO> = emptyList()
  var parameters: ClusteringParametersDTO? = null
  var graph: ClusteringGraphDTO? = null
  var error: String? = null
  var serviceUnavailable: Boolean? = null

  fun toBusinessObject() = Clustering(elements.map { it.toBusinessObject() }, subgraphs.map { it.toBusinessObject() }, clusters.map { it.toBusinessObject() }, parameters?.toBusinessObject(), graph?.toBusinessObject(), error, serviceUnavailable)

  companion object {
    fun fromBusinessObject(bo: Clustering) = ClusteringDTO().also { dto ->
      dto.elements = bo.elements?.map { ClusteringElementDTO.fromBusinessObject(it) } ?: emptyList()
      dto.subgraphs = bo.subgraphs?.map { SubgraphDTO.fromBusinessObject(it) } ?: emptyList()
      dto.clusters = bo.clusters?.map { ClusterDTO.fromBusinessObject(it) } ?: emptyList()
      dto.parameters = bo.parameters?.let { ClusteringParametersDTO.fromBusinessObject(it) }
      dto.graph = bo.graph?.let { ClusteringGraphDTO.fromBusinessObject(it) }
      dto.error = bo.error
      dto.serviceUnavailable = bo.serviceUnavailable
    }
  }
}