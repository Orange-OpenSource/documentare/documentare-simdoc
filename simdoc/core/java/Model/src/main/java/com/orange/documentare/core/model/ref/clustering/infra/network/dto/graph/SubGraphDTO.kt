package com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph

import com.orange.documentare.core.model.ref.clustering.graph.SubGraph

class SubGraphDTO {
  var clusterIndices: List<Int> = emptyList()
  var meanQ: Float = (-1).toFloat()
  var standardDeviationQ: Float = (-1).toFloat()
  var meanArea: Float = (-1).toFloat()
  var standardDeviationArea: Float = (-1).toFloat()

  var itemIndices: List<Int> = emptyList()
  var edges: List<GraphEdgeDTO> = emptyList()
  var groupId = -1

  fun toBusinessObject() = SubGraph(clusterIndices, meanQ, standardDeviationQ, meanArea, standardDeviationArea).also { subGraph ->
    subGraph.itemIndices.addAll(itemIndices)
    subGraph.edges.addAll(edges.map { it.toBusinessObject() })
    subGraph.groupId = groupId
  }

  companion object {
    fun fromBusinessObject(bo: SubGraph) = SubGraphDTO().also { dto ->
      dto.clusterIndices = bo.clusterIndices
      dto.meanQ = bo.meanQ
      dto.standardDeviationQ = bo.standardDeviationQ
      dto.meanArea = bo.meanArea
      dto.standardDeviationArea = bo.standardDeviationArea

      dto.itemIndices = bo.itemIndices
      dto.edges = bo.edges.map { GraphEdgeDTO.fromBusinessObject(it) }
      dto.groupId = bo.groupId
    }
  }
}
