package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@ToString
@EqualsAndHashCode
public class Subgraph {
  public final List<Integer> elementsIndices;
  public final List<Edge> edges;
  public final Integer distanceThreshold;

  public Subgraph(List<Integer> elementsIndices, List<Edge> edges, Integer distanceThreshold) {
    this.elementsIndices = elementsIndices;
    this.edges = edges;
    this.distanceThreshold = distanceThreshold;
  }
}
