package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
@EqualsAndHashCode
/** A group of items. This group is included in a cluster and in a subgraph, but is a cluster or a subgraph */
public abstract class GraphGroup {

  /**
   * indices of contained items
   */
  public final List<Integer> itemIndices = new ArrayList<>();

  /**
   * contained edges
   */
  public final List<GraphEdge> edges = new ArrayList<>();

  private int groupId = -1;

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public int getGroupId() {
    return groupId;
  }
}
