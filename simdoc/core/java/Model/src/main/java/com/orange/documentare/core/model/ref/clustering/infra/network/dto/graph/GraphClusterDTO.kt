package com.orange.documentare.core.model.ref.clustering.infra.network.dto.graph

import com.orange.documentare.core.model.ref.clustering.graph.GraphCluster

class GraphClusterDTO {
  var subgraphId: Int = -1
  var multisetElementsIndices: List<Int> = emptyList()

  var itemIndices: List<Int> = emptyList()
  var edges: List<GraphEdgeDTO> = emptyList()
  var groupId = -1

  fun toBusinessObject() = GraphCluster(subgraphId, multisetElementsIndices).also { graphCluster ->
    graphCluster.itemIndices.addAll(itemIndices)
    graphCluster.edges.addAll(edges.map { it.toBusinessObject() })
    graphCluster.groupId = groupId
  }

  companion object {
    fun fromBusinessObject(bo: GraphCluster) = GraphClusterDTO().also { dto ->
      dto.subgraphId = bo.subgraphId
      dto.multisetElementsIndices = bo.multisetElementsIndices

      dto.itemIndices = bo.itemIndices
      dto.edges = bo.edges.map { GraphEdgeDTO.fromBusinessObject(it) }
      dto.groupId = bo.groupId
    }
  }
}
