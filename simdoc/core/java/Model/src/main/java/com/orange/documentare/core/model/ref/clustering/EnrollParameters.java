package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public final class EnrollParameters extends ClusteringCoreParameters {
  private static final float SLOOP_SCUT_SD_FACTOR_DEFAULT = 4;

  public EnrollParameters(
    float qcutSdFactor,
    float acutSdFactor,
    float scutSdFactor,
    int ccutPercentile,
    int knnThreshold,
    boolean sloop) {
    super(qcutSdFactor, acutSdFactor, scutSdFactor, ccutPercentile, knnThreshold, sloop, false);
  }

  public static EnrollParametersBuilder builder() {
    return new EnrollParametersBuilder();
  }

  public static class EnrollParametersBuilder {
    private float qcutSdFactor = -1;
    private float acutSdFactor = -1;
    private float scutSdFactor = -1;
    private int ccutPercentile = -1;
    private int knnThreshold = -1;
    private boolean sloop = true;

    private EnrollParametersBuilder() {
    }

    public EnrollParameters build() {
      if (sloop && scutSdFactor < 0) {
        scutSdFactor = SLOOP_SCUT_SD_FACTOR_DEFAULT;
      }
      return new EnrollParameters(qcutSdFactor, acutSdFactor, scutSdFactor, ccutPercentile, knnThreshold, sloop);
    }

    public EnrollParametersBuilder qcut() {
      return qcut(Q_DEFAULT_SD_FACTOR);
    }

    public EnrollParametersBuilder qcut(float qcutSdFactor) {
      this.qcutSdFactor = qcutSdFactor;
      return this;
    }

    public EnrollParametersBuilder acut() {
      return acut(A_DEFAULT_SD_FACTOR);
    }

    public EnrollParametersBuilder acut(float acutSdFactor) {
      this.acutSdFactor = acutSdFactor;
      return this;
    }

    public EnrollParametersBuilder scut() {
      return scut(SCUT_DEFAULT_SD_FACTOR);
    }

    public EnrollParametersBuilder scut(float scutSdFactor) {
      this.scutSdFactor = scutSdFactor;
      return this;
    }

    public EnrollParametersBuilder ccut() {
      return ccut(CCUT_DEFAULT_PERCENTILE);
    }

    public EnrollParametersBuilder ccut(int ccutPercentile) {
      this.ccutPercentile = ccutPercentile;
      return this;
    }

    public EnrollParametersBuilder knn(int knnThreshold) {
      this.knnThreshold = knnThreshold;
      return this;
    }

    public EnrollParametersBuilder disableSloop() {
      sloop = false;
      return this;
    }
  }
}

