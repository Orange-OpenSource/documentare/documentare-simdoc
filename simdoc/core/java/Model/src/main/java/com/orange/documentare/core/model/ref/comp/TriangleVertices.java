package com.orange.documentare.core.model.ref.comp;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.json.bind.annotation.JsonbProperty;
import java.util.Arrays;
import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class TriangleVertices {
  @JsonbProperty(nillable = true)
  private NearestItem vertex2;
  @JsonbProperty(nillable = true)
  private NearestItem vertex3;
  @JsonbProperty(nillable = true)
  private Integer edge13;

  /**
   * Based on the KNN criterion, if we did not found nearest items to build the triangle, then we declare this item as a singleton
   */
  private Boolean knnSingleton;

  public TriangleVertices(ClusteringItem clusteringItem, ClusteringItem[] items, int knnThreshold) {
    NearestItem[] vertex1Nearest = clusteringItem.getNearestItems();
    if (vertex1Nearest == null || vertex1Nearest.length <= 1) {
      // only one element during the clustering...
      return;
    }

    NearestItem v1Nearest = vertex1Nearest[1];

    if (vertex1Nearest.length == 2) {
      // only two elements during the clustering...
      vertex2 = v1Nearest;
      return;
    }

    NearestItem[] vertex2Nearest = items[v1Nearest.getIndex()].getNearestItems();
    NearestItem v2Nearest = searchVertex3(vertex1Nearest, vertex2Nearest);

    init(vertex1Nearest, v2Nearest, knnThreshold);
  }

  /**
   * Useful for PrepClustering when we use directly the distance matrix to avoid keeping in memory all nearest arrays
   */
  public TriangleVertices(NearestItem[] vertex1Nearest, NearestItem v2Nearest, int knnThreshold) {
    init(vertex1Nearest, v2Nearest, knnThreshold);
  }

  public boolean getKnnSingleton() {
    return knnSingleton != null && knnSingleton;
  }

  @JsonbTransient
  public boolean hasOnlyOneVertex() {
    return vertex2 == null && vertex3 == null;
  }

  public boolean hasOnlyTwoVertices() {
    return vertex2 != null && vertex3 == null;
  }

  @JsonbTransient
  public int getEdge12() {
    return vertex2.getDistance();
  }

  @JsonbTransient
  public int getEdge23() {
    return vertex3.getDistance();
  }

  private void init(NearestItem[] vertex1Nearest, NearestItem v2Nearest, int knnThreshold) {
    NearestItem v1Nearest = vertex1Nearest[1];
    int edge13Candidate = searchEdge13(vertex1Nearest, v2Nearest.getIndex(), knnThreshold);

    if (edge13Candidate < 0) {
      knnSingleton = true;
    } else {
      this.vertex2 = v1Nearest;
      this.vertex3 = v2Nearest;
      this.edge13 = edge13Candidate;
    }
  }

  private NearestItem searchVertex3(NearestItem[] vertex1Nearest, NearestItem[] vertex2Nearest) {
    NearestItem vertex1 = vertex1Nearest[0];
    NearestItem vertex3 = vertex2Nearest[1];
    return vertex3.getIndex() == vertex1.getIndex() ? vertex2Nearest[2] : vertex3;
  }

  private int searchEdge13(NearestItem[] nearestItemsVertex1, int vertex3Index, int knnThreshold) {
    // + 1 since first nearest is item itself with null distance
    // + 1 since we need to include nearest k + 1 to know if nearest k can be accepted
    int limitNearest = knnThreshold > Integer.MAX_VALUE - 2 ?
      Integer.MAX_VALUE :
      knnThreshold + 1 + 1;

    Optional<NearestItem> v3 = Arrays.stream(nearestItemsVertex1)
      .limit(limitNearest)
      .filter(nearestItem -> nearestItem.getIndex() == vertex3Index)
      .findAny();

    return v3.isPresent() ? v3.get().getDistance() : -1;
  }
}
