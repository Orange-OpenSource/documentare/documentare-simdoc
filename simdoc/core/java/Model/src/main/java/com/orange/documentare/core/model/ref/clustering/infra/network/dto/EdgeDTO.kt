package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.ref.clustering.Edge

class EdgeDTO {
  var v1Index: Int = -1
  var v2Index: Int = -1
  var length: Int = -1

  fun toBusinessObject() = Edge(v1Index, v2Index, length)

  companion object {
    fun fromBusinessObject(bo: Edge) = EdgeDTO().also { dto ->
      dto.v1Index = bo.v1Index
      dto.v2Index = bo.v2Index
      dto.length = bo.length
    }
  }
}
