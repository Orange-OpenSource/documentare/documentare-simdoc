package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public final class ClusteringParameters extends ClusteringCoreParameters {
  public final boolean enroll;
  public final EnrollParameters enrollParameters;

  public ClusteringParameters(
    float qcutSdFactor,
    float acutSdFactor,
    float scutSdFactor,
    int ccutPercentile,
    int knnThreshold,
    boolean sloop,
    boolean consolidateCluster,
    boolean enroll,
    EnrollParameters enrollParameters) {
    super(qcutSdFactor, acutSdFactor, scutSdFactor, ccutPercentile, knnThreshold, sloop, consolidateCluster);
    this.enroll = enroll;
    this.enrollParameters = enrollParameters;
  }

  @Override
  public String toString() {
    String str = super.toString();
    str += ", enroll=" + enroll;
    str += ", enrollParameters=(" + enrollParameters + ")";
    return str;
  }

  public static ClusteringParametersBuilder builder() {
    return new ClusteringParametersBuilder();
  }

  public static class ClusteringParametersBuilder {
    private static final int SLOOP_SCUT_SD_FACTOR_DEFAULT = 3;
    private float qcutSdFactor = -1;
    private float acutSdFactor = -1;
    private float scutSdFactor = -1;
    private int ccutPercentile = -1;
    private int knnThreshold = -1;
    private boolean sloop;
    private boolean consolidateCluster;
    private boolean enroll;
    private EnrollParameters enrollParameters = EnrollParameters.builder().build();

    private ClusteringParametersBuilder() {
    }

    public ClusteringParameters build() {
      if (sloop && scutSdFactor < 0) {
        scutSdFactor = SLOOP_SCUT_SD_FACTOR_DEFAULT;
      }
      return new ClusteringParameters(qcutSdFactor, acutSdFactor, scutSdFactor, ccutPercentile, knnThreshold, sloop, consolidateCluster, enroll, enrollParameters);
    }

    public ClusteringParametersBuilder qcut() {
      return qcut(Q_DEFAULT_SD_FACTOR);
    }

    public ClusteringParametersBuilder qcut(float qcutSdFactor) {
      this.qcutSdFactor = qcutSdFactor;
      return this;
    }

    public ClusteringParametersBuilder acut() {
      return acut(A_DEFAULT_SD_FACTOR);
    }

    public ClusteringParametersBuilder acut(float acutSdFactor) {
      this.acutSdFactor = acutSdFactor;
      return this;
    }

    public ClusteringParametersBuilder scut() {
      return scut(SCUT_DEFAULT_SD_FACTOR);
    }

    public ClusteringParametersBuilder scut(float scutSdFactor) {
      this.scutSdFactor = scutSdFactor;
      return this;
    }

    public ClusteringParametersBuilder ccut() {
      return ccut(CCUT_DEFAULT_PERCENTILE);
    }

    public ClusteringParametersBuilder ccut(int ccutPercentile) {
      this.ccutPercentile = ccutPercentile;
      return this;
    }

    public ClusteringParametersBuilder knn(int knnThreshold) {
      this.knnThreshold = knnThreshold;
      return this;
    }

    public ClusteringParametersBuilder sloop() {
      sloop = true;
      return this;
    }

    public ClusteringParametersBuilder consolidateCluster() {
      consolidateCluster = true;
      return this;
    }

    public ClusteringParametersBuilder enroll() {
      enroll = true;
      return this;
    }

    public ClusteringParametersBuilder enrollParameters(EnrollParameters enrollParameters) {
      this.enrollParameters = enrollParameters;
      return this;
    }
  }
}
