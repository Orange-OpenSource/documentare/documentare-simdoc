package com.orange.documentare.core.model.ref.comp

data class IntStatistics(val min: Int, val max: Int, val mean: Double, val standardDeviation: Double, val percentile: Double)