package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class Edge {
  public final int v1Index;
  public final int v2Index;
  public final int length;

  public Edge(int v1Index, int v2Index, int length) {
    this.v1Index = v1Index;
    this.v2Index = v2Index;
    this.length = length;
  }
}
