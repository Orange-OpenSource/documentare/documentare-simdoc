package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public abstract class ClusteringCoreParameters {
  public static final float A_DEFAULT_SD_FACTOR = 2;
  public static final float Q_DEFAULT_SD_FACTOR = 2;
  public static final int CCUT_DEFAULT_PERCENTILE = 75;
  public static final float SCUT_DEFAULT_SD_FACTOR = 2;

  public final float qcutSdFactor;
  public final float acutSdFactor;
  public final float scutSdFactor;
  public final int ccutPercentile;
  public final int knnThreshold;
  public final boolean sloop;
  public final boolean consolidateCluster;

  public ClusteringCoreParameters(float qcutSdFactor, float acutSdFactor, float scutSdFactor, int ccutPercentile, int knnThreshold, boolean sloop, boolean consolidateCluster) {
    this.qcutSdFactor = qcutSdFactor;
    this.acutSdFactor = acutSdFactor;
    this.scutSdFactor = scutSdFactor;
    this.ccutPercentile = ccutPercentile;
    this.knnThreshold = knnThreshold;
    this.sloop = sloop;
    this.consolidateCluster = consolidateCluster;
  }

  public boolean acut() {
    return acutSdFactor > 0;
  }

  public boolean qcut() {
    return qcutSdFactor > 0;
  }

  public boolean scut() {
    return scutSdFactor > 0;
  }

  public boolean ccut() {
    return ccutPercentile > 0;
  }

  public boolean knn() {
    return knnThreshold > 0;
  }

  @Override
  public String toString() {
    String str = "acut=" + acut() + ", qcut=" + qcut() + ", scut=" + scut() + ", ccut=" + ccut() + ", knn=" + knn() + ", sloop=" + sloop + ", consolidateCluster=" + consolidateCluster;
    str += acut() ? ", acutSd=" + acutSdFactor : "";
    str += qcut() ? ", qcutSd=" + qcutSdFactor : "";
    str += scut() ? ", scutSd=" + scutSdFactor : "";
    str += ccut() ? ", ccutSd=" + ccutPercentile : "";
    str += knn() ? ", knn=" + knnThreshold : "";
    return str;
  }
}
