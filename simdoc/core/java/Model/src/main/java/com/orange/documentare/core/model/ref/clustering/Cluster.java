package com.orange.documentare.core.model.ref.clustering;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
@EqualsAndHashCode
public class Cluster {
  public final List<Integer> elementsIndices;
  public final List<Integer> multisetElementsIndices;

  public Cluster(List<Integer> elementsIndices, List<Integer> multisetElementsIndices) {
    this.elementsIndices = elementsIndices;
    this.multisetElementsIndices = multisetElementsIndices == null ? new ArrayList<>() : multisetElementsIndices;
  }

  public Cluster(List<Integer> elementsIndices) {
    this(elementsIndices, new ArrayList<>());
  }
}
