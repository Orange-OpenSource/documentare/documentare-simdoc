package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.ref.clustering.ClusteringElement

class ClusteringElementDTO {
  lateinit var id: String
  var clusterId: Int = -1
  var clusterCenter: Boolean? = null
  var enrolled: Boolean? = null
  var duplicateOf: Int? = null
  var distanceToCenter: Int? = null

  fun toBusinessObject() = ClusteringElement(id, clusterId, clusterCenter, enrolled, duplicateOf, distanceToCenter)

  companion object {
    fun fromBusinessObject(bo: ClusteringElement) = ClusteringElementDTO().also { dto ->
      dto.id = bo.id
      dto.clusterId = bo.clusterId
      dto.clusterCenter = bo.clusterCenter
      dto.enrolled = bo.enrolled
      dto.duplicateOf = bo.duplicateOf
      dto.distanceToCenter = bo.distanceToCenter
    }
  }
}
