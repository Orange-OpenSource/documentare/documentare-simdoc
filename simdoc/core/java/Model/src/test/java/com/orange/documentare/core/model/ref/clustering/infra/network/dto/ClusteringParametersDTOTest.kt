package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.EnrollParameters
import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.io.File

class ClusteringParametersDTOTest {

  private val json = JsonGenericHandler(true)
  private val file = File("serialize_then_deserialize_business_object.json")

  @AfterEach
  fun cleanup() {
    file.deleteRecursively()
  }

  @Test
  fun serialize_then_deserialize_business_object() {
    //given
    val bo = ClusteringParameters.builder().acut(2f).enroll().enrollParameters(EnrollParameters.builder().qcut(3.1f).build()).build()
    val dto = ClusteringParametersDTO.fromBusinessObject(bo)

    //when
    json.writeObjectToJsonFileNewApi(dto, file)
    val readDTO = json.getObjectFromJsonFileNewApi(ClusteringParametersDTO::class.java, file) as ClusteringParametersDTO
    val readBO = readDTO.toBusinessObject()

    //then
    assertThat(readBO).isEqualTo(bo)
  }
}