package com.orange.documentare.core.model.ref.clustering

import org.fest.assertions.Assertions.assertThat
import org.junit.jupiter.api.Test

class ClusteringTest {

  @Test
  fun a_clustering_with_error_is_marked_failed() {
    // given
    val errorMessage = "this is an error"

    // when
    val clustering = Clustering.error(ClusteringParameters.builder().build(), errorMessage)

    // then
    assertThat(clustering.failed()).isTrue()
    assertThat(clustering.error).isEqualTo(errorMessage)
  }

  @Test
  fun a_clustering_was_requested_but_server_service_was_not_available() {
    // when
    val clustering = Clustering.serviceUnavailable()

    // then
    assertThat(clustering.failed()).isTrue()
    assertThat(clustering.serviceWasUnavailable()).isTrue()
    assertThat(clustering.error).isEqualTo("Clustering service unavailable")
  }
}
