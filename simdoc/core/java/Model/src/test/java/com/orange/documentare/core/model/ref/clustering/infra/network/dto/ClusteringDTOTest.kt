package com.orange.documentare.core.model.ref.clustering.infra.network.dto

import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.model.ref.clustering.*
import com.orange.documentare.core.model.ref.clustering.graph.*
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*

internal class ClusteringDTOTest {

  private val json = JsonGenericHandler(true)
  private val file = File("we_can_serialize_then_deserialize_a_clustering_object.json")

  @AfterEach
  fun cleanup() {
    file.deleteRecursively()
  }

  @Test
  fun we_can_serialize_then_deserialize_a_clustering_object() {
    // given
    val parameters = ClusteringParameters.builder().knn(12).build()
    val bo = Clustering(elements(), subgraphs(), clusters(), parameters, clusteringGraph(), "an error", true)
    val dto = ClusteringDTO.fromBusinessObject(bo)

    // when
    json.writeObjectToJsonFileNewApi(dto, file)
    val readDTO = json.getObjectFromJsonFileNewApi(ClusteringDTO::class.java, file) as ClusteringDTO
    val readBO = readDTO.toBusinessObject()

    // then
    then(readBO).isEqualTo(bo)
  }

  private fun elements(): List<ClusteringElement> {
    val elements = ArrayList<ClusteringElement>()
    elements.add(ClusteringElement("1", 1, true, true, null, null))
    return elements
  }

  private fun subgraphs(): List<Subgraph> {
    val subgraphs = ArrayList<Subgraph>()

    val elementsIndices = ArrayList<Int>()
    elementsIndices.add(10)

    val edges = ArrayList<Edge>()
    edges.add(Edge(100, 200, 10000))

    val distanceThreshold = 1234

    subgraphs.add(Subgraph(elementsIndices, edges, distanceThreshold))
    return subgraphs
  }

  private fun clusters(): List<Cluster> {
    val clusters = ArrayList<Cluster>()

    val elementsIndices = ArrayList<Int>()
    elementsIndices.add(10)

    clusters.add(Cluster(elementsIndices))
    return clusters
  }

  private fun clusteringGraph(): ClusteringGraph {
    val items = ArrayList<GraphItem>()
    val graphItem = GraphItem()
    graphItem.vertexName = "titi"
    graphItem.knnSingleton = true
    items.add(graphItem)
    val clusteringGraph = ClusteringGraph(items)

    val subgraph = SubGraph(listOf(29), 1.2f, 2.3f, 3.4f, 4.5f)
    subgraph.itemIndices.add(23)
    subgraph.groupId = 2
    subgraph.edges.add(GraphEdge(2, 3, 55))

    val cluster = GraphCluster()
    clusteringGraph.subGraphs[1] = subgraph
    clusteringGraph.clusters[2] = cluster

    return clusteringGraph
  }
}
