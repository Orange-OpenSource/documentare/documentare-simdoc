package com.orange.documentare.core.prepdata

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

internal class ThreadSafeCopyTest {

  private lateinit var source: File
  private lateinit var target: File

  @TempDir
  lateinit var tmpDir: File

  @BeforeEach
  fun setup() {
    source = File("${tmpDir}/source")
    target = File("${tmpDir}/target")

    Files.copy(ThreadSafeCopy::class.java.getResourceAsStream("ThreadSafeCopy.class"), source.toPath(), StandardCopyOption.REPLACE_EXISTING)
  }

  @Test
  fun `multiple threads can try to copy to the same destination concurrently`() {
    // given
    val threadsCount = 100
    val copyTimes = 2
    val es = Executors.newFixedThreadPool(threadsCount)
    var actualException: Exception? = null

    // when
    repeat(threadsCount) {
      es.submit {
        repeat(copyTimes) {
          try {
            ThreadSafeCopy.copy(source, target)
          } catch (e: Exception) {
            actualException = e
          }
        }
      }
    }

    es.shutdown()
    es.awaitTermination(5, TimeUnit.SECONDS)

    // then
    then(actualException).isNull()
  }
}
