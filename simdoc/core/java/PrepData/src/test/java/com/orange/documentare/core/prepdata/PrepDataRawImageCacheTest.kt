/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.prepdata

import com.orange.documentare.core.prepdata.cache.FileCache
import org.apache.commons.io.FileUtils.deleteQuietly
import org.apache.commons.io.FileUtils.readFileToByteArray
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.File
import java.io.IOException
import java.util.*
import java.util.zip.CRC32

class PrepDataRawImageCacheTest {

  @AfterEach
  fun cleanup() {
    FileCache.clearStats()
    deleteQuietly(PREPPED_DATA_JSON)
    deleteQuietly(METADATA_JSON)
    deleteQuietly(SAFE_WORKING_DIRECTORY)
    deleteQuietly(RAW_CACHE_DIRECTORY)
  }

  @Test
  fun raise_an_exception_if_cache_directory_can_not_be_created() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val pixelCount = 100 * 1024

    // When/Then
    Assertions.assertThrows(IOException::class.java) {
      PrepData.builder()
        .inputDirectory(inputDirectory)
        .safeWorkingDirConverter()
        .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
        .withRawConverter(true)
        .withRawCacheDirectory("/abcdef")
        .expectedRawPixelsCount(pixelCount)
        .metadataOutputFile(METADATA_JSON)
        .build()
    }
  }

  @Test
  fun raw_file_should_be_added_to_raw_cache_the_first_time_and_used_the_second_time() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val pixelCount = 100 * 1024
    val prepData = PrepData.builder()
      .inputDirectory(inputDirectory)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .withRawConverter(true)
      .withRawCacheDirectory(RAW_CACHE_DIRECTORY.absolutePath)
      .expectedRawPixelsCount(pixelCount)
      .metadataOutputFile(METADATA_JSON)
      .build()

    // When/Then
    prepData.prep()

    var cacheFiles = RAW_CACHE_DIRECTORY.list()
    assertThat(cacheFiles).isNotNull
    assertThat(cacheFiles).containsExactly("raw-102400-384E2D65817F755A8862C18765D4D84D")
    assertThat(PrepData.rawCacheHits()).isEqualTo(0)
    assertThat(PrepData.rawCacheMisses()).isEqualTo(1)

    val safeFiles = SAFE_WORKING_DIRECTORY.listFiles()!!
    Arrays.sort(safeFiles)
    val crcRawFile = CRC32().apply { update(readFileToByteArray(safeFiles[0])) }
    val crcCachedFile = CRC32().apply { update(readFileToByteArray(RAW_CACHE_DIRECTORY.listFiles()!![0])) }
    assertThat(crcRawFile.value).isEqualTo(crcCachedFile.value)

    deleteQuietly(PREPPED_DATA_JSON)
    deleteQuietly(METADATA_JSON)
    deleteQuietly(SAFE_WORKING_DIRECTORY)


    // When/Then
    prepData.prep()
    cacheFiles = RAW_CACHE_DIRECTORY.list()
    assertThat(cacheFiles).isNotNull
    assertThat(cacheFiles).containsExactly("raw-102400-384E2D65817F755A8862C18765D4D84D")
    assertThat(PrepData.rawCacheHits()).isEqualTo(1)
    assertThat(PrepData.rawCacheMisses()).isEqualTo(1)
  }

  @Test
  fun prep_data_stats() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val prepData = PrepData.builder()
      .inputDirectory(inputDirectory)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .withRawConverter(true)
      .withRawCacheDirectory(RAW_CACHE_DIRECTORY.absolutePath)
      .expectedRawPixelsCount(100 * 1024)
      .metadataOutputFile(METADATA_JSON)
      .build()

    PrepData.clearStats()

    // When
    prepData.prep()
    val stats = PrepData.statsDurationMillis()

    // Then
    assertThat(stats).contains("Raw cache hits and misses: 0/1")
  }

  companion object {
    private val PREPPED_DATA_JSON = File("bytes-data.json")
    private val METADATA_JSON = File("metadata.json")
    private val SAFE_WORKING_DIRECTORY = File("safe-working-directory")
    private val RAW_CACHE_DIRECTORY = File("raw-cache-directory")
  }
}
