package com.orange.documentare.core.prepdata.cache

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class FileSignatureTest {

  private val testFile = "/prep-data-test-input-dir/image.png"

  @Test
  fun `file signature is based on md5sum`() {
    // given
    val fileAbsolutePath = javaClass.getResource(testFile).file

    // when
    val signature: Signature = FileSignature.forFile(fileAbsolutePath)

    // then
    assertThat(signature.value).isEqualTo("384E2D65817F755A8862C18765D4D84D")
  }

  @Test
  fun `prefix can be added to signature`() {
    // given
    val fileAbsolutePath = javaClass.getResource(testFile).file

    // when
    val signature: Signature = FileSignature.forFile(fileAbsolutePath, prefix = "titi100")

    // then
    assertThat(signature.value).isEqualTo("titi100-384E2D65817F755A8862C18765D4D84D")
  }
}