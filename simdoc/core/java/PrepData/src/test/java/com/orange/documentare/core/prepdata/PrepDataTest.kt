/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.prepdata

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.comp.distance.bytesdistances.PreppedBytesData
import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.system.inputfilesconverter.Metadata
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FileUtils.deleteQuietly
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*
import java.util.zip.CRC32

class PrepDataTest {

  @AfterEach
  fun cleanup() {
    deleteQuietly(PREPPED_DATA_JSON)
    deleteQuietly(METADATA_JSON)
    deleteQuietly(SAFE_WORKING_DIRECTORY)
  }

  @Test
  fun raise_exception_if_input_directory_is_null() {
    assertThrows(IllegalStateException::class.java
    ) {
      PrepData.builder()
        .inputDirectory(null)
        .metadataOutputFile(METADATA_JSON)
        .build()
    }
  }

  @Test
  fun raise_exception_if_input_directory_does_not_exists() {
    assertThrows(IllegalStateException::class.java
    ) {
      PrepData.builder()
        .inputDirectory(File("/pouet-pouet"))
        .metadataOutputFile(METADATA_JSON)
        .build()
    }
  }

  @Test
  fun raise_exception_if_safe_working_dir_but_dir_is_null() {
    assertThrows(IllegalStateException::class.java) {
      val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
      PrepData.builder()
        .inputDirectory(inputDirectory)
        .safeWorkingDirConverter()
        .safeWorkingDirectory(null)
        .metadataOutputFile(METADATA_JSON)
        .build()
    }
  }

  @Test
  fun raise_exception_if_safe_working_dir_but_metadata_file_is_null() {
    assertThrows(IllegalStateException::class.java) {
      val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
      PrepData.builder()
        .inputDirectory(inputDirectory)
        .safeWorkingDirConverter()
        .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
        .metadataOutputFile(null)
        .build()
    }
  }

  @Test
  fun raise_exception_if_no_converter() {
    assertThrows(IllegalStateException::class.java) {
      val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
      PrepData.builder()
        .inputDirectory(inputDirectory)
        .metadataOutputFile(METADATA_JSON)
        .build()
    }
  }

  @Test
  fun prep_bytes_data() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val prepData = PrepData.builder()
      .inputDirectory(inputDirectory)
      .preppedBytesDataOutputFile(PREPPED_DATA_JSON)
      .build()

    val jsonGenericHandler = JsonGenericHandler()
    // When
    prepData.prep()
    val preppedBytesData = jsonGenericHandler.getObjectFromJsonFileNewApi(PreppedBytesData::class.java, PREPPED_DATA_JSON) as PreppedBytesData

    // Then
    assertThat(preppedBytesData.bytesData).isNotNull
    assertThat(preppedBytesData.bytesData.size).isEqualTo(2)
    assertThat(preppedBytesData.bytesData[0].ids).isEqualTo(listOf("image.png"))
    assertThat(preppedBytesData.bytesData[1].ids).isEqualTo(listOf("subdir/opossum"))
    assertThat(preppedBytesData.bytesData[0].filepaths).isEqualTo(listOf(File(inputDirectory.absolutePath + "/image.png").absolutePath))
    assertThat(preppedBytesData.bytesData[1].filepaths).isEqualTo(listOf(File(inputDirectory.absolutePath + "/subdir/opossum").absolutePath))
    assertThat(preppedBytesData.bytesData[0].bytes).isNull()
    assertThat(preppedBytesData.bytesData[1].bytes).isNull()
  }

  @Test
  fun prep_bytes_data_with_bytes() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val prepData = PrepData.builder()
      .inputDirectory(inputDirectory)
      .preppedBytesDataOutputFile(PREPPED_DATA_JSON)
      .withBytes(true)
      .build()

    val jsonGenericHandler = JsonGenericHandler()
    // When
    prepData.prep()
    val preppedBytesData = jsonGenericHandler.getObjectFromJsonFileNewApi(PreppedBytesData::class.java, PREPPED_DATA_JSON) as PreppedBytesData

    // Then
    assertThat(preppedBytesData.bytesData).isNotNull
    assertThat(preppedBytesData.bytesData.size).isEqualTo(2)
    assertThat(preppedBytesData.bytesData[0].ids).isEqualTo(listOf("image.png"))
    assertThat(preppedBytesData.bytesData[1].ids).isEqualTo(listOf("subdir/opossum"))
    assertThat(preppedBytesData.bytesData[0].filepaths).isEqualTo(listOf(File(inputDirectory.absolutePath + "/image.png").absolutePath))
    assertThat(preppedBytesData.bytesData[1].filepaths).isEqualTo(listOf(File(inputDirectory.absolutePath + "/subdir/opossum").absolutePath))
    assertThat(preppedBytesData.bytesData[0].bytes).isNotNull()
    assertThat(preppedBytesData.bytesData[1].bytes).isNotNull()
  }

  @Test
  fun prep_safe_working_directory() {
    // Given
    val safeWdPath = SAFE_WORKING_DIRECTORY.absolutePath
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val prepData = PrepData.builder()
      .inputDirectory(inputDirectory)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .metadataOutputFile(METADATA_JSON)
      .build()

    val jsonGenericHandler = JsonGenericHandler()

    // When
    prepData.prep()
    val safeFiles = SAFE_WORKING_DIRECTORY.listFiles()!!
    Arrays.sort(safeFiles)
    val metadata = jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata::class.java, METADATA_JSON) as Metadata
    val filesMap = metadata.filesMap

    // Then
    assertThat(safeFiles.size).isEqualTo(2)
    assertThat(safeFiles[0].absolutePath).isEqualTo(File("$safeWdPath/0").absolutePath)
    assertThat(safeFiles[1].absolutePath).isEqualTo(File("$safeWdPath/1").absolutePath)
    assertThat(metadata.inputDirectoryPath).isEqualTo(inputDirectory.absolutePath)
    assertThat(metadata.rawConversion).isFalse()
    assertThat(filesMap.size).isEqualTo(2)
    filesMap[0]?.let { assertThat(File(it).exists()).isTrue() }
    filesMap[1]?.let { assertThat(File(it).exists()).isTrue() }
  }

  @Test
  fun prep_safe_working_directory_with_raw_from_input_directory() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val prepData = PrepData.builder()
      .inputDirectory(inputDirectory)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .withRawConverter(true)
      .expectedRawPixelsCount(100 * 1024)
      .metadataOutputFile(METADATA_JSON)
      .build()

    val jsonGenericHandler = JsonGenericHandler()

    // When
    prepData.prep()
    val safeFiles = SAFE_WORKING_DIRECTORY.listFiles()!!
    Arrays.sort(safeFiles)
    val metadata = jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata::class.java, METADATA_JSON) as Metadata
    val filesMap = metadata.filesMap

    val rawBytes = FileUtils.readFileToByteArray(safeFiles[0])
    val crc32 = CRC32()
    crc32.update(rawBytes)

    // Then
    assertThat(safeFiles.size).isEqualTo(2)
    assertThat(metadata.rawConversion).isTrue()
    assertThat(filesMap.size).isEqualTo(2)
    assertThat(crc32.value).isEqualTo(3969735716L)
  }

  @Test
  fun prep_safe_working_directory_with_raw_from_bytes_data() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val bytesData = BytesData.loadFromDirectory(inputDirectory)

    val prepData = PrepData.builder()
      .bytesData(bytesData)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .withRawConverter(true)
      .metadataOutputFile(METADATA_JSON)
      .build()

    val jsonGenericHandler = JsonGenericHandler()

    // When
    prepData.prep()
    val safeFiles = SAFE_WORKING_DIRECTORY.listFiles()!!
    Arrays.sort(safeFiles)
    val metadata = jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata::class.java, METADATA_JSON) as Metadata
    val filesMap = metadata.filesMap

    val rawBytes = FileUtils.readFileToByteArray(safeFiles[0])
    val crc32 = CRC32()
    crc32.update(rawBytes)

    // Then
    assertThat(safeFiles.size).isEqualTo(2)
    assertThat(metadata.rawConversion).isTrue()
    assertThat(filesMap.size).isEqualTo(2)
    assertThat(crc32.value).isEqualTo(2829745108L)
  }

  @Test
  fun prep_from_bytes_data_with_bytes_but_without_filepaths_should_do_nothing() {
    // Given
    val jsonGenericHandler = JsonGenericHandler()

    val bytesData = arrayOfNulls<BytesData>(1)
    bytesData[0] = BytesData(listOf(""), byteArrayOf(0x12))

    val prepData = PrepData.builder()
      .bytesData(bytesData)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .withRawConverter(true)
      .metadataOutputFile(METADATA_JSON)
      .build()

    // When
    prepData.prep()

    val metadata = jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata::class.java, METADATA_JSON) as Metadata
    val filesMap = metadata.filesMap

    // Then
    assertThat(SAFE_WORKING_DIRECTORY.listFiles()?.size).isEqualTo(0)
    assertThat(filesMap.size).isEqualTo(0)
  }

  @Test
  fun prep_data_stats() {
    // Given
    val inputDirectory = File(javaClass.getResource("/prep-data-test-input-dir").file)
    val prepData = PrepData.builder()
      .inputDirectory(inputDirectory)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .withRawConverter(true)
      .expectedRawPixelsCount(100 * 1024)
      .metadataOutputFile(METADATA_JSON)
      .build()

    PrepData.clearStats()

    // When
    prepData.prep()
    val safeFiles = SAFE_WORKING_DIRECTORY.listFiles()!!
    Arrays.sort(safeFiles)

    val rawBytes = FileUtils.readFileToByteArray(safeFiles[0])
    val crc32 = CRC32()
    crc32.update(rawBytes)

    val stats = PrepData.statsDurationMillis()

    // Then
    assertThat(stats).contains("prep data duration millis")
    assertThat(stats).contains("raw conversion duration millis")
  }

  companion object {
    private val PREPPED_DATA_JSON = File("bytes-data.json")
    private val METADATA_JSON = File("metadata.json")
    private val SAFE_WORKING_DIRECTORY = File("safe-working-directory")
  }
}
