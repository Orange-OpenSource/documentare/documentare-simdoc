package com.orange.documentare.core.prepdata;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.system.inputfilesconverter.FilesMap;
import com.orange.documentare.core.system.inputfilesconverter.Metadata;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;

public class PrepDataConcatenationTest {

  private static File PREPPED_DATA_JSON = new File("bytes-data.json");
  private static File METADATA_JSON = new File("metadata.json");

  private static File SAFE_WORKING_DIRECTORY = new File("safe-working-directory");

  private JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(PREPPED_DATA_JSON);
    FileUtils.deleteQuietly(METADATA_JSON);
    FileUtils.deleteQuietly(SAFE_WORKING_DIRECTORY);
  }

  @Test
  public void prep_data_with_concatenation_without_raw_transformation() throws IOException {
    // Given
    File f0 = new File(getClass().getResource(
      "/prep-data-test-input-dir/subdir/opossum").getFile());
    File f1 = new File(getClass().getResource(
      "/prep-data-test-input-dir/image.png").getFile());

    BytesData[] bytesData = {
      new BytesData(asList("f0", "f1"), asList(f0.getAbsolutePath(), f1.getAbsolutePath()))
    };

    PrepData prepData = PrepData.builder()
      .bytesData(bytesData)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .metadataOutputFile(METADATA_JSON)
      .build();

    // When
    prepData.prep();
    File[] safeFiles = SAFE_WORKING_DIRECTORY.listFiles();
    Arrays.sort(safeFiles);
    Metadata metadata =
      (Metadata) jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata.class, METADATA_JSON);
    FilesMap filesMap = metadata.filesMap;

    // Then
    String safeWdPath = SAFE_WORKING_DIRECTORY.getAbsolutePath();
    assertThat(safeFiles.length).isEqualTo(1);
    assertThat(safeFiles[0].getAbsolutePath()).isEqualTo(new File(safeWdPath + "/0").getAbsolutePath());
    assertThat(safeFiles[0].length()).isEqualTo(17608);
    assertThat(metadata.rawConversion).isFalse();
    assertThat(filesMap.size()).isEqualTo(1);
    assertThat(new File(filesMap.get(0)).exists()).isTrue();
  }

  @Test
  public void prep_data_with_concatenation_with_raw_transformation() throws IOException {
    // Given
    File f0 = new File(getClass().getResource("/prep-data-test-input-dir/image.png").getFile());

    BytesData[] bytesData = {
      new BytesData(asList("f0", "f1"), asList(f0.getAbsolutePath(), f0.getAbsolutePath()))
    };

    PrepData prepData = PrepData.builder()
      .bytesData(bytesData)
      .withRawConverter(true)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .metadataOutputFile(METADATA_JSON)
      .build();

    // When
    prepData.prep();
    File[] safeFiles = SAFE_WORKING_DIRECTORY.listFiles();
    Arrays.sort(safeFiles);
    Metadata metadata =
      (Metadata) jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata.class, METADATA_JSON);
    FilesMap filesMap = metadata.filesMap;

    // Then
    String safeWdPath = SAFE_WORKING_DIRECTORY.getAbsolutePath();
    assertThat(safeFiles.length).isEqualTo(1);
    assertThat(safeFiles[0].getAbsolutePath()).isEqualTo(new File(safeWdPath + "/0").getAbsolutePath());
    assertThat(safeFiles[0].length()).isEqualTo(2640);
    assertThat(metadata.rawConversion).isTrue();
    assertThat(filesMap.size()).isEqualTo(1);
    assertThat(new File(filesMap.get(0)).exists()).isTrue();
  }

  @Test
  public void prep_data_with_concatenation_with_raw_transformation_mixing_image_and_text_files() throws IOException {
    // Given
    File f0 = new File(getClass().getResource(
      "/prep-data-test-input-dir/subdir/opossum").getFile());
    File f1 = new File(getClass().getResource(
      "/prep-data-test-input-dir/image.png").getFile());

    BytesData[] bytesData = {
      new BytesData(asList("f0", "f1"), asList(f0.getAbsolutePath(), f1.getAbsolutePath()))
    };

    PrepData prepData = PrepData.builder()
      .bytesData(bytesData)
      .withRawConverter(true)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(SAFE_WORKING_DIRECTORY)
      .metadataOutputFile(METADATA_JSON)
      .build();

    // When
    prepData.prep();
    File[] safeFiles = SAFE_WORKING_DIRECTORY.listFiles();
    Arrays.sort(safeFiles);
    Metadata metadata =
      (Metadata) jsonGenericHandler.getObjectFromJsonFileNewApi(Metadata.class, METADATA_JSON);
    FilesMap filesMap = metadata.filesMap;

    // Then
    String safeWdPath = SAFE_WORKING_DIRECTORY.getAbsolutePath();
    assertThat(safeFiles.length).isEqualTo(1);
    assertThat(safeFiles[0].getAbsolutePath()).isEqualTo(new File(safeWdPath + "/0").getAbsolutePath());
    assertThat(safeFiles[0].length()).isEqualTo(18404);
    assertThat(metadata.rawConversion).isTrue();
    assertThat(filesMap.size()).isEqualTo(1);
    assertThat(new File(filesMap.get(0)).exists()).isTrue();
  }
}
