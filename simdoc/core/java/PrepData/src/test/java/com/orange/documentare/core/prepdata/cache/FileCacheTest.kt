package com.orange.documentare.core.prepdata.cache

import org.apache.commons.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import java.io.File
import java.io.IOException

class FileCacheTest {
  private val testFile = "/prep-data-test-input-dir/image.png"
  private val cacheDirectoryAbsolutePath = "/tmp/file-cache"

  @BeforeEach
  @AfterEach
  fun clean() {
    FileCache.clearStats()
    FileUtils.deleteQuietly(File(cacheDirectoryAbsolutePath))
  }

  @Test
  fun `can reuse an existing cache directory without crashing`() {
    FileCache(cacheDirectoryAbsolutePath)
    FileCache(cacheDirectoryAbsolutePath)
  }

  @Test
  fun `raise IO exception if we can not create cache directory`() {
    try {
      FileCache("/abcdefgh")
      fail("should have thrown")
    } catch (e: Exception) {
      assertThat(e).isInstanceOf(IOException::class.java)
      assertThat(e.message).isEqualTo("Failed to create cache directory at '/abcdefgh'")
    }
  }

  @Test
  fun `raise Security exception if we can not create a file in cache directory`() {
    try {
      FileCache("/bin")
      fail("should have thrown")
    } catch (e: Exception) {
      assertThat(e).isInstanceOf(SecurityException::class.java)
      assertThat(e.message).isEqualTo("Failed to write in cache directory")
    }
  }

  @Test
  fun `can not get a file which is not in cache`() {
    // given
    val fileCache = FileCache(cacheDirectoryAbsolutePath)
    val fileAbsolutePath = javaClass.getResource(testFile).file

    // when
    val cacheFileAbsolutePath = fileCache.get(FileSignature.forFile(fileAbsolutePath))

    // then
    assertThat(cacheFileAbsolutePath).isNull()
  }

  @Test
  fun `can add and then retrieve a file from cache`() {
    // given
    val fileCache = FileCache(cacheDirectoryAbsolutePath)
    val fileAbsolutePath = javaClass.getResource(testFile).file

    // when
    fileCache.add(FileSignature.forFile(fileAbsolutePath), File(fileAbsolutePath))
    val cacheFileAbsolutePath = fileCache.get(FileSignature.forFile(fileAbsolutePath))

    // then
    assertThat(cacheFileAbsolutePath).isNotNull()
    assertThat(File(cacheFileAbsolutePath!!).isFile).isTrue()
    assertThat(cacheFileAbsolutePath).isEqualTo("/tmp/file-cache/384E2D65817F755A8862C18765D4D84D")
  }

  @Test
  fun `can list cache files`() {
    val fileCache = FileCache(cacheDirectoryAbsolutePath)
    val fileAbsolutePath = javaClass.getResource(testFile).file
    fileCache.add(FileSignature.forFile(fileAbsolutePath), File(fileAbsolutePath))

    // when
    val cacheFiles = fileCache.files()

    // then
    assertThat(cacheFiles).containsExactly("/tmp/file-cache/384E2D65817F755A8862C18765D4D84D")
  }

  @Test
  fun `can get cache size`() {
    val fileCache = FileCache(cacheDirectoryAbsolutePath)
    val fileAbsolutePath = javaClass.getResource(testFile).file
    fileCache.add(FileSignature.forFile(fileAbsolutePath), File(fileAbsolutePath))

    // when
    val cacheSize = fileCache.size()

    // then
    assertThat(cacheSize.sizeInMegaBytes).isEqualTo(4.99725341796875E-4)
  }

  @Test
  fun `can get cache hits`() {
    val fileCache = FileCache(cacheDirectoryAbsolutePath)
    val fileAbsolutePath = javaClass.getResource(testFile).file
    fileCache.add(FileSignature.forFile(fileAbsolutePath), File(fileAbsolutePath))

    // when/then
    assertThat(FileCache.hits()).isEqualTo(0)

    fileCache.get(FileSignature.forFile(fileAbsolutePath))
    assertThat(FileCache.hits()).isEqualTo(1)
  }

  @Test
  fun `can get cache misses`() {
    val fileCache = FileCache(cacheDirectoryAbsolutePath)
    val fileAbsolutePath = javaClass.getResource(testFile).file

    // when/then
    assertThat(FileCache.misses()).isEqualTo(0)

    fileCache.get(FileSignature.forFile(fileAbsolutePath))
    assertThat(FileCache.misses()).isEqualTo(1)
  }
}