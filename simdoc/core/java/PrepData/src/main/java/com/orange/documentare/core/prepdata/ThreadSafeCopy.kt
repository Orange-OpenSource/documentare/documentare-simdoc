package com.orange.documentare.core.prepdata

import java.io.File
import java.lang.IllegalStateException
import java.nio.file.FileAlreadyExistsException
import java.nio.file.Files
import java.nio.file.NoSuchFileException
import java.nio.file.StandardCopyOption

object ThreadSafeCopy {

  @JvmStatic
  fun copy(src: File, dest: File) {
    try {
      Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING)
    } catch (e: FileAlreadyExistsException) {
      waitForFileToBeWrittenByAnotherThread(dest)
    } catch (e: NoSuchFileException) {
      waitForFileToBeWrittenByAnotherThread(dest)
    }
  }

  private fun waitForFileToBeWrittenByAnotherThread(dest: File) {
    var timeoutLoop = 500 // 5000 ms
    while (!Files.isWritable(dest.toPath())) {
      try {
        Thread.sleep(10)
        timeoutLoop--
        if (timeoutLoop <= 0) {
          throw IllegalStateException("Stop waiting for file (${dest.absolutePath}) to be written")
        }
      } catch (ex: InterruptedException) {
        throw RuntimeException(ex)
      }
    }
  }
}
