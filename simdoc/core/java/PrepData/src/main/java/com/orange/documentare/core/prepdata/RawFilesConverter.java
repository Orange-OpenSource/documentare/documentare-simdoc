package com.orange.documentare.core.prepdata;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import com.orange.documentare.core.comp.clustering.stats.SummaryStats;
import com.orange.documentare.core.prepdata.cache.FileCache;
import com.orange.documentare.core.prepdata.cache.FileSignature;
import com.orange.documentare.core.prepdata.cache.Signature;
import com.orange.documentare.core.system.Bytes;
import com.orange.documentare.core.system.inputfilesconverter.FileConverter;
import com.orange.documentare.core.system.inputfilesconverter.FileConverterException;
import com.orange.documentare.core.system.inputfilesconverter.FilesToConcat;
import com.orange.documentare.core.system.inputfilesconverter.RegularFilesConverter;
import com.orange.documentare.core.system.nativeinterface.NativeInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.stream;

@Slf4j
@RequiredArgsConstructor
public class RawFilesConverter implements FileConverter {

  private static SummaryStats conversionDurationStats = new SummaryStats();

  public static String statsDurationMillis() {
    return "raw conversion duration millis (img -> raw or regular file rewrite):\n" + conversionDurationStats;
  }

  public static void clearStats() {
    conversionDurationStats.clear();
  }


  private static final String[] IMAGES_EXTENSION = {
    ".png", ".jpg", ".jpeg", ".tif", ".tiff", ".bmp"
  };

  private final RegularFilesConverter regularFilesConverter = new RegularFilesConverter();
  private final int expectedPixelsCount;
  private final FileCache fileCache;

  @Override
  public void convert(FilesToConcat source, @NotNull File destination) {
    long t0 = System.currentTimeMillis();

    if (source.size() == 1) {
      convertSingleFile(source, destination);
    } else {
      convertFilesAndConcat(source, destination);
    }

    conversionDurationStats.addValue(System.currentTimeMillis() - t0);
  }

  private void convertFilesAndConcat(FilesToConcat source, File destination) {
    byte[] concatBytes = {};
    byte[] bytes;
    for (File file : source) {
      if (isImage(file)) {
        convertToRaw(file, destination);
        try {
          bytes = FileUtils.readFileToByteArray(destination);
        } catch (IOException e) {
          throw new FileConverterException(String.format("Failed to read destination file '%s'", destination.getAbsolutePath()));
        }
      } else {
        bytes = loadRegularFileBytes(file);
      }
      concatBytes = Bytes.concat(concatBytes, bytes);
    }
    writeBytesTo(concatBytes, destination);
  }

  private void convertSingleFile(FilesToConcat source, File destination) {
    if (isImage(source.get(0))) {
      convertToRaw(source.get(0), destination);
    } else {
      regularFilesConverter.convert(source, destination);
    }
  }

  private void convertToRaw(File source, File destination) {
    boolean fileRetrievedFromCache = tryToRetrieveFileFromCache(source, destination);
    if (fileRetrievedFromCache) {
      return;
    }

    List<String> options = new ArrayList<>();
    options.add(String.valueOf(expectedPixelsCount));
    options.add(destination.getAbsolutePath());
    options.add(source.getAbsolutePath());

    NativeInterface.launch(
      "raw-converter", options.toArray(new String[options.size()]));

    if (fileCache != null) {
      addRawFileToCache(fileCache, source, destination, expectedPixelsCount);
    }
  }

  private boolean tryToRetrieveFileFromCache(File source, File destination) {
    if (fileCache == null) {
      return false;
    }
    String cachedFileAbsolutePath = fileCache.get(cacheFileSignature(source, expectedPixelsCount));
    if (cachedFileAbsolutePath != null) {
      try {
        ThreadSafeCopy.copy((new File(cachedFileAbsolutePath)), destination);
        return true;
      } catch (Exception e) {
        log.error("An error occurred while copying file from raw image cache", e);
      }
    }
    return false;
  }

  private void addRawFileToCache(FileCache fileCache, File pristineFile, File rawFile, int expectedPixelsCount) {
    fileCache.add(cacheFileSignature(pristineFile, expectedPixelsCount), rawFile);
  }

  private Signature cacheFileSignature(File pristineFile, int expectedPixelsCount) {
    return FileSignature.INSTANCE.forFile(pristineFile.getAbsolutePath(), String.format("raw-%d", expectedPixelsCount));
  }

  private byte[] loadRegularFileBytes(File file) {
    byte[] bytes;
    try {
      bytes = FileUtils.readFileToByteArray(file);
    } catch (IOException e) {
      throw new FileConverterException(String.format("Failed to load file '%s'", file.getAbsolutePath()));
    }
    return bytes;
  }

  private void writeBytesTo(byte[] bytes, File destination) {
    try {
      FileUtils.writeByteArrayToFile(destination, bytes);
    } catch (IOException e) {
      throw new IllegalStateException(String.format("Failed to write file to '%s': %s", destination.getAbsolutePath(), e.getMessage()));
    }
  }

  public boolean isImage(File source) {
    String lowerCaseFilename = source.getName().toLowerCase();
    return stream(IMAGES_EXTENSION)
      .anyMatch(lowerCaseFilename::endsWith);
  }
}
