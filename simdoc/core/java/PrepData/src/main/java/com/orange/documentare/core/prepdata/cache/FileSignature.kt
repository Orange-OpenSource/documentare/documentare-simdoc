package com.orange.documentare.core.prepdata.cache

import org.apache.commons.codec.digest.DigestUtils
import java.io.File
import java.util.*

object FileSignature {
  fun forFile(fileAbsolutePath: String, prefix: String = "") =
    Signature(prefix + (if (prefix.isEmpty()) "" else "-") + signContent(fileAbsolutePath))

  private fun signContent(fileAbsolutePath: String) =
    DigestUtils.md5Hex(File(fileAbsolutePath).readBytes()).uppercase(Locale.getDefault())
}
