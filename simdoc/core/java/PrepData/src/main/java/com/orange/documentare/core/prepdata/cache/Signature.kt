package com.orange.documentare.core.prepdata.cache

data class Signature(val value: String)