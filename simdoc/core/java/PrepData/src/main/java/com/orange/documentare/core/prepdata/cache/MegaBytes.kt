package com.orange.documentare.core.prepdata.cache

data class MegaBytes(val sizeInMegaBytes: Double) {

  companion object {
    fun forSizeInBytes(sizeInBytes: Long) = MegaBytes(sizeInBytes.toDouble() / 1024 / 1024)
  }
}