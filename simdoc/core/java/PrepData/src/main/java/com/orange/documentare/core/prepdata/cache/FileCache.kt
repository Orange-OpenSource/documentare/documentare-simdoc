package com.orange.documentare.core.prepdata.cache

import com.orange.documentare.core.prepdata.ThreadSafeCopy
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.IOException
import java.util.*

data class FileCache(
  private val cacheDirectoryAbsolutePath: String,
  private val cacheDirectory: File = File(cacheDirectoryAbsolutePath)
) {

  constructor(cacheDirectoryAbsolutePath: String) :
    this(cacheDirectoryAbsolutePath, File(cacheDirectoryAbsolutePath))

  init {
    if (!cacheDirectory.exists()) {
      val cacheDirectoryCreated = cacheDirectory.mkdirs()
      if (!cacheDirectoryCreated) {
        throw IOException("Failed to create cache directory at '$cacheDirectoryAbsolutePath'")
      }
    }
    try {
      val randomFile = File("$cacheDirectoryAbsolutePath/${UUID.randomUUID()}")
      randomFile.createNewFile()
      randomFile.delete()
    } catch (e: Exception) {
      throw SecurityException("Failed to write in cache directory", e)
    }
  }

  fun get(signature: Signature): String? {
    val cachedFile = cachedFile(signature)
    return if (cachedFile.isFile) {
      hit++
      cachedFile.absolutePath
    } else {
      miss++
      null
    }
  }

  fun add(fileSignature: Signature, file: File) {
    ThreadSafeCopy.copy(file, cachedFile(fileSignature))
  }

  fun files() = cacheDirectory.listFiles()?.let { files -> files.map { it.absolutePath } } ?: emptyList()
  fun size() = MegaBytes.forSizeInBytes(FileUtils.sizeOfDirectory(cacheDirectory))

  private fun cachedFile(signature: Signature) =
    File(cacheDirectoryAbsolutePath + "/" + signature.value)

  companion object {
    private var hit = 0L
    private var miss = 0L
    fun hits() = hit
    fun misses() = miss

    fun clearStats() {
      hit = 0
      miss = 0
    }
  }
}
