/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.system.inputfilesconverter

import com.orange.documentare.core.system.Bytes.concat
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Files.createLink

open class RegularFilesConverter : FileConverter {

  private val log: Logger = LoggerFactory.getLogger(RegularFilesConverter::class.java)
  private var canNotCreateHardLink = false

  override fun convert(source: FilesToConcat, destination: File) {
    if (source.size == 1) {
      createLink(destination, source)
    } else {
      concatFilesToDestination(source, destination)
    }
  }

  private fun createLink(destination: File, source: FilesToConcat) {
    val sourceFile = source[0]
    if (!sourceFile.isFile) {
      throw FileConverterException("Failed to create link, '${sourceFile.absolutePath}' is not a file")
    }

    // TODO FIXME: test following logic
    if (canNotCreateHardLink) {
      createSymbolicLink(destination, sourceFile)
    } else {
      tryToCreateHardLink(destination, sourceFile)?.let {
        canNotCreateHardLink = true
        createSymbolicLink(destination, sourceFile)
      }
    }
  }

  private fun tryToCreateHardLink(destination: File, sourceFile: File): String? {
    return try {
      createHardLink(destination, sourceFile)
      null
    } catch (e: IOException) {
      val error = "Failed to create link '${sourceFile.absolutePath}' -> '${destination.absolutePath}'; error was '$e'"
      log.warn(error)
      error
    }
  }

  private fun createSymbolicLink(destination: File, sourceFile: File) {
    try {
      Files.createSymbolicLink(destination.toPath(), sourceFile.canonicalFile.toPath())
    } catch (e: IOException) {
      throw FileConverterException("Failed to create link '${sourceFile.absolutePath}' -> '${destination.absolutePath}'; error was '$e'")
    }
  }

  private fun concatFilesToDestination(source: FilesToConcat, destination: File) {
    var bytes = byteArrayOf()
    for (file in source) {
      bytes = try {
        concat(bytes, FileUtils.readFileToByteArray(file))
      } catch (e: IOException) {
        throw FileConverterException("Failed to load file '${file.absolutePath}'")
      }
    }
    try {
      FileUtils.writeByteArrayToFile(destination, bytes)
    } catch (e: IOException) {
      throw FileConverterException("Failed to write file '${destination.absolutePath}'")
    }
  }

  // open for test purpose
  open fun createHardLink(destination: File, sourceFile: File) {
    createLink(destination.toPath(), sourceFile.canonicalFile.toPath())
  }
}