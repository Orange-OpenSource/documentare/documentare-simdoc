/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.system.inputfilesconverter

import java.io.File
import java.util.*

class FilesMap : HashMap<Int, String>() {
  fun simpleFilenameAt(index: Int): String? = simpleFilename(get(index))

  fun getKeyByValue(value: String): Int? =
    entries.find { it.value == value }?.key

  companion object {
    @JvmStatic
    fun simpleFilename(absFilename: String?): String? {
      val lastIndexOfDirSep = absFilename!!.lastIndexOf(File.separator)
      return if (lastIndexOfDirSep < 0) absFilename else absFilename.substring(lastIndexOfDirSep + 1)
    }
  }
}