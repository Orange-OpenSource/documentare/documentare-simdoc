package com.orange.documentare.core.system.thumbnail;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ThumbnailOptions {
  public final Optional<Boolean> forcePng;
  public final Optional<String> label;
  public final Optional<String[]> convertOptions;

  public static ThumbnailOptionsBuilder builder() {
    return new ThumbnailOptionsBuilder();
  }

  public static ThumbnailOptions defaults() {
    return builder().build();
  }

  public static class ThumbnailOptionsBuilder {
    private Optional<Boolean> forcePng = Optional.empty();
    private Optional<String> label = Optional.empty();
    private Optional<String[]> convertOptions = Optional.empty();

    public ThumbnailOptions build() {
      return new ThumbnailOptions(forcePng, label, convertOptions);
    }

    public ThumbnailOptionsBuilder forcePng() {
      forcePng = Optional.of(true);
      return this;
    }

    public ThumbnailOptionsBuilder label(String label) {
      this.label = Optional.of(label);
      return this;
    }

    public ThumbnailOptionsBuilder convertOptions(String[] convertOptions) {
      this.convertOptions = Optional.of(convertOptions);
      return this;
    }
  }
}
