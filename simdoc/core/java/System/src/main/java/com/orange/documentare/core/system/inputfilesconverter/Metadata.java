package com.orange.documentare.core.system.inputfilesconverter;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbProperty;
import java.util.Map;

public class Metadata {
  public final String inputDirectoryPath;
  public final FilesMap filesMap;
  public final boolean rawConversion;

  public Metadata(
    String inputDirectoryPath,
    FilesMap filesMap,
    boolean rawConversion) {
    this.inputDirectoryPath = inputDirectoryPath;
    this.filesMap = filesMap;
    this.rawConversion = rawConversion;
  }

  @JsonbCreator
  public Metadata(
    @JsonbProperty("inputDirectoryPath") String inputDirectoryPath,
    @JsonbProperty("filesMap") Map<String, String> filesMap,
    @JsonbProperty("rawConversion") boolean rawConversion) {
    this(inputDirectoryPath, buildFilesMap(filesMap), rawConversion);
  }

  static private FilesMap buildFilesMap(Map<String, String> filesMap) {
    FilesMap map = new FilesMap();
    filesMap.forEach((key, value) -> map.put(Integer.valueOf(key), value));
    return map;
  }
}
