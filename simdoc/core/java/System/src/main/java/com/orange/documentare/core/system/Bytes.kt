/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.system

object Bytes {

  @JvmStatic
  fun concat(vararg arrays: ByteArray): ByteArray {
    val length = arrays.map { it.size }.sum()
    val result = ByteArray(length)
    var pos = 0
    for (array in arrays) {
      System.arraycopy(array, 0, result, pos, array.size)
      pos += array.size
    }
    return result
  }

}