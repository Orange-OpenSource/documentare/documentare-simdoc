package com.orange.documentare.core.system.inputfilesconverter

import org.assertj.core.api.BDDAssertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class RegularFilesConverterTest {

  private val converter = RegularFilesConverter()

  @TempDir
  lateinit var destDir: File

  @Test
  fun `convert source file to destination`() {
    // given
    val sourceFile = File(javaClass.getResource("/bastet.png").file)
    val destinationFile = File(destDir.absolutePath + "/dest-file")
    val sourceFilesToConcat = FilesToConcat().also { it.add(sourceFile) }

    // when
    converter.convert(sourceFilesToConcat, destinationFile)

    // then
    then(destinationFile.length()).isEqualTo(206658)
  }

  @Test
  fun `symbolic link target is an absolute path`() {
    // given
    val sourceFile = File("${destDir.absolutePath}${File.separator}titi").also { it.createNewFile() }
    val destinationFile = File("${destDir.absolutePath}${File.separator}dest-file")
    val sourceFilesToConcat = FilesToConcat().also { it.add(sourceFile) }

    // when
    converter.convert(sourceFilesToConcat, destinationFile)

    // then
    then(destinationFile).exists()
  }

  @Test
  fun `convert source fileS to destination`() {
    // given
    val sourceFile1 = File(javaClass.getResource("/bastet.png").file)
    val sourceFile2 = File(javaClass.getResource("/image.png").file)
    val destinationFile = File(destDir.absolutePath + "/dest-file")
    val sourceFilesToConcat = FilesToConcat().also { it.addAll(listOf(sourceFile1, sourceFile2)) }

    // when
    converter.convert(sourceFilesToConcat, destinationFile)

    // then
    then(destinationFile.length()).isEqualTo(207182)
  }

  @Test
  fun `throw exception if source file is not reachable`() {
    // given
    val sourceFile = File("/rototo")
    val destinationFile = File(destDir.absolutePath + "/dest-file")
    val sourceFilesToConcat = FilesToConcat().also { it.add(sourceFile) }

    // when
    val throwable = catchThrowable { converter.convert(sourceFilesToConcat, destinationFile) }

    // then
    then(throwable.message).isEqualTo("Failed to create link, '/rototo' is not a file")
  }

  @Test
  fun `throw exception if destination file is not reachable`() {
    // given
    val sourceFile = File(javaClass.getResource("/bastet.png").file)
    val destinationFile = File("/rototo")
    val sourceFilesToConcat = FilesToConcat().also { it.add(sourceFile) }

    // when
    val throwable = catchThrowable { converter.convert(sourceFilesToConcat, destinationFile) }

    // then
    then(throwable).isInstanceOf(FileConverterException::class.java)
    then(throwable.message).contains("bastet.png' -> '/rototo'")
  }
}
