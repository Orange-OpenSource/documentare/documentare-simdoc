package com.orange.documentare.core.system.inputfilesconverter.thumbnail;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import com.orange.documentare.core.system.nativeinterface.NativeException;
import com.orange.documentare.core.system.thumbnail.Thumbnail;
import com.orange.documentare.core.system.thumbnail.ThumbnailOptions;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(ZohhakRunner.class)
public class ThumbnailTest {

  private File image = new File(getClass().getResource("/bastet.png").getFile());
  private File thumbnail = new File("thumbnail.png");
  private File thumbnailLog = new File("thumbnail.png.log");
  private File thumbnailNoExt = new File("thumbnail");
  private File thumbnailNoExtLog = new File("thumbnail.log");

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(thumbnail);
    FileUtils.deleteQuietly(thumbnailNoExt);
    FileUtils.deleteQuietly(thumbnailLog);
    FileUtils.deleteQuietly(thumbnailNoExtLog);
  }

  @TestWith({
    "file.pnG, true",
    "file.jpG, true",
    "file.jpEg, true",
    "file.tiF, true",
    "file.tiFf, true",
    "file.pDf, true",

    "png.f, false",
    "jpg.f, false",
    "jpeg.f, false",
    "tif.f, false",
    "tiff.f, false",
    "pdf.f, false"
  })
  public void supported_image_extension(String filename, boolean expectedSupported) throws IOException {
    // when
    boolean supported = Thumbnail.canCreateThumbnail(new File(filename));
    // then
    assertThat(supported).isEqualTo(expectedSupported);
  }

  @Test(expected = NullPointerException.class)
  public void raise_exception_if_input_image_is_null() throws IOException {
    Thumbnail.createThumbnail(null, thumbnail, ThumbnailOptions.defaults());
  }

  @Test(expected = NativeException.class)
  public void raise_exception_if_input_image_is_not_readable() throws IOException {
    Thumbnail.createThumbnail(new File("/pouet-pouet"), thumbnail, ThumbnailOptions.defaults());
  }

  @Test(expected = NullPointerException.class)
  public void raise_exception_if_output_image_is_null() throws IOException {
    Thumbnail.createThumbnail(image, null, ThumbnailOptions.defaults());
  }

  @Test
  public void create_thumbnail() throws IOException {
    // When
    Thumbnail.createThumbnail(image, thumbnail, ThumbnailOptions.defaults());
    // Then
    assertThat(thumbnail.exists()).isTrue();
  }

  @Test
  public void create_thumbnail_and_force_png() throws IOException {
    // Given
    ThumbnailOptions options = ThumbnailOptions.builder().forcePng().build();
    // When
    Thumbnail.createThumbnail(image, thumbnailNoExt, options);
    // Then
    assertThat(thumbnailNoExt.exists()).isTrue();
  }

  @Test
  public void create_thumbnail_with_options() throws IOException {
    // Given
    String[] convertOptions = {
      "-thumbnail", "100x100^", "-gravity", "center", "-extent", "100x100"
    };
    ThumbnailOptions options = ThumbnailOptions.builder().convertOptions(convertOptions).build();
    // When
    Thumbnail.createThumbnail(image, thumbnail, options);
    // Then
    assertThat(thumbnail.exists()).isTrue();
  }
}
