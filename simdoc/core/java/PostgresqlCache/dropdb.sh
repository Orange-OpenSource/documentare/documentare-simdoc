#!/usr/bin/env bash

# docker run --name postgresql-cache --rm -d -v postgresql-cache:/var/lib/postgresql/data -p5432:5432 -e POSTGRES_PASSWORD=sandbox postgres:15


# Macos flavor (need to be adapted for linux)
POSTGRES_SERVICE="postgresql@15"
DROPDB="$(brew --prefix $POSTGRES_SERVICE)/bin/dropdb"

die() {
	echo
	echo "❌ $1"
	echo
	exit 1
}

dropdb() {
  $DROPDB "$1"
}


####################################### MAIN #######################################
dropdb "cachedb" || die "Failed to drop database"
