#!/usr/bin/env bash

# docker stop postgresql-cache
# docker volume rm postgresql-cache


# Macos flavor (need to be adapted for linux)
POSTGRES_SERVICE="postgresql@15"
CMD_STOP_SERVICE="brew services stop"
CMD_GREP_POSTGRES_PROCESS="pgrep -l postgres"

die() {
	echo
	echo "❌ $1"
	echo
	exit 1
}

stop_service() {
	if $CMD_STOP_SERVICE "$1" > /dev/null
	then
		echo "✅ [SERVICE] $1 stopped"
	else
		die "[SERVICE] failed to stop $1"
	fi
}

if $CMD_GREP_POSTGRES_PROCESS > /dev/null
then
  stop_service "$POSTGRES_SERVICE"
else
  echo "✅ [SERVICE] $POSTGRES_SERVICE already stopped"
fi
