#!/usr/bin/env bash

# docker run --name postgresql-cache --rm -d -v postgresql-cache:/var/lib/postgresql/data -p5432:5432 -e POSTGRES_PASSWORD=sandbox postgres:15


# Macos flavor (need to be adapted for linux)
POSTGRES_SERVICE="postgresql@15"
PSQL="$(brew --prefix $POSTGRES_SERVICE)/bin/psql"
CMD_START_SERVICE="brew services run"
CMD_GREP_POSTGRES_PROCESS="pgrep -l postgres"

die() {
	echo
	echo "❌ $1"
	echo
	exit 1
}

start_service() {
	if $CMD_START_SERVICE "$1" > /dev/null
	then
		echo "✅ [SERVICE] $1 started"
	else
		die "[SERVICE] failed to start $1"
	fi
}

wait_for_postgres() {
  while :
  do
  	if $PSQL postgres -c 'SELECT * FROM pg_roles;' &> /dev/null
    then
  		echo "✅ [$POSTGRES_SERVICE] running"
  		break
  	fi
  	sleep 1
  done
}
create_postgres_user() {
  $PSQL postgres -c "CREATE USER postgres PASSWORD 'sandbox';" &> /dev/null
}
update_postgres_user_password() {
  if ! $PSQL postgres -c "ALTER USER postgres PASSWORD 'sandbox';" > /dev/null
  then
    die "[$POSTGRES_SERVICE] failed to update password"
  fi
}
update_postgres_create_db_permission() {
  if ! $PSQL postgres -c "ALTER USER postgres createdb;" > /dev/null
  then
    die "[$POSTGRES_SERVICE] failed to add 'createdb' permission"
  fi
}


####################################### MAIN #######################################
if $CMD_GREP_POSTGRES_PROCESS > /dev/null
then
  echo "✅ [SERVICE] $POSTGRES_SERVICE already started"
else
  start_service "$POSTGRES_SERVICE"
fi
wait_for_postgres
create_postgres_user
update_postgres_user_password
update_postgres_create_db_permission
echo "✅ [$POSTGRES_SERVICE] configured"
