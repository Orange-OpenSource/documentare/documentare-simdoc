package com.orange.documentare.core.postgresql.cache;

import java.util.HashMap;

public class FloatCacheInMemory implements FloatCache {

    private final HashMap<FloatCacheKey, Float> cache = new HashMap<>();

    @Override
    public void add(FloatCacheKey key, float value) {
        cache.put(key, value);
    }

    @Override
    public Float get(FloatCacheKey key) {
        return cache.get(key);
    }
}
