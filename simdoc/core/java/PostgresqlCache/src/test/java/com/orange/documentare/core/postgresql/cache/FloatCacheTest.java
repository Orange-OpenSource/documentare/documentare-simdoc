package com.orange.documentare.core.postgresql.cache;

import static com.orange.documentare.core.postgresql.cache.PostgresCache.*;
import static java.lang.Thread.sleep;
import static org.assertj.core.api.BDDAssertions.then;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.orange.documentare.core.postgresql.cache.FloatCache.FloatCacheKey;

class FloatCacheTest {

  private final int[] insertionDone = new int[1];

  static {
    PostgresCacheIO.disableMetrology();
  }

  @BeforeEach
  void setup() {
    PostgresCache.testOnlySetup(size -> insertionDone[0] = size);
  }

  @Test
  void in_memory_add_to_cache_then_retrieve_value() {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    // given
    FloatCacheKey key = new FloatCacheKey("key");
    int value = 1234;
    FloatCache floatCache = new FloatCacheInMemory();

    // when
    floatCache.add(key, value);

    // then
    then(floatCache.get(key)).isEqualTo(value);
  }

  @Test
  void in_memory_cache_miss_then_return_null() {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    FloatCacheInMemory cache = new FloatCacheInMemory();
    FloatCacheKey key = new FloatCacheKey("key");

    then(cache.get(key)).isNull();
  }


  // Integration test, postgresql must be up and running
  @Test
  void postgresql_cache_miss_then_return_null() {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    // when
    Float value = PostgresCache.get("not-existing-key");

    // then
    then(value).isNull();

    System.out.println(PostgresCacheStats.stats());
  }

  // Integration test, postgresql must be up and running
  @Test
  void postgresql_enqueue_data_to_insert_then_retrieve_it() throws InterruptedException {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    // given
    PostgresCache.setForceDequeueThresholdForTests(100);
    PostgresCache.flushQueueSync(); // interrupt thread to force timeout

    // when
    // 100 -> should force thread notification to persiste all data
    IntStream.range(0, 100).forEach(i -> PostgresCache.add(String.valueOf(i), i));

    // then
    waitForInsertion(100);
    IntStream.range(0, 100).forEach(i -> then(PostgresCache.get(String.valueOf(i))).isEqualTo(i));
  }

  // Integration test, postgresql must be up and running
  @Test
  void postgresql_enqueue_data_to_insert_then_retrieve_it_with_persistence_done_through_timeout() throws InterruptedException {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    // given
    PostgresCache.setFlushTimeoutMillisecondsForTests(1 * 1000);
    PostgresCache.flushQueueSync(); // interrupt thread to force timeout

    // when
    // first insert will trig setup and insertion will be done at once
    IntStream.range(0, 10).forEach(i -> PostgresCache.add(String.valueOf(i), i));
    waitForInsertion(10);
    // second insert will have to wait for timeout to trigger insertion
    IntStream.range(0, 20).forEach(i -> PostgresCache.add(String.valueOf(i), i));

    // then
    waitForInsertion(20);
    IntStream.range(0, 10).forEach(i -> then(PostgresCache.get(String.valueOf(i))).isEqualTo(i));
  }

  // Integration test, postgresql must be up and running
  @Test
  void postgresql_purge_first_entries_if_we_reach_threshold() {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    // given
    final int purgeCount = 10;
    final int cacheMaxSize = 20;
    PostgresCache.setFlushTimeoutMillisecondsForTests(1 * 1000);
    PostgresCache.setCacheSizeAndPurgeCountForTests(cacheMaxSize, purgeCount);
    PostgresCache.flushQueueSync(); // interrupt thread to force timeout

    insert_then_purge_then_check_values_test(cacheMaxSize, purgeCount);
  }

  // LONG Integration test, postgresql must be up and running
  @Disabled
  @Test
  void postgresql_purge_first_entries_if_we_reach_threshold_real_threshold_and_max_size() {
    insert_then_purge_then_check_values_test(PostgresCache.CACHE_MAX_SIZE, PostgresCache.PURGE_COUNT);
  }

  // Integration test, postgresql must be up and running
  @Test
  void postgresql_multiple_threads_insert_and_get_in_parallel() {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    // given
    AtomicInteger done = new AtomicInteger();
    int nbInsertions = 10;
    int nbThreads = Runtime.getRuntime().availableProcessors();
    IntStream.range(0, nbThreads).forEach(threadIndex -> new Thread(() -> {
      IntStream.range(threadIndex * nbInsertions, (threadIndex + 1) * nbInsertions).forEach(insertIndex -> {
        String key = md5For(insertIndex);
        float distance = distanceFor(insertIndex);
        PostgresCache.add(key, distance);
        try {
          sleep(1);
        }
        catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      });
      System.out.println("Thread " + Thread.currentThread().getId() + ": insertion done");
      done.getAndIncrement();
    }).start());
    while (done.get() != nbThreads) {
      try {
        sleep(1000);
      }
      catch (InterruptedException ignored) {
      }
    }
    done.set(0);
    PostgresCache.flushQueueSync();


    int readThreads = nbThreads;
    IntStream.range(0, readThreads).forEach(_ignored -> new Thread(() -> {
      IntStream.range(0, nbInsertions * readThreads).forEach(insertIndex -> {
        String key = md5For(insertIndex);
        Float value = PostgresCache.get(key);
        if (value == null || value != distanceFor(insertIndex)) {
          System.out.printf(
            "❌ Thread %d: insertIndex=%s, expected=%f, got=%f%n", Thread.currentThread().getId(), insertIndex, distanceFor(insertIndex),
            value == null ? -1 : value);
        }
      });
      System.out.println("Thread " + Thread.currentThread().getId() + ": read done");
      done.getAndIncrement();
    }).start());
    while (done.get() != readThreads) {
      try {
        sleep(100);
      }
      catch (InterruptedException ignored) {
      }
    }

    System.out.println(PostgresCacheStats.stats());
  }

  private void insert_then_purge_then_check_values_test(int cacheMaxSize, int purgeCount)
  {
    IntStream.range(0, cacheMaxSize / purgeCount).forEach(loopIndex -> {
      IntStream.range(loopIndex * purgeCount, (loopIndex + 1) * purgeCount)
        .forEach(i -> {
          if (i == 99999) {
            System.out.println("Inserting " + 99999);
          }
          PostgresCache.add(md5For(i), distanceFor(i));
        });
      while (PostgresCache.isQueueNotEmpty()) {
        try {
          sleep(100);
        }
        catch (InterruptedException ignored) {
        }
      }
    });


    // then
    IntStream.range(purgeCount, cacheMaxSize).forEach(i -> {
      Float value = PostgresCache.get(md5For(i));
      then(value).isEqualTo(distanceFor(i));
    });
  }

  private void waitForInsertion(int nbInserted) throws InterruptedException {
    while (insertionDone[0] != nbInserted) {
      sleep(1000);
    }
  }

  private static String md5For(int i) {
    try {
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      return byteArrayToHex(md5.digest(ByteBuffer.allocate(4).putInt(i).array()));
    }
    catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  private static String byteArrayToHex(byte[] a) {
    StringBuilder sb = new StringBuilder(a.length * 2);
    for (byte b : a)
      sb.append(String.format("%02x", b));
    return sb.toString();
  }

  private static float distanceFor(int i) {
    return 0.89974123f + (float) i / 1000000;
  }
}
