package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

record PostgresCacheMultithreadReadConnexion(Connection connection, PreparedStatement valueOfStatement) {

    private static final Logger LOG = LoggerFactory.getLogger(PostgresCacheMultithreadReadConnexion.class);

    static PostgresCacheMultithreadReadConnexion create() throws SQLException {
        Connection connection = PostgresCachePool.getConnection();
        PreparedStatement valueOfStatement;
        try {
            valueOfStatement = connection.prepareStatement("SELECT VALUE FROM cache WHERE KEY = ?");
        }
        catch (SQLException e) {
            return null;
        }
        return new PostgresCacheMultithreadReadConnexion(connection, valueOfStatement);
    }

    void closeSafely() {
        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                LOG.warn("[⚠️ PostgresCacheMultithreadReadConnexion] Failed to close connection", e);
            }
        }
    }
}
