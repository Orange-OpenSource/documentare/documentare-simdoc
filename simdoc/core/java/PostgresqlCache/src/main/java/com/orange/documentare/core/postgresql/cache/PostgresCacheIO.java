package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.sql.*;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PostgresCacheIO {

  private static final Logger LOG = LoggerFactory.getLogger(PostgresCacheIO.class);
  private static final int RETRY_INTERVAL_SECONDS = 60 * 1000;
  private static final String DATABASE_NAME = "cachedb";

  // SERIAL: auto-incrementing integer from 1 to 2^31-1
  // MD5 * 2: 32 * 2 = 64 hex characters since we can save the compression of X.Y with MD5xMD5y as key
  // KEY: unique constraint, handled with ON CONFLICT DO NOTHING in insert statement
  private static final String CREATE_CACHE_TABLE_IF_NOT_EXISTS = """
    CREATE TABLE IF NOT EXISTS cache (
            ID SERIAL PRIMARY KEY,
            KEY VARCHAR(64) NOT NULL UNIQUE,
            VALUE FLOAT NOT NULL
        )
    """;

  private static final String CREATE_CACHE_KEY_INDEX = "CREATE INDEX IF NOT EXISTS key_index ON cache ( KEY )";

  private static boolean ready;
  private static boolean disableOperationalTests;
  private static boolean disableMetrology;

  static boolean isReady() {
    return ready;
  }

  static void disableMetrology() {
    disableMetrology = true;
  }
  static void disableOperationalTests() {
    disableOperationalTests = true;
  }

  private static PostgresCacheThreadConnexion postgresCacheThreadConnexion;

  static void setup(String host, int port, String user, String password) {
    closePostgresConnexionSafely();

    ready = false;

    boolean ok;
    do {
      ok = createDatabaseIfNotExists(host, port, user, password);
      if (ok) {
        Connection connection = createTablesIfNotExist(host, port, user, password);
        postgresCacheThreadConnexion = PostgresCacheThreadConnexion.create(connection);

        if (disableOperationalTests) {
          LOG.info("[🔵 POSTGRES CACHE] skip operational tests");
        }

        if (postgresCacheThreadConnexion == null || !disableOperationalTests && !PostgresCacheOperationalTests.operationalTests()) {
          ok = false;
          closePostgresConnexionSafely();
        }
        else if (count() == 0) {
          if (disableMetrology) {
            LOG.info("[🔵 POSTGRES CACHE] skip metrology");
          }
          else {
            // run metrology tests in a separate thread
            // to avoid blocking the main thread (cache thread... which would not write to cache!)
            new Thread(PostgresCacheMetrologyTests::metrologyTests).start();
          }
        }
      }
      if (!ok) {
        sleepForAWhile();
      }
    }
    while (!ok);

    ready = true;
  }

  private static void closePostgresConnexionSafely() {
    if (postgresCacheThreadConnexion != null) {
      postgresCacheThreadConnexion.closeSafely();
    }
  }

  static boolean insert(List<CacheEntry> cacheEntries) {
    try {
      Connection connection = postgresCacheThreadConnexion.connection();
      connection.setAutoCommit(false);
      PreparedStatement ps = postgresCacheThreadConnexion.insertStatement();
      for (CacheEntry cacheEntry : cacheEntries) {
        ps.setString(1, cacheEntry.key());
        ps.setFloat(2, cacheEntry.value());
        ps.addBatch();
      }
      ps.executeBatch();
      connection.commit();
      connection.setAutoCommit(true);
      return true;
    }
    catch (Throwable e) {
      LOG.error("[🔴 POSTGRES CACHE] Failed to insert {} cache entries (first entry: {})", cacheEntries.size(), cacheEntries.get(0), e);
    }
    return false;
  }

  static int count() {
    try {
      ResultSet resultSet = postgresCacheThreadConnexion.countStatement().executeQuery();
      if (resultSet.next()) {
        return resultSet.getInt(1);
      }
      else {
        LOG.error("[🔴 POSTGRES CACHE] Failed to count cache entries: resultSet.next() is null");
      }
    }
    catch (Throwable e) {
      LOG.error("[🔴 POSTGRES CACHE] Failed to count cache entries", e);
    }
    return -1;
  }

  // Thread Safe method, since multiple threads can try to read at the same time
  static Float valueOf(String key) {
    Float value = null;
    PostgresCacheMultithreadReadConnexion postgresCacheMultithreadReadConnexion = null;
    try {
      postgresCacheMultithreadReadConnexion = PostgresCacheMultithreadReadConnexion.create();
      PreparedStatement ps = postgresCacheMultithreadReadConnexion.valueOfStatement();
      ps.setString(1, key);
      ResultSet resultSet = ps.executeQuery();
      if (resultSet.next()) {
        value = resultSet.getFloat(1);
      }
    }
    catch (Throwable e) {
      LOG.error("[🔴 POSTGRES CACHE] Failed to retrieve value for key: {}", key, e);
    }
    finally {
      if (postgresCacheMultithreadReadConnexion != null) {
        postgresCacheMultithreadReadConnexion.closeSafely();
      }
    }
    return value;
  }

  static boolean deleteFirstRows(int count) {
    try {
      postgresCacheThreadConnexion.connection().setAutoCommit(true);
      PreparedStatement ps = postgresCacheThreadConnexion.deleteFirstRowsStatement();
      ps.setInt(1, count);
      int rowsDeleted = ps.executeUpdate();
      if (rowsDeleted == count) {
        return true;
      }
      else {
        LOG.warn("[⚠️ POSTGRES CACHE] did not delete all rows ({}), rowsDeleted={}", count, rowsDeleted);
      }
    }
    catch (Throwable e) {
      LOG.error("[🔴 POSTGRES CACHE] Failed to delete rows count {}", count, e);
    }
    return false;
  }

  static boolean deleteTestKey(String key) {
    try {
      postgresCacheThreadConnexion.connection().setAutoCommit(true);
      PreparedStatement ps = postgresCacheThreadConnexion.deleteStatement();
      ps.setString(1, key);
      int rowsDeleted = ps.executeUpdate();
      if (rowsDeleted >= 0) {
        return true;
      }
      else {
        LOG.error("[🔴 POSTGRES CACHE] Failed to deleteTestKey key '{}', rowsDeleted={}", key, rowsDeleted);
      }
    }
    catch (SQLException e) {
      LOG.error("[🔴 POSTGRES CACHE] Failed to deleteTestKey key {}", key, e);
    }
    return false;
  }

  private static void sleepForAWhile() {
    try {
      LOG.warn("[⚠️ POSTGRES CACHE] will retry to connect in {} seconds...", RETRY_INTERVAL_SECONDS / 1000);
      Thread.sleep(RETRY_INTERVAL_SECONDS);
    }
    catch (InterruptedException ignored) {
    }
  }

  private static boolean createDatabaseIfNotExists(String host, int port, String user, String password) {
    Connection c = connect(host, port, user, password, null);
    if (c == null) {
      return false;
    }
    boolean ok = doCreateDatabaseIfNotExists(DATABASE_NAME, c);
    try {
      c.close();
    }
    catch (Throwable e) {
      LOG.error("[🔴 POSTGRES CACHE] error closing connection", e);
    }
    return ok;
  }

  private static Connection createTablesIfNotExist(String host, int port, String user, String password) {
    Connection c = connect(host, port, user, password, DATABASE_NAME);
    if (c == null) {
      return null;
    }
    if (doCreateTablesIfNotExists(c)) {
      return c;
    }
    closePostgresConnexionSafely();
    return null;
  }

  private static Connection connect(String host, int port, String user, String password, String databaseName) {
    try {
      if (databaseName == null) {
        // to create database
        return DriverManager.getConnection(jdbcUrl(host, port, null), user, password);
      } else {
        PostgresCachePool.init(host, port, user, password, databaseName);
        return PostgresCachePool.getConnection();
      }
    }
    catch (Throwable e) {
      LOG.error("[❌ POSTGRES CACHE] Failed to connect: {}: {}", e.getClass().getName(), e.getMessage());
      return null;
    }
  }

  private static String jdbcUrl(String hostname, int port, String databaseName) {
    return String.format("jdbc:postgresql://%s:%d/%s", hostname, port, databaseName == null ? "" : databaseName);
  }

  private static boolean doCreateDatabaseIfNotExists(String databaseName, Connection connection) {
    boolean ok = false;
    Statement statement = null;
    try {
      statement = connection.createStatement();
      statement.executeQuery("SELECT count(*) FROM pg_database WHERE datname = '" + databaseName + "'");
      ResultSet resultSet = statement.getResultSet();
      resultSet.next();
      int count = resultSet.getInt(1);

      if (count <= 0) {
        statement.executeUpdate("CREATE DATABASE " + databaseName);
        LOG.info("[🟢 POSTGRES CACHE] Database '{}' created", databaseName);
      }
      else {
        LOG.info("[🔵 POSTGRES CACHE] Database '{}' already exist", databaseName);
      }
      ok = true;
    }
    catch (Throwable e) {
      LOG.error("[🔴 POSTGRES CACHE] Failed to init database '{}'", databaseName, e);
    }
    finally {
      try {
        if (statement != null) {
          statement.close();
        }
      }
      catch (Throwable e) {
        LOG.error("[🔴 POSTGRES CACHE] error closing statement", e);
      }
    }
    return ok;
  }

  private static boolean doCreateTablesIfNotExists(Connection connection) {
    boolean ok = false;
    Statement statement = null;
    try {
      statement = connection.createStatement();
      statement.executeUpdate(CREATE_CACHE_TABLE_IF_NOT_EXISTS);
      statement.executeUpdate(CREATE_CACHE_KEY_INDEX);
      LOG.info("[🟢 POSTGRES CACHE] Table 'cache' OK");
      ok = true;
    }
    catch (Throwable e) {
      LOG.error("[🔴 POSTGRES CACHE] Failed to init 'cache' table", e);
    }
    finally {
      try {
        if (statement != null) {
          statement.close();
        }
      }
      catch (Throwable e) {
        LOG.error("[🔴 POSTGRES CACHE] error closing statement", e);
      }
    }
    return ok;
  }
}
