package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.lang.ref.SoftReference;
import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

class PostgresCachePool {

  private static SoftReference<PostgresCachePool> instance = new SoftReference<>(null);

  private final HikariDataSource ds;

  private PostgresCachePool(String host, int port, String user, String password, String databaseName) {
    HikariConfig config = new HikariConfig();
    config.setJdbcUrl("jdbc:postgresql://" + host + ":" + port + "/" + databaseName);
    config.setUsername(user);
    config.setPassword(password);
    config.setMaximumPoolSize(Runtime.getRuntime().availableProcessors());

    ds = new HikariDataSource(config);
  }

  static void init(String host, int port, String user, String password, String databaseName) {
    if (instance.get() == null) {
      instance = new SoftReference<>(new PostgresCachePool(host, port, user, password, databaseName));
    }
    instance.get();
  }

  static Connection getConnection() throws SQLException {
    PostgresCachePool postgresCachePool = instance.get();
    if (postgresCachePool == null) {
      throw new IllegalStateException("PostgresCachePool not initialized");
    }
    return postgresCachePool.ds.getConnection();
  }
}
