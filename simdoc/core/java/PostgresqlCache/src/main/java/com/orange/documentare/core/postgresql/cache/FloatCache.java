package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.util.Objects;

interface FloatCache {

    void add(FloatCacheKey key, float value);

    Float get(FloatCacheKey key);

    class FloatCacheKey {

        private final String key;

        public FloatCacheKey(String key) {
            this.key = key;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FloatCacheKey that = (FloatCacheKey) o;
            return Objects.equals(key, that.key);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(key);
        }
    }
}
