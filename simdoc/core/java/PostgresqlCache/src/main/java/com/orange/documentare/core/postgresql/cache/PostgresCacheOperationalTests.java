package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PostgresCacheOperationalTests {

    private static final Logger LOG = LoggerFactory.getLogger(PostgresCacheOperationalTests.class);

    private static final String KEY_1 = "11111111111111111111111111111111";
    private static final String KEY_2 = "22222222222222222222222222222222";
    private static final float VALUE_1 = 0.111f;
    private static final float VALUE_2 = 0.222f;

    private static final List<CacheEntry> CACHE_ENTRIES = List.of(
        new CacheEntry(KEY_1, VALUE_1),
        new CacheEntry(KEY_2, VALUE_2)
    );

    static boolean operationalTests() {
        // given
        PostgresCacheIO.deleteTestKey(KEY_1);
        PostgresCacheIO.deleteTestKey(KEY_2);

        int initialCacheEntriesCount = PostgresCacheIO.count();

        // when
        boolean ok = PostgresCacheIO.insert(CACHE_ENTRIES);
        if (ok) {
            int cacheEntriesCount = PostgresCacheIO.count();

            // then
            if (cacheEntriesCount == initialCacheEntriesCount + CACHE_ENTRIES.size()) {
                float retrievedValue1 = PostgresCacheIO.valueOf(KEY_1);
                float retrievedValue2 = PostgresCacheIO.valueOf(KEY_2);

                if (retrievedValue1 != VALUE_1 || retrievedValue2 != VALUE_2) {
                    LOG.error("[🔴 POSTGRES CACHE] values retrieved are not correct: {}={}, {}={}", KEY_1, retrievedValue1, KEY_2, retrievedValue2);
                    return false;
                }

                ok = PostgresCacheIO.deleteTestKey(KEY_1) && PostgresCacheIO.deleteTestKey(KEY_2);

                if (ok) {
                    LOG.info("[🟢 POSTGRES CACHE] operational tests");
                }

                return ok;
            }
            LOG.error("[🔴 POSTGRES CACHE] count cache entries after insertion is not correct: expected {} but got {}", initialCacheEntriesCount + 2,
                cacheEntriesCount);
            return false;
        }
        return false;
    }
}
