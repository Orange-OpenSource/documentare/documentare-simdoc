package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

record PostgresCacheThreadConnexion(Connection connection, PreparedStatement insertStatement, PreparedStatement countStatement, PreparedStatement deleteStatement, PreparedStatement deleteFirstRowsStatement)
{

    private static final Logger LOG = LoggerFactory.getLogger(PostgresCacheThreadConnexion.class);

    static PostgresCacheThreadConnexion create(Connection connection) {
        if (connection == null) {
            return null;
        }
        PreparedStatement insertStatement;
        PreparedStatement countStatement;
        PreparedStatement deleteStatement;
        PreparedStatement deleteFirstRowsStatement;
        try {
            // ON CONFLICT: postgres way to handle UPSERT
            insertStatement = connection.prepareStatement("INSERT INTO cache (KEY, VALUE) VALUES (?, ?) ON CONFLICT(KEY) DO NOTHING");
            countStatement = connection.prepareStatement("SELECT COUNT(*) FROM cache");
            deleteStatement = connection.prepareStatement("DELETE FROM cache WHERE KEY = ?");
            deleteFirstRowsStatement = connection.prepareStatement("DELETE FROM cache WHERE ID IN (SELECT ID FROM cache ORDER BY ID LIMIT ?)");
        }
        catch (SQLException e) {
            return null;
        }
        return new PostgresCacheThreadConnexion(connection, insertStatement, countStatement, deleteStatement, deleteFirstRowsStatement);
    }

    void closeSafely() {
        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                LOG.warn("[⚠️ PostgresCacheThreadConnexion] Failed to close connection", e);
            }
        }
    }
}
