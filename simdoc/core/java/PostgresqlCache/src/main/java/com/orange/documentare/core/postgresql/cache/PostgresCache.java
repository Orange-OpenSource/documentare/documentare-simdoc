package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostgresCache {

  public static final String DEFAULT_HOSTNAME = "localhost";
  public static final int DEFAULT_PORT = 5432;

  private static boolean DISABLE;

  private static final Logger LOG = LoggerFactory.getLogger(PostgresCache.class);
  private static final long FLUSH_QUEUE_TIMEOUT_MILLISECONDS = 60 * 1000; // 1 minute
  private static final int FORCE_DEQUEUE_THRESHOLD = 10000;
  private static final int QUEUE_MAX_SIZE = 100000;
  static final int CACHE_MAX_SIZE = 10 * 1000000; // 10M
  static final int PURGE_COUNT = 100000;

  private static String hostname;
  private static int port;
  private static String user;
  private static String password;
  private static final List<CacheEntry> queue = new ArrayList<>();
  private static final Thread thread = new Thread(PostgresCache::run);
  private static PostgresCacheListener listener;

  private static int cacheMaxSize = CACHE_MAX_SIZE;
  private static int purgeCount = PURGE_COUNT;
  private static long flushQueueTimeoutMilliseconds = FLUSH_QUEUE_TIMEOUT_MILLISECONDS;
  private static long forceDequeueThreshold = FORCE_DEQUEUE_THRESHOLD;
  private static boolean threadIsSleeping;

  public static boolean isDisabled() {
    return DISABLE;
  }

  public static void setup(String hostname, int port, String user, String password, PostgresCacheListener listener) {

    if (DISABLE) {
      LOG.warn("[⚠️ POSTGRES CACHE] is disabled, EXIT");
      return;
    }

    cacheMaxSize = CACHE_MAX_SIZE;
    purgeCount = PURGE_COUNT;
    flushQueueTimeoutMilliseconds = FLUSH_QUEUE_TIMEOUT_MILLISECONDS;
    forceDequeueThreshold = FORCE_DEQUEUE_THRESHOLD;

    PostgresCache.hostname = hostname;
    PostgresCache.port = port;
    PostgresCache.user = user;
    PostgresCache.password = password;
    PostgresCache.listener = listener;
    if (!thread.isAlive()) {
      thread.start();
    }
  }

  public static boolean isReady() {
    return PostgresCacheIO.isReady();
  }

  public static void add(String key, float value) {
    if (DISABLE) {
      return;
    }

    if (queue.size() >= QUEUE_MAX_SIZE) {
      LOG.warn("[⚠️ POSTGRES CACHE] queue is full ({} elements), drop entry", queue.size());
    }
    else {
      synchronized (queue) {
        queue.add(new CacheEntry(key, value));
      }
    }
    if (queue.size() >= forceDequeueThreshold) {
      if (threadIsSleeping) {
        LOG.info("[🔵 POSTGRES CACHE] force dequeue ({} elements) -> wake up thread  ⏰", queue.size());
        thread.interrupt();
      }
    }
  }

  public static Float get(String key) {
    if (DISABLE) {
      return null;
    }

    if (PostgresCacheIO.isReady()) {
      long t0 = System.nanoTime();
      Float value = PostgresCacheIO.valueOf(key);
      PostgresCacheStats.addReadDuration((System.nanoTime() - t0) / 1000, value != null);
      return value;
    }
    else {
      return null;
    }
  }

  static boolean isQueueNotEmpty() {
    return !queue.isEmpty();
  }

  static void setCacheSizeAndPurgeCountForTests(Integer cacheMaxSize, Integer purgeCount) {
    PostgresCache.cacheMaxSize = cacheMaxSize;
    PostgresCache.purgeCount = purgeCount;
  }

  static void setFlushTimeoutMillisecondsForTests(long flushQueueTimeoutMilliseconds) {
    PostgresCache.flushQueueTimeoutMilliseconds = flushQueueTimeoutMilliseconds;
  }

  static void setForceDequeueThresholdForTests(long forceDequeueThreshold) {
    PostgresCache.forceDequeueThreshold = forceDequeueThreshold;
  }

  public static void flushQueueSync() {
    if (threadIsSleeping) {
      thread.interrupt();
    }
    while (!queue.isEmpty()) {
      try {
        LOG.info("[🔵 POSTGRES CACHE] waiting for queue to be empty ({} elements)", queue.size());
        Thread.sleep(1000);
      }
      catch (InterruptedException ignored) {
      }
    }
  }

  private static void run() {
    while (true) {
      if (PostgresCacheIO.isReady()) {

        long t0 = System.currentTimeMillis();

        List<CacheEntry> cacheEntries;

        // ⚠️ We need sync here to avoid concurrent modification of the queue (add elements while we are modifying it)
        synchronized (queue) {
          // ⚠️ Performance optimization: ArrayList constructor internally uses System.arraycopy to copy the elements, which is highly optimized.
          cacheEntries = new ArrayList<>(queue);
          // ⚠️ Performance optimization: clear the queue completely, so that we don't have to remove elements after insertion
          // advantage: no removeAll() operation after the insert (can be costly)
          // drawback: we may lose some entries if the insert fails
          queue.clear();
        }

        if (!cacheEntries.isEmpty()) {

          long t1 = System.currentTimeMillis();

          if (PostgresCacheIO.insert(cacheEntries)) {

            long t2 = System.currentTimeMillis();
            long dtInsert = t2 - t1;
            long dtTotal = t2 - t0;

            LOG.info("[🚀 POSTGRES CACHE] inserted {} entries (insert {} ms / all {} ms)", cacheEntries.size(), dtInsert, dtTotal);

            if (listener != null) {
              listener.inserted(cacheEntries.size());
            }
            if (PostgresCacheIO.count() >= cacheMaxSize) {
              t0 = System.currentTimeMillis();
              PostgresCacheIO.deleteFirstRows(purgeCount);
              long dtDelete = System.currentTimeMillis() - t0;
              LOG.warn("[⚠️ POSTGRES CACHE] cache max size '{}' reached -> purged {} oldest cache entries ({} ms)", cacheMaxSize, purgeCount, dtDelete);
            }
          }
          else {
            // insert failed, try to set up again DB
            PostgresCacheIO.setup(hostname, port, user, password);
          }
        }
        // if we have less than 10% of the threshold, we can wait a while
        if (queue.size() < forceDequeueThreshold/10) {
          waitAWhile();
        }
      }
      else {
        // we are single threaded here, so we should not launch this concurrently
        PostgresCacheIO.setup(hostname, port, user, password);
      }
    }
  }

  private static void waitAWhile() {
    if (DISABLE) {
      LOG.info("[⚠️ POSTGRES CACHE] is disabled...");
    }

    threadIsSleeping = true;
    try {
      long timeoutMilliseconds = flushQueueTimeoutMilliseconds;
      LOG.info("[🔵 POSTGRES CACHE] will sleep for {} seconds", timeoutMilliseconds / 1000);
      Thread.sleep(timeoutMilliseconds);
    }
    catch (InterruptedException ignored) {
    }
    threadIsSleeping = false;
  }

  public static boolean disableIfPostgresNotRunning(String host, int port) {
    if (!serverListening(host, port)) {
      LOG.warn("[⚠️ POSTGRES CACHE] port {}:{} is not opened... DISABLE CACHE", host, port);
      DISABLE = true;
    }
    return DISABLE;
  }

  public static void disableMetrology() {
    PostgresCacheIO.disableMetrology();
  }

  public static void disableOperationalTests() {
    PostgresCacheIO.disableMetrology();
    PostgresCacheIO.disableOperationalTests();
  }

  private static boolean serverListening(String host, int port) {
    try (Socket ignored = new Socket(host, port)) {
      return true;
    }
    catch (IOException ex) {
      /* ignore */
    }
    return false;
  }

  public static long cacheHit() {
    return PostgresCacheStats.stats().cacheHits();
  }

  public static void testOnlySetup() {
    testOnlySetup(null);
  }

  public static void testOnlySetup(PostgresCacheListener listener) {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);

    if (!PostgresCache.isDisabled()) {
      PostgresCache.disableMetrology();
      PostgresCache.setup(DEFAULT_HOSTNAME, DEFAULT_PORT, "postgres", "sandbox", listener);
      while (!PostgresCache.isReady()) {
        try {
          Thread.sleep(10);
        }
        catch (InterruptedException ignored) {
        }
      }

      PostgresCacheIO.deleteFirstRows(Integer.MAX_VALUE);
    }
  }
}
