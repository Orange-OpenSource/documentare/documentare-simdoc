package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import static java.lang.Thread.sleep;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PostgresCacheMetrologyTests {

  private static final Logger LOG = LoggerFactory.getLogger(PostgresCacheMetrologyTests.class);

  static void metrologyTests() {
    // given
    final int initialCacheEntriesCount = PostgresCacheIO.count();
    if (initialCacheEntriesCount != 0) {
      LOG.error("[🔵 POSTGRES CACHE METRO] disable metrology tests: cache is not empty ({})", initialCacheEntriesCount);
      return;
    }

    final AtomicInteger done = new AtomicInteger();
    final int nbInsertionsPerThread = 5000;
    final int nbThreads = Runtime.getRuntime().availableProcessors();
    final int nbEntries = nbInsertionsPerThread * nbThreads;

    // When Insertion
    long t0 = System.currentTimeMillis();
    IntStream.range(0, nbThreads).forEach(threadIndex -> new Thread(() -> {
      insertTask(threadIndex, nbInsertionsPerThread);
      done.getAndIncrement();
    }).start());

    waitForAllInsertionsDone(done, nbThreads);
    LOG.info("[🚀 POSTGRES CACHE METRO] time to insert {} entries with {} threads: {} ms", nbEntries, nbThreads, System.currentTimeMillis() - t0);


    // When Read
    done.set(0);
    t0 = System.currentTimeMillis();
    IntStream.range(0, nbThreads).forEach(threadIndex -> new Thread(() -> {
      readTask(threadIndex, nbInsertionsPerThread);
      done.getAndIncrement();
    }).start());
    while (done.get() != nbThreads) {
      try {
        sleep(10);
      }
      catch (InterruptedException ignored) {
      }
    }
    LOG.info("[🚀 POSTGRES CACHE METRO] time to read {} entries with {} threads: {} ms", nbEntries, nbThreads, System.currentTimeMillis() - t0);


    // When Delete
    t0 = System.currentTimeMillis();
    if (PostgresCacheIO.deleteFirstRows(nbEntries)) {
      LOG.info("[🚀 POSTGRES CACHE METRO] time to delete {} entries: {} ms", nbEntries, System.currentTimeMillis() - t0);
    }
    else {
      LOG.error("[🔴 POSTGRES CACHE METRO] failed to delete test data");
      return;
    }

    int finalCacheEntriesCount = PostgresCacheIO.count();
    if (finalCacheEntriesCount != 0) {
      LOG.error("[🔴 POSTGRES CACHE METRO] final cache entries count is not correct: expected {} but got {}", 0, finalCacheEntriesCount);
    }

    LOG.info("[🚀 POSTGRES CACHE METRO] we are done 🎉");
  }

  private static void readTask(int threadIndex, int nbInsertionsPerThread) {
    IntStream.range(threadIndex * nbInsertionsPerThread, (threadIndex + 1) * nbInsertionsPerThread).forEach(readIndex -> {
      String key = readIndex + "";
      Float value = PostgresCache.get(key);
      if (value == null || value != readIndex) {
        LOG.error(
          "[🔴 POSTGRES CACHE METRO] Thread {}: readIndex={}}, expected={}}, got={}}%n", Thread.currentThread().getId(), readIndex, readIndex,
          value == null ? -1 : value);
      }
    });
  }

  private static void waitForAllInsertionsDone(AtomicInteger done, int nbThreads) {
    while (done.get() != nbThreads) {
      try {
        sleep(10);
      }
      catch (InterruptedException ignored) {
      }
    }

    done.set(0);

    PostgresCache.flushQueueSync();
  }

  private static void insertTask(int threadIndex, int nbInsertionsPerThread) {
    IntStream.range(threadIndex * nbInsertionsPerThread, (threadIndex + 1) * nbInsertionsPerThread).forEach(insertIndex -> {
      String key = insertIndex + "";
      PostgresCache.add(key, insertIndex);
      try {
        sleep(1);
      }
      catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    });
  }

}
