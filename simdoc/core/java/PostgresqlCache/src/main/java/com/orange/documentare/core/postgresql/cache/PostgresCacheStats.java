package com.orange.documentare.core.postgresql.cache;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


public class PostgresCacheStats {

  private static long cacheHits;
  private static long cacheMisses;
  private static long meanReadDurationMicros;
  private static long readCount;

  public record Stats(long cacheHits, long cacheMisses, long meanReadDurationMicros, long readCount) {

    @Override
    public String toString() {
      return "PostgresCacheStats {" + "\n" +
        "\tcacheHits=" + cacheHits + "\n" +
        "\tcacheMisses=" + cacheMisses + "\n" +
        "\tcacheHitsPercent=" + cacheHitsPercent() + "\n" +
        "\tmeanReadDurationMillis=" + ((float)meanReadDurationMicros)/1000 + "\n" +
        "\treadCount=" + readCount + "\n" +
        '}';
    }

    private long cacheHitsPercent() {
      long hitsPlusMisses = cacheHits + cacheMisses;
      if (hitsPlusMisses == 0) {
        return 0;
      }
      return 100 * cacheHits / hitsPlusMisses;
    }
  }

  public static Stats stats() {
    return new Stats(cacheHits, cacheMisses, meanReadDurationMicros, readCount);
  }
  public static void clearStats() {
    cacheHits = 0;
    cacheMisses = 0;
    meanReadDurationMicros = 0;
    readCount = 0;
  }

  static synchronized void addReadDuration(long durationMicros, boolean hit) {
    if (hit) {
      cacheHits++;
    }
    else {
      cacheMisses++;
    }
    meanReadDurationMicros = (meanReadDurationMicros * readCount + durationMicros) / (readCount + 1);
    readCount++;
  }
}
