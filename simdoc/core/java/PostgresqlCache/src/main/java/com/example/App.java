package com.example;

/*
 * Copyright (c) 2024 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import static com.orange.documentare.core.postgresql.cache.PostgresCache.*;

import com.orange.documentare.core.postgresql.cache.PostgresCache;

public class App {

  public static void main(String[] args) {
    PostgresCache.disableIfPostgresNotRunning(DEFAULT_HOSTNAME, DEFAULT_PORT);
    if (PostgresCache.isDisabled())
      return;

    PostgresCache.setup(DEFAULT_HOSTNAME, DEFAULT_PORT, "postgres", "sandbox", _ignored -> {});
  }
}
