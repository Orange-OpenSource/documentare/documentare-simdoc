/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.computationserver.biz.distances

import com.orange.documentare.core.computationserver.biz.clustering.RequestValidation
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.slf4j.Logger
import org.slf4j.LoggerFactory

data class DistancesController(
  private val tasks: Tasks,
  private val appPrepData: AppPrepData) {

  enum class DistanceRequestState { BAD_REQUEST, CAN_NOT_ACCEPT_A_NEW_TASK, TASK_LAUNCHED, ERROR }

  data class DistanceRequestStatus(val state: DistanceRequestState, val taskId: String?, val error: String?)

  fun launchDistancesComputation(req: com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest): DistanceRequestStatus {
    val validation = req.validate()
    if (!validation.ok) {
      log.error("[DISTANCES REQ] bad request, '${validation.error}', '$req'")
      return badRequest(validation)
    }

    log.debug("[DISTANCES REQ] for element ids: '${req.elementDTO[0]?.ids}'")

    val computeDistance = ComputeDistanceTask(req, tasks, appPrepData)

    val taskId: String
    if (tasks.canAcceptNewTask()) {
      taskId = tasks.newTask()
      tasks.run({ computeDistance.waitTaskInBackground(taskId) }, taskId, computeDistance)
    } else {
      log.debug("[DISTANCES REQ] can not accept more tasks")
      return canNotAcceptANewTask()
    }

    return taskId(taskId)
  }

  private fun taskId(taskId: String) =
    DistanceRequestStatus(DistanceRequestState.TASK_LAUNCHED, taskId, null)

  private fun canNotAcceptANewTask() =
    DistanceRequestStatus(DistanceRequestState.CAN_NOT_ACCEPT_A_NEW_TASK, null, "can not accept more tasks")

  private fun badRequest(validation: com.orange.documentare.core.computationserver.biz.clustering.RequestValidation) =
    DistanceRequestStatus(DistanceRequestState.BAD_REQUEST, null, validation.error)

  companion object {
    private val log: Logger = LoggerFactory.getLogger(DistancesController::class.java)
  }
}
