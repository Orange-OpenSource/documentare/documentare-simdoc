/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.computationserver.biz.task

import org.slf4j.LoggerFactory

class TaskRunnableErrorCatcher(private val runnable: () -> Unit, private val listener: RunningTaskListener, private val taskId: String) : Runnable {

  interface RunningTaskListener {
    fun finished(taskId: String)
  }

  override fun run() {
    try {
      runnable()
    } catch (throwable: Throwable) {
      if (throwable is InterruptedException) {
        log.debug("Thread interrupted")
      } else {
        log.error("Thread crashed", throwable)
      }
    }
    listener.finished(taskId)
  }

  companion object {
    private val log = LoggerFactory.getLogger(TaskRunnableErrorCatcher::class.java)
  }
}
