package com.orange.documentare.core.computationserver.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.computationserver.biz.FileIO;
import com.orange.documentare.core.model.ref.clustering.Cluster;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import com.orange.documentare.core.system.inputfilesconverter.FilesMap;
import com.orange.documentare.core.computationserver.biz.FileIO;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class ClusteringRemap {

  static Clustering remapInFilesMode(FileIO fileIO, Clustering cl) {
    FilesMap map = fileIO.loadFilesMap();

    List<ClusteringElement> remappedElements = new ArrayList<>();

    cl.elements.forEach(el -> {
      int fileId = Integer.parseInt(el.id);
      String fileAbsPath = map.get(fileId);
      /* +1 to remove leading '/' */
      String relPath = fileAbsPath.substring(fileIO.inputDirectoryAbsPath.length() + 1);
      ClusteringElement remappedElement = new ClusteringElement(
        relPath, el.clusterId, el.clusterCenter, el.enrolled, el.duplicateOf, el.distanceToCenter
      );
      remappedElements.add(remappedElement);
    });

    return cl.updateElementsId(remappedElements);
  }

  public static Clustering remapInBytesDataMode(BytesData[] bytesData, Clustering clustering, FileIO fileIO) {
    return fileIO.filesPrepped() ?
      remapInBytesDataModeWithFilesPreparation(bytesData, clustering, fileIO) :
      remapInBytesDataMode(bytesData, clustering);
  }

  private static Clustering remapInBytesDataModeWithFilesPreparation(BytesData[] bytesData, Clustering clustering, FileIO fileIO) {
    FilesMap filesMap = fileIO.loadFilesMap();

    List<ClusteringElement> remappedElements = new ArrayList<>();
    Map<Integer, Integer> oldNewIndice = new HashMap<>();
    IntStream.range(0, bytesData.length).forEach(i -> {
      BytesData bd = bytesData[i];
      // Normalize filePath : replace // by /
      File file = new File(bd.filepaths.get(0));
      int fileId = filesMap.getKeyByValue(file.getAbsolutePath());
      ElementPosition elementPosition = lookUpElement(clustering.elements, fileId);
      oldNewIndice.put(elementPosition.position, i);
      ClusteringElement remappedElement = new ClusteringElement(bd.ids.get(0), elementPosition.element.clusterId, elementPosition.element.clusterCenter, elementPosition.element.enrolled, elementPosition.element.duplicateOf, elementPosition.element.distanceToCenter);
      remappedElements.add(remappedElement);
    });

    List<Cluster> remappedClusters = new ArrayList<>();
    for (Cluster cluster : clustering.clusters) {
      Cluster remappedCluster;
      List<Integer> remapElementsIndices = new ArrayList<>();
      List<Integer> remapMultisetElementsIndices = new ArrayList<>();
      if (cluster.elementsIndices != null) {
        for (int iElement = 0; iElement < cluster.elementsIndices.size(); iElement++) {
          remapElementsIndices.add(findNewIndice(oldNewIndice, cluster.elementsIndices.get(iElement)));
        }
      }
      if (cluster.multisetElementsIndices != null) {
        for (int iElement = 0; iElement < cluster.multisetElementsIndices.size(); iElement++) {
          remapMultisetElementsIndices.add(findNewIndice(oldNewIndice, cluster.multisetElementsIndices.get(iElement)));
        }
      }
      remappedCluster = new Cluster(remapElementsIndices, remapMultisetElementsIndices);
      remappedClusters.add(remappedCluster);
    }
    if (remappedClusters.isEmpty()) {
      remappedClusters = clustering.clusters;
    }

    return clustering.updateElementsIndice(remappedElements, remappedClusters);
  }

  private static Integer findNewIndice(Map<Integer, Integer> oldNewIndice, Integer oldIndice) {
    Integer newIndice = 0;
    for (Map.Entry<Integer, Integer> entry : oldNewIndice.entrySet()) {
      if (entry.getKey().equals(oldIndice)) {
        newIndice = entry.getValue();
        break;
      }
    }
    return newIndice;
  }


  private static ElementPosition lookUpElement(List<ClusteringElement> elements, int fileId) {
    int i = 0;
    ClusteringElement element = null;
    for (ClusteringElement clusteringElement : elements) {
      if (Integer.parseInt(clusteringElement.id) == fileId) {
        element = clusteringElement;
        break;
      }
      i++;
    }
    return (new ElementPosition(i, element));
  }

  private static Clustering remapInBytesDataMode(BytesData[] bytesData, Clustering clustering) {
    List<ClusteringElement> remappedElements = new ArrayList<>();
    IntStream.range(0, bytesData.length).forEach(i -> {
      ClusteringElement el = clustering.elements.get(i);
      remappedElements.add(new ClusteringElement(bytesData[i].ids.get(0), el.clusterId, el.clusterCenter, el.enrolled, el.duplicateOf, el.distanceToCenter));
    });
    return clustering.updateElementsId(remappedElements);
  }
}
