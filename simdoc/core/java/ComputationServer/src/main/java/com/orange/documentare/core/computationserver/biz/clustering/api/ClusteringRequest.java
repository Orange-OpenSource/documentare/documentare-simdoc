package com.orange.documentare.core.computationserver.biz.clustering.api;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;
import com.orange.documentare.core.computationserver.biz.clustering.RequestValidation;
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters.ClusteringParametersBuilder;
import com.orange.documentare.core.model.ref.clustering.EnrollParameters;
import com.orange.documentare.core.computationserver.biz.clustering.RequestValidation;
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO;
import lombok.*;

import jakarta.json.bind.annotation.JsonbNillable;
import jakarta.json.bind.annotation.JsonbProperty;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonbNillable
public class ClusteringRequest {
  public String inputDirectory;
  @JsonbProperty(value = "bytesData", nillable = true)
  public BytesDataDTO[] bytesData;
  public String outputDirectory;
  public Boolean rawConverter;
  public Integer pCount;
  public Boolean debug;
  public Float acutSdFactor;
  public Float qcutSdFactor;
  public Float scutSdFactor;
  public Integer ccutPercentile;
  public Integer knnThreshold;
  public Boolean sloop;
  public Boolean consolidateCluster;
  public Boolean enroll;
  public Float enrollAcutSdFactor;
  public Float enrollQcutSdFactor;
  public Float enrollScutSdFactor;
  public Integer enrollCcutPercentile;
  public Integer enrollKnnThreshold;
  public SuffixArrayAlgorithm suffixArrayAlgorithm;

  public RequestValidation validate() {
    boolean valid = false;
    String error = null;
    if (inputDirectory == null && bytesData == null) {
      error = "inputDirectory and bytesData are missing";
    } else if (outputDirectory == null) {
      error = "outputDirectory is missing";
    } else {
      valid = true;
    }
    return new RequestValidation(valid, error);
  }

  public ClusteringParameters clusteringParameters() {
    ClusteringParametersBuilder builder = ClusteringParameters.builder();
    if (acutSdFactor != null) {
      builder.acut(acutSdFactor);
    }
    if (qcutSdFactor != null) {
      builder.qcut(qcutSdFactor);
    }
    if (scutSdFactor != null) {
      builder.scut(scutSdFactor);
    }
    if (ccutPercentile != null) {
      builder.ccut(ccutPercentile);
    }
    if (knnThreshold != null) {
      builder.knn(knnThreshold);
    }
    if (sloop != null && sloop) {
      builder.sloop();
    }
    if (consolidateCluster != null && consolidateCluster) {
      builder.consolidateCluster();
    }
    if (enroll != null && enroll) {
      builder.enroll();

      EnrollParameters.EnrollParametersBuilder enrollParametersBuilder = EnrollParameters.builder();
      if (enrollAcutSdFactor != null) {
        enrollParametersBuilder.acut(enrollAcutSdFactor);
      }
      if (enrollQcutSdFactor != null) {
        enrollParametersBuilder.qcut(enrollQcutSdFactor);
      }
      if (enrollScutSdFactor != null) {
        enrollParametersBuilder.scut(enrollScutSdFactor);
      }
      if (enrollCcutPercentile != null) {
        enrollParametersBuilder.ccut(enrollCcutPercentile);
      }
      if (enrollKnnThreshold != null) {
        enrollParametersBuilder.knn(enrollKnnThreshold);
      }
      builder.enrollParameters(enrollParametersBuilder.build());
    }
    return builder.build();
  }

  /**
   * can not be a computed field, since json deserialization is not using the ctor
   */
  public boolean bytesDataMode() {
    return bytesData != null;
  }

  public boolean debug() {
    return debug != null && debug;
  }
}
