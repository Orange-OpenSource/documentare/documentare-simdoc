package com.orange.documentare.core.computationserver.biz.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.jgrapht.dotexport.DOT;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;

public class IdProvider {

  public static String getName(GraphItem graphItem) {
    return String.format("c%d_%s_%d", graphItem.clusterId, DOT.getWithoutAnyExtension(graphItem.vertexName), (int) (graphItem.q * 100));
  }
}
