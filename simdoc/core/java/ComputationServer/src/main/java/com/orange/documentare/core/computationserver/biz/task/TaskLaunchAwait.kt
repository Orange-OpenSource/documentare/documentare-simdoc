package com.orange.documentare.core.computationserver.biz.task

interface TaskLaunchAwait {
  fun waitTaskInBackground(simdocTaskId: String)
}
