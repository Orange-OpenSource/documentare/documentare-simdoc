package com.orange.documentare.core.computationserver.biz.distances.api;

import static com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO.*;

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.computationserver.biz.clustering.RequestValidation;
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO;

import jakarta.json.bind.annotation.JsonbProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DistancesRequest {

  @JsonbProperty("element")
  public BytesDataDTO[] elementDTO;
  @JsonbProperty("elements")
  public BytesDataDTO[] elementsDTOs;
  public Boolean rawConverter;
  public Integer pCount;
  public SuffixArrayAlgorithm suffixArrayAlgorithm;

  public static DistancesRequestBuilder builder() {
    return new DistancesRequestBuilder();
  }

  public RequestValidation validate() {
    boolean valid = false;
    String error = null;
    if (elementDTO == null) {
      error = "element is missing";
    }
    else if (elementsDTOs == null) {
      error = "elements are missing";
    }
    else {
      valid = true;
    }
    return new RequestValidation(valid, error);
  }

  public BytesData[] element() {
    return dtoToBytesData(elementDTO);
  }

  public BytesData[] elements() {
    return dtoToBytesData(elementsDTOs);
  }

  public static class DistancesRequestBuilder {

    private BytesDataDTO[] element;
    private BytesDataDTO[] elements;
    private SuffixArrayAlgorithm suffixArrayAlgorithm;
    private Boolean rawConverter;
    private Integer pCount;

    public DistancesRequest build() {
      return new DistancesRequest(element, elements, rawConverter, pCount, suffixArrayAlgorithm);
    }

    public DistancesRequestBuilder element(BytesData[] element) {
      this.element = dtoFromBytesData(element);
      return this;
    }

    public DistancesRequestBuilder compareTo(BytesData[] elements) {
      this.elements = dtoFromBytesData(elements);
      return this;
    }

    public DistancesRequestBuilder suffixArrayAlgorithm(SuffixArrayAlgorithm suffixArrayAlgorithm) {
      this.suffixArrayAlgorithm = suffixArrayAlgorithm;
      return this;
    }

    public DistancesRequestBuilder rawConverter(Boolean rawConverter) {
      this.rawConverter = rawConverter;
      return this;
    }

    public DistancesRequestBuilder pCount(Integer pCount) {
      this.pCount = pCount;
      return this;
    }
  }
}
