/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.computationserver.biz.clustering

import com.orange.documentare.core.computationserver.biz.FileIO
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.slf4j.Logger
import org.slf4j.LoggerFactory

data class ClusteringController(
    private val appPrepData: AppPrepData,
    private val sharedDirectory: SharedDirectory,
    private val tasks: Tasks
) {

  enum class ClusteringRequestState { BAD_REQUEST, CAN_NOT_ACCEPT_A_NEW_TASK, TASK_LAUNCHED, ERROR }

  data class ClusteringRequestStatus(val state: ClusteringRequestState, val taskId: String?, val error: String?)

  fun launchClustering(req: ClusteringRequest): ClusteringRequestStatus {
    log.debug("[CLUSTERING REQ] request: '$req'")

    var validation = req.validate()
    if (!validation.ok) {
      log.error("[CLUSTERING REQ] bad request, '${validation.error}', '$req'")
      return badRequest(validation)
    }

    val fileIO = FileIO(sharedDirectory, req)
    validation = fileIO.validate()
    if (!validation.ok) {
      log.error("[CLUSTERING REQ] bad request, '${validation.error}', '$req'")
      return badRequest(validation)
    }

    val computeClustering = ComputeClusteringTask(req, tasks, appPrepData, fileIO)

    val taskId: String
    if (tasks.canAcceptNewTask()) {
      taskId = tasks.newTask()
      tasks.run({ computeClustering.waitTaskInBackground(taskId) }, taskId, computeClustering)
    } else {
      log.info("[CLUSTERING REQ] can not accept more tasks")
      return canNotAcceptANewTask()
    }

    return taskId(taskId)
  }


  private fun taskId(taskId: String) =
      ClusteringRequestStatus(
          ClusteringRequestState.TASK_LAUNCHED,
          taskId,
          null
      )

  private fun canNotAcceptANewTask() =
      ClusteringRequestStatus(
          ClusteringRequestState.CAN_NOT_ACCEPT_A_NEW_TASK,
          null,
          "can not accept more tasks"
      )

  private fun badRequest(validation: RequestValidation) =
      ClusteringRequestStatus(
          ClusteringRequestState.BAD_REQUEST,
          null,
          validation.error
      )


  companion object {
    private val log: Logger = LoggerFactory.getLogger(ClusteringController::class.java)
  }
}
