/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.computationserver.biz.stats

import com.orange.documentare.core.comp.ncd.cache.NcdCache
import com.orange.documentare.core.prepdata.PrepData
import com.orange.documentare.core.system.measure.MemoryState

object CachesStats {
  fun get() =
    "Memory: " + MemoryState().displayString() + "\n\n" +
      PrepData.statsDurationMillis() + "\n\n" +
      NcdCache.stats()
}
