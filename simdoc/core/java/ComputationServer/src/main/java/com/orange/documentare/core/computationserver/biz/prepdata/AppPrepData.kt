package com.orange.documentare.core.computationserver.biz.prepdata

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.prepdata.PrepData
import com.orange.documentare.core.system.inputfilesconverter.FilesMap
import com.orange.documentare.core.computationserver.biz.FileIO
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.distances.DistancesSegment
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO.dtoToBytesData
import org.apache.commons.io.FileUtils
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException

class AppPrepData(
    private val distancePrepDir: String,
    private val rawImageCacheDir: String,
    private val sharedDirectory: SharedDirectory,
    private val prepDataImpl: (prepData: PrepData) -> Unit = { it.prep() }
) {

  fun createSafeWorkingDirectoryForDistances(req: DistancesRequest, taskId: String): DistancesSegment {

    val elementAsArray = req.element()
    val fileIO1 = FileIO(sharedDirectory, elementAsArray, distancePrepDir + "1").forTaskId(taskId)
    val fileIO2 = FileIO(sharedDirectory, req.elements(), distancePrepDir + "2").forTaskId(taskId)

    val elementBytesData = createSafeWorkingDirectory(fileIO1, elementAsArray, req.rawConverter, req.pCount)
    val elements = createSafeWorkingDirectory(fileIO2, req.elements(), req.rawConverter, req.pCount)

    return DistancesSegment(elementBytesData[0], elements)
  }

  fun createSafeWorkingDirectoryForClustering(taskId: String, req: ClusteringRequest): Array<BytesData> {
    val fileIO = FileIO(sharedDirectory, req).forTaskId(taskId)
    return createSafeWorkingDirectory(fileIO, if (req.bytesData == null) null else dtoToBytesData(req.bytesData), req.rawConverter, req.pCount)
  }

  private fun createSafeWorkingDirectory(fileIO: FileIO, bytesData: Array<BytesData>?, rawConverter: Boolean?, pCount: Int?): Array<BytesData> {
    val expectedPixelsCount = pCount ?: 1024 * 1024
    val withRawConverter = rawConverter ?: false

    val prepData = buildPrepDataFromRequest(fileIO, bytesData, withRawConverter, expectedPixelsCount)
    prepDataImpl(prepData)

    val safeWorkingDirectory = fileIO.safeWorkingDirectory()

    return BytesData.loadFromDirectory(
      safeWorkingDirectory, BytesData.relativePathIdProvider(safeWorkingDirectory)
    )
  }

  private fun buildPrepDataFromRequest(fileIO: FileIO, bytesData: Array<BytesData>?, withRawConverter: Boolean, expectedPixelsCount: Int): PrepData {
    return PrepData.builder()
      .inputDirectory(fileIO.inputDirectory()) // may be null
      .bytesData(bytesData) // may be null
      .withRawConverter(withRawConverter)
      .withRawCacheDirectory(rawImageCacheDir)
      .expectedRawPixelsCount(expectedPixelsCount)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(fileIO.safeWorkingDirectory())
      .metadataOutputFile(fileIO.metadataFile())
      .build()
  }

  fun remapDistances(req: DistancesRequest, safeElements: Array<BytesData>, distances: IntArray, taskId: String): IntArray {
    val fileIO = FileIO(sharedDirectory, req.elements(), distancePrepDir + "2").forTaskId(taskId)
    val filesMap = fileIO.loadFilesMap()

    return doRemapDistances(distances, req, filesMap, safeElements)
  }

  fun doRemapDistances(distances: IntArray, req: DistancesRequest, filesMap: FilesMap, safeElements: Array<BytesData>): IntArray {
    return distances.indices.map { i ->
      val bd = req.elementsDTOs[i]
      val fileId = filesMap.getKeyByValue(bd.filepaths[0])!!
      val element = lookUpElement(safeElements, fileId)
      val index = safeElements.indexOf(element)
      distances[index]
    }.toIntArray()
  }

  private fun lookUpElement(elements: Array<BytesData>, fileId: Int): BytesData =
    elements.first { it.ids[0] == fileId.toString() }

  fun removeSafeWorkingDirectoryForDistance(taskId: String) {
    val distancePrepDir1 = distancePrepDir + "1" + File.separator + taskId
    val distancePrepDir2 = distancePrepDir + "2" + File.separator + taskId

    try {
      FileUtils.forceDelete(File(distancePrepDir1))
      FileUtils.forceDelete(File(distancePrepDir2))
      log.debug("directories '{}' and '{}' deleted", distancePrepDir1, distancePrepDir2)
    } catch (e: IOException) {
      log.error("Failed to delete directories ({}, {})", distancePrepDir1, distancePrepDir2, e)
    }

    return
  }

  companion object {
    private val log = LoggerFactory.getLogger(AppPrepData::class.java)
  }
}
