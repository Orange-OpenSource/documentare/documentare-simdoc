@file:Suppress("UNNECESSARY_NOT_NULL_ASSERTION")

package com.orange.documentare.core.computationserver.biz.distances

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.distance.Distance
import com.orange.documentare.core.comp.distance.DistanceParameters
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.TaskLaunchAwait
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.slf4j.Logger
import org.slf4j.LoggerFactory

open class ComputeDistanceTask(
    private val req: DistancesRequest,
    private val tasks: Tasks,
    private val appPrepData: AppPrepData
) : TaskLaunchAwait {

  private var simdocTaskId: String = "no yet initialized id"

  private lateinit var distancesSegment: DistancesSegment

  val distance = Distance(DistanceParameters(if (req.suffixArrayAlgorithm == null) SuffixArrayAlgorithm.LIBDIVSUFSORT else req.suffixArrayAlgorithm))

  override fun waitTaskInBackground(simdocTaskId: String) {
    try {
      this.simdocTaskId = simdocTaskId
      distancesSegment = appPrepData.createSafeWorkingDirectoryForDistances(req, simdocTaskId)

      // FIXME: we should not use a DTO here, but rather distances array directly...
      val result = compute(distancesSegment)

      onDistancesReceived(result)
    } catch (throwable: Throwable) {
      onError(throwable)
    }
  }

  open fun onDistancesReceived(distancesRequestResultDTO: DistancesRequestResultDTO) {
    // FIXME: we should not use DTO here, it is impossible to have error == true
    if (distancesRequestResultDTO.error) {
      tasks.addErrorResult(simdocTaskId, distancesRequestResultDTO)
    }

    distancesSegment = distancesSegment.withDistances(distancesRequestResultDTO.distances)
    val t0 = System.currentTimeMillis()
    val remappedDistances = appPrepData.remapDistances(req, distancesSegment.elements, distancesSegment.distances, simdocTaskId)
    val durationToRemapDistancesMs = System.currentTimeMillis() - t0

    tasks.addResult(simdocTaskId, DistancesRequestResultDTO.with(remappedDistances))
    cleanUpWorkingDir()

    log.debug("[DISTANCE COMPUTATION - RESULT RECEIVED] simdocTaskId '$simdocTaskId' - time to remap distances '${durationToRemapDistancesMs}ms' for '${distancesSegment.elements.size}' elements and '${distancesSegment.distances.size}' distances")
  }

  fun onError(throwable: Throwable) {
    if (throwable is InterruptedException && !tasks.shouldWaitForTaskCompletion(simdocTaskId!!)) {
      return
    }

    log.error(String.format("[DISTANCE COMPUTATION - RESULT ERROR] simdocTaskId '%s'", simdocTaskId), throwable)
    tasks.addErrorResult(simdocTaskId, DistancesRequestResultDTO.error(throwable.message))
    cleanUpWorkingDir()
  }

  open fun cleanUpWorkingDir() {
    appPrepData.removeSafeWorkingDirectoryForDistance(simdocTaskId)
  }

  private fun compute(segment: DistancesSegment): DistancesRequestResultDTO {
    val distances = distance.compute(segment.element, segment.elements)
    return DistancesRequestResultDTO.with(distances)
  }

  fun testPurposeSetIds(simdocTaskId: String) {
    this.simdocTaskId = simdocTaskId
  }

  companion object {
    val log: Logger = LoggerFactory.getLogger(ComputeDistanceTask::class.java)
  }
}
