package com.orange.documentare.core.computationserver.biz.distances;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import jakarta.json.bind.annotation.JsonbProperty;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class DistancesRequestResultClusteringServerDTO {
  @JsonbProperty(nillable = true)
  public int[] distances;
  public boolean error;
  @JsonbProperty(nillable = true)
  public String errorMessage;

  public static DistancesRequestResultClusteringServerDTO error(String errorMessage) {
    return new DistancesRequestResultClusteringServerDTO(null, true, errorMessage);
  }

  public static DistancesRequestResultClusteringServerDTO with(int[] distances) {
    return new DistancesRequestResultClusteringServerDTO(distances, false, null);
  }
}
