package com.orange.documentare.core.computationserver.biz.task

/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import org.slf4j.LoggerFactory
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

open class Tasks : TaskRunnableErrorCatcher.RunningTaskListener {

  val maxTasks = availableProcessors()
  private var executor: ExecutorService = initThreadPool()

  private val tasks = hashMapOf<String, com.orange.documentare.core.computationserver.biz.task.Task>()
  private val activeTasks = mutableListOf<String>()
  private val runnableContexts = hashMapOf<String, Pair<TaskLaunchAwait, Future<*>>>()

  @Synchronized
  fun newTask(): String {
    val task = com.orange.documentare.core.computationserver.biz.task.Task()
    tasks[task.id] = task
    return task.id
  }

  @Synchronized
  fun exists(id: String) = tasks[id] != null

  @Synchronized
  fun isDone(id: String): Boolean {
    val task = tasks[id]
    if (task == null) {
      log.warn("Invalid task ids: $id")
      return true
    }
    return task.result.isPresent
  }

  @Synchronized
  fun isIdle() = activeTasks.size == 0

  @Synchronized
  fun addResult(id: String, result: Any?) {
    tasks[id]?.let {
      if (isDone(id)) {
        log.warn("Do not save result for task '$id' since task is done")
      } else {
        tasks[id] = it.withResult(result)
      }
    }
  }

  @Synchronized
  fun addErrorResult(id: String, result: Any) {
    tasks[id]?.let {
      if (isDone(id)) {
        log.warn("Do not save result for task '$id' since task is done")
      } else {
        tasks[id] = it.withErrorResult(result)
      }
    }
  }

  @Synchronized
  fun pop(id: String): com.orange.documentare.core.computationserver.biz.task.Task {
    val task = tasks[id] ?: throw IllegalStateException("Trying to pop an unexisting task '$id'")
    tasks.remove(id)
    return task
  }

  @Synchronized
  fun present(id: String) = tasks.containsKey(id)

  @Synchronized
  fun run(runnable: () -> Unit, taskId: String, runnableObject: TaskLaunchAwait) {
    val wrappedRunnable = TaskRunnableErrorCatcher(runnable, this, taskId)
    val future = executor.submit(wrappedRunnable)
    runnableContexts[taskId] = Pair(runnableObject, future)
    activeTasks.add(taskId)
  }

  @Synchronized
  override fun finished(taskId: String) {
    activeTasks.remove(taskId)
    runnableContexts.remove(taskId)
  }

  @Synchronized
  open fun shouldWaitForTaskCompletion(simdocTaskId: String): Boolean {
    return activeTasks.contains(simdocTaskId) && !isDone(simdocTaskId)
  }

  @Synchronized
  fun canAcceptNewTask() = activeTasks.size < maxTasks

  @Synchronized
  fun availableResultsTasksId(): List<String> {
    return tasks.values
      .filter { it.result.isPresent }
      .map { it.id }
  }

  @Synchronized
  fun killAll() {
    executor.shutdownNow()
    executor = initThreadPool()

    tasks.clear()
    activeTasks.clear()
    runnableContexts.clear()
  }

  fun isClean() = activeTasks.isEmpty() && runnableContexts.isEmpty() && tasks.isEmpty()

  companion object {
    private val log = LoggerFactory.getLogger(Tasks::class.java)
    fun availableProcessors() = Runtime.getRuntime().availableProcessors()

    private fun initThreadPool() = Executors.newFixedThreadPool(availableProcessors())
  }
}
