package com.orange.documentare.core.computationserver.biz.distances

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData

class DistancesSegment(
  val element: BytesData,
  val elements: Array<BytesData>,
  val distances: IntArray = intArrayOf()) {

  fun withDistances(computedDistances: IntArray): DistancesSegment =
    DistancesSegment(element, elements, computedDistances)
}
