package com.orange.documentare.core.computationserver.biz

/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

class RemoteTaskDTO {
  lateinit var id: String

  constructor() {
  }

  constructor(id: String) {
    this.id = id
  }
}
