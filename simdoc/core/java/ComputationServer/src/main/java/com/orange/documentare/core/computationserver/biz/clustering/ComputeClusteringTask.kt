@file:Suppress("UNNECESSARY_NOT_NULL_ASSERTION")

package com.orange.documentare.core.computationserver.biz.clustering

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.comp.clustering.compute.ComputeClustering
import com.orange.documentare.core.comp.distance.DistanceParameters
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.computationserver.biz.FileIO
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.TaskLaunchAwait
import com.orange.documentare.core.computationserver.biz.task.Tasks
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO.*
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO.Companion.fromBusinessObject
import org.slf4j.LoggerFactory
import java.io.IOException

open class ComputeClusteringTask(
  private val req: ClusteringRequest,
  private val tasks: Tasks,
  private val appPrepData: AppPrepData,
  private var fileIO: FileIO
) : TaskLaunchAwait {

  private var simdocTaskId: String = "not yet initialized id"
  private var doCompute: () -> Clustering = { doComputeClustering() }

  val computeClustering = ComputeClustering(DistanceParameters(if (req.suffixArrayAlgorithm == null) SuffixArrayAlgorithm.LIBDIVSUFSORT else req.suffixArrayAlgorithm))

  fun testPurposeInjectDoCompute(doCompute: () -> Clustering) {
    this.doCompute = doCompute
  }

  override fun waitTaskInBackground(simdocTaskId: String) {
    try {
      this.simdocTaskId = simdocTaskId

      val clustering = doCompute.invoke()

      onClusteringReceived(clustering)
    } catch (throwable: Throwable) {
      onError(throwable)
    }
  }

  private fun doComputeClustering(): Clustering {
    fileIO = fileIO.forTaskId(simdocTaskId)
    fileIO.deleteAllClusteringFiles()
    if (req.debug()) {
      fileIO.writeRequest(req)
    }
    val bytesDataArray = prepData(simdocTaskId, req)

    return computeClustering.compute(bytesDataArray, req.clusteringParameters())
  }

  private fun onError(throwable: Throwable) {
    // cleanup at once, so unit tests can check it is done as soon as task is available on the next line
    cleanUpWorkingDir(simdocTaskId)

    log.error(String.format("[CLUSTERING - RESULT ERROR] simdocTaskId '$simdocTaskId'"), throwable)
    tasks.addErrorResult(simdocTaskId!!, mapToDTO(Clustering.error(req.clusteringParameters(), throwable.message)))
  }

  open fun onClusteringReceived(clustering: Clustering) {
    log.debug("[CLUSTERING - RESULT RECEIVED] simdocTaskId '$simdocTaskId'")
    if (clustering.failed()) {
      // cleanup at once, so unit tests can check it is done as soon as task is available on the next line
      cleanUpWorkingDir(simdocTaskId)

      tasks.addErrorResult(simdocTaskId, mapToDTO(clustering))
    } else {
      val remappedClustering = remap(clustering)
      val result = mapToDTO(remappedClustering)

      // cleanup at once, so unit tests can check it is done as soon as task is available on the next line
      cleanUpWorkingDir(simdocTaskId)

      tasks.addResult(simdocTaskId, result)
    }
  }

  private fun mapToDTO(clustering: Clustering): ClusteringDTO {
    return fromBusinessObject(clustering)
  }

  private fun remap(clustering: Clustering): Clustering {
    var remappedClustering =
      if (req.bytesDataMode())
        ClusteringRemap.remapInBytesDataMode(
          dtoToBytesData(
            req.bytesData
          ), clustering, fileIO
        )
      else
        ClusteringRemap.remapInFilesMode(fileIO, clustering)

    if (req.debug()) {
      try {
        fileIO.writeClusteringRequestResult(remappedClustering)
      } catch (e: IOException) {
        e.printStackTrace()
      }
    } else {
      fileIO.cleanupClustering()
    }

    remappedClustering = remappedClustering.dropGraph()
    return remappedClustering
  }

  private fun prepData(taskId: String, req: ClusteringRequest): Array<BytesData> {
    // if bytes are already loaded, there is no directory to prep
    val nothingToPrep = req.bytesDataMode() && req.bytesData[0].bytes != null
    return if (nothingToPrep) {
      dtoToBytesData(req.bytesData)
    } else appPrepData.createSafeWorkingDirectoryForClustering(taskId, req)
  }

  open fun cleanUpWorkingDir(simdocTaskId: String) {
    fileIO.deleteAllFiles(simdocTaskId)
  }

  fun testPurposeSetIds(simdocTaskId: String) {
    this.simdocTaskId = simdocTaskId
  }

  companion object {
    private val log = LoggerFactory.getLogger(ComputeClusteringTask::class.java)
  }
}
