package com.orange.documentare.core.computationserver.infrastructure.transport;

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;

import java.util.Arrays;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class BytesDataDTO {
  public List<String> ids;
  public List<String> filepaths;
  public byte[] bytes;

  public BytesDataDTO() {
    this(null, null, null);
  }

  public BytesDataDTO(List<String> ids, List<String> filepaths, byte[] bytes) {
    this.ids = ids;
    this.filepaths = filepaths;
    this.bytes = bytes;
  }


  public static BytesDataDTO dtoFromBytesData(BytesData bytesData) {
    return new BytesDataDTO(bytesData.ids, bytesData.filepaths, bytesData.bytes);
  }

  public static BytesData dtoToBytesData(BytesDataDTO bytesDataDTO) {
    return bytesDataDTO.bytes == null ? new BytesData(bytesDataDTO.ids, bytesDataDTO.filepaths) : new BytesData(bytesDataDTO.ids, bytesDataDTO.bytes);
  }

  public static BytesDataDTO[] dtoFromBytesData(BytesData[] bytesData) {
    return Arrays.stream(bytesData)
      .map(BytesDataDTO::dtoFromBytesData)
      .toArray(BytesDataDTO[]::new);
  }

  public static BytesData[] dtoToBytesData(BytesDataDTO[] bytesDataDTOs) {
    return Arrays.stream(bytesDataDTOs)
      .map(BytesDataDTO::dtoToBytesData)
      .toArray(BytesData[]::new);
  }
}
