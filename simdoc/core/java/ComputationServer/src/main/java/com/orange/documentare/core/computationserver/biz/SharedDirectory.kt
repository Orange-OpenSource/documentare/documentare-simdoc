package com.orange.documentare.core.computationserver.biz

interface SharedDirectory {
  fun sharedDirectoryAvailable(): Boolean
  fun sharedDirectoryRootPath(): String?

  class Examples {
    companion object {
      fun notAvailable(): SharedDirectory {
        return object : SharedDirectory {
          override fun sharedDirectoryAvailable() = false
          override fun sharedDirectoryRootPath() = null
        }
      }
    }

  }
}
