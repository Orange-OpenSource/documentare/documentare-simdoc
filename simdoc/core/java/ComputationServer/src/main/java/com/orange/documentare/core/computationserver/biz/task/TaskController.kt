/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.computationserver.biz.task

data class TaskController(private val tasks: Tasks) {

  enum class TaskState { NOT_FOUND, NOT_FINISHED, ERROR, DONE }

  fun retrieveTaskResult(taskId: String): Pair<TaskState, Any?> {
    if (!tasks.exists(taskId)) {
      return Pair(TaskState.NOT_FOUND, null)
    }

    if (!tasks.isDone(taskId)) {
      return Pair(TaskState.NOT_FINISHED, null)
    }

    val task = tasks.pop(taskId)
    if (task.error) {
      return Pair(TaskState.ERROR, task.result)
    }

    return Pair(TaskState.DONE, task.result.get())
  }

  fun retrieveAvailableResultsTasksId() = AvailableResultsTasksIdDTO(tasks.availableResultsTasksId())

  fun killAll() {
    tasks.killAll()
  }
}
