package com.orange.documentare.core.computationserver.biz.clustering;

import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ElementPosition {
  public final int position;
  public final ClusteringElement element;
}
