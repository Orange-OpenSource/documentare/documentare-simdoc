package com.orange.documentare.core.computationserver.biz.distances;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import jakarta.json.bind.annotation.JsonbNillable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonbNillable
public class DistancesRequestResultDTO {
  public int[] distances;
  public boolean error;
  public String errorMessage;
  public boolean serviceUnavailable;

  public static DistancesRequestResultDTO error(String errorMessage) {
    return new DistancesRequestResultDTO(null, true, errorMessage, false);
  }

  public static DistancesRequestResultDTO with(int[] distances) {
    return new DistancesRequestResultDTO(distances, false, null, false);
  }
}
