package com.orange.documentare.core.computationserver.biz.prepdata

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.system.inputfilesconverter.FilesMap
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class AppPrepDataRemapDistancesTest {

  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  private lateinit var appPrepData: AppPrepData

  private val elementsCount = 10
  private val filesMap = FilesMap()

  @TempDir
  lateinit var tmpDir: File

  @BeforeEach
  fun setup() {
    appPrepData = AppPrepData(distancePrepDir.absolutePath + "/distances_prep_dir_", rawImageCacheDir.absolutePath,
        SharedDirectory.Examples.notAvailable()
    )
  }

    @Test
  fun `remap distances, useful to check performance issues`() {
    // given
    val distances = (0..elementsCount).map { it }.toIntArray()
    val element = arrayOf(BytesData())
    val elements = buildElements()
    val req = com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest.builder().element(element).compareTo(elements).build()
    val safeElements = buildSafeElements()

    // when
    val t0 = System.currentTimeMillis()
    val remappedDistances = appPrepData.doRemapDistances(distances, req, filesMap, safeElements)
    val dt = System.currentTimeMillis() - t0
    println("duration ${dt}ms")

    // then
    then(remappedDistances.reversed().toIntArray()).isEqualTo(distances)
  }

  private fun buildElements(): Array<BytesData> {
    return (0..elementsCount).map {
      val file = File("${tmpDir.absolutePath}/file-$it")
      filesMap[it] = file.absolutePath
      file.createNewFile()
      BytesData(listOf("xyz$it"), listOf(file.absolutePath))
    }.reversed().toTypedArray()
  }

  private fun buildSafeElements(): Array<BytesData> {
    return (0..elementsCount).map {
      val file = File("${tmpDir.absolutePath}/xyzfile-$it")
      file.createNewFile()
      BytesData(listOf("$it"), listOf(file.absolutePath))
    }.toTypedArray()
  }
}
