/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.core.computationserver.biz.task

import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.util.*

class TasksTest {

  private val runnableObject = object : TaskLaunchAwait {
    override fun waitTaskInBackground(simdocTaskId: String) {
    }
  }

  @Test
  fun task_id_is_unique() {
    // given
    val tasks = Tasks()

    // when
    val id1 = tasks.newTask()
    val id2 = tasks.newTask()

    // then
    then(id1).isNotEqualTo(id2)
  }

  @Test
  fun can_not_pop_an_unknown_task_id() {
    // given
    val tasks = Tasks()

    // when
    try {
      tasks.pop("123456")
    } catch (e: IllegalStateException) {
      // then
      then(e.message).isEqualTo("Trying to pop an unexisting task '123456'")
    }
  }

  @Test
  fun invalid_task_id() {
    // given
    val tasks = Tasks()

    // when
    tasks.isDone("invalid")

    // then
    then(tasks.exists("invalid")).isFalse()
  }

  @Test
  fun add_task_then_flag_it_as_finished_then_remove_it_when_retrieved() {
    // given
    val tasks = Tasks()
    val result = Any()

    // when / then
    val id = tasks.newTask()
    println("task ids = $id")

    then(tasks.isDone(id)).isFalse()
    tasks.addResult(id, result)
    then(tasks.isDone(id)).isTrue()

    val task = tasks.pop(id)
    then(tasks.present(id)).isFalse()
    then(task.result.get()).isEqualTo(result)
  }

  @Test
  fun add_error_result() {
    // given
    val tasks = Tasks()
    val result = com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO.error("err")
    val id = tasks.newTask()

    // when
    tasks.addErrorResult(id, result)
    val task = tasks.pop(id)

    // then
    then(task.error).isTrue()
  }

  @Test
  fun `can not save OK result if task is DONE`() {
    // given
    val tasks = Tasks()
    val taskOK = tasks.newTask()
    tasks.addResult(taskOK, "ok")

    // when
    tasks.addResult(taskOK, "ok 2")

    // then
    then(tasks.pop(taskOK).result.get() as String).isEqualTo("ok")
  }

  @Test
  fun `can not save ERROR result if task is DONE`() {
    // given
    val tasks = Tasks()
    val taskError = tasks.newTask()
    tasks.addErrorResult(taskError, "error")

    // when
    tasks.addErrorResult(taskError, "error 2")

    // then
    then(tasks.pop(taskError).result.get() as String).isEqualTo("error")
  }

  @Test
  fun `available results list is empty if no task is finished yet`() {
    // given
    val tasks = Tasks()
    tasks.newTask()

    // when
    val availableResultsTasksId: List<String> = tasks.availableResultsTasksId()

    // then
    then(availableResultsTasksId).isEmpty()
  }

  @Test
  fun `available results list contains a finished task`() {
    // given
    val tasks = Tasks()
    val result = Any()
    val id = tasks.newTask()
    tasks.addResult(id, result)

    // when
    val availableResultsTasksId: List<String> = tasks.availableResultsTasksId()

    // then
    then(availableResultsTasksId).containsExactly(id)
  }

  @Test
  fun `available results list contains a finished task and a finished task with error`() {
    // given
    val tasks = Tasks()
    val result = "ok"
    val resultError = "not ok"
    val id = tasks.newTask()
    val idError = tasks.newTask()
    tasks.addResult(id, result)
    tasks.addErrorResult(idError, resultError)

    // when
    val availableResultsTasksId: List<String> = tasks.availableResultsTasksId()

    // then
    then(availableResultsTasksId).containsExactlyInAnyOrder(id, idError)
  }

  @Test
  fun `check we can reach max number of tasks several times, even if all tasks are killed sometimes`() {
    // given
    val tasks = Tasks()

    // when / then
    repeat(3) {

      fillTasks(tasks)

      then(tasks.canAcceptNewTask()).isFalse()
      tasks.killAll()
    }
  }

  @Test
  fun `check we can reach max number of tasks event if previous tasks crashed`() {
    // given
    val tasks = Tasks()

    // when
    repeat(Runtime.getRuntime().availableProcessors()) {
      then(tasks.canAcceptNewTask()).isTrue()
      tasks.run({ throw Error("crash") }, UUID.randomUUID().toString(), runnableObject)
    }

    // then
    // wait for tasks to crash and then to be cleanup
    do {
      Thread.sleep(10)
    } while (!tasks.isIdle())

    fillTasks(tasks)
    then(tasks.canAcceptNewTask()).isFalse()
  }

  @Test
  fun `check tasks are cleanup after a task ended`() {
    // given
    val tasks = Tasks()
    val taskId = tasks.newTask()
    var done = false

    // when
    tasks.run({ done = true }, taskId, runnableObject)

    // then
    // wait for tasks to end and then to be cleanup
    do {
      Thread.sleep(10)
    } while (!tasks.isIdle() || !done)
    tasks.pop(taskId)

    then(tasks.isClean()).isTrue()
  }

  @Test
  fun `check we should wait for a running task, then not wait anymore after it ended`() {
    // given
    val tasks = Tasks()
    val taskId = tasks.newTask()
    var done = false

    // when
    tasks.run({

      then(tasks.shouldWaitForTaskCompletion(taskId)).isTrue()

      done = true
    }, taskId, runnableObject)

    // then
    // wait for tasks to end and then to be cleanup
    do {
      Thread.sleep(10)
    } while (!tasks.isIdle() || !done)

    then(tasks.shouldWaitForTaskCompletion(taskId)).isFalse()
  }

  @Test
  fun `check we should not wait for a task not ended but for which we got the result`() {
    // given
    val tasks = Tasks()
    val taskId = tasks.newTask()

    // when
    var done = false
    var shouldNotWaitAfterResultWasAdded = false
    tasks.run({
      then(tasks.shouldWaitForTaskCompletion(taskId)).isTrue()
      tasks.addErrorResult(taskId, "a result")
      shouldNotWaitAfterResultWasAdded = !tasks.shouldWaitForTaskCompletion(taskId)
      done = true
    }, taskId, runnableObject)

    // then
    // wait for tasks to end and then to be cleanup
    do {
      Thread.sleep(10)
    } while (!tasks.isIdle() || !done)

    then(shouldNotWaitAfterResultWasAdded).isTrue()
  }

  private fun fillTasks(tasks: Tasks) {
    repeat(Runtime.getRuntime().availableProcessors()) {
      then(tasks.canAcceptNewTask()).isTrue()
      tasks.run({
        do {
          Thread.sleep(1000000)
        } while (true)
      }, UUID.randomUUID().toString(), runnableObject)
    }
  }
}
