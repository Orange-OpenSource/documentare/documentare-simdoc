package com.orange.documentare.core.computationserver.biz.clustering

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.computationserver.biz.FileIO
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

internal class ComputeClusteringTaskTest {

  @Test
  fun `suffix array algo is not provided, then select LIBDIVSUFSORT as default algorithm`() {
    // given
    val req = ClusteringRequest()
    // when
    val computeClusteringTask = ComputeClusteringTask(req, Tasks(), AppPrepData("", "", SharedDirectory.Examples.notAvailable()),
        FileIO(SharedDirectory.Examples.notAvailable(), req)
    )

    // then
    then(computeClusteringTask.computeClustering.distance.distanceParameters.suffixArrayAlgorithm).isEqualTo(SuffixArrayAlgorithm.LIBDIVSUFSORT)
  }

  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `use provided algorithm`(algo: SuffixArrayAlgorithm) {
    // given
    val req = ClusteringRequest().apply { suffixArrayAlgorithm = algo }

    // when
    val computeClusteringTask = ComputeClusteringTask(req, Tasks(), AppPrepData("", "", SharedDirectory.Examples.notAvailable()),
        FileIO(SharedDirectory.Examples.notAvailable(), req)
    )

    // then
    then(computeClusteringTask.computeClustering.distance.distanceParameters.suffixArrayAlgorithm).isEqualTo(algo)
  }
}
