package com.orange.documentare.core.computationserver.biz.clustering

import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.EnrollParameters
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

class ClusteringRequestToParametersTest {

  @Test
  fun `clustering parameters are extracted from request`() {
    // given
    val acut = 1.1f
    val qcut = 2.1f
    val scut = 3.1f
    val ccut = 4
    val k = 6

    val enrollAcut = 2.1f
    val enrollQcut = 3.1f
    val enrollScut = 4.1f
    val enrollCcut = 5
    val enrollK = 7

    val req = ClusteringRequest.builder()
      .bytesData(arrayOf())
      .outputDirectory("")
      .acutSdFactor(acut)
      .qcutSdFactor(qcut)
      .scutSdFactor(scut)
      .ccutPercentile(ccut)
      .knnThreshold(k)
      .sloop(true)
      .consolidateCluster(true)
      .enroll(true)
      .enrollAcutSdFactor(enrollAcut)
      .enrollQcutSdFactor(enrollQcut)
      .enrollScutSdFactor(enrollScut)
      .enrollCcutPercentile(enrollCcut)
      .enrollKnnThreshold(enrollK)
      .build()

    // when
    val parameters = req.clusteringParameters()

    // then
    val expectedParameters = ClusteringParameters.builder()
      .acut(acut)
      .qcut(qcut)
      .scut(scut)
      .ccut(ccut)
      .knn(k)
      .sloop()
      .consolidateCluster()
      .enroll()
      .enrollParameters(
        EnrollParameters.builder()
          .acut(enrollAcut)
          .qcut(enrollQcut)
          .scut(enrollScut)
          .ccut(enrollCcut)
          .knn(enrollK)
          .build()
      )
      .build()

    assertThat(parameters).isEqualTo(expectedParameters)
  }
}
