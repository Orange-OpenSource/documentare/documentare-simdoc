package com.orange.documentare.core.computationserver.biz.clustering

import com.nhaarman.mockitokotlin2.spy
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import com.orange.documentare.core.computationserver.biz.FileIO
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ComputeClusteringTest {

  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  private lateinit var appPrepData: AppPrepData
  private lateinit var computeClustering: ComputeClusteringTask

  private val tasks: Tasks = spy()
  private val sharedDirectory = SharedDirectory.Examples.notAvailable()

  @BeforeEach
  fun setup() {
    appPrepData = AppPrepData(distancePrepDir.absolutePath + "/distances_prep_dir_", rawImageCacheDir.absolutePath,
        SharedDirectory.Examples.notAvailable()
    )
    computeClustering = ComputeClusteringTask(
        ClusteringRequest(), tasks, appPrepData,
        FileIO(sharedDirectory, ClusteringRequest())
    )
  }

  @Test
  fun `an error occurred during clustering computation`() {
    // given
    val simdocTaskId = tasks.newTask()
    computeClustering.testPurposeSetIds(simdocTaskId)
    val doCompute = { throw IllegalStateException("I am an error") }
    computeClustering.testPurposeInjectDoCompute(doCompute)

    // when
    computeClustering.waitTaskInBackground(simdocTaskId)

    // then
    val result = tasks.pop(simdocTaskId).result.get()
    then(result).isInstanceOf(ClusteringDTO::class.java)
    then((result as ClusteringDTO).error).isEqualTo("I am an error")
  }

  @Test
  fun `task result contains an error`() {
    // given
    val simdocTaskId = tasks.newTask()
    computeClustering.testPurposeSetIds(simdocTaskId)
    val clustering = Clustering.error(ClusteringParameters.builder().build(), "I am a clustering error")
    val doCompute = { clustering }
    computeClustering.testPurposeInjectDoCompute(doCompute)

    // when
    computeClustering.waitTaskInBackground(simdocTaskId)

    // then
    val result = tasks.pop(simdocTaskId).result.get()
    then(result).isInstanceOf(ClusteringDTO::class.java)
    then((result as ClusteringDTO).error).isEqualTo("I am a clustering error")
  }
}
