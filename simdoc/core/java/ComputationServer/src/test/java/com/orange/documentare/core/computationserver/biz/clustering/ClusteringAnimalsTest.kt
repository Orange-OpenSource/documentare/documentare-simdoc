package com.orange.documentare.core.computationserver.biz.clustering

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.TaskController
import com.orange.documentare.core.computationserver.biz.task.Tasks
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO.dtoFromBytesData
import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import org.apache.commons.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ClusteringAnimalsTest {

  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  @BeforeEach
  internal fun setup() {
    cleanup()
    File(OUTPUT_DIRECTORY).mkdir()
  }
  @AfterEach
  internal fun cleanup() {
    FileUtils.deleteQuietly(File(OUTPUT_DIRECTORY))
  }


  @Test
  internal fun build_animals_dna_clustering() {
    // Given
    val req = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDirAbsolutePath())
      .build()

    test(req, "expected-clustering-result-animals-dna.json")
  }

  @Test
  internal fun build_animals_dna_clustering_in_debug_mode() {
    // Given
    val req = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build()

    test(req, "expected-clustering-result-animals-dna.json")
  }

  @Test
  internal fun build_animals_dna_clustering_with_bytes_data_bytes_in_debug_mode() {
    // Given
    val jsonGenericHandler = JsonGenericHandler()
    val bytesDataJson = File(javaClass.getResource("/bytes-data-bytes-animals-dna.json").file)
    val bytesDataArray = jsonGenericHandler.getObjectFromJsonFileNewApi(BytesDataArray::class.java, bytesDataJson) as BytesDataArray

    val req = ClusteringRequest.builder()
      .bytesData(dtoFromBytesData(bytesDataArray.bytesData))
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build()

    test(req, "expected-clustering-result-bytes-data-animals-dna.json")
  }

  @Test
  internal fun build_animals_dna_clustering_with_bytes_data_files_in_debug_mode() {
    // Given
    val bytesData = BytesData.loadFromDirectory(File(inputDirectory())) { it.name }

    val req = ClusteringRequest.builder()
      .bytesData(dtoFromBytesData(bytesData))
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build()

    test(req, "expected-clustering-result-bytes-data-files-animals-dna.json")
  }

  private fun test(req: ClusteringRequest, expectedJson: String) {
    val tasks = setupTasks()
    val taskController = setupTaskController(tasks)
    val clusteringController = setupClusteringController(tasks, distancePrepDir.absolutePath, rawImageCacheDir.absolutePath)

    // when
    val taskId = clusteringController.launchClustering(req).taskId!!

    var clustering: Clustering?
    do {
      Thread.sleep(10)
      clustering = (taskController.retrieveTaskResult(taskId).second as ClusteringDTO?)?.toBusinessObject()
    } while (clustering == null)

    // then
    JsonGenericHandler.instance().writeObjectToJsonFileNewApi(clustering, File("new-$expectedJson"))

    val expected = expectedClusteringResult(expectedJson)
    assertThat(clustering).isEqualTo(expected)

    val prepDir = File(OUTPUT_DIRECTORY)
    System.out.println(prepDir.absolutePath)
    assertThat(prepDir.list()).isEmpty()
  }

  private fun inputDirectory(): String {
    return File(javaClass.getResource("/animals-dna").file).absolutePath
  }

  private fun expectedClusteringResult(expectedJson: String): Clustering {
    return (JsonGenericHandler.instance().getObjectFromJsonFileNewApi(ClusteringDTO::class.java, File(javaClass.getResource("/$expectedJson").file)) as ClusteringDTO)
      .toBusinessObject()
  }

  private fun outputDirAbsolutePath(): String {
    return File(OUTPUT_DIRECTORY).absolutePath
  }

  private fun setupTaskController(tasks: Tasks): TaskController {
    return TaskController(tasks)
  }

  private fun setupTasks(): Tasks = Tasks()

  companion object {

    /**
     * set to true to run against a real clustering server
     */
    private const val OUTPUT_DIRECTORY = "out"

    private const val DISTANCES_PREP_DIR_PREFIX = "distances_prep_dir_"

    fun setupClusteringController(tasks: Tasks, distancePrepDir: String, rawImageCacheDir: String): ClusteringController {
      val appPrepData = AppPrepData("$distancePrepDir/$DISTANCES_PREP_DIR_PREFIX", rawImageCacheDir, SharedDirectory.Examples.notAvailable())
      return ClusteringController(
        appPrepData,
        SharedDirectory.Examples.notAvailable(),
        tasks
      )
    }
  }
}
