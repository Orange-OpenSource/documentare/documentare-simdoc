package com.orange.documentare.core.computationserver.biz.distances.api

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class DistancesRequestTest {
  @Test
  fun `element is not provided, request is invalid`() {
    // given
    val request = DistancesRequest.builder().build()

    // then
    then(request.validate().ok).isFalse
    then(request.validate().error).isEqualTo("element is missing")
  }

  @Test
  fun `elements are not provided, request is invalid`() {
    // given
    val request = DistancesRequest.builder().element(emptyArray()).build()

    // then
    then(request.validate().ok).isFalse
    then(request.validate().error).isEqualTo("elements are missing")
  }

  @Test
  fun `request is valid`() {
    // given
    val request = DistancesRequest.builder().element(emptyArray()).compareTo(emptyArray()).build()

    // then
    then(request.validate().ok).isTrue
  }

  @Test
  fun `change default parameters`() {
    // given
    val request = DistancesRequest.builder()
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.SAIS)
      .rawConverter(true)
      .pCount(1234)
      .build()

    // then
    then(request.suffixArrayAlgorithm).isEqualTo(SuffixArrayAlgorithm.SAIS)
    then(request.pCount).isEqualTo(1234)
    then(request.rawConverter).isTrue
  }

  @Test
  fun `change default parameters, inverse values`() {
    // given
    val request = DistancesRequest.builder()
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
      .rawConverter(false)
      .pCount(4321)
      .build()

    // then
    then(request.suffixArrayAlgorithm).isEqualTo(SuffixArrayAlgorithm.LIBDIVSUFSORT)
    then(request.pCount).isEqualTo(4321)
    then(request.rawConverter).isFalse
  }
}
