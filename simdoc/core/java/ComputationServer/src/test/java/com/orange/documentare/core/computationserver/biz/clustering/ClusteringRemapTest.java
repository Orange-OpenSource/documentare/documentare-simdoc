package com.orange.documentare.core.computationserver.biz.clustering;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.ref.clustering.Cluster;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import com.orange.documentare.core.system.inputfilesconverter.FilesMap;
import com.orange.documentare.core.computationserver.biz.FileIO;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static com.orange.documentare.core.computationserver.biz.clustering.ClusteringRemap.remapInBytesDataMode;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClusteringRemapTest {

  public final String PATH_TO_FILE_0 = filePath("file0");
  public final String PATH_TO_FILE_1 = filePath("file1");
  public final String PATH_TO_FILE_2 = filePath("file2");
  public final FileIO fileIO = mock(FileIO.class);

  @Test
  public void remap_elements_indices_in_prepped_bytes_data_mode() {
    // given
    FilesMap filesMap = filesMap();
    when(fileIO.filesPrepped()).thenReturn(true);
    when(fileIO.loadFilesMap()).thenReturn(filesMap);
    BytesData[] bytesData = bytesData();
    Clustering clustering = clustering();

    // when
    Clustering remappedClustering = remapInBytesDataMode(bytesData, clustering, fileIO);

    // then
    Cluster firstCluster = remappedClustering.clusters.get(0);
    assertThat(firstCluster.elementsIndices).containsExactly(2, 0);
    assertThat(firstCluster.multisetElementsIndices).containsExactly(2, 0);
  }

  private Clustering clustering() {
    List<ClusteringElement> elements = asList(
      new ClusteringElement("0", 10, false, false, null, null),
      new ClusteringElement("1", 10, false, false, null, null),
      new ClusteringElement("2", 20, false, false, null, null)
    );
    List<Integer> elementsIndices1 = asList(0, 1);
    List<Integer> multisetElementsIndices1 = asList(0, 1);
    List<Integer> elementsIndices2 = asList(2);
    List<Integer> multisetElementsIndices2 = asList(2);
    List<Cluster> clusters = asList(
      new Cluster(elementsIndices1, multisetElementsIndices1),
      new Cluster(elementsIndices2, multisetElementsIndices2)
    );

    return new Clustering(elements, null, clusters, null, null, null);
  }

  private BytesData[] bytesData() {
    BytesData[] bytesData = {
      new BytesData(asList("file 2"), asList(PATH_TO_FILE_2)),
      new BytesData(asList("file 0"), asList(PATH_TO_FILE_0)),
      new BytesData(asList("file 1"), asList(PATH_TO_FILE_1))
    };

    return bytesData;
  }

  private FilesMap filesMap() {
    final FilesMap filesMap = new FilesMap();
    filesMap.put(0, PATH_TO_FILE_1);
    filesMap.put(1, PATH_TO_FILE_2);
    filesMap.put(2, PATH_TO_FILE_0);
    return filesMap;
  }

  private String filePath(String filename) {
    return new File(getClass().getResource("/" + filename).getFile()).getAbsolutePath();
  }
}
