package com.orange.documentare.core.computationserver.biz.clustering

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.model.json.JsonGenericHandler
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringAnimalsTest.Companion.setupClusteringController
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.task.TaskController
import com.orange.documentare.core.computationserver.biz.task.Tasks
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO.dtoFromBytesData
import org.apache.commons.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ClusteringGlyphsTest {
  @BeforeEach
  fun setup() {
    cleanup()
    File(OUTPUT_DIRECTORY).mkdir()
  }

  @AfterEach
  fun cleanup() {
    FileUtils.deleteQuietly(File(OUTPUT_DIRECTORY))
  }

  @Test
  fun build_glyphs_clustering_with_bytes_data(@TempDir distancePrepDir: File, @TempDir rawImageCacheDir: File) {
    // Given
    val inputDirectory = File(inputDirectory())
    val bytesData = BytesData.loadFromDirectory(inputDirectory, BytesData.relativePathIdProvider(inputDirectory))
    val req = ClusteringRequest.builder()
      .bytesData(dtoFromBytesData(bytesData))
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build()

    test(req, distancePrepDir, rawImageCacheDir)

    val prepDir = File(OUTPUT_DIRECTORY)
    assertThat(prepDir.list()).isEmpty()
  }

  private fun test(req: ClusteringRequest, distancePrepDir: File, rawImageCacheDir: File): String {
    val tasks = setupTasks()
    val taskController = setupTaskController(tasks)
    val clusteringController = setupClusteringController(tasks, distancePrepDir.absolutePath, rawImageCacheDir.absolutePath)

    // when
    val taskId = clusteringController.launchClustering(req).taskId!!

    var clustering: Clustering?
    do {
      Thread.sleep(10)
      clustering = (taskController.retrieveTaskResult(taskId).second as ClusteringDTO?)?.toBusinessObject()
    } while (clustering == null)

    // then
    JsonGenericHandler.instance().writeObjectToJsonFileNewApi(clustering, File("new-expected-clustering-result-glyphs.json"))

    val expected = expectedClusteringResult()
    assertThat(clustering).isEqualTo(expected)

    return taskId
  }

  private fun setupTaskController(tasks: Tasks): TaskController {
    return TaskController(tasks)
  }

  private fun setupTasks(): Tasks = Tasks()


  private fun inputDirectory(): String {
    return File(javaClass.getResource("/glyphs").file).absolutePath
  }

  private fun expectedClusteringResult(): Clustering {
    return (JsonGenericHandler.instance().getObjectFromJsonFileNewApi(ClusteringDTO::class.java,
      File(javaClass.getResource("/expected-clustering-result-glyphs.json").file)
    ) as ClusteringDTO)
      .toBusinessObject()
  }

  private fun outputDirAbsolutePath(): String {
    return File(OUTPUT_DIRECTORY).absolutePath
  }

  companion object {
    private const val OUTPUT_DIRECTORY = "out"
  }
}
