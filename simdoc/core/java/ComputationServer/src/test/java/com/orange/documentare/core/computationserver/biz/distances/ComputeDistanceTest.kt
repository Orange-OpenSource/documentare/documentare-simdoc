package com.orange.documentare.core.computationserver.biz.distances

import com.nhaarman.mockitokotlin2.*
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.mockito.Mockito.`when`
import java.io.File

class ComputeDistanceTest {
  private lateinit var appPrepData: AppPrepData
  private lateinit var computeDistance: ComputeDistanceTask

  private val tasks: Tasks = spy(Tasks())

  @BeforeEach
  fun setup(@TempDir distancePrepDir: File, @TempDir rawImageCacheDir: File) {
    appPrepData = AppPrepData(distancePrepDir.absolutePath + "/distances_prep_dir_", rawImageCacheDir.absolutePath,
        SharedDirectory.Examples.notAvailable()
    )
    computeDistance = spy(ComputeDistanceTask(com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest(), tasks, appPrepData))
  }

  @Test
  fun `should notify an error if thread was interrupted but we should still wait for task`() {
    // given
    val simdocTaskId = tasks.newTask()
    computeDistance.testPurposeSetIds(simdocTaskId)
    `when`(tasks.shouldWaitForTaskCompletion(simdocTaskId)).thenReturn(true)
    doNothing().`when`(computeDistance).cleanUpWorkingDir()

    // when
    computeDistance.onError(InterruptedException("not a normal interruption"))

    // then
    val result = tasks.pop(simdocTaskId).result.get()
    then(result).isInstanceOf(com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO::class.java)
    then((result as com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO).errorMessage).isEqualTo("not a normal interruption")
  }

  @Test
  fun `should do nothing if error was due to a result provided by callback api, ie task is done and thread was interrupted`() {
    // given
    val simdocTaskId = "12"
    computeDistance.testPurposeSetIds(simdocTaskId)
    doReturn(false).`when`(tasks).shouldWaitForTaskCompletion(simdocTaskId)

    // when
    computeDistance.onError(InterruptedException())

    // then
    then(tasks.exists(simdocTaskId)).isFalse
    verify(computeDistance, never()).cleanUpWorkingDir()
  }
}
