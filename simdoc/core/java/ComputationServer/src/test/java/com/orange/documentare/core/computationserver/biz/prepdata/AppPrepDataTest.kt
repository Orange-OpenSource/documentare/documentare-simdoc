package com.orange.documentare.core.computationserver.biz.prepdata

import com.orange.documentare.core.prepdata.PrepData
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class AppPrepDataTest {

  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  @TempDir
  lateinit var outputDir: File

  @Test
  fun `build PrepData based on request, without raw conversion`() {
    // given
    val req = defaultRequest().build()
    val expectedPrepData = defaultPrepData().build()

    test(expectedPrepData, req)
  }

  @Test
  fun `build PrepData based on request, with raw conversion set to false`() {
    // given
    val req = defaultRequest().rawConverter(false).build()
    val expectedPrepData = defaultPrepData().build()

    test(expectedPrepData, req)
  }

  @Test
  fun `build PrepData based on request, with raw conversion set to true and pixel count to 1234 and cache directory`() {
    // given
    val req = defaultRequest().rawConverter(true).pCount(1234).build()
    val expectedPrepData = defaultPrepData()
      .withRawConverter(true)
      .withRawCacheDirectory(rawImageCacheDir.absolutePath)
      .expectedRawPixelsCount(1234).build()

    test(expectedPrepData, req)
  }

  private fun test(expectedPrepData: PrepData?, req: com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest) {
    var called = false

    val prepDataImpl: (prepData: PrepData) -> Unit = {
      // then
      called = true
      assertThat(it).isEqualTo(expectedPrepData)

      File(safeWorkingDir()).mkdir()
    }

    val appPrepData = AppPrepData(
      distancePrepDir.absolutePath + "/distances_prep_dir_",
      rawImageCacheDir.absolutePath,
        SharedDirectory.Examples.notAvailable(),
      prepDataImpl)

    // when
    appPrepData.createSafeWorkingDirectoryForClustering(TASK_ID, req)

    // then 2
    assertThat(called).isTrue()
  }

  private fun defaultRequest(): com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest.ClusteringRequestBuilder {
    return com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest.builder()
      .outputDirectory(outputDirAbsolutePath())
      .inputDirectory(inputDir().absolutePath)
  }

  private fun defaultPrepData(): PrepData.PrepDataBuilder {
    return PrepData.builder()
      .inputDirectory(inputDir())
      .safeWorkingDirectory(File(safeWorkingDir()))
      .metadataOutputFile(File(metadataOutputFile()))
      .safeWorkingDirConverter()
      .withRawCacheDirectory(rawImageCacheDir.absolutePath)
      .expectedRawPixelsCount(1024 * 1024)
  }

  private fun inputDir() = File("/tmp")
  private fun outputDirAbsolutePath() = outputDir.absolutePath
  private fun safeWorkingDir() = "${outputDirAbsolutePath()}/$TASK_ID/safe-working-dir"
  private fun metadataOutputFile() = "${outputDirAbsolutePath()}/$TASK_ID/metadata.json"

  companion object {
    private const val TASK_ID = "1234"
  }
}
