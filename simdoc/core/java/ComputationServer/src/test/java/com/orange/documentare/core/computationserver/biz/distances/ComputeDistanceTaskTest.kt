package com.orange.documentare.core.computationserver.biz.distances

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

internal class ComputeDistanceTaskTest {

  @Test
  fun `default suffix array algo is LIBDIVSUFSORT if not provided in request`() {
    // given
    val req = DistancesRequest.builder().build()

    // when
    val computeDistanceTask = ComputeDistanceTask(req, Tasks(), AppPrepData("", "", SharedDirectory.Examples.notAvailable()))

    // then
    then(computeDistanceTask.distance.distanceParameters.suffixArrayAlgorithm).isEqualTo(SuffixArrayAlgorithm.LIBDIVSUFSORT)
  }

  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `distance is initialized with provided algo`(suffixArrayAlgorithm: SuffixArrayAlgorithm) {
    // given
    val req = DistancesRequest.builder().suffixArrayAlgorithm(suffixArrayAlgorithm).build()

    // when
    val computeDistanceTask = ComputeDistanceTask(req, Tasks(), AppPrepData("", "", SharedDirectory.Examples.notAvailable()))

    // then
    then(computeDistanceTask.distance.distanceParameters.suffixArrayAlgorithm).isEqualTo(suffixArrayAlgorithm)
  }
}
