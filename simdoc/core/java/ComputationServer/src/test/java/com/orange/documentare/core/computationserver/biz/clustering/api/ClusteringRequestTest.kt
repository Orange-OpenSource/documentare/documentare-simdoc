package com.orange.documentare.core.computationserver.biz.clustering.api

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import jakarta.json.bind.JsonbBuilder
import jakarta.json.bind.spi.JsonbProvider

class ClusteringRequestTest {

  @Test
  fun `JSONB, serialize null values then can deserialize json`() {
    // when
    val expectedReq = com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest.builder().build()
    val json = JsonbBuilder.create().toJson(expectedReq)

    // then
    val req = JsonbProvider.provider().create().build().fromJson(json, com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest::class.java)
    then(req).isEqualTo(expectedReq)
  }
}
