package com.orange.documentare.core.computationserver.biz.clustering

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.computationserver.biz.clustering.ClusteringAnimalsTest.Companion.setupClusteringController
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController.ClusteringRequestState
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.apache.commons.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class InvalidClusteringRequestTest {
  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  @BeforeEach
  fun setup() {
    cleanup()
    File(OUTPUT_DIRECTORY).mkdir()
  }

  @AfterEach
  fun cleanup() {
    FileUtils.deleteQuietly(File(INPUT_DIRECTORY))
    FileUtils.deleteQuietly(File(OUTPUT_DIRECTORY))
  }


  @Test
  fun clustering_api_return_bad_request_if_input_directory_and_bytes_data_are_missing() {
    // Given
    val expectedMessage = "inputDirectory and bytesData are missing"
    val req = ClusteringRequest.builder()
      .outputDirectory(OUTPUT_DIRECTORY)
      .build()

    test(req, expectedMessage)
  }

  @Test
  fun clustering_api_return_bad_request_if_input_directory_is_not_reachable() {
    // Given
    val expectedMessage = "inputDirectory can not be reached: /xxx"
    val req = ClusteringRequest.builder()
      .inputDirectory("/xxx")
      .outputDirectory(OUTPUT_DIRECTORY)
      .build()

    test(req, expectedMessage)
  }

  @Test
  fun clustering_api_return_bad_request_if_input_directory_is_not_a_directory() {
    // Given
    val expectedMessage = "inputDirectory is not a directory: "
    File(INPUT_DIRECTORY).writeText("hi")
    val req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory(OUTPUT_DIRECTORY)
      .build()

    test(req, expectedMessage)
  }

  @Test
  fun clustering_api_return_bad_request_if_output_directory_is_missing() {
    // Given
    val expectedMessage = "outputDirectory is missing"
    createInputDirectory()
    val req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .build()

    test(req, expectedMessage)
  }

  @Test
  fun clustering_api_return_bad_request_if_output_directory_is_not_a_directory() {
    // Given
    val expectedMessage = "outputDirectory is not a directory: "
    cleanup()
    createInputDirectory()
    File(OUTPUT_DIRECTORY).writeText("hi")
    val req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory(OUTPUT_DIRECTORY)
      .build()

    test(req, expectedMessage)
  }

  @Test
  fun clustering_api_return_bad_request_if_output_directory_is_not_writable() {
    // Given
    val expectedMessage = "outputDirectory is not writable:"
    createInputDirectory()
    val req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory("/")
      .build()

    test(req, expectedMessage)
  }

  private fun test(req: ClusteringRequest, expectedMessage: String) {
    val tasks = setupTasks()
    val clusteringController = setupClusteringController(tasks, distancePrepDir.absolutePath, rawImageCacheDir.absolutePath)

    // when
    val status = clusteringController.launchClustering(req)

    // then
    assertThat(status.state).isEqualTo(ClusteringRequestState.BAD_REQUEST)
    assertThat(status.error).contains(expectedMessage)
  }

  private fun setupTasks(): Tasks = Tasks()

  private fun createInputDirectory() {
    File(INPUT_DIRECTORY).mkdir()
  }

  companion object {
    private const val INPUT_DIRECTORY = "in"
    private const val OUTPUT_DIRECTORY = "out"
  }
}
