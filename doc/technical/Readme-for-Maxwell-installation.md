# Introduction

This document allows you to install different versions of Maxwell on your system (linux or docker). It will updated, depending of Maxwell evolutions. 

We recommand preferably to use docker installation, especially for testing what is doing Maxwell. 

Indeed, except compatibility between yaml file and docker compose, user will not be confronted with compatibility with used third parties products.

## Linux installations

### Maxwell 6

This version is dedicated to be used in a debian 10 (buster) ennvironment. Note that you will have to care with MongoDB compatibility, because this package is not included in buster bundle.

For downloading and installing Maxwell 6, click [***here***](./doc/technical/maxwell-6-linux-install.md)

## Docker installation

We provide a unique version of Maxwell under docker.

The current version is dedicated to be used under docker and docker-compose (version 1.23.1), we assume that these packages are installed on your system.

For downloading and installing yaml file of Maxwell 6, click [***here***](./doc/technical/maxwell-docker-install.md)



