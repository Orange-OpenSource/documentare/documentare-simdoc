The goal of this documentation is to explain how to configure your environment and install **maxwell 6** application.

# Installation on Debian systems

This documentation is tested in a Debian buster version.

## Download & Install mongodb

We use mongodb version 3.6, under agpl v3.0 license

**Important remark on mongoDB** : mongoDB package is not provided in current debian buster bundle. That occurs problems with version 3.6 installation if previous installed.
We recommand that you install mongoDB on a complete new and clean debian buster. 
However, if you need to plan an upgrade to debian buster AND your current system has mongoDB, we strongly recommand that you remove mongoDB **before** upgrading to buster.

NB, if you are missing the `sudo` tool:
 - install it: `apt-get install sudo`
 - logged as root (`su`), give current user the sudo rights: `adduser titi sudo`
 - log out, and relog so that modification will be taken into account


- install Mongo Inc repository gpg key:  `sudo apt-key adv --keyserver keyserver.ubuntu.com --keyserver-options http-proxy=http://proxy:8080 --recv-keys 58712A2291FA4AD5 ;` NB: you may need to adapt/remove proxy configuration depending on your network environment
- create file /etc/apt/sources.list.d/mongodb.list and add the following line: `deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/3.6 main`
- update the package list : `sudo apt update`
- install the mongo server: `sudo apt install mongodb-org`
- run it: `sudo service mongod start`
- enable mongodb start after reboot: `sudo systemctl enable mongod.service`

## Install redis

We use redis
- install redis: `sudo apt install redis`


## Download & Install image-converter debian package

We have a dedicated repository to the image-converter debian package: https://gitlab.com/Orange-OpenSource/documentare/image-converter

To download the last release version:
  - go to the Pipelines tab: https://gitlab.com/Orange-OpenSource/documentare/image-converter/pipelines
  - download the last image-converter release job artifacts (icon on the right and Download build-package artifacts)
    - for instance for release `1.2.0` : https://gitlab.com/Orange-OpenSource/documentare/image-converter/-/jobs/226709682/artifacts/download
  - unzip it, you should have the debian package, for instance: `raw-converter_1.2.0_amd64.deb`


Install the image-converter debian package: `sudo dpkg -i raw-converter_1.2.0_amd64.deb`
If there is some problem you must install the missing dependencies: `sudo apt-get -f install` (-f stands for "fix broken")

Now image-converter should be installed.

## Download & Install graphviz debian package

As we need to modify graphviz for our needs, we have a dedicated repository to build the debian package: https://gitlab.com/Orange-OpenSource/documentare/documentare-graphviz

To download the last package version:
 - go to the Pipelines tab: https://gitlab.com/Orange-OpenSource/documentare/documentare-graphviz/pipelines
 - download the last "passed" job artifacts (icon on the right)
 - unzip it, you should have the debian package, for instance: `graphviz_2.38.0-18_amd64.deb`

Install the debian package: `sudo dpkg -i graphviz_2.38.0-18_amd64.deb`

You may have such error:
```
dpkg: dependency problems prevent configuration of graphviz:
 graphviz depends on libann0; however:
  Package libann0 is not installed.
 graphviz depends on libgvc6; however:
  Package libgvc6 is not installed.
 graphviz depends on libgvpr2; however:
  Package libgvpr2 is not installed.
```

It means that some graphviz dependencies are missing.

Install the missing dependencies: `sudo apt-get -f install` (-f stands for "fix broken")

Lock graphviz package update : `sudo apt-mark hold graphviz`

Now graphviz should be installed. You can give it a try with for instance: `sfdp -?`


## Download & Install simdoc debian package

We have a dedicated repository to the image-converter debian package: https://gitlab.com/Orange-OpenSource/documentare/documentare-simdoc

To download the last release version:
   - go to the Pipelines tab: https://gitlab.com/Orange-OpenSource/documentare/documentare-simdoc/pipelines
   - download the last simdoc release job artifacts (icon on the right and Download servers artifacts)
        - for instance for release `6.0.2` : https://gitlab.com/Orange-OpenSource/documentare/documentare-simdoc/-/jobs/258067428/artifacts/raw/simdoc_6.0.2_amd64.deb
    

Install the simdoc debian package: `sudo dpkg -i simdoc_6.0.2_amd64.deb`

If there is some problem you must install the missing dependencies: `sudo apt-get -f install` (-f stands for "fix broken")

Now simdoc servers : mediation-server and clustering-server should be installed.

## Download & Install clustering-ui debian package

We have a dedicated repository to the image-converter debian package: https://gitlab.com/Orange-OpenSource/documentare/documentare-clustering-ui

To download the last release version:
   - go to the Pipelines tab: https://gitlab.com/Orange-OpenSource/documentare/documentare-clustering-ui/pipelines
   - download the last simdoc release job artifacts (icon on the right and Download release-job artifacts)
        - for instance for release `6.0.4` : https://gitlab.com/Orange-OpenSource/documentare/documentare-clustering-ui/-/jobs/258069009/artifacts/download
   - unzip it, you should have the debian package, for instance: `clustering-ui_6.0.4_amd64.deb`
    

Install the clustering-ui debian package: `sudo dpkg -i clustering-ui_6.0.4_amd64.deb`

If there is some problem you must install the missing dependencies: `sudo apt-get -f install` (-f stands for "fix broken")

Now clustering-ui server should be installed.