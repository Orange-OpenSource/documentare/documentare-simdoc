# Maxwell application with docker
This documentation is tested with version 1.23.1 of docker-compose.

We assume that user knows how to install docker and docker-compose (both software are needed) on his machine. They are embedded in the same distribution on Windows
and MacOS Systems, but need a separate installation on Linux systems.

You must upload in gitlab the docker-compose.yml file corresponding of the lastest release.
Example for the release `6.0.5` you can upload docker-compose.yml in https://gitlab.com/Orange-OpenSource/documentare/documentare-clustering-ui/tree/6.0.5

## Run Maxwell application
You can run Maxwell application with `docker-compose up -d`

## Stop Maxwell application
You can stop Maxwell application with `docker-compose down`
